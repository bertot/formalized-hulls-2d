Require Export hulls extremal_points perturbations.
Require Extraction.

Extract Inductive sumbool => "bool" [ "true" "false" ].
Extract Inductive bool => "bool" [ "true" "false" ].

Extract Inductive list => "list" [ "[]" "(::)" ].

Extraction Inline Fix.

(*
Module jarvisP(A:OField).

Module PE := perturb(A).
Module EX := extr(A).
Import EX PE DA PS D KA P C A.

Definition jarvisP := jarvis Plane.
*)

Extraction Library hulls.
Extraction Library extremal_points.
Extraction Library perturbations.
