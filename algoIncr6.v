(* AddPath "$HOME/ctcoq/stage". *)
Require Export algoIncr5.
Require Export triangle.
Require Export Arith.
Require Export List.
Require Export Classical.

Opaque succ_CH.
Opaque convex.

Definition commence_par (lCC : CC) (s : Plan) :=
  (exists l : CC, lCC = s :: l).

Definition CH_egal_cycle (cycle : Plan * CC) (cc : CC) :=
  cycle_dans_CH (@fst _ _ cycle) (@snd _ _ cycle) cc /\
  commence_par (@snd _ _ cycle) (succ_CH (@fst _ _ cycle) cc).

Theorem convexOK :
 forall (p t : Plan) (lCC cc : CC),
 CH_egal_cycle (t, lCC) cc -> CH_egal_cycle (convex p t lCC) (p :: cc).
intros.
unfold CH_egal_cycle in H; simpl in H; elim H; clear H; intros.
elim H0; clear H0; intros.
rewrite H0; rewrite H0 in H.
unfold CH_egal_cycle in |- *; simpl in |- *; split.
apply convex_dans_CH; auto.
unfold commence_par in |- *.
elim (debut_convex_dans_CH p t x cc); auto.
intros b H1; elim H1; clear H1; intros.
elim H1; clear H1; intros.
exists x0.
rewrite H1.
elim
 (unicite_CH_Prop_droite (@fst Plan CC (convex p t (succ_CH t cc :: x))) b
    (succ_CH (@fst Plan CC (convex p t (succ_CH t cc :: x))) (p :: cc))
    (p :: cc)); auto.
apply succ_CH_Prop.
unfold point_extremal in |- *; right; exists b; auto.
Qed.

Lemma point_extremaux_dans_cycle :
 forall (a b t : Plan) (lCC cc : CC) (c : Plan),
 cycle_dans_CH t (c :: lCC) cc ->
 CH_Prop cc (a, b) -> in_CC a (c :: lCC) -> in_couple a b t (c :: lCC).
intros a b t lCC cc; induction lCC as [| a0 lCC HreclCC]; intros.
elim H1; clear H1; intro.
rewrite H1; rewrite H1 in H.
simpl in |- *; split; auto.
apply (unicite_CH_Prop_droite a t b cc); auto.
apply H.
simpl in |- *; auto.
elim H1.
elim H1; clear H1; intro.
rewrite H1; rewrite H1 in H.
cut (b = a0).
intro.
rewrite H2; simpl in |- *; auto.
apply (unicite_CH_Prop_droite a b a0 cc); auto.
apply H.
simpl in |- *; auto.
generalize (HreclCC a0); intros Hrec.
simpl in |- *; right; simpl in Hrec; apply Hrec; clear Hrec; auto.
eauto.
Qed.

Lemma point_extremal_dans_cycle :
 forall (a t : Plan) (lCC cc : CC) (c : Plan),
 cycle_dans_CH t (c :: lCC) cc ->
 point_extremal a cc -> sensDirect t c a -> t = a \/ in_CC a (c :: lCC).
intros a t lCC cc; induction lCC as [| a0 lCC HreclCC]; intros.
elim (H c t).
simpl in |- *; intros.
elim H3; clear H3; intros.
cut (infPolarEq c t a).
intros.
absurd (sensDirect t c a); auto.
apply (points_extremaux_Prop c t a cc); auto.
apply point_extremal_dans_cc; auto.
simpl in |- *; auto.
elim (classic (sensDirect t a0 a)); intro.
elim (HreclCC a0); auto.
intros.
simpl in |- *; auto.
eauto.
elim (H c a0).
simpl in |- *; intros.
elim H4; clear H4; intros.
cut (infPolarEq c a0 a).
intros.
elim (classic (c = a)); intro; auto.
cut (c <> a0).
intro.
elim (classic (a0 = a :>Plan)); intro; auto.
elim (classic (t = a :>Plan)); intro; auto.
elim (classic (t = a0 :>Plan)); intro.
rewrite <- H11 in H5.
cut (infPolarEq c t a).
intro.
absurd (sensDirect t c a); auto.
apply (points_extremaux_Prop c t a cc); auto.
apply point_extremal_dans_cc; auto.
absurd (point_extremal a cc); auto.
apply dans_triangle_pas_point_extremal.
exists t; exists c; exists a0.
split.
apply point_extremal_dans_cc;
 apply (cycle_point_extremal_t t (a0 :: lCC) cc c); 
 auto.
split.
apply point_extremal_dans_cc; apply (cycle_point_extremal t (a0 :: lCC) cc c);
 auto.
simpl in |- *; auto.
split.
apply point_extremal_dans_cc; apply (cycle_point_extremal t (a0 :: lCC) cc c);
 auto.
simpl in |- *; auto.
unfold dansTriangle in |- *; split; auto.
split.
apply infPolarEq_strict; auto.
apply infPolarEq_strict; auto.
apply (points_extremaux_distincts c a0 cc); auto.
apply (points_extremaux_Prop c a0 a cc); auto.
apply point_extremal_dans_cc; auto.
simpl in |- *; auto.
Qed.

Theorem CH_egal_cycle_inv :
 forall (t : Plan) (lCC cc : CC),
 CH_egal_cycle (t, lCC) cc ->
 forall a b : Plan,
 CH_Prop cc (a, b) -> in_couple a b t lCC \/ t = a /\ commence_par lCC b.
intros t lCC cc H' a b H'0; try assumption.
cut (t = a \/ in_CC a lCC).
intros H'1.
elim H'; simpl in |- *; intros.
elim H'1; clear H'1; intros H'1.
right; split; auto.
cut (b = succ_CH t cc).
intro.
rewrite <- H1 in H0; auto.
rewrite <- H'1 in H'0.
apply (unicite_CH_Prop_droite t b (succ_CH t cc) cc); auto.
apply succ_CH_Prop; auto.
unfold point_extremal in |- *; right; exists b; auto.
elim H0; simpl in |- *; clear H0; intros.
left; rewrite H0; rewrite H0 in H'1; rewrite H0 in H.
apply (point_extremaux_dans_cycle a b t x cc); auto.
elim H'; simpl in |- *; intros.
elim H0; clear H0; intros.
rewrite H0 in H; rewrite H0; rewrite H0 in H'.
elim (classic (t = a)); intro; auto.
elim (classic (a = succ_CH t cc)); intro.
simpl in |- *; auto.
apply (point_extremal_dans_cycle a t x cc (succ_CH t cc)); auto.
unfold point_extremal in |- *; right; exists b; auto.
elim (succ_CH_Prop t cc); simpl in |- *; intros.
elim H4; clear H4; intros.
apply infPolarEq_strict; auto.
apply (points_extremaux_Prop t (succ_CH t cc) a cc); auto.
apply point_extremal_dans_cc; auto.
unfold point_extremal in |- *; right; exists b; auto.
apply (points_extremaux_distincts t (succ_CH t cc) cc); auto.
apply (cycle_point_extremal_t t x cc (succ_CH t cc)); auto.
Qed.
