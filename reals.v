
Parameter R:Set.

Parameter one:R.

Parameter zero:R.

Parameter Rplus:R -> R ->R.

Parameter zero_neutral:(x:R)(Rplus zero x) =x.

Parameter zero_neutral_r:(x:R)(Rplus x zero) =x.

Parameter Rplus_commutative:(x, y:R)(Rplus x y) =(Rplus y x).

Parameter Rplus_associative:
          (x, y, z:R)(Rplus x (Rplus y z)) =(Rplus (Rplus x y) z).

Definition two := (Rplus one one).

Inductive natural: R ->Prop :=
   zero_natural: (natural zero)
  | plus_natural: (x:R) (natural x) ->(natural (Rplus one x)).

Parameter R_null_characteristic:(n:R) (natural n) ->~ (Rplus one n) =zero.

Parameter Rtimes:R -> R ->R.

Parameter one_neutral:(x:R)(Rtimes one x) =x.

Parameter one_neutral_r:(x:R)(Rtimes x one) =x.

Parameter Rtimes_commutative:(x, y:R)(Rtimes x y) =(Rtimes y x).

Parameter Rtimes_associative:
          (x, y, z:R)(Rtimes x (Rtimes y z)) =(Rtimes (Rtimes x y) z).

Parameter Rtimes_distribute_plus_r:
          (x, y, z:R)(Rtimes (Rplus y z) x) =(Rplus (Rtimes y x) (Rtimes z x)).

Parameter Rtimes_distribute_plus_l:
          (x, y, z:R)(Rtimes x (Rplus y z)) =(Rplus (Rtimes x y) (Rtimes x z)).

Parameter double:(x:R)(Rplus x x) =(Rtimes two x).

Parameter Rminus:R -> R ->R.

Parameter Rminus_self:(x:R)(Rminus x x) =zero.

Parameter Rminus_plus_assoc_r:
          (x, y, z:R)(Rminus x (Rplus y z)) =(Rminus (Rminus x y) z).

Parameter Rminus_plus_assoc_l:
          (x, y, z:R)(Rminus (Rplus x y) z) =(Rplus x (Rminus y z)).

Parameter Rminus_minus_assoc:
          (x, y, z:R)(Rminus x (Rminus y z)) =(Rplus (Rminus x y) z).

Parameter Rtimes_distribute_minus_r:
          (x, y, z:R)(Rtimes (Rminus y z) x) =(Rminus (Rtimes y x) (Rtimes z x)).

Parameter Rtimes_distribute_minus_l:
          (x, y, z:R)(Rtimes x (Rminus y z)) =(Rminus (Rtimes x y) (Rtimes x z)).

Parameter Rdiv:R -> R ->R.

Parameter Rdiv_self:(x:R) ~ x =zero ->(Rdiv x x) =one.


Parameter Rdiv_self_mult:(x, y:R) ~ x =zero ->(Rtimes x (Rdiv y x)) =y.

Parameter Rdiv_times_assoc_r:
          (x, y, z:R)(Rdiv x (Rtimes y z)) =(Rdiv (Rdiv x y) z).

Parameter Rdiv_times_assoc_l:
          (x, y, z:R)(Rdiv (Rtimes x y) z) =(Rtimes x (Rdiv y z)).

Parameter Rdiv_div_assoc:(x, y, z:R)(Rdiv x (Rdiv y z)) =(Rtimes (Rdiv x y) z).

Parameter Rdiv_distribute_plus:
          (x, y, z:R)(Rdiv (Rplus y z) x) =(Rplus (Rdiv y x) (Rdiv z x)).

Parameter Rdiv_distribute_minus:
          (x, y, z:R)(Rdiv (Rminus y z) x) =(Rminus (Rdiv y x) (Rdiv z x)).

Definition Square := [x:R](Rtimes x x).

Definition four := (Square two).

Parameter Square_div:(x:R) ~ x =zero ->(Rdiv (Square x) x) =x.

Definition Rtimes_self: (x:R)(Rtimes x x) =(Square x) :=
   [x:R](refl_equal R (Rtimes x x)).

Parameter Root:R ->R.


Parameter Gt:R -> R ->Prop.
Definition Ge:R -> R ->Prop := [x,y:R] ((Gt x y)/\~(x=y))\/(~(Gt x y)/\(x=y)).
Definition Lt:R -> R ->Prop := [x,y:R] (Gt y x).
Definition Le:R -> R ->Prop := [x,y:R] (Ge y x).

Parameter Gt_stable_plus : (x,y:R)
  (Gt x zero)/\(Gt y zero) -> (Gt (Rplus x y) zero).

Definition Roppose: R->R := [x:R] (Rminus zero x).
Parameter Inverse_signe_r : (x:R) (Gt x zero) -> ~(Gt (Roppose x) zero).
Parameter Inverse_signe_l : (x:R) ~(Gt (Roppose x) zero) -> (Gt x zero)\/(x=zero).
Parameter Roppose_inverse_plus : (x,y:R)
  (Rplus x y)=zero -> y = (Roppose x).





Parameter Root_def:(x:R) (Ge x zero) ->(Square (Root x)) =x.

Parameter Rminus_zero:(x:R)(Rminus x zero) =x.

Parameter non_zero_product:
          (x, y:R) ~ x =zero -> ~ y =zero ->~ (Rtimes x y) =zero.
Hints Resolve non_zero_product.

Parameter square_div:(x, y:R)(Square (Rdiv x y)) =(Rdiv (Square x) (Square y)).

Parameter non_zero_two:~ two =zero.
Hints Resolve non_zero_two.


