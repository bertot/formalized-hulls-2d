(* The set of all known facts is an ordered list of oriented triangles. *)


type state = (int * int * int) list;;


let cmp : (int * int * int)  -> (int * int * int) -> bool =
  fun (a,b,c) (a',b',c') ->
 (a < a') || (a = a' && b < b') || (a = a' && b = b' && c < c');;

(* This is an easy implementation of a sorting algorithm with
  removal of duplications. *)
let rec insert t = function
  | [] -> [t]
  | (t'::l) as l0 ->
    if cmp t' t then
      t'::insert t l
    else
      if t = t' then l0 else t::l0;;

let rec sort = function
  | [] -> []
  | t::l ->
    let l' = sort l in insert t l';;

(* Check whether a triangle is already on this set. *)

let rec present t = function
  | [] -> false
  | t'::l ->
    if t = t' then
      true
    else
      if cmp t' t then present t l else false;;

let rec remove rems = function
  | [] -> []
  | t::l -> if present t rems then remove rems l else t::remove rems l;;

(* axiom 1: rotate the points of a triangle. *)

let ax1 (a, b, c) = (b, c, a);;

(* for every triangle in l, add its rotations in l' *)
let rec sat1 l l' =
  match l with
    | [] -> l'
    | t::l1 ->
      insert t (insert (ax1 t) (insert (ax1 (ax1 t)) (sat1 l1 l')));;

let opposite (a, b, c) = (a, c, b);;

let inconsistent l =
  let rec aux = function
    | [] -> false
    | t::l' -> present (opposite t) l || aux l' in
  aux l;;

(* produce a consequence of axiom5 if there exists one. *)
let rec add5 l l' =
  match l with
    | [] -> []
    | (a,b,c)::l1 ->
      let l2 = List.filter (fun (a', b', c') ->
         a = a' && b = b' && not (c = c')) l' in
      let rec aux1 = function
        | [] -> add5 l1 l'
	| (_, _, c')::tl ->
          try
            let (_, _, c2) =
	      List.find
		(fun (_, _, c2) ->
		  not (c2 = c) && not (c2 = c') && present (a, c, c') l' &&
		    present (a, c', c2) l' && not (present (a, c, c2) l')) l2 in
	    let t = (a, c, c2) in
            (print_string ("applying axiom5 to " ^ (string_of_int a) ^ " "
                              ^ (string_of_int b) ^ " " ^ (string_of_int c)
                              ^ " " ^ (string_of_int c') ^ " "
                              ^ (string_of_int c2) ^ " producing "
                              ^ (string_of_int a) ^ " " ^ (string_of_int c)
                              ^ " " ^ (string_of_int c2) ^ "\n");
            [t; ax1 t; ax1 (ax1 t)])
	  with  Not_found -> aux1 tl in
      aux1 l2;;

(* produce a consequence of axiom5' if there exists one. *)
let rec add5' l l' =
  match l with
    | [] -> []
    | (a,b,c)::l1 ->
      let l2 = List.filter (fun (a', b', c') ->
         a = a' && b = b' && not (c = c')) l' in
      let rec aux1 = function
        | [] -> add5' l1 l'
	| (_, _, c')::tl ->
          try
            let (_, _, c2) =
	      List.find
		(fun (_, _, c2) ->
		  not (c2 = c) && not (c2 = c') && present (b, c, c') l' &&
		    present (b, c', c2) l' && not (present (b, c, c2) l')) l2 in
	    let t = (b, c, c2) in
            (print_string ("applying axiom5' to " ^ (string_of_int a) ^ " "
                              ^ (string_of_int b) ^ " " ^ (string_of_int c)
                              ^ " " ^ (string_of_int c') ^ " "
                              ^ (string_of_int c2) ^ " producing "
                              ^ (string_of_int b) ^ " " ^ (string_of_int c)
                              ^ " " ^ (string_of_int c2) ^ "\n");
            [t; ax1 t; ax1 (ax1 t)])
	  with  Not_found -> aux1 tl in
      aux1 l2;;

(* produce a consequence of axiom4 if there exists one. *)

let rec add4 l l' =
  match l with
    | [] -> []
    | (a, b, c)::l1 ->
      try
	let (_, d, _) = List.find (fun (b', d, c') -> b = b' && c = c' &&
	    present (d, a, c) l' && not (present (a, b, d) l')) l' in
	let t = (a, b, d) in
        (print_string ("applying axiom4 to " ^ (string_of_int a) ^ " "
           ^ (string_of_int b) ^ " " ^ (string_of_int d) ^ " "
           ^ (string_of_int c) ^ "\n" );
	[t; ax1 t; ax1 (ax1 t)])
      with Not_found -> add4 l1 l';;

let empty = function [] -> true | _ -> false;;

(* saturate only with axiom5 and axiom1, stop at first inconsistency. *)
let rec sat5 l =
  let l1 = add5 l l in
  let l' = List.fold_right insert l1 l in
    if inconsistent l' then l'
    else sat5 l';;

let rec sat l =
  let l1 = add5 l l in
  if empty l1 then
    let l1 = add5' l l in
    if empty l1 then
      let l2 = add4 l l in
      if empty l2 then l else sat (List.fold_right insert l2 l)
    else
      sat (List.fold_right insert l1 l)
  else
    sat (List.fold_right insert l1 l);;

let list_to_tuple = function
  | [a;b;c] -> (a, b, c)
  | _ -> failwith "wrong size of list for list_to_tuple";;

(* wrong code, might be interesting for
  debugging exercise.
let rec enumerate len n =
  if n < len then
    []
  else if 0 = len then
    []
  else if n = 1 then
    [[1]]
  else
    (List.map (fun e -> 1::List.map ((+) 1) e)
       (enumerate (len - 1) (n - 1))) @
      (List.map (fun e -> List.map ((+) 1) e)
	 (enumerate len (n - 1)));;

*)

let rec enumerate len n =
  if n < len then
    []
  else if 0 = len then
    []
  else if len = 1 then
    [1]::List.map (fun e -> List.map ((+) 1) e) (enumerate len (n - 1))
  else if n = 1 then
    [[1]]
  else
    (List.map (fun e -> 1::List.map ((+) 1) e)
       (enumerate (len - 1) (n - 1))) @
      (List.map (fun e -> List.map ((+) 1) e)
	 (enumerate len (n - 1)));;

let rec refute_rec splits l =
  if inconsistent l then
    true
  else match splits with
      t::spl ->
	if present t l || present (opposite t) l then
	  refute_rec spl (sat l)
	else
          (let a, b, c = t in
             print_string ("split on " ^ (string_of_int a)
                             ^ (string_of_int b)
                             ^ (string_of_int c) ^ "\n");
	  let l1 = insert t (insert (ax1 t) (insert (ax1 (ax1 t)) l)) in
	  if refute_rec spl (sat l1) then
	    let t' = opposite t in
	    let l2 = insert t' (insert (ax1 t') (insert (ax1 (ax1 t')) l)) in
	    refute_rec spl (sat l2)
	  else
	    false)
    | [] -> false;;

let rec obtain_rec target splits l =
  if inconsistent l then
    true
  else if present target l then
    true
  else if present (opposite target) l then
    false
  else match splits with
    t::spl ->
    if present t l || present (opposite t) l then
      obtain_rec target spl (sat l)
    else
      (let a, b, c = t in
         print_string ("split on " ^ (string_of_int a)
                         ^ (string_of_int b)
                         ^ (string_of_int c) ^ "\n");
        let l1 = insert t (insert (ax1 t) (insert (ax1 (ax1 t)) l)) in
          if obtain_rec target spl (sat l1) then
            (print_string ("finished a branch on " ^ (string_of_int a)
                     ^ (string_of_int b) ^ (string_of_int c) ^ "\n");
            let t' = opposite t in
            let l2 = insert t' (insert (ax1 t') (insert (ax1 (ax1 t')) l)) in
             obtain_rec target spl (sat l2))
          else false)
    | [] -> false;;

let rec max_point l =
  match l with
  | [] -> 0
  | (a,b,c)::tl ->
     let maxt = if a > b then a else if b > c then b else c in
     let r = max_point tl in if maxt > r then maxt else r;;

let refute l =
  let l = sort l in
  let n = max_point l in
  refute_rec (List.map list_to_tuple (enumerate 3 n))
    (sat1 l l);;

let obtain_with_postpone target postpone l =
  let l = sort l in
  let n = max_point l in
  let splits = remove postpone (List.map list_to_tuple (enumerate 3 n))
                  @ postpone in
  obtain_rec target splits (sat1 l l);;

let obtain target l = obtain_with_postpone target [] l;;

let s =  [1,2,4;2,3,4;3,1,4;5,6,1;5,6,2;5,6,3;6,5,4];;

(* Probleme raised by C. Sartori for Delaunay: every point inside a triangle
 is in one of the three sub-triangles created when decomposing this triangle
 for a new point.

  points outside the triangle, 1 2 3, two points inside 4 and 5,
  if 5 is not in the first two triangles with 1,2,4 and 2,3,4, then
  it must be in the triangle 3,1,4. *)

let s2_0 = [1,2,4; 2,3,4; 3,1,4; 1,2,5; 2,3,5; 3,1,5];;

let s2_1 = [1,2,4; 2,3,4; 3,1,4; 1,2,5; 2,3,5; 3,1,5;
            4,1,5; 4,2,5; 4,3,5];;

let s2_2 = [1,2,4; 2,3,4; 3,1,4; 1,2,5; 2,3,5; 3,1,5;
            1,4,5; 2,4,5; 3,4,5];;

let s2_3 = [1,2,3; 1,2,4; 2,3,4; 3,1,4; 1,2,5; 2,3,5; 3,1,5;
            1,4,5; 2,4,5; 5,3,4];;

let inside_triangle_and_left_of =
  [1,2,3;1,2,6;1,6,3;6,2,3;4,5,1;4,5,2;4,5,3;4,3,2;4,2,1;4,3,1];;
