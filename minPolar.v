(* AddPath "$HOME/ctcoq/stage". *)
Require Export ccDefinition.
Require Import Classical.
Require Import List.
 
Fixpoint minPolarRec (t minDep : Plan) (cc : CC) {struct cc} : Plan :=
  match cc with
  | nil => minDep
  | a :: l =>
      (fun min : Plan =>
       match directBool t a min with
       | true => a
       | false => min
       end) (minPolarRec t minDep l)
  end.
 
Lemma minPolarRec_dans_cc :
 forall (t min : Plan) (cc : CC), in_CC (minPolarRec t min cc) (min :: cc).
intros.
induction cc as [| a cc Hreccc].
simpl in |- *; auto.
simpl in |- *.
 CaseEq (directBool t a (minPolarRec t min cc)).
auto.
elim Hreccc; auto.
Qed.
 
Theorem minPolarRecOK :
 forall (cc : CC) (s t min : Plan),
 t <> min ->
 points_extremaux (min :: cc) s t ->
 points_extremaux cc t (minPolarRec t min cc).
intros.
induction cc as [| a cc Hreccc].
simpl in |- *; auto.
cut (points_extremaux cc t (minPolarRec t min cc)).
intros HypRec; clear Hreccc.
simpl in |- *.
 CaseEq (directBool t a (minPolarRec t min cc)).
apply points_extremaux_rec; auto.
apply (Points_extremaux_trans s t a (minPolarRec t min cc) cc); auto.
cut (in_CC (minPolarRec t min cc) (min :: cc)).
intros.
simpl in H2; elim H2; intros; clear H2.
rewrite <- H3; inversion H0; auto.
apply (points_extremaux_Prop s t (minPolarRec t min cc) cc); auto.
inversion H0.
inversion H5; auto.
apply minPolarRec_dans_cc.
apply sym_not_eq; apply (direct_distincts t a (minPolarRec t min cc)); auto.
inversion H0; auto.
apply points_extremaux_rec; auto.
apply Hreccc.
inversion H0.
inversion H3; auto.
Qed.
 
Lemma minPolarRec_non_t :
 forall (t min : Plan) (cc : CC), t <> min -> t <> minPolarRec t min cc.
intros.
induction cc as [| a cc Hreccc].
simpl in |- *; auto.
simpl in |- *.
 CaseEq (directBool t a (minPolarRec t min cc)).
apply (direct_distincts t a (minPolarRec t min cc)); auto.
auto.
Qed.
 
Theorem minPolarRecOK_min :
 forall (cc : CC) (s t min : Plan),
 t <> min ->
 points_extremaux (min :: cc) s t -> infPolarEq t (minPolarRec t min cc) min.
intros.
induction cc as [| a cc Hreccc].
simpl in |- *; auto.
simpl in |- *.
cut (infPolarEq t (minPolarRec t min cc) min).
intros HypRec; clear Hreccc.
 CaseEq (directBool t a (minPolarRec t min cc)).
apply (infPolarEq_trans_bis s t a (minPolarRec t min cc) min); auto.
apply (points_extremaux_distincts s t (min :: a :: cc)); auto.
apply minPolarRec_non_t; auto.
inversion H0.
inversion H4; auto.
cut (in_CC (minPolarRec t min cc) (min :: cc)).
intros.
simpl in H2; elim H2; intros; clear H2.
rewrite <- H3; inversion H0; auto.
apply (points_extremaux_Prop s t (minPolarRec t min cc) cc); auto.
inversion H0.
inversion H5; auto.
apply minPolarRec_dans_cc; auto.
inversion H0; auto.
auto.
inversion H0.
inversion H3.
auto.
Qed.
 
Fixpoint minPolar (t : Plan) (cc : CC) {struct cc} : Plan :=
  match cc with
  | nil => t
  | a :: l =>
      match egalBool a t with
      | true => minPolar t l
      | false => minPolarRec t a l
      end
  end.
 
Theorem minPolarOK :
 forall (t s : Plan) (cc : CC),
  (exists a : Plan, a <> t /\ in_CC a cc) ->
 points_extremaux cc s t -> points_extremaux cc t (minPolar t cc).
intros.
elim H; intros.
elim H1; (intros; clear H H1).
induction cc as [| a cc Hreccc].
auto.
simpl in |- *.
 CaseEq (egalBool a t).
elim H3; clear H3; intros.
rewrite <- H1 in H2; absurd (a = t); auto.
apply points_extremaux_rec.
inversion H0; auto.
cut (a = t); auto.
apply points_extremaux_rec.
apply (minPolarRecOK cc s t a); auto.
apply sym_not_eq; auto.
apply (minPolarRecOK_min cc s t a); auto.
apply sym_not_eq; auto.
Qed.
 
Lemma minPolar_dans_cc :
 forall (cc : CC) (t : Plan),
 (exists a : Plan, a <> t /\ in_CC a cc) -> in_CC (minPolar t cc) cc.
intros.
elim H; intros.
elim H0; intros; clear H H0.
induction cc as [| a cc Hreccc].
simpl in H2; elim H2; auto.
simpl in |- *.
 CaseEq (egalBool a t).
simpl in H2; elim H2; intros; clear H2; auto.
rewrite <- H0 in H1; absurd (a = t); auto.
cut (in_CC (minPolarRec t a cc) (a :: cc)).
intros H3; simpl in H3; auto.
apply minPolarRec_dans_cc; auto.
Qed.
 
Theorem points_extremal_suivant :
 forall (cc : CC) (s t : Plan),
 CH_Prop cc (s, t) -> exists r : Plan, CH_Prop cc (t, r).
intros.
unfold CH_Prop in H; simpl in H; elim H; intros; clear H.
elim H1; intros; clear H1.
exists (minPolar t cc).
unfold CH_Prop in |- *; split; simpl in |- *.
auto.
cut (s <> t).
intro.
split.
apply minPolar_dans_cc.
exists s; auto.
apply (minPolarOK t s cc).
exists s; auto.
auto.
apply (points_extremaux_distincts s t cc); auto.
Qed.
 