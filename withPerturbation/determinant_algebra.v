Require Export reels.
Require Export sensDirect.
Require Export polynomial_sign.

Module det_alg(A:OField).
Module PS := pol_sign(A).
Import PS D KA P C A.

Theorem determinant_column_sum1 :
 forall a b c d e f g h i j k l,
 determinant3x3 (Kplus a b) c d (Kplus e f) g h (Kplus i j) k l =
 Kplus (determinant3x3 a c d e g h i k l) (determinant3x3 b c d f g h j k l).
intros; unfold determinant3x3 in |- *; ring.
Qed.

Theorem determinant_column_sum2 :
 forall a b c d e f g h i j k l,
 determinant3x3 c (Kplus a b) d g (Kplus e f) h k (Kplus i j) l =
 Kplus (determinant3x3 c a d g e h k i l) (determinant3x3 c b d g f h k j l).
intros; unfold determinant3x3 in |- *; ring.
Qed.

Theorem determinant_column_sum3 :
 forall a b c d e f g h i j k l,
 determinant3x3 c d (Kplus a b) g h (Kplus e f) k l (Kplus i j) =
 Kplus (determinant3x3 c d a g h e k l i) (determinant3x3 c d b g h f k l j).
intros; unfold determinant3x3 in |- *; ring.
Qed.

Theorem determinant_column_mult2 :
 forall a b c d e f g h i k,
 determinant3x3 a (Ktimes k b) c d (Ktimes k e) f g (Ktimes k h) i =
 Ktimes k (determinant3x3 a b c d e f g h i).
intros; unfold determinant3x3 in |- *; ring.
Qed.

Theorem determinant_rotate_column :
 forall a b c d e f g h i,
 determinant3x3 a b c d e f g h i = determinant3x3 b c a e f d h i g.
intros; unfold determinant3x3 in |- *; ring.
Qed.

Theorem determinant_permute_column :
 forall a b c d e f g h i,
 determinant3x3 a b c d e f g h i =
 Koppose (determinant3x3 b a c e d f h g i).
intros; unfold determinant3x3 in |- *; ring.
Qed.

Theorem determinant_column_mult1 :
 forall a b c d e f g h i k,
 determinant3x3 (Ktimes k a) b c (Ktimes k d) e f (Ktimes k g) h i =
 Ktimes k (determinant3x3 a b c d e f g h i).
intros; unfold determinant3x3 in |- *; ring.
Qed.

End det_alg.