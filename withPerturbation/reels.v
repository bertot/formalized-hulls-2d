Require Export Ring.

(* ---------------------  Axiomes ------------------- *)
Module Type OField.

Parameter K : Type.
Parameter one : K.
Parameter zero : K.
Parameter Kplus : K -> K -> K.
Parameter Ktimes : K -> K -> K.
Parameter Koppose : K -> K.
Parameter Ksub : K -> K -> K.
Parameter RT : ring_theory zero one Kplus Ktimes Ksub Koppose (@eq K).
Parameter Gt : K -> K -> Prop.

Parameter Kinv : K -> K.

Axiom not_one_zero : one <> zero.

Axiom Kinv_def : forall x : K, x <> zero -> Ktimes x (Kinv x) = one.

Axiom K_integre : forall a b : K, Ktimes a b = zero -> a = zero \/ b = zero.

Axiom Ordre_total : forall a b : K, Gt a b \/ a = b \/ Gt b a.

Axiom ordre_decidable : forall x y : K, {Gt x y} + {~ Gt x y}.

Axiom egal_decidable : forall x y : K, {x = y} + {x <> y}.

Axiom Gt_antisym : forall a b : K, Gt a b -> ~ Gt b a.

Axiom
  Gt_trans : forall a b c : K, Gt a b -> Gt b c -> Gt a c.

Axiom
  Gt_plus :
    forall a b c : K, Gt a b -> Gt (Kplus c a) (Kplus c b).

Axiom
  Gt_mult :
    forall a b c : K,
    Gt c zero -> Gt a b -> Gt (Ktimes c a) (Ktimes c b).
End OField.

Module Field_complements (A:OField).
Import A.

Add Ring KRing : RT (abstract).

Theorem mult_plus_distr :
 forall a b c : K, Ktimes (Kplus a b) c = Kplus (Ktimes a c) (Ktimes b c).
exact (Rdistr_l RT).
Qed.

Definition Kplus_sym := Radd_comm RT.

Definition Ge (x y : K) : Prop := Gt x y \/ x = y.

Hint Resolve Gt_antisym Gt_plus Gt_mult.
(* ---------------------  resultats de base sur R -------------- *)

Lemma Gt_Ge : forall x y : K, Gt x y -> Ge x y.
unfold Ge in |- *; auto.
Qed.
Hint Resolve Gt_Ge.

Lemma Gt_Ge_trans : forall a b c : K, Gt a b -> Ge b c -> Gt a c.
intros a b c H' H'0; elim H'0.
intros; apply Gt_trans with b; auto.
intros H'1; rewrite <- H'1.
auto.
Qed.

Lemma non_Gt_Ge : forall x y : K, ~ Gt x y -> Ge y x.
intros x y H'; elim (Ordre_total x y); auto.
intros H'0; elim H'; auto.
intros H'0; elim H'0; unfold Ge in |- *; auto.
Qed.
Hint Resolve non_Gt_Ge.

Lemma Gt_plus_GeGt :
 forall a b c d : K,
 Ge a c -> Gt b d -> Gt (Kplus a b) (Kplus c d).
intros a b c d H' H'0.
elim H'; intros H'1.
apply Gt_trans with (Kplus a d); auto.
cut (forall x y : K, Kplus x y = Kplus y x); [ intros H'2 | intros; ring ].
rewrite (H'2 a d); rewrite (H'2 c d); auto.
rewrite H'1; auto.
Qed.

Lemma Koppose_Gt : forall a : K, Gt a zero -> Gt zero (Koppose a).
intros a H'; replace zero with (Kplus (Koppose a) a); try ring.
pattern (Koppose a) at 2 in |- *;
 replace (Koppose a) with (Kplus (Koppose a) zero); 
 try ring.
auto.
Qed.

Lemma Gt_Koppose : forall a : K, Gt zero a -> Gt (Koppose a) zero.
intros.
replace zero with (Kplus (Koppose a) a); try ring.
pattern (Koppose a) at 1 in |- *;
 replace (Koppose a) with (Kplus (Koppose a) zero); 
 try ring; auto.
Qed.
Hint Resolve Koppose_Gt Gt_Koppose.

Lemma Gt_stable_plus_l :
 forall x y : K, Gt x zero -> Ge y zero -> Gt (Kplus x y) zero.
intros x y H' H'0.
replace zero with (Kplus y (Koppose y)); try ring;
 replace (Kplus x y) with (Kplus y x); try ring.
apply Gt_plus.
elim H'0; intros H'1.
apply Gt_trans with zero; auto.
rewrite H'1; replace (Koppose zero) with zero; try ring; auto.
Qed.
Hint Resolve Gt_stable_plus_l.

Lemma Gt_stable_plus :
 forall x y : K,
 Gt x zero -> Gt y zero -> Gt (Kplus x y) zero.
auto.
Qed.

Lemma Gt_stable_plus_r :
 forall x y : K, Ge x zero -> Gt y zero -> Gt (Kplus x y) zero.
intros.
replace (Kplus x y) with (Kplus y x); try ring; auto.
Qed.
Hint Resolve Gt_stable_plus Gt_stable_plus_r.

Lemma Ge_stable_plus :
 forall x y : K, Ge x zero -> Ge y zero -> Ge (Kplus x y) zero.
intros x y H' H'0; elim H'; elim H'0; unfold Ge in |- *; auto.
intros H'1 H'2; rewrite H'1; rewrite H'2.
right; ring.
Qed.
Hint Resolve Ge_stable_plus.

Lemma Gt_stable_mult :
 forall x y : K,
 Gt x zero -> Gt y zero -> Gt (Ktimes x y) zero.
intros; replace zero with (Ktimes x zero); try ring; auto.
Qed.
Hint Resolve Gt_stable_mult.

Lemma Ge_stable_mult :
 forall x y : K, Ge x zero -> Ge y zero -> Ge (Ktimes x y) zero.
intros x y H' H'0; elim H'; elim H'0; unfold Ge in |- *; auto; intros H'1 H'2;
 try rewrite H'1; try rewrite H'2; right; ring.
Qed.
Hint Resolve Ge_stable_mult.

Lemma Inverse_signe_r :
 forall x : K, Gt x zero -> ~ Gt (Koppose x) zero.
auto.
Qed.
Hint Resolve Inverse_signe_r.

Lemma Inverse_signe_l :
 forall x : K, ~ Gt (Koppose x) zero -> Gt x zero \/ x = zero.
intros x H'.
elim (Ordre_total x zero); intros H'0; auto.
elim H'0; intros H'1; auto.
case H'; auto.
Qed.

Lemma Koppose_inverse_plus :
 forall x y : K, Kplus x y = zero -> y = Koppose x.
intros x y H'.
replace (Koppose x) with (Kplus (Koppose x) zero); try (ring; fail).
rewrite <- H'; ring.
Qed.

Lemma Gt_non_nul : forall x : K, Gt x zero -> x <> zero.
intros x H'; red in |- *; intros H'0; generalize H'.
rewrite H'0.
intros H'1; apply (Gt_antisym zero zero H'1); auto.
Qed.
Hint Resolve Gt_non_nul.

Lemma carre_positif : forall a : K, Ge (Ktimes a a) zero.
intros a; unfold Ge in |- *.
elim (Ordre_total a zero).
intros H'; replace zero with (Ktimes a zero); try ring; auto.
intros H'; elim H'; intros H'0.
right; rewrite H'0; ring.
replace (Ktimes a a) with (Ktimes (Koppose a) (Koppose a)); try ring; auto.
Qed.

Lemma somme_nulle :
 forall a b : K,
 Ge a zero -> Ge b zero -> Kplus a b = zero -> a = zero /\ b = zero.
intros a b H' H'0 H'1; elim H'; elim H'0; intros H'2 H'3; auto;
 absurd (Kplus a b = zero); auto.
Qed.

Lemma Ktimes_Gt :
 forall a b : K, Ge a zero -> Gt (Ktimes a b) zero -> Gt b zero.
intros a b H' H'0.
elim (Ordre_total b zero); intros H'1; trivial.
elim H'1; intros H'2.
elim (Gt_non_nul _ H'0); rewrite H'2; ring.
elim H'; intros H'3.
elim (Gt_antisym _ _ H'0); auto.
replace (Ktimes a b) with (Koppose (Ktimes a (Koppose b))); try ring.
apply Koppose_Gt; auto.
elim (Gt_non_nul _ H'0); rewrite H'3; ring.
Qed.

Lemma diff_nul : forall a b : K, Kplus a (Koppose b) = zero -> a = b.
intros a b H'; replace b with (Kplus b zero); try (rewrite <- H'; ring; fail);
 ring.
Qed.

Lemma Kopp_opp : forall a : K, Koppose (Koppose a) = a.
intros a.
ring.
Qed.
(* ------------------------ tactic equivalente a CaseEq ----------- *)

Ltac Match a :=
  generalize (refl_equal a); pattern a at -1 in |- *; case a; intros.

Ltac CaseEq a := generalize (refl_equal a); pattern a at -1 in |- *; case a.

Definition Let_non_imp (A B : Set) (a : A) (b : A -> B) := b a.
Notation Let := (Let_non_imp _ _) (only parsing).

Theorem times_simplify :
 forall a b c : K, a <> zero -> Ktimes a b = Ktimes a c -> b = c.
intros a b c H H0; cut (Kplus b (Koppose c) = zero).
intros H'; apply diff_nul; auto.
cut (Ktimes a (Kplus b (Koppose c)) = zero).
intros H'; elim K_integre with (1 := H').
intros H'1; elim H; auto.
auto.
rewrite (Rmul_comm RT a). 
rewrite (Rdistr_l RT).
repeat rewrite <- (Rmul_comm RT a); rewrite H0; ring.
Qed.

Theorem plus_simplify : forall a b c : K, Kplus a b = Kplus a c -> b = c.
intros a b c H'; replace b with (Kplus (Koppose a) (Kplus a b)).
rewrite H'; ring.
ring.
Qed.
End Field_complements.