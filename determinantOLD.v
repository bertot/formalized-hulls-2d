AddPath "$HOME/ctcoq/stage".
Require Export reals.
 
Definition determinant3x3 : R -> R -> R -> R -> R -> R -> R -> R -> R -> R :=
   [x11, x12, x13, x21, x22, x23, x31, x32, x33 : R]  (Rminus (Rplus (Rtimes x11 (Rtimes x22 x33)) (Rplus (Rtimes x21 (Rtimes x32 x13)) (Rtimes x31 (Rtimes x12 x23)))) (Rplus (Rtimes x31 (Rtimes x22 x13)) (Rplus (Rtimes x21 (Rtimes x12 x33)) (Rtimes x11 (Rtimes x32 x23))))).
 
Lemma det3x3_cyclique_ligne: (xA, yA, zA, xB, yB, zB, xC, yC, zC : R)  (determinant3x3 xA yA
                                                                                       zA xB
                                                                                       yB zB
                                                                                       xC yC
                                                                                       zC) = (determinant3x3 xB yB
                                                                                                             zB xC
                                                                                                             yC zC
                                                                                                             xA yA
                                                                                                             zA).
Intros.
Unfold determinant3x3.
Rewrite Rplus_associative.
Rewrite (Rplus_commutative (Rtimes xA (Rtimes yB zC)) (Rtimes xB (Rtimes yC zA))).
Rewrite <- Rplus_associative.
Rewrite (Rplus_commutative (Rtimes xA (Rtimes yB zC)) (Rtimes xC (Rtimes yA zB))).
Rewrite (Rplus_commutative (Rtimes xB (Rtimes yA zC)) (Rtimes xA (Rtimes yC zB))).
Rewrite (Rplus_associative (Rtimes xC (Rtimes yB zA)) (Rtimes xA (Rtimes yC zB))
                           (Rtimes xB (Rtimes yA zC))).
Rewrite (Rplus_commutative (Rtimes xC (Rtimes yB zA)) (Rtimes xA (Rtimes yC zB))).
Rewrite (Rplus_associative (Rtimes xA (Rtimes yC zB)) (Rtimes xC (Rtimes yB zA))
                           (Rtimes xB (Rtimes yA zC))).
Trivial.
Qed.
 
Lemma det3x3_inverse_permutation: (xA, yA, zA, xB, yB, zB, xC, yC, zC : R)  (determinant3x3 xA yA
                                                                                            zA xC
                                                                                            yC zC
                                                                                            xB yB
                                                                                            zB) = (Roppose (determinant3x3 xA yA
                                                                                                                           zA xB
                                                                                                                           yB zB
                                                                                                                           xC yC
                                                                                                                           zC)).
Intros.
Unfold determinant3x3.
Apply Roppose_inverse_plus.
Generalize (Rtimes xA (Rtimes yB zC)) (Rtimes xB (Rtimes yC zA))
           (Rtimes xC (Rtimes yA zB)) (Rtimes xC (Rtimes yB zA))
           (Rtimes xB (Rtimes yA zC)) (Rtimes xA (Rtimes yC zB)).
Intros.
Cut (a, b, c, d : R)  (Rplus (Rminus a b) (Rminus c d)) = (Rplus (Rminus a d) (Rminus c b)).
Intros.
Rewrite H.
Cut (a, b, c : R)  (Rplus a (Rplus b c)) = (Rplus c (Rplus a b)).
Intros.
Rewrite (H0 r0 r1 r).
Rewrite (Rminus_self (Rplus r (Rplus r0 r1))).
Rewrite (H0 r2 r3 r4).
Rewrite (Rminus_self (Rplus r4 (Rplus r2 r3))).
Rewrite zero_neutral.
Trivial.
Intros.
Rewrite (Rplus_associative a b c).
Rewrite (Rplus_commutative (Rplus a b) c).
Trivial.
Intros.
Rewrite <- (Rminus_plus_assoc_l (Rminus a b) c d).
Rewrite (Rplus_commutative (Rminus a b) c).
Rewrite <- (Rminus_plus_assoc_l c a b).
Rewrite <- (Rminus_plus_assoc_r (Rplus c a) b d).
Rewrite (Rplus_commutative b d).
Rewrite (Rminus_plus_assoc_r (Rplus c a) d b).
Rewrite (Rminus_plus_assoc_l c a d).
Rewrite (Rplus_commutative c (Rminus a d)).
Rewrite (Rminus_plus_assoc_l (Rminus a d) c b).
Trivial.
Qed.
