Require Export reels.
Require Import Ring.
Require Import Classical.

Module Plane(A:OField).
Module C := Field_complements(A).
Import A C.
Definition Plane := (K * K)%type.

(* ------------------ Definitions ------------------- *)

Definition abscisse (p : Plane) : K := match p with
                                      | (x, y) => x
                                      end.

Definition ordonnee (p : Plane) : K := match p with
                                      | (x, y) => y
                                      end.

Definition determinant3x3 (x11 x12 x13 x21 x22 x23 x31 x32 x33 : K) : K :=
  Kplus
    (Kplus (Ktimes x11 (Ktimes x22 x33))
       (Kplus (Ktimes x21 (Ktimes x32 x13)) (Ktimes x31 (Ktimes x12 x23))))
    (Koppose
       (Kplus (Ktimes x31 (Ktimes x22 x13))
          (Kplus (Ktimes x21 (Ktimes x12 x33)) (Ktimes x11 (Ktimes x32 x23))))).

Definition det (p q r : Plane) : K :=
  determinant3x3 (abscisse p) (ordonnee p) one (abscisse q) 
    (ordonnee q) one (abscisse r) (ordonnee r) one.

Definition sensDirect (p q r : Plane) : Prop := Gt (det p q r) zero.

Lemma direct_distincts : forall p q r : Plane, sensDirect p q r -> p <> q.
(* ------------------ Axiome (prouve) supplementaire ------------------- *)
intros.
elim (classic (p = q)); (intros; auto).
rewrite H0 in H; unfold sensDirect in H.
absurd (det q q r = zero).
apply Gt_non_nul; auto.
unfold det, determinant3x3 in |- *; ring.
Qed.
Hint Resolve direct_distincts.

Definition directBool (a b c : Plane) : bool :=
  match ordre_decidable (det a b c) zero with
  | left h => true
  | right h => false
  end.

Lemma directBoolTrue :
 forall p q r : Plane, directBool p q r = true -> sensDirect p q r.
unfold directBool, sensDirect in |- *.
intros p q r.
case (ordre_decidable (det p q r) zero).
trivial.
intros H H1; discriminate H1.
Qed.

Lemma directBoolFalse :
 forall p q r : Plane, directBool p q r = false -> ~ sensDirect p q r.
unfold directBool, sensDirect in |- *.
intros p q r.
case (ordre_decidable (det p q r) zero).
intros H H1; discriminate H1.
trivial.
Qed.
Hint Resolve directBoolTrue directBoolFalse.

Lemma decompose_det :
 forall p q r t : Plane,
 det p q r = Kplus (det t q r) (Kplus (det p t r) (det p q t)).
(* ------------------ calcul de determinants ------------------- *)
intros.
unfold det, determinant3x3 in |- *.
ring.
Qed.

Lemma det_inverse : forall p q r : Plane, det p q r = Koppose (det p r q).
intros.
unfold det, determinant3x3 in |- *.
ring.
Qed.

Lemma det_cyclique : forall p q r : Plane, det p q r = det q r p.
intros.
unfold det, determinant3x3 in |- *.
ring.
Qed.

End Plane.