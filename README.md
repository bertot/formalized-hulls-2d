This is formalization of convex hull algorithms made by David Pichardie
in the years 1999-2000.  This code was ported to more recent versions of
Coq by Yves Bertot in 2017.  It was checked to compile with coq version 8.13
albeit with warnings about future deprecation of some idioms.

The directory also contains an implementation of Delaunay
triangulation in C, aiming for a later formalization.

Also, the file contains a small piece of code to verify properties of
the oriented predicate (in file oriented_sat.ml).  This should probably
not be trusted when it give negative answers.   The way to use it is to call

refute

with a list of of triangles that are oriented (and the opposite of a
conclusion one would want to draw).  It returns true if this list of
fact is indeed inconsistent.

There is also a direct command

obtain_with_pospone _target_ _postpone_ _known_facts_

this will attempt to prove _target_ as a consequence of _known_facts_.
The argument _postpone_ is used to control the order in which
reasoning by cases is performed.  as illustration, the following
figure is proved using the sequence of known facts named
inside_triangle_and_left_of in file oriented_sat.

![4 figures showing uses of axiom5 and axiom5'](oriented_sat_illustration/triangle_inside_2_red.png)
