(* AddPath "$HOME/ctcoq/stage". *)
Require Export reels.
Require Import Ring.

Definition determinant3x3 (x11 x12 x13 x21 x22 x23 x31 x32 x33 : R) : R :=
  Rplus
    (Rplus (Rtimes x11 (Rtimes x22 x33))
       (Rplus (Rtimes x21 (Rtimes x32 x13)) (Rtimes x31 (Rtimes x12 x23))))
    (Roppose
       (Rplus (Rtimes x31 (Rtimes x22 x13))
          (Rplus (Rtimes x21 (Rtimes x12 x33)) (Rtimes x11 (Rtimes x32 x23))))).

Lemma det3x3_cyclique_ligne :
 forall xA yA zA xB yB zB xC yC zC : R,
 determinant3x3 xA yA zA xB yB zB xC yC zC =
 determinant3x3 xB yB zB xC yC zC xA yA zA.
intros.
unfold determinant3x3 in |- *.
ring.
Qed.

Lemma det3x3_inverse_permutation :
 forall xA yA zA xB yB zB xC yC zC : R,
 determinant3x3 xA yA zA xC yC zC xB yB zB =
 Roppose (determinant3x3 xA yA zA xB yB zB xC yC zC).
intros.
unfold determinant3x3 in |- *.
ring.
Qed.