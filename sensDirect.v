(* AddPath "$HOME/ctcoq/stage". *)
Require Export determinant.
Require Import Ring.
 
(* ------------------ Definitions ------------------- *)

Definition Plan := (R * R)%type.
 
Definition abscisse (p : Plan) : R := match p with
                                      | (x, y) => x
                                      end.
 
Definition ordonnee (p : Plan) : R := match p with
                                      | (x, y) => y
                                      end.
 
Definition det (p q r : Plan) : R :=
  determinant3x3 (abscisse p) (ordonnee p) one (abscisse q) 
    (ordonnee q) one (abscisse r) (ordonnee r) one.
 
Definition sensDirect (p q r : Plan) : Prop := reels.Gt (det p q r) zero.
(* ------------------ Hypothese supplementaire ------------------- *)

Parameter
  aucun_triplet_aligne :
    forall p q r : Plan, det p r q <> zero \/ p = q \/ p = r \/ q = r.

(* ------------------ Axiome (prouve) supplementaire ------------------- *)
Lemma direct_distincts : forall p q r : Plan, sensDirect p q r -> p <> q.
intros.
Require Import Classical.
elim (classic (p = q)); (intros; auto).
rewrite H0 in H; unfold sensDirect in H.
absurd (det q q r = zero).
apply Gt_non_nul; auto.
unfold det, determinant3x3 in |- *; ring.
Qed.
Hint Resolve direct_distincts.

(* ------------------ directBool ------------------- *)

Parameter directBool : Plan -> Plan -> Plan -> bool.
Axiom
  directBoolTrue :
    forall p q r : Plan, directBool p q r = true -> sensDirect p q r.
Axiom
  directBoolFalse :
    forall p q r : Plan, directBool p q r = false -> ~ sensDirect p q r.
 
Hint Resolve directBoolTrue directBoolFalse.


(* ------------------ calcul de determinants ------------------- *)

Lemma decompose_det :
 forall p q r t : Plan,
 det p q r = Rplus (det t q r) (Rplus (det p t r) (det p q t)).
intros.
unfold det, determinant3x3 in |- *.
 ring.
Qed.
 
Lemma Cramer_abscisse :
 forall p q r t : Plan,
 Rtimes (det p q r) (abscisse t) =
 Rplus (Rtimes (det t q r) (abscisse p))
   (Rplus (Rtimes (det p t r) (abscisse q)) (Rtimes (det p q t) (abscisse r))).
intros.
unfold det, determinant3x3 in |- *.
 ring.
Qed.
 
Lemma Cramer_ordonnee :
 forall p q r t : Plan,
 Rtimes (det p q r) (ordonnee t) =
 Rplus (Rtimes (det t q r) (ordonnee p))
   (Rplus (Rtimes (det p t r) (ordonnee q)) (Rtimes (det p q t) (ordonnee r))).
intros.
unfold det, determinant3x3 in |- *.
 ring.
Qed.
 
Lemma convex_combinaison :
 forall p q r s t : Plan,
 Rplus (Rtimes (det t q r) (det t s p))
   (Rplus (Rtimes (det t r p) (det t s q)) (Rtimes (det t p q) (det t s r))) =
 zero.
intros.
unfold det, determinant3x3 in |- *.
 ring.
Qed.
 
