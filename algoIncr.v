(* AddPath "$HOME/ctcoq/stage". *)
Require Export convexIncr.
Require Export Arith.
Require Export List.
Require Export Classical.
 
Inductive Cycle : Set :=
    cycle : Plan -> CC -> Cycle.
 
Fixpoint convexRec (couleurDep : CouleurType) (p t : Plan) 
 (listCC : CC) {struct listCC} : CouleurType * (Plan * CC) :=
  match listCC with
  | nil => (Rouge, (bidon, nil))
  | a :: q =>
      match q with
      | nil =>
          match couleurDep with
          | Rouge =>
              match directBool a t p with
              | true => (Bleu, (t, a :: nil))
              | false => (Rouge, (p, nil))
              end
          | Bleu =>
              match directBool a t p with
              | true => (Bleu, (t, a :: nil))
              | false => (Rouge, (t, p :: nil))
              end
          end
      | b :: l =>
          (fun resulRec : CouleurType * (Plan * CC) =>
           match resulRec with
           | (couleur, (tRec, lRec)) =>
               match couleur with
               | Rouge =>
                   match directBool a b p with
                   | true => (Bleu, (tRec, a :: b :: lRec))
                   | false => (Rouge, (tRec, lRec))
                   end
               | Bleu =>
                   match directBool a b p with
                   | true => (Bleu, (tRec, a :: lRec))
                   | false => (Rouge, (tRec, p :: lRec))
                   end
               end
           end) (convexRec couleurDep p t q)
      end
  end.
 
Fixpoint succ_cycle (a t : Plan) (lCC : CC) {struct lCC} : Plan :=
  match lCC with
  | nil => bidon
  | b :: q =>
      match egalBool a b with
      | true => match q with
                | nil => t
                | c :: l => c
                end
      | false => succ_cycle a t q
      end
  end.
 
Fixpoint in_couple (a b t : Plan) (lCC : CC) {struct lCC} : Prop :=
  match lCC with
  | nil => False
  | c :: q =>
      match q with
      | nil => c = a /\ t = b
      | d :: l => c = a /\ d = b \/ in_couple a b t q
      end
  end.
 
Definition cycle_dans_CH (t : Plan) (lCC cc : CC) :=
  forall a b : Plan, in_couple a b t lCC -> CH_Prop cc (a, b).
 
Lemma petit_cycle_inclus_dans_grand :
 forall (t : Plan) (lCC cc : CC) (a b : Plan),
 cycle_dans_CH t (b :: a :: lCC) cc -> cycle_dans_CH t (a :: lCC) cc.
intros.
unfold cycle_dans_CH in |- *; unfold cycle_dans_CH in H.
intros.
apply H.
simpl in |- *; auto.
Qed.
Hint Resolve petit_cycle_inclus_dans_grand.
 
Lemma cycle_point_extremal :
 forall (t : Plan) (lCC cc : CC) (a : Plan),
 cycle_dans_CH t (a :: lCC) cc ->
 forall b : Plan, in_CC b (a :: lCC) -> point_extremal b cc.
intros t lCC cc.
induction lCC as [| a lCC HreclCC]; intros.
simpl in H0.
elim H0; clear H0; intros.
rewrite <- H0.
unfold point_extremal in |- *; right.
exists t.
unfold cycle_dans_CH in H.
apply H; simpl in |- *; auto.
elim H0.
simpl in H0; elim H0; clear H0; intros.
rewrite <- H0.
unfold point_extremal in |- *; right.
exists a.
unfold cycle_dans_CH in H.
apply H; simpl in |- *; auto.
apply (HreclCC a); eauto.
Qed.
 
Lemma in_couple_in_CC_gauche :
 forall (u v t : Plan) (lCC : CC) (a : Plan),
 in_couple u v t (a :: lCC) -> in_CC u (a :: lCC).
intros u v t lCC; induction lCC as [| a lCC HreclCC]; intros.
simpl in |- *; simpl in H.
elim H; auto.
simpl in H.
elim H; clear H; intros.
elim H; clear H; intros.
simpl in |- *; auto.
simpl in |- *; right.
simpl in HreclCC; auto.
Qed.
 
Lemma in_couple_in_CC_droite :
 forall (u v t : Plan) (lCC : CC) (a : Plan),
 in_couple u v t (a :: lCC) -> in_CC v (a :: lCC) \/ v = t.
intros u v t lCC; induction lCC as [| a lCC HreclCC]; intros.
simpl in |- *; simpl in H.
elim H; auto.
simpl in H.
elim H; clear H; intros.
elim H; clear H; intros.
simpl in |- *; auto.
elim (HreclCC a); simpl in |- *; auto.
Qed.
 
Lemma succ_CH_dans_cycle :
 forall (t : Plan) (lCC cc : CC) (a u v : Plan),
 cycle_dans_CH t (a :: lCC) cc ->
 in_couple u v t (a :: lCC) -> v = succ_CH u cc.
intros.
apply (unicite_CH_Prop_droite u v (succ_CH u cc) cc).
unfold cycle_dans_CH in H; auto.
apply succ_CH_Prop.
apply (cycle_point_extremal t lCC cc a).
auto.
apply (in_couple_in_CC_gauche u v t lCC a); auto.
Qed.
 
Lemma cycle_point_extremal_t :
 forall (t : Plan) (lCC cc : CC) (a : Plan),
 cycle_dans_CH t (a :: lCC) cc -> point_extremal t cc.
intros t lCC cc.
unfold point_extremal in |- *.
induction lCC as [| a lCC HreclCC].
left.
exists a.
apply (H a t).
simpl in |- *; auto.
intros.
apply (HreclCC a).
unfold cycle_dans_CH in |- *.
intros.
apply (H a1 b).
simpl in |- *.
simpl in H0.
auto.
Qed.
 
Lemma pred_CH_dans_cycle :
 forall (t : Plan) (lCC cc : CC) (a u v : Plan),
 cycle_dans_CH t (a :: lCC) cc ->
 in_couple u v t (a :: lCC) -> u = pred_CH v cc.
intros.
apply (unicite_CH_Prop_gauche u (pred_CH v cc) v cc).
unfold cycle_dans_CH in H; auto.
apply pred_CH_Prop.
elim (in_couple_in_CC_droite u v t lCC a).
intro; apply (cycle_point_extremal t lCC cc a); auto.
intro.
rewrite H1; apply (cycle_point_extremal_t t lCC cc a); auto.
auto.
Qed.
 
Theorem convexRec_couleur_precedent :
 forall (p t b : Plan) (c : CouleurType) (lCC cc : CC),
 cycle_dans_CH t (b :: lCC) cc ->
 calculCouleur b (succ_CH b cc) p = @fst _ _ (convexRec c p t (b :: lCC)).
intros.
simpl in |- *.
 CaseEq lCC.
cut (t = succ_CH b cc).
intro.
 CaseEq c; CaseEq (directBool b t p); simpl in |- *; rewrite H1 in H3; auto.
apply (succ_CH_dans_cycle t lCC cc b b t); auto.
rewrite H0; simpl in |- *; auto.
 CaseEq (convexRec c p t (p0 :: l)).
 CaseEq p1.
cut (p0 = succ_CH b cc).
 CaseEq c0; CaseEq (directBool b p0 p); simpl in |- *; rewrite <- H4; auto.
apply (succ_CH_dans_cycle t lCC cc b b p0); auto.
rewrite H0; simpl in |- *; auto.
Qed.
 
Theorem ConvexRec_commence_par_bleu :
 forall (couleur : CouleurType) (p t : Plan) (lCC : CC) (b : Plan),
 @fst _ _ (convexRec couleur p t (b :: lCC)) = Bleu ->
   (exists l : CC,
    @snd _ _ (@snd _ _ (convexRec couleur p t (b :: lCC))) = b :: l).
intros.
simpl in |- *.
 CaseEq lCC.
 CaseEq couleur; CaseEq (directBool b t p); simpl in |- *.
exists (nil (A:=Plan)); auto.
simpl in H; rewrite H0 in H; rewrite H1 in H; rewrite H2 in H; simpl in H.
discriminate H.
exists (nil (A:=Plan)); auto.
simpl in H; rewrite H0 in H; rewrite H1 in H; rewrite H2 in H; simpl in H.
discriminate H.
 CaseEq (convexRec couleur p t (p0 :: l)).
 CaseEq p1.
 CaseEq c; CaseEq (directBool b p0 p); simpl in |- *.
exists (p0 :: c0); auto.
simpl in H; rewrite H0 in H; rewrite H1 in H; rewrite H2 in H;
 rewrite H3 in H; rewrite H4 in H; simpl in H.
discriminate H.
exists c0; auto.
simpl in H; rewrite H0 in H; rewrite H1 in H; rewrite H2 in H;
 rewrite H3 in H; rewrite H4 in H; simpl in H.
discriminate H.
Qed.
 
Theorem ConvexRec_commence_par_rouge :
 forall (couleur : CouleurType) (p t : Plan) (lCC : CC) (b : Plan),
 @fst _ _ (convexRec couleur p t (b :: lCC)) = Rouge ->
   (exists l : CC,
    @snd _ _ (@snd _ _ (convexRec couleur p t (b :: lCC))) = p :: l) \/
 @snd _ _ (@snd _ _ (convexRec couleur p t (b :: lCC))) = nil /\
 @fst _ _ (@snd _ _ (convexRec couleur p t (b :: lCC))) = p.
intros couleur p t lCC.
induction lCC as [| a lCC HreclCC].
simpl in |- *.
 CaseEq couleur; CaseEq (directBool b t p); simpl in |- *.
rewrite H1 in H0; simpl in H0.
discriminate H0.
right; auto.
rewrite H1 in H0; simpl in H0.
discriminate H0.
left; exists (nil (A:=Plan)); auto.
intros b.
 CaseEq (convexRec couleur p t (a :: lCC)).
 CaseEq p0.
rewrite H1 in H.
simpl in |- *; simpl in H; rewrite H; simpl in H0; rewrite H in H0.
 CaseEq c; CaseEq (directBool b a p); simpl in |- *.
rewrite H2 in H0; rewrite H3 in H0; simpl in H0.
discriminate H0.
elim (HreclCC a).
simpl in |- *; rewrite H; simpl in |- *.
auto.
simpl in |- *; rewrite H; simpl in |- *.
auto.
simpl in |- *; rewrite H; auto.
rewrite H2 in H0; rewrite H3 in H0; simpl in H0.
discriminate H0.
left; exists c0; auto.
Qed.