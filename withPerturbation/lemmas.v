Require Export reels.
Require Export axiomsKnuth.

Module det_lemmas(A:OField).
Module KA := K_KA(A).
Import KA P C A.

Theorem determinant_colum_mult :
 forall a b c d e f g h i k : K,
 determinant3x3 a b (Ktimes k c) d e (Ktimes k f) g h (Ktimes k i) =
 Ktimes k (determinant3x3 a b c d e f g h i).
intros a b c d e f g h i k; try assumption.
unfold determinant3x3 in |- *.
ring.
Qed.

Theorem recompose_squares :
 forall a b : K,
 Ktimes (Ktimes a a) (Ktimes b b) = Ktimes (Ktimes a b) (Ktimes a b).
intros.
ring.
Qed.

Theorem map_squares :
 forall a b c : K,
 Ktimes (Ktimes a a) (Kplus (Ktimes b b) (Ktimes c c)) =
 Kplus (Ktimes (Ktimes a b) (Ktimes a b)) (Ktimes (Ktimes a c) (Ktimes a c)).
intros; ring.
Qed.

Theorem develop_square_sum :
 forall a b c : K,
 Ktimes (Kplus (Ktimes a b) c) (Kplus (Ktimes a b) c) =
 Kplus
   (Kplus (Kplus (Ktimes (Ktimes a b) (Ktimes a b)) (Ktimes (Ktimes c a) b))
      (Ktimes (Ktimes c a) b)) (Ktimes (Ktimes c c) one).
intros; ring.
Qed.

Theorem determinant_add_column1_to_3 :
 forall a b c d e f g h i k : K,
 determinant3x3 a b c d e f g h i =
 determinant3x3 a b (Kplus c (Ktimes k a)) d e (Kplus f (Ktimes k d)) g h
   (Kplus i (Ktimes k g)).
intros; unfold determinant3x3 in |- *; ring.
Qed.

Theorem determinant_add_column2_to_3 :
 forall a b c d e f g h i k : K,
 determinant3x3 a b c d e f g h i =
 determinant3x3 a b (Kplus c (Ktimes k b)) d e (Kplus f (Ktimes k e)) g h
   (Kplus i (Ktimes k h)).
intros; unfold determinant3x3 in |- *; ring.
Qed.

Theorem map_squares2 :
 forall a b c : K,
 Kplus (Ktimes (Ktimes a c) (Ktimes a c)) (Ktimes (Ktimes b c) (Ktimes b c)) =
 Ktimes (Kplus (Ktimes a a) (Ktimes b b)) (Ktimes c c).
intros; ring.
Qed.

Theorem simplify_second_determinant :
 forall x1 y1 x2 y2 x3 y3 t t' b : K,
 Ktimes t' y1 = Kplus (Ktimes t x1) b ->
 Ktimes t' y2 = Kplus (Ktimes t x2) b ->
 Ktimes t' y3 = Kplus (Ktimes t x3) b ->
 Ktimes (Ktimes t' t')
   (determinant3x3 one x1 (Kplus (Ktimes x1 x1) (Ktimes y1 y1)) one x2
      (Kplus (Ktimes x2 x2) (Ktimes y2 y2)) one x3
      (Kplus (Ktimes x3 x3) (Ktimes y3 y3))) =
 Ktimes (Kplus (Ktimes t' t') (Ktimes t t))
   (determinant3x3 one x1 (Ktimes x1 x1) one x2 (Ktimes x2 x2) one x3
      (Ktimes x3 x3)).
intros x1 y1 x2 y2 x3 y3 t t' b H' H'0 H'1.
rewrite <- determinant_colum_mult.
repeat rewrite map_squares.
rewrite H'.
rewrite H'0.
rewrite H'1.
repeat rewrite develop_square_sum.
repeat rewrite (Radd_assoc RT).
rewrite <- determinant_add_column1_to_3.
rewrite <- determinant_add_column2_to_3.
rewrite <- determinant_add_column2_to_3.
repeat rewrite map_squares2.
apply determinant_colum_mult.
Qed.

Theorem factorize_van_der_monde :
 forall x1 x2 x3 : K,
 determinant3x3 one x1 (Ktimes x1 x1) one x2 (Ktimes x2 x2) one x3
   (Ktimes x3 x3) =
 Ktimes (Kplus x1 (Koppose x2))
   (Ktimes (Kplus x2 (Koppose x3)) (Kplus x3 (Koppose x1))).
intros x1 x2 x3; auto.
unfold determinant3x3 in |- *.
ring.
Qed.

Theorem factorize_second_determinant :
 forall x1 y1 x2 y2 x3 y3 t t' b : K,
 Ktimes t' y1 = Kplus (Ktimes t x1) b ->
 Ktimes t' y2 = Kplus (Ktimes t x2) b ->
 Ktimes t' y3 = Kplus (Ktimes t x3) b ->
 Ktimes (Ktimes t' t')
   (determinant3x3 one x1 (Kplus (Ktimes x1 x1) (Ktimes y1 y1)) one x2
      (Kplus (Ktimes x2 x2) (Ktimes y2 y2)) one x3
      (Kplus (Ktimes x3 x3) (Ktimes y3 y3))) =
 Ktimes (Kplus (Ktimes t' t') (Ktimes t t))
   (Ktimes (Kplus x1 (Koppose x2))
      (Ktimes (Kplus x2 (Koppose x3)) (Kplus x3 (Koppose x1)))).
intros x1 y1 x2 y2 x3 y3 t t' b H' H'0 H'1; try assumption.
rewrite simplify_second_determinant with x1 y1 x2 y2 x3 y3 t t' b; auto.
rewrite factorize_van_der_monde.
auto.
Qed.

Definition line_equation (mn md b : K) (p : Plane) :=
  Kplus (Koppose (Ktimes md (ordonnee p))) (Kplus (Ktimes mn (abscisse p)) b).

Theorem colinear_decompose :
 forall p q r : Plane,
 det p q r = zero ->
 exists mn : K,
   (exists md : K,
      (exists b : K,
         ~ (mn = zero /\ md = zero) /\
         zero = line_equation mn md b p /\
         zero = line_equation mn md b q /\ zero = line_equation mn md b r)).
intros p q r; try assumption.
case (egal_decidable (abscisse p) (abscisse q)).
case (egal_decidable (ordonnee p) (ordonnee q)).
unfold line_equation in |- *.
intros H'; rewrite H'.
intros H'0; rewrite H'0.
intros H'1.
case (egal_decidable (ordonnee r) (ordonnee q)).
intros H'2; rewrite H'2.
case (egal_decidable (abscisse r) (abscisse q)).
intros H'3; rewrite H'3.
case (egal_decidable (ordonnee q) zero).
intros H'4; rewrite H'4.
exists zero; exists one; exists zero; repeat split; try ring.
red in |- *; intros H'5; elim H'5; intros H'6 H'7; clear H'5.
elim not_one_zero; auto.
exists (ordonnee q); exists (abscisse q); exists zero; repeat split; try ring.
red in |- *; intros H'4; elim H'4; intros H'5 H'6; elim n; auto.
exists zero; exists one; exists (ordonnee q); repeat split; try ring.
red in |- *; intros H'5; elim H'5; intros H'6 H'7; clear H'5.
elim not_one_zero; auto.
intros H'2; try assumption.
exists (Kplus (ordonnee r) (Koppose (ordonnee q))).
exists (Kplus (abscisse r) (Koppose (abscisse q))).
exists
 (Kplus (Ktimes (abscisse r) (ordonnee q))
    (Koppose (Ktimes (abscisse q) (ordonnee r)))).
repeat split; try ring.
red in |- *; intros H'3; elim H'3; intros H'4 H'5; try exact H'4; clear H'3.
apply H'2 || case H'2; try assumption.
replace (ordonnee q) with (Kplus (ordonnee q) zero).
rewrite <- H'4.
ring.
ring.
intros H' H'0 H'1; try assumption.
unfold line_equation in |- *.
exists (Kplus (ordonnee p) (Koppose (ordonnee q))).
exists (Kplus (abscisse p) (Koppose (abscisse q))).
exists
 (Kplus (Ktimes (ordonnee q) (abscisse p))
    (Koppose (Ktimes (ordonnee p) (abscisse q)))).
repeat split; try ring.
red in |- *; intros H'2; elim H'2; intros H'3 H'4; try exact H'3; clear H'2.
apply H' || case H'; replace (ordonnee q) with (Kplus (ordonnee q) zero).
rewrite <- H'3.
ring.
ring.
rewrite <- H'1.
unfold det, determinant3x3 in |- *; ring.
intros H' H'0; try assumption.
unfold line_equation in |- *.
exists (Kplus (ordonnee p) (Koppose (ordonnee q))).
exists (Kplus (abscisse p) (Koppose (abscisse q))).
exists
 (Kplus (Ktimes (ordonnee q) (abscisse p))
    (Koppose (Ktimes (ordonnee p) (abscisse q)))).
repeat split; try ring.
red in |- *; intros H'1; elim H'1; intros H'2 H'3; try exact H'3; clear H'1.
apply H' || case H'; replace (abscisse q) with (Kplus zero (abscisse q)).
rewrite <- H'3.
ring.
ring.
rewrite <- H'0.
unfold det, determinant3x3 in |- *; ring.
Qed.

Definition perturbate_point (t : K) (p : Plane) : Plane :=
  match p with
  | (x, y) =>
      (Kplus x (Ktimes t y),
      Kplus y (Ktimes t (Kplus (Ktimes x x) (Ktimes y y))))
  end.

Theorem Gt_not_eq : forall a b : K, Gt a b -> a <> b.
intros a b H'; red in |- *; intros H'0; try exact H'0.
generalize H'.
rewrite <- H'0.
pattern a at 1 in |- *; rewrite H'0.
intros H'1; try assumption.
elim (Gt_antisym _ _ H'1); auto.
Qed.

Lemma Gt_Ge_trans : forall a b c : K, Gt a b -> Ge b c -> Gt a c.
intros a b c H' H'0; elim H'0.
intros; apply Gt_trans with b; auto.
intros H'1; rewrite <- H'1.
auto.
Qed.

Theorem GT_Koppose_Koppose :
 forall a b : K, Gt a b -> Gt (Koppose b) (Koppose a).
intros a b H'; try assumption.
replace (Koppose b) with (Kplus (Koppose a) (Kplus a (Koppose b))).
pattern (Koppose a) at 2 in |- *;
 replace (Koppose a) with (Kplus (Koppose a) (Kplus (Koppose b) b)).
apply Gt_plus.
rewrite (Radd_comm RT a).
apply Gt_plus.
auto.
ring.
ring.
Qed.

Theorem Kinv_Koppose :
 forall x : K, x <> zero -> Kinv (Koppose x) = Koppose (Kinv x).
intros x H'; apply times_simplify with x; auto.
replace (Ktimes x (Koppose (Kinv x))) with (Koppose (Ktimes x (Kinv x))) by
ring.
rewrite Kinv_def; auto.
replace (Ktimes x (Kinv (Koppose x))) with
 (Koppose (Ktimes (Koppose x) (Kinv (Koppose x)))).
rewrite Kinv_def; auto.
intros H'0.
apply H'; try assumption.
replace zero with (Kplus x (Koppose x)).
rewrite H'0.
ring.
ring.
ring.
Qed.

Theorem Kmult_Koppose_permute :
 forall a b : K, Ktimes a (Koppose b) = Koppose (Ktimes a b).
intros; ring.
Qed.
Hint Resolve GT_Koppose_Koppose.

Theorem Gt_Koppose_Koppose2 :
 forall a b : K, Gt (Koppose a) (Koppose b) -> Gt b a.
intros a b H'; try assumption.
rewrite <- (Kopp_opp a).
rewrite <- (Kopp_opp b).
auto.
Qed.

Theorem Kmult_Koppose_permute1 :
 forall a b : K, Ktimes (Koppose a) b = Koppose (Ktimes a b).
intros; ring.
Qed.

Theorem Koppose_around2 :
 forall a b : K, Gt a (Koppose b) -> Gt b (Koppose a).
intros a b H'; try assumption.
rewrite <- (Kopp_opp b).
auto.
Qed.

Theorem inv_non_nul : forall a : K, a <> zero -> Kinv a <> zero.
intros a H'; red in |- *; intros H'0; try exact H'0.
apply H' || case H'; auto.
replace a with (Ktimes a (Ktimes (Kinv a) a)).
rewrite (Rmul_assoc RT a (Kinv a) a).
rewrite (Rmul_comm RT a (Kinv a)).
rewrite <- (Rmul_assoc RT (Kinv a) a a).
rewrite H'0.
ring.
rewrite (Rmul_assoc RT a (Kinv a) a).
rewrite Kinv_def.
ring.
auto.
Qed.

Theorem Gt_non_nul : forall a : K, Gt a zero -> a <> zero.
intros a H'; red in |- *; intros H'0; try exact H'0.
elim (Gt_antisym a zero).
auto.
generalize H'.
rewrite H'0.
auto.
Qed.

Theorem inv_sign : forall a : K, Gt a zero -> Gt (Kinv a) zero.
intros a H'; try assumption.
replace (Kinv a) with (Ktimes a (Ktimes (Kinv a) (Kinv a))).
apply Gt_stable_mult.
auto.
elim (carre_positif (Kinv a)).
auto.
intros H'0; try assumption.
cut (Kinv a <> zero).
intros H'1; try assumption.
elim K_integre with (1 := H'0); intros; elim H'1; auto.
apply inv_non_nul.
apply Gt_non_nul.
auto.
rewrite (Rmul_assoc RT a (Kinv a) (Kinv a)).
rewrite Kinv_def.
ring.
apply Gt_non_nul.
auto.
Qed.

Theorem Gt_non_nul2 : forall a : K, Gt zero a -> a <> zero.
intros a H'; try assumption.
red in |- *; intros H'0; try exact H'0.
generalize H'.
rewrite H'0.
intros H'1; try assumption.
elim (Gt_antisym zero zero); auto.
Qed.

Theorem Gt_Gt_zero :
 forall a b : K, Gt (Kplus a (Koppose b)) zero -> Gt a b.
intros a b H'; try assumption.
replace b with (Kplus b zero).
replace a with (Kplus b (Kplus a (Koppose b))).
apply Gt_plus.
auto.
ring.
ring.
Qed.

Theorem Gt_zero_GT :
 forall a b : K, Gt a b -> Gt (Kplus a (Koppose b)) zero.
intros a b H'; try assumption.
replace zero with (Kplus (Koppose b) b).
rewrite (Radd_comm RT a (Koppose b)).
apply Gt_plus.
auto.
ring.
Qed.

Theorem Ge_Gt_trans :
 forall a b c : K, Ge a b -> Gt b c -> Gt a c.
intros a b c H'; elim H'.
intros H'0 H'1; try assumption.
apply Gt_trans with b; auto.
intros H'0; rewrite H'0.
auto.
Qed.

Theorem Gt_one_zero : Gt one zero.
elim (carre_positif one).
rewrite (Rmul_1_l RT one).
auto.
rewrite (Rmul_1_l RT one).
intros H'; try assumption.
case not_one_zero; auto.
Qed.

Theorem mult_zero : forall x : K, Ktimes zero x = zero.
intros; ring.
Qed.

Theorem zero_mult : forall x : K, Ktimes x zero = zero.
intros; ring.
Qed.

Theorem oppose_zero : Koppose zero = zero.
intros; ring.
Qed.

Theorem Ge_trans : forall a b c : K, Ge a b -> Ge b c -> Ge a c.
intros a b c H'; elim H'.
intros H'0 H'1; try assumption.
apply Gt_Ge.
apply Gt_Ge_trans with b.
auto.
auto.
intros H'0; rewrite H'0.
auto.
Qed.

Definition abs (x : K) : K :=
  match ordre_decidable zero x with
  | left h => Koppose x
  | right h' => x
  end.

Theorem Ge_abs : forall x : K, Ge (abs x) x.
intros x; unfold abs, Ge in |- *; case (ordre_decidable zero x); auto.
intros H'; try assumption.
left; try assumption.
apply Gt_trans with zero.
auto.
auto.
Qed.

Theorem Ge_abs2 : forall x : K, Ge (abs x) (Koppose x).
intros x; unfold abs, Ge in |- *; case (ordre_decidable zero x); auto.
intros H'; try assumption.
elim (non_Gt_Ge zero x H'); auto.
intros H'0; left; auto.
apply Gt_trans with zero; auto.
intros H'0; rewrite H'0.
right; auto.
ring.
Qed.

Theorem Koppose_Kplus :
 forall a b : K, Koppose (Kplus a b) = Kplus (Koppose a) (Koppose b).
intros; ring.
Qed.
Hint Resolve Ge_abs2 Ge_abs.

Theorem Ge_Ge_zero : forall a b : K, Ge (Kplus a (Koppose b)) zero -> Ge a b.
intros a b H'; elim H'.
intros H'0; try assumption.
unfold Ge in |- *.
left; auto.
apply Gt_Gt_zero.
auto.
intros H'0; try assumption.
unfold Ge in |- *.
replace b with (Kplus b zero).
rewrite <- H'0.
right; auto.
ring.
rewrite (Radd_comm RT b zero).
rewrite (Radd_0_l RT b).
auto.
Qed.

Theorem Ge_zero_Ge : forall a b : K, Ge a b -> Ge (Kplus a (Koppose b)) zero.
intros a b H'; elim H'.
intros H'0; try assumption.
red in |- *.
left; try assumption.
apply Gt_zero_GT.
auto.
intros H'0; auto.
red in |- *.
rewrite <- H'0.
right; auto.
ring.
Qed.

Theorem Ge_sum : forall x y : K, Ge (Kplus (abs x) (abs y)) (abs (Kplus x y)).
intros x y; auto.
unfold abs at 3 in |- *; case (ordre_decidable zero (Kplus x y)).
rewrite (Koppose_Kplus x y).
intros H'; try assumption.
apply Ge_Ge_zero.
rewrite (Koppose_Kplus (Koppose x) (Koppose y)).
rewrite <-
 (Radd_assoc RT (abs x) (abs y)
    (Kplus (Koppose (Koppose x)) (Koppose (Koppose y))))
 .
rewrite
 (Radd_assoc RT (abs y) (Koppose (Koppose x)) (Koppose (Koppose y)))
 .
rewrite (Radd_comm RT (abs y) (Koppose (Koppose x))).
rewrite <-
 (Radd_assoc RT (Koppose (Koppose x)) (abs y) (Koppose (Koppose y)))
 .
rewrite
 (Radd_assoc RT (abs x) (Koppose (Koppose x))
    (Kplus (abs y) (Koppose (Koppose y)))).
apply Ge_stable_plus; apply Ge_zero_Ge; auto.
intros H'; try assumption.
apply Ge_Ge_zero.
rewrite (Koppose_Kplus x y).
rewrite <- (Radd_assoc RT (abs x) (abs y) (Kplus (Koppose x) (Koppose y))).
rewrite (Radd_assoc RT (abs y) (Koppose x) (Koppose y)).
rewrite (Radd_comm RT (abs y) (Koppose x)).
rewrite <- (Radd_assoc RT (Koppose x) (abs y) (Koppose y)).
rewrite (Radd_assoc RT (abs x) (Koppose x) (Kplus (abs y) (Koppose y))).
apply Ge_stable_plus; apply Ge_zero_Ge; auto.
Qed.

Theorem abs_gt : forall x : K, x <> zero -> Gt (abs x) zero.
intros x; try assumption.
unfold abs in |- *.
case (ordre_decidable zero x).
intros H' H'0; try assumption.
auto.
intros H' H'0; try assumption.
elim (non_Gt_Ge zero x); auto.
intros H'1; try assumption.
apply H'0 || case H'0; auto.
Qed.

Theorem abs_ge : forall x : K, Ge (abs x) zero.
intros x; unfold abs, Ge in |- *.
case (ordre_decidable zero x).
intros H'; left; auto.
intros H'; auto.
exact (non_Gt_Ge _ _ H').
Qed.

Theorem abs_oppose : forall x : K, abs x = abs (Koppose x).
intros x; try assumption.
unfold abs in |- *; case (ordre_decidable zero x).
case (ordre_decidable zero (Koppose x)).
intros H' H'0; try assumption.
elim Gt_antisym with (1 := H').
auto.
intros H'; auto.
case (ordre_decidable zero (Koppose x)).
intros H' H'0; auto.
rewrite Kopp_opp.
auto.
intros H' H'0; try assumption.
elim (non_Gt_Ge zero (Koppose x)); auto.
intros H'1; try assumption.
apply H'0 || case H'0; try assumption.
rewrite <- (Kopp_opp x); auto.
intros H'1; rewrite H'1.
rewrite <- (Kopp_opp x); auto.
rewrite H'1.
ring.
Qed.

Theorem abs_mult : forall x y : K, abs (Ktimes x y) = Ktimes (abs x) (abs y).
unfold abs in |- *.
intros x y; try assumption.
case (ordre_decidable zero x).
case (ordre_decidable zero y).
case (ordre_decidable zero (Ktimes x y)).
intros H' H'0 H'1; try assumption.
elim (Gt_antisym zero (Ktimes x y)).
auto.
replace (Ktimes x y) with (Ktimes (Koppose x) (Koppose y)).
apply Gt_stable_mult; auto.
ring.
intros; ring.
case (ordre_decidable zero (Ktimes x y)).
intros; ring.
intros H' H'0 H'1; try assumption.
elim (non_Gt_Ge zero y); auto.
intros H'2; try assumption.
apply H' || case H'; try assumption.
replace (Ktimes x y) with (Ktimes (Koppose x) (Koppose y)).
rewrite (Rmul_comm RT (Koppose x) (Koppose y)).
rewrite (Kmult_Koppose_permute1 y (Koppose x)).
apply Koppose_Gt.
auto.
intros; ring.
intros H'2; rewrite H'2.
intros; ring.
case (ordre_decidable zero y).
case (ordre_decidable zero (Ktimes x y)).
intros; ring.
intros H' H'0 H'1; try assumption.
elim (non_Gt_Ge zero x); auto.
intros H'2; try assumption.
apply H' || case H'; try assumption.
replace (Ktimes x y) with (Ktimes (Koppose x) (Koppose y)).
rewrite (Kmult_Koppose_permute1 x (Koppose y)).
apply Koppose_Gt.
auto.
intros; ring.
intros H'2; rewrite H'2.
intros; ring.
case (ordre_decidable zero (Ktimes x y)).
intros H' H'0; try exact H'0.
case (non_Gt_Ge zero y); auto.
intros H'1 H'2; try assumption.
case (non_Gt_Ge zero x); auto.
intros H'3; try assumption.
case Gt_antisym with (1 := H'); auto.
intros H'3; rewrite H'3.
ring.
intros H'1; rewrite H'1.
intros; ring.
intros H' H'0 H'1; auto.
Qed.

Theorem Ge_neg_abs : forall x : K, Ge x (Koppose (abs x)).
intros x; try assumption.
apply Ge_Ge_zero.
rewrite Kopp_opp.
rewrite (Radd_comm RT x (abs x)).
pattern x at 2 in |- *; rewrite <- (Kopp_opp x).
apply Ge_zero_Ge.
auto.
Qed.

Theorem abs_pos_eq : forall x : K, Ge x zero -> x = abs x.
intros x H'; elim H'.
intros H'0; unfold abs in |- *.
case (ordre_decidable zero x).
intros H'1; try assumption.
case (Gt_antisym zero x); auto.
intros H'1; auto.
intros H'0; rewrite H'0.
unfold abs in |- *.
case (ordre_decidable zero zero); intros; ring.
Qed.

Theorem Ge_move_right :
 forall a b : K, Ge (Kplus a b) zero -> Ge a (Koppose b).
intros a b H'; try assumption.
apply Ge_Ge_zero.
rewrite (Kopp_opp b).
auto.
Qed.

Theorem Ge_move_right_2 :
 forall a b : K, Ge a (Koppose b) -> Ge (Kplus a b) zero.
intros a b H'; try assumption.
rewrite <- (Kopp_opp b).
apply Ge_zero_Ge.
auto.
Qed.

Theorem or_permute : forall a b : Prop, a \/ b -> b \/ a.
intros a b H; elim H; auto.
Qed.

Theorem sumbool_permute : forall a b : Prop, {a} + {b} -> {b} + {a}.
intros a b H; elim H; auto.
Qed.

Theorem Ge_Koppose_zero_right :
 forall x : K, Ge (Koppose x) zero -> Ge zero x.
intros x H; apply Ge_Ge_zero.
rewrite (Radd_0_l RT (Koppose x)).
auto.
Qed.

Theorem Ge_antisym : forall x y : K, Ge x y -> Ge y x -> x = y.
intros x y H'; elim H'.
intros H'0 H'1; elim H'1.
intros H'2; elim Gt_antisym with (1 := H'2); assumption.
auto.
auto.
Qed.

Theorem Gt_not_Ge : forall x y : K, Gt x y -> ~ Ge y x.
intros x y H'; red in |- *; intros H'0; elim H'0.
intros H'1; elim Gt_antisym with (1 := H'1); auto.
intros H'1; elim Gt_not_eq with (1 := H'); auto.
Qed.

Theorem Ge_mult_pos :
 forall x y z : K, Gt x zero -> Ge (Ktimes x y) (Ktimes x z) -> Ge y z.
intros x y z H' H'0; try assumption.
elim H'0.
intros H'1; try assumption.
left.
apply Gt_Gt_zero.
apply Ktimes_Gt with (a := x).
auto.
rewrite (Rmul_comm RT x (Kplus y (Koppose z))).
rewrite (Rdistr_l RT y (Koppose z) x).
rewrite (Kmult_Koppose_permute1 z x).
apply Gt_zero_GT.
rewrite (Rmul_comm RT z x).
rewrite (Rmul_comm RT y x).
auto.
intros H'1; try assumption.
right.
apply times_simplify with (a := x); auto.
Qed.

Theorem abs_square : forall x : K, abs (Ktimes x x) = Ktimes x x.
intros x; try assumption.
case (ordre_decidable zero x).
intros H'; try assumption.
rewrite abs_mult.
rewrite abs_oppose.
rewrite <- abs_pos_eq.
rewrite (Kmult_Koppose_permute1 x (Koppose x)).
rewrite <- (Kmult_Koppose_permute x (Koppose x)).
rewrite (Kopp_opp x).
auto.
auto.
intros H'; try assumption.
rewrite abs_mult.
rewrite <- abs_pos_eq.
auto.
auto.
Qed.
 
Theorem abs_neg_eq : forall a, Gt zero a -> abs a = Koppose a.
intros a H'; try assumption.
unfold abs in |- *.
case (ordre_decidable zero a).
auto.
intros H'0; try assumption.
apply H'0 || case H'0; auto.
Qed.
 
Theorem abs_inv_eq : forall x, x <> zero -> abs (Kinv x) = Kinv (abs x).
intros x H'.
case (ordre_decidable x zero).
intros H'0.
rewrite <- (abs_pos_eq (Kinv x)).
rewrite <- (abs_pos_eq x); auto.
unfold Ge in |- *.
left.
apply inv_sign; auto.
intros H'0.
rewrite (abs_neg_eq x).
rewrite (abs_neg_eq (Kinv x)).
rewrite <- (Kinv_Koppose x); auto.
rewrite <- (Kopp_opp x).
rewrite Kinv_Koppose.
apply Koppose_Gt.
apply inv_sign.
elim (non_Gt_Ge x zero); auto.
intros H'1.
apply H' || case H'; auto.
red in |- *; intros H'1; auto.
apply H' || case H'; auto.
replace zero with (Kplus x (Koppose x)).
rewrite H'1.
rewrite (Radd_comm RT x zero).
rewrite (Radd_0_l RT x).
auto.
ring.
elim (non_Gt_Ge x zero); auto.
intros H'1.
apply H' || case H'; auto.
Qed.
 
Theorem not_abs_zero : forall x, x <> zero -> abs x <> zero.
intros x H'; try assumption.
unfold abs in |- *.
case (ordre_decidable zero x).
intros H'0; try assumption.
red in |- *; intros H'1; try exact H'1.
apply H' || case H'; try assumption.
replace zero with (Kplus x (Koppose x)).
rewrite H'1.
ring.
ring.
intros H'0; auto.
Qed.

End det_lemmas.