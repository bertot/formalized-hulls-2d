(* AddPath "$HOME/ctcoq/stage". *)
Require Export maxPolar.
Require Export List.
Require Export Classical.


(* ------------------- succ_CH ------------------ *)

Definition succ_CH (t : Plan) (cc : CC) := minPolar t cc.

Theorem CH_Prop_gauche :
 forall (t : Plan) (cc : CC),
 point_extremal t cc -> (exists s : Plan, CH_Prop cc (s, t)).
intros.
elim H; intro; auto.
elim H0; clear H0; intros r H0.
apply (points_extremal_precedent cc r t); auto.
Qed.

Theorem CH_Prop_droit :
 forall (t : Plan) (cc : CC),
 point_extremal t cc -> (exists r : Plan, CH_Prop cc (t, r)).
intros.
elim H; intro; auto.
elim H0; clear H0; intros r H0.
apply (points_extremal_suivant cc r t); auto.
Qed.


Theorem succ_CH_Prop :
 forall (t : Plan) (cc : CC),
 point_extremal t cc -> CH_Prop cc (t, succ_CH t cc).
intros.
elim (CH_Prop_gauche t cc); auto.
clear H; intros s H.
unfold CH_Prop in |- *; simpl in |- *.
unfold CH_Prop in H; simpl in H.
elim H; clear H; intros H H1.
elim H1; clear H1; intros H1 H2.
split; auto.
unfold succ_CH in |- *.
split.
apply minPolar_dans_cc.
exists s.
split; auto.
apply (points_extremaux_distincts s t cc); auto.
apply (minPolarOK t s cc); auto.
exists s.
split; auto.
apply (points_extremaux_distincts s t cc); auto.
Qed.

Lemma succ_CH_in_cc :
 forall (t : Plan) (cc : CC), point_extremal t cc -> in_CC (succ_CH t cc) cc.
intros.
unfold succ_CH in |- *.
apply minPolar_dans_cc.
elim (CH_Prop_droit t cc); auto.
intros.
exists x.
unfold CH_Prop in H0; simpl in H0.
elim H0; clear H0; intros H0 H1.
elim H1; clear H1; intros H1 H2.
split; auto.
apply sym_not_eq; apply (points_extremaux_distincts t x cc); auto.
Qed.

(* ------------------- pred_CH ------------------ *)

Definition pred_CH (t : Plan) (cc : CC) := maxPolar t cc.

Theorem pred_CH_Prop :
 forall (t : Plan) (cc : CC),
 point_extremal t cc -> CH_Prop cc (pred_CH t cc, t).
intros.
elim (CH_Prop_droit t cc); auto.
clear H; intros s H.
unfold CH_Prop in |- *; simpl in |- *.
unfold CH_Prop in H; simpl in H.
elim H; clear H; intros H H1.
elim H1; clear H1; intros H1 H2.
unfold pred_CH in |- *.
split.
apply maxPolar_dans_cc.
exists s.
split; auto.
apply sym_not_eq; apply (points_extremaux_distincts t s cc); auto.
split; auto.
apply (maxPolarOK t s cc); auto.
exists s.
split; auto.
apply sym_not_eq; apply (points_extremaux_distincts t s cc); auto.
Qed.
Hint Resolve succ_CH_Prop pred_CH_Prop.

Lemma pred_CH_in_cc :
 forall (t : Plan) (cc : CC), point_extremal t cc -> in_CC (pred_CH t cc) cc.
intros.
unfold pred_CH in |- *.
apply maxPolar_dans_cc.
elim (CH_Prop_droit t cc); auto.
intros.
exists x.
unfold CH_Prop in H0; simpl in H0.
elim H0; clear H0; intros H0 H1.
elim H1; clear H1; intros H1 H2.
split; auto.
apply sym_not_eq; apply (points_extremaux_distincts t x cc); auto.
Qed.
Hint Resolve pred_CH_in_cc succ_CH_in_cc.

(* -------------------- CH_incremental ------------------------ *)

Lemma CH_incremental_non_p :
 forall (p s t : Plan) (cc : CC),
 p <> s -> p <> t -> CH_Prop (p :: cc) (s, t) -> CH_Prop cc (s, t).
intros.
unfold CH_Prop in |- *; simpl in |- *.
unfold CH_Prop in H1; simpl in H1.
elim H1; clear H1; intros.
elim H2; clear H2; intros.
elim H2; intro.
absurd (p = t); auto.
elim H1; intro.
absurd (p = s); auto.
clear H2 H1.
repeat (split; auto).
inversion H3; auto.
Qed.

Lemma CH_incremental_t_egal_p :
 forall (p s t : Plan) (cc : CC),
 au_moins_deux_points cc -> CH_Prop (p :: cc) (s, p) -> point_extremal s cc.
intros.
unfold point_extremal in |- *.
unfold CH_Prop in H0; simpl in H0.
elim H0; clear H0; intros.
elim H1; clear H1; intros.
elim H0; clear H1 H0; intro.
absurd (s = p);
 [ apply (points_extremaux_distincts s p (p :: cc)); auto | auto ].
elim (classic (in_CC p cc)); intro.
right; exists p.
inversion H2.
unfold CH_Prop in |- *; repeat (split; auto).
elim (points_extremal_precedent (p :: cc) p s).
intros r H3.
unfold CH_Prop in H3; simpl in H3.
elim H3; clear H3; intros.
elim H4; clear H4; intros.
elim H3; clear H4 H3; intro.
rewrite <- H3 in H5.
absurd (au_moins_deux_points cc); auto.
apply (plus_de_deux_pas_moins p cc).
auto.
apply (points_extremaux_sym s p (p :: cc)); auto.
left; exists r.
unfold CH_Prop in |- *; simpl in |- *.
inversion H5; repeat (split; auto).
unfold CH_Prop in |- *; simpl in |- *; repeat (split; auto).
Qed.

Lemma CH_incremental_s_egal_p :
 forall (p s t : Plan) (cc : CC),
 au_moins_deux_points cc -> CH_Prop (p :: cc) (p, t) -> point_extremal t cc.
intros.
unfold point_extremal in |- *.
unfold CH_Prop in H0; simpl in H0.
elim H0; clear H0; intros.
elim H1; clear H1; intros.
elim H1; clear H1 H0; intro.
absurd (p = t);
 [ apply (points_extremaux_distincts p t (p :: cc)); auto | auto ].
elim (classic (in_CC p cc)); intro.
left; exists p.
inversion H2.
unfold CH_Prop in |- *; repeat (split; auto).
elim (points_extremal_suivant (p :: cc) p t).
intros r H3.
unfold CH_Prop in H3; simpl in H3.
elim H3; clear H3; intros.
elim H4; clear H4; intros.
elim H4; clear H4 H3; intro.
rewrite <- H3 in H5.
absurd (au_moins_deux_points cc); auto.
apply (plus_de_deux_pas_moins p cc).
auto.
apply (points_extremaux_sym t p (p :: cc)); auto.
right; exists r.
unfold CH_Prop in |- *; simpl in |- *.
inversion H5; repeat (split; auto).
unfold CH_Prop in |- *; simpl in |- *; repeat (split; auto).
Qed.


Theorem CH_incremental_inv_non_p :
 forall (p s t : Plan) (cc : CC),
 infPolarEq s t p -> CH_Prop cc (s, t) -> CH_Prop (p :: cc) (s, t).
intros.
unfold CH_Prop in H0; simpl in H0.
elim H0; clear H0; intros H0 H1.
elim H1; clear H1; intros H1 H2.
unfold CH_Prop in |- *; simpl in |- *.
split; auto.
Qed.

Theorem CH_incremental_inv_t_egal_p :
 forall (p s : Plan) (cc : CC),
 point_extremal s cc ->
 infPolarEq s (pred_CH s cc) p ->
 sensDirect s (succ_CH s cc) p -> CH_Prop (p :: cc) (p, s).
intros.
unfold CH_Prop in |- *; simpl in |- *.
split; auto.
split.
right.
elim (CH_Prop_droit s cc).
intros.
elim H2; auto.
auto.
apply points_extremaux_rec; auto.
apply (Points_extremaux_trans_max (succ_CH s cc) s p (pred_CH s cc) cc); auto.
apply (points_extremaux_Prop s (succ_CH s cc) (pred_CH s cc) cc); auto.
elim (succ_CH_Prop s cc); auto.
simpl in |- *; intros.
elim H3; auto.
apply (direct_distincts p s (succ_CH s cc)); auto.
elim (pred_CH_Prop s cc); auto.
simpl in |- *; intros.
elim H3; auto.
apply points_extremaux_rec; auto.
elim (succ_CH_Prop s cc); auto.
simpl in |- *; intros.
elim H3; auto.
Qed.

Theorem CH_incremental_inv_s_egal_p :
 forall (p t : Plan) (cc : CC),
 point_extremal t cc ->
 infPolarEq t p (succ_CH t cc) ->
 sensDirect (pred_CH t cc) t p -> CH_Prop (p :: cc) (t, p).
intros.
unfold CH_Prop in |- *; simpl in |- *.
split; auto.
right.
elim (CH_Prop_droit t cc).
intros.
elim H2; auto.
auto.
split; auto.
apply points_extremaux_rec; auto.
apply (Points_extremaux_trans (pred_CH t cc) t p (succ_CH t cc) cc); auto.
apply (points_extremaux_Prop (pred_CH t cc) t (succ_CH t cc) cc); auto.
elim (pred_CH_Prop t cc); auto.
simpl in |- *; intros.
elim H3; auto.
apply sym_not_eq; apply (direct_distincts t p (pred_CH t cc)); auto.
elim (succ_CH_Prop t cc); auto.
simpl in |- *; intros.
elim H3; auto.
apply points_extremaux_rec; auto.
elim (pred_CH_Prop t cc); auto.
simpl in |- *; intros.
elim H3; auto.
Qed.

(* ---------------------- unicite des points extremaux ------------------------ *)

Lemma unicite_CH_Prop_gauche :
 forall (s1 s2 t : Plan) (cc : CC),
 CH_Prop cc (s1, t) -> CH_Prop cc (s2, t) -> s1 = s2.
intros.
unfold CH_Prop in H; simpl in H.
elim H; clear H; intros.
elim H1; clear H1; intros.
unfold CH_Prop in H0; simpl in H0.
elim H0; clear H0; intros.
elim H3; clear H3; intros.
clear H3.
cut (infPolarEq s2 t s1); intros.
cut (infPolarEq s1 t s2); intros.
elim (infPolarEq_egal s1 t s2); auto.
intro; absurd (t = s2); auto.
apply sym_not_eq; apply (points_extremaux_distincts s2 t cc); auto.
intros.
elim H6; clear H6; intros.
absurd (s1 = t); auto.
apply (points_extremaux_distincts s1 t cc); auto.
auto.
apply (points_extremaux_Prop s1 t s2 cc); auto.
apply (points_extremaux_Prop s2 t s1 cc); auto.
Qed.

Lemma unicite_CH_Prop_droite :
 forall (s t1 t2 : Plan) (cc : CC),
 CH_Prop cc (s, t1) -> CH_Prop cc (s, t2) -> t1 = t2.
intros.
unfold CH_Prop in H; simpl in H.
elim H; clear H; intros.
elim H1; clear H1; intros.
unfold CH_Prop in H0; simpl in H0.
elim H0; clear H0; intros.
elim H3; clear H3; intros.
clear H0.
cut (infPolarEq s t2 t1); intros.
cut (infPolarEq s t1 t2); intros.
elim (infPolarEq_egal s t1 t2); auto.
intros.
elim H6; clear H6; intros.
absurd (s = t1 :>Plan); auto.
apply (points_extremaux_distincts s t1 cc); auto.
absurd (s = t2 :>Plan); auto.
apply (points_extremaux_distincts s t2 cc); auto.
apply (points_extremaux_Prop s t1 t2 cc); auto.
apply (points_extremaux_Prop s t2 t1 cc); auto.
Qed.
