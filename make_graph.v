AddPath "/net/home/bertot/experiments/graph".
Add ML Path "/net/home/bertot/experiments/graph".

Require Graph_com.

Require PolyList.
Require Arith.
Require Le.
Require algoIncr6.

FileDepAfter "R" "R" "axioms.dot" "axioms.gps".

