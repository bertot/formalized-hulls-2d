Require Export sensDirect.
Require Import Classical.
Require Import List.
Require Import Arith.
Require Import Ring.

Module Type KnuthAxioms.
Parameter Plane : Type.
Parameter OT : Plane -> Plane -> Plane -> Prop.

Axiom Axiom1 : forall p q r, OT p q r -> OT q r p.

Axiom Axiom2 : forall p q r, OT p q r -> ~ OT p r q.

Axiom Axiom4 : forall p q r t, OT t q r -> OT p t r -> OT p q t -> OT p q r.

Axiom Axiom5 :
 forall t s p q r, OT t s p -> OT t s q -> OT t s r ->
    OT t p q -> OT t q r -> OT t p r.

End KnuthAxioms.

Module K_KA (A : OField) <: KnuthAxioms.
Module P := Plane (A).
Import P C A.

Definition Plane := Plane.
Definition OT := sensDirect.

Theorem Axiom1 : forall p q r : Plane, OT p q r -> OT q r p.
unfold OT, sensDirect in |- *.
intros; rewrite <- det_cyclique; trivial.
Qed.

Theorem Axiom2 : forall p q r : Plane, OT p q r -> ~ OT p r q.
unfold OT, sensDirect in |- *.
intros; rewrite det_inverse; apply Inverse_signe_r; trivial.
Qed.

Theorem Axiom4 :
 forall p q r t : Plane,
 OT t q r -> OT p t r -> OT p q t -> OT p q r.
unfold OT, sensDirect in |- *; intros; rewrite (decompose_det p q r t); auto.
Qed.
Hint Resolve Axiom1 Axiom2.

Theorem Axiom5 :
 forall t s p q r : Plane,
 OT t s p ->
 OT t s q ->
 OT t s r -> OT t p q -> OT t q r -> OT t p r.
intros t s p q r tsp tsq tsr tpq tqr; unfold OT, sensDirect in |- *.
apply (Ktimes_Gt (det t s q)); auto.
replace (Ktimes (det t s q) (det t p r)) with
 (Kplus (Ktimes (det t q r) (det t s p)) (Ktimes (det t p q) (det t s r)));
 auto; try (unfold det, determinant3x3 in |- *; ring).
Qed.

Lemma Axiom5bis :
 forall t s p q r : Plane,
 OT s t p ->
 OT s t q ->
 OT s t r -> OT t p q -> OT t q r -> OT t p r.
intros t s p q r stp stq str tpq tqr; unfold OT, sensDirect in |- *.
apply (Ktimes_Gt (det s t q)); auto.
replace (Ktimes (det s t q) (det t p r)) with
 (Kplus (Ktimes (det t q r) (det s t p)) (Ktimes (det t p q) (det s t r)));
 auto; try (unfold det, determinant3x3 in |- *; ring).
Qed.

(* The following two theorems show that if the triangle pqr is a non-degenerated, then any point
    is a linear combination of p,q,r.  This linear combination is expressed using determinants.
    When these determinants are all positive (if three are positive, the fourth is), pqr encompasses t
    and t is a positive linear combination of pqr.  This makes it possible to relate the notion of convex
    hull based on sequences of oriented edges and the notion based on positive linear combinations.
*)

Theorem lin_comb :
 forall t1 t2 p1 p2 q1 q2 r1 r2,
 Ktimes t1 (det (p1, p2) (q1, q2) (r1, r2)) =
 Kplus (Ktimes p1 (det (q1, q2) (r1, r2) (t1, t2)))
   (Kplus (Ktimes q1 (det (r1, r2) (p1, p2) (t1, t2)))
      (Ktimes r1 (det (p1, p2) (q1, q2) (t1, t2)))).
intros t1 t2 p1 p2 q1 q2 r1 r2; try assumption.
unfold det, determinant3x3 in |- *.
simpl in |- *.
ring.
Qed.

Theorem lin_comb2 :
 forall t1 t2 p1 p2 q1 q2 r1 r2,
 Ktimes t2 (det (p1, p2) (q1, q2) (r1, r2)) =
 Kplus (Ktimes p2 (det (q1, q2) (r1, r2) (t1, t2)))
   (Kplus (Ktimes q2 (det (r1, r2) (p1, p2) (t1, t2)))
      (Ktimes r2 (det (p1, p2) (q1, q2) (t1, t2)))).
intros t1 t2 p1 p2 q1 q2 r1 r2; unfold det, determinant3x3 in |- *;
 simpl in |- *.
ring.
Qed.

Lemma OT_distincts : forall p q r, OT p q r -> p <> q.
exact direct_distincts.
Qed.

End K_KA.

