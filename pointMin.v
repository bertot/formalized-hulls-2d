AddPath "$HOME/ctcoq/stage".
Require Export axiomsKnuth.
Require Import Ring.
 
Definition infLexico :=
   [p, q : Plan]  (Gt (abscisse q) (abscisse p)) \/
                  (abscisse q) = (abscisse p) /\ (Gt (ordonnee q) (ordonnee p)).
 
Theorem minLexico_pas_dans_triangle: (t, p, q, r : Plan) (infLexico t p) -> (infLexico t q) -> (infLexico t r) -> ~ (dansTriangle t p
                                                                                                                                  q r).
Intros.
Red; Intro.
Unfold dansTriangle sensDirect in H2; Elim H2; Clear H2; Intros.
Elim H3; Clear H3; Intros.
 a.
