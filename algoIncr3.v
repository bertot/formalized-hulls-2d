(* AddPath "$HOME/ctcoq/stage". *)
Require Export algoIncr2.
Require Export Arith.
Require Export List.
Require Export Classical.
 
Fixpoint in_list (a : Plan) (lCC : CC) {struct lCC} : bool :=
  match lCC with
  | nil => false
  | b :: l =>
      match egalBool a b with
      | true => true
      | false => in_list a l
      end
  end.
 
Definition in_cycle (a t : Plan) (lCC : CC) :=
  match egalBool a t with
  | true => true
  | false => in_list a lCC
  end.
 
Lemma in_cycle_PropNeg :
 forall (a t : Plan) (lCC : CC),
 in_cycle a t lCC = false -> a <> t /\ ~ in_CC a lCC.
simple induction lCC; intros.
split.
unfold in_cycle in H.
 CaseEq (egalBool a t).
rewrite H0 in H.
discriminate H.
rewrite H0 in H.
auto.
simpl in |- *; auto.
unfold in_cycle in H.
 CaseEq (egalBool a t).
unfold in_cycle in H0.
rewrite H1 in H0.
discriminate H0.
rewrite H1 in H.
split; auto.
unfold in_cycle in H0.
rewrite H1 in H0.
simpl in H0.
 CaseEq (egalBool a a0).
rewrite H2 in H0.
discriminate H0.
rewrite H2 in H0.
red in |- *; intros.
simpl in H3; elim H3; clear H3; intros.
absurd (a = a0); auto.
elim H; auto.
Qed.
 
Lemma in_cycle_Prop :
 forall (a t : Plan) (lCC : CC),
 in_cycle a t lCC = true -> a = t \/ in_CC a lCC.
intros.
unfold in_cycle in H.
 CaseEq (egalBool a t); rewrite H0 in H.
left; auto.
right; induction lCC as [| a0 lCC HreclCC].
simpl in H; discriminate H.
simpl in H; CaseEq (egalBool a a0); rewrite H1 in H.
simpl in |- *; left.
apply sym_eq; auto.
simpl in |- *; right; auto.
Qed.
 
Definition convex (p t : Plan) (lCC : CC) :=
  match in_cycle p t lCC with
  | true => (t, lCC)
  | false =>
      match lCC with
      | nil => (t, p :: nil)
      | a :: l =>
          match directBool t a p with
          | true =>
              (fun convRec : CouleurType * (Plan * CC) =>
               match convRec with
               | (c, (tRec, lRec)) =>
                   match c with
                   | Bleu => (tRec, lRec)
                   | Rouge => (tRec, a :: lRec)
                   end
               end) (convexRec Bleu p t lCC)
          | false =>
              (fun convRec : CouleurType * (Plan * CC) =>
               match convRec with
               | (c, (tRec, lRec)) =>
                   match c with
                   | Bleu =>
                       match egalBool tRec p with
                       | true => (tRec, lRec)
                       | false => (tRec, p :: lRec)
                       end
                   | Rouge =>
                       match egalBool tRec p with
                       | true =>
                           match lRec with
                           | nil => (p, nil)
                           | x :: r => (p, r)
                           end
                       | false => (tRec, lRec)
                       end
                   end
               end) (convexRec Rouge p t lCC)
          end
      end
  end.
 
Lemma pred_de_succ :
 forall (t : Plan) (cc : CC),
 point_extremal t cc -> t = pred_CH (succ_CH t cc) cc.
intros.
apply
 (unicite_CH_Prop_gauche t (pred_CH (succ_CH t cc) cc) (succ_CH t cc) cc).
apply succ_CH_Prop; auto.
apply pred_CH_Prop.
unfold point_extremal in |- *; left; exists t.
apply succ_CH_Prop; auto.
Qed.
Opaque convexRec.
 
Lemma CH_Prop_p_dans_cc :
 forall (p a b : Plan) (cc : CC),
 CH_Prop cc (a, b) -> in_CC p cc -> CH_Prop (p :: cc) (a, b).
intros.
unfold CH_Prop in H; elim H; simpl in |- *; clear H; intros.
elim H1; clear H1; intros.
unfold CH_Prop in |- *; simpl in |- *; split; auto.
split; auto.
apply points_extremaux_non_ind.
apply sym_not_eq; apply (points_extremaux_distincts a b cc); auto.
intros.
apply (points_extremaux_Prop a b a0 cc); auto.
elim H3; clear H3; intros; auto.
rewrite H3 in H0; auto.
Qed.
Hint Resolve CH_Prop_p_dans_cc.
 
Theorem convex_dans_CH :
 forall (p t : Plan) (lCC cc : CC),
 cycle_dans_CH t (succ_CH t cc :: lCC) cc ->
 cycle_dans_CH (@fst _ _ (convex p t (succ_CH t cc :: lCC)))
   (@snd _ _ (convex p t (succ_CH t cc :: lCC))) (p :: cc).
intros.
unfold convex in |- *; simpl in |- *;
 CaseEq (in_cycle p t (succ_CH t cc :: lCC)).
simpl in |- *.
cut (in_CC p cc).
intro.
unfold cycle_dans_CH in |- *; intros.
apply CH_Prop_p_dans_cc; auto.
elim (in_cycle_Prop p t (succ_CH t cc :: lCC)); intros; auto.
rewrite H1; apply point_extremal_dans_cc.
apply (cycle_point_extremal_t t lCC cc (succ_CH t cc)); auto.
apply point_extremal_dans_cc;
 apply (cycle_point_extremal t lCC cc (succ_CH t cc)); 
 auto.
 CaseEq (directBool t (succ_CH t cc) p).
cut (calculCouleur t (succ_CH t cc) p = Bleu).
intro.
 CaseEq (convexRec Bleu p t (succ_CH t cc :: lCC)).
 CaseEq p0.
rewrite H4 in H3.
 CaseEq c; simpl in |- *.
generalize (convexRec_dans_CH p t lCC cc (succ_CH t cc)); intros.
rewrite H2 in H6.
rewrite H3 in H6; simpl in H6.
unfold cycle_dans_CH in |- *; intros.
simpl in H7.
elim (ConvexRec_commence_par_rouge Bleu p t lCC (succ_CH t cc)).
rewrite H3; simpl in |- *.
intro.
elim H8; clear H8; intros.
rewrite H8 in H7.
elim H7; clear H7; intros.
elim H7; clear H7; intros.
rewrite <- H9; rewrite <- H7; clear H7 H9.
apply CH_incremental_inv_s_egal_p.
unfold point_extremal in |- *; left; exists t.
apply succ_CH_Prop.
apply (cycle_point_extremal_t t lCC cc (succ_CH t cc)); auto.
generalize (convexRec_couleur_precedent p t (succ_CH t cc) Bleu lCC cc);
 intros.
rewrite H3 in H7; simpl in H7.
rewrite H5 in H7; auto.
elim (pred_de_succ t cc).
auto.
apply (cycle_point_extremal_t t lCC cc (succ_CH t cc)); auto.
rewrite <- H8 in H7.
unfold cycle_dans_CH in H6; auto.
rewrite H3; simpl in |- *.
intros.
elim H8; clear H8; intros.
rewrite H8 in H7; rewrite H9 in H7.
elim H7; clear H7; intros.
generalize (convexRecBleu_garde_tete t p lCC (succ_CH t cc)); intro.
rewrite H3 in H11; simpl in H11.
rewrite H9 in H11; absurd (p = t); eauto.
rewrite H3; simpl in |- *; auto.
auto.
generalize (convexRec_dans_CH p t lCC cc (succ_CH t cc)); intros.
rewrite H2 in H6; rewrite H3 in H6; simpl in H6.
auto.
auto.
cut (calculCouleur t (succ_CH t cc) p = Rouge).
intro.
 CaseEq (convexRec Rouge p t (succ_CH t cc :: lCC)).
 CaseEq p0.
rewrite H4 in H3.
 CaseEq c; CaseEq (egalBool p1 p); simpl in |- *.
 CaseEq c0; simpl in |- *.
unfold cycle_dans_CH in |- *; intros.
simpl in H8; elim H8.
generalize (convexRec_dans_CH p t lCC cc (succ_CH t cc)); intros.
rewrite H2 in H8.
rewrite H3 in H8; simpl in H8.
rewrite H7 in H8.
cut (p = p1).
intro.
 CaseEq l.
unfold cycle_dans_CH in |- *; intros.
simpl in H11; elim H11.
rewrite <- H9 in H8; rewrite H10 in H8.
apply (petit_cycle_inclus_dans_grand p l0 (p :: cc) p3 p2); auto.
apply sym_eq; auto.
generalize (convexRec_dans_CH p t lCC cc (succ_CH t cc)); intros.
rewrite H2 in H7.
rewrite H3 in H7; simpl in H7.
auto.
generalize (convexRec_dans_CH p t lCC cc (succ_CH t cc)); intros.
rewrite H2 in H7.
rewrite H3 in H7; simpl in H7.
auto.
generalize (ConvexRec_commence_par_bleu Rouge p t lCC (succ_CH t cc)); intro.
rewrite H3 in H7; simpl in H7.
elim H7; (intros; auto).
unfold cycle_dans_CH in |- *; intros.
rewrite H8 in H9; elim H9; clear H9; intros.
elim H9; clear H9; intros.
rewrite <- H9; rewrite <- H10; clear H9 H10 a b.
apply CH_incremental_inv_t_egal_p; auto.
unfold point_extremal in |- *; left; exists t.
apply succ_CH_Prop.
apply (cycle_point_extremal_t t lCC cc (succ_CH t cc)); auto.
elim (pred_de_succ t cc).
auto.
apply (cycle_point_extremal_t t lCC cc (succ_CH t cc)); auto.
generalize (convexRec_couleur_precedent p t (succ_CH t cc) Rouge lCC cc);
 intro.
rewrite H3 in H9; simpl in H9.
rewrite H5 in H9; auto.
rewrite <- H8 in H9.
generalize (convexRec_dans_CH p t lCC cc (succ_CH t cc)); intros.
rewrite H2 in H10.
rewrite H3 in H10; simpl in H10.
unfold cycle_dans_CH in H10; auto.
auto.
Qed.