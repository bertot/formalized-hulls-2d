(* AddPath "$HOME/ctcoq/stage". *)
Require Export algoIncr4.
Require Export Arith.
Require Export List.
Require Export Classical.
Transparent convexRec.
 
Lemma depart_rouge_non_t_egal_p :
 forall (p t : Plan) (lCC cc : CC) (a : Plan),
 cycle_dans_CH t (a :: lCC) cc ->
 @fst _ _ (@snd _ _ (convexRec Rouge p t (a :: lCC))) <> p ->
 sensDirect (pred_CH (@fst _ _ (@snd _ _ (convexRec Rouge p t (a :: lCC)))) cc)
   (@fst _ _ (@snd _ _ (convexRec Rouge p t (a :: lCC)))) p.
intros p t lCC cc; induction lCC as [| a lCC HreclCC]; intros.
simpl in |- *; CaseEq (directBool a t p); simpl in |- *.
generalize (pred_CH_dans_cycle t nil cc a a t); intro.
rewrite <- H2; auto.
simpl in |- *; auto.
simpl in H0; rewrite H1 in H0; simpl in H0.
absurd (p = p); auto.
 CaseEq (convexRec Rouge p t (a :: lCC)).
 CaseEq p0.
rewrite H2 in H1.
generalize H1; simpl in |- *; intros Htemp; rewrite Htemp; clear Htemp.
 CaseEq c; CaseEq (directBool a0 a p); simpl in |- *; generalize (HreclCC a);
  intro; generalize H1; simpl in H5; intros Htemp; 
  simpl in Htemp; rewrite Htemp in H5; clear Htemp; 
  simpl in H5; generalize H1; simpl in H0; intros Htemp; 
  simpl in Htemp; rewrite Htemp in H0; clear Htemp; 
  rewrite H3 in H0; rewrite H4 in H0; simpl in H0; 
  eauto.
Qed.
 
Lemma non_tete_egal_p_rouge_depart :
 forall (p t : Plan) (lCC cc : CC) (a : Plan),
 cycle_dans_CH t (a :: lCC) cc ->
 p <> @fst _ _ (@snd _ _ (convexRec Rouge p t (a :: lCC))) ->
 sensDirect (pred_CH t cc) t p.
simple induction lCC; intros.
Transparent convexRec.
simpl in H0.
 CaseEq (directBool a t p).
rewrite H1 in H0; simpl in H0.
elim (pred_CH_dans_cycle t nil cc a a t); auto.
simpl in |- *; auto.
rewrite H1 in H0; simpl in H0.
absurd (p = p); auto.
 CaseEq (convexRec Rouge p t (a :: l)).
 CaseEq p0.
rewrite H3 in H2.
apply (H cc a).
eauto.
generalize H2; intros Htemp; simpl in Htemp; simpl in H1; rewrite Htemp in H1;
 clear Htemp.
rewrite H2; simpl in |- *.
 CaseEq c; CaseEq (directBool a0 a p); rewrite H4 in H1; rewrite H5 in H1;
  simpl in H1; auto.
Qed.
 
Lemma non_tete_egal_p :
 forall (p t : Plan) (c : CouleurType) (lCC : CC) (a : Plan),
 p <> @fst _ _ (@snd _ _ (convexRec c p t (a :: lCC))) ->
 t = @fst _ _ (@snd _ _ (convexRec c p t (a :: lCC))).
simple induction lCC; intros.
Transparent convexRec.
simpl in |- *.
 CaseEq c; CaseEq (directBool a t p); simpl in |- *; auto.
simpl in H; rewrite H0 in H; rewrite H1 in H; simpl in H.
absurd (p = p); auto.
 CaseEq (convexRec c p t (a :: l)).
 CaseEq p0.
rewrite H2 in H1.
generalize (H a); clear H; intro.
rewrite H1 in H; simpl in H.
generalize H1; simpl in |- *; intros Htemp; simpl in H0; rewrite Htemp;
 rewrite Htemp in H0; clear Htemp.
 CaseEq c0; CaseEq (directBool a0 a p); rewrite H3 in H0; rewrite H4 in H0;
  simpl in |- *; simpl in H0; auto.
Qed.
 
Theorem debut_convex_dans_CH :
 forall (p t : Plan) (lCC cc : CC),
 cycle_dans_CH t (succ_CH t cc :: lCC) cc ->
   (exists b : Plan,
      (exists l : CC,
       @snd _ _ (convex p t (succ_CH t cc :: lCC)) = b :: l /\
       CH_Prop (p :: cc) (@fst _ _ (convex p t (succ_CH t cc :: lCC)), b))).
intros.
Opaque convexRec.
unfold convex in |- *.
 CaseEq (in_cycle p t (succ_CH t cc :: lCC)).
simpl in |- *.
exists (succ_CH t cc); exists lCC; split; auto.
apply CH_Prop_p_dans_cc; auto.
apply succ_CH_Prop.
apply (cycle_point_extremal_t t lCC cc (succ_CH t cc)); auto.
elim (in_cycle_Prop p t (succ_CH t cc :: lCC)); auto.
intro.
rewrite H1; apply point_extremal_dans_cc.
apply (cycle_point_extremal_t t lCC cc (succ_CH t cc)); auto.
intro.
apply point_extremal_dans_cc;
 apply (cycle_point_extremal t lCC cc (succ_CH t cc)); 
 auto.
 CaseEq (directBool t (succ_CH t cc) p).
cut (calculCouleur t (succ_CH t cc) p = Bleu).
intros.
 CaseEq (convexRec Bleu p t (succ_CH t cc :: lCC)).
 CaseEq p0.
rewrite H4 in H3.
generalize (convexRecBleu_garde_tete t p lCC (succ_CH t cc)); intro.
rewrite H3 in H5; simpl in H5; rewrite H5 in H3.
 CaseEq c; simpl in |- *.
rewrite H5; exists (succ_CH t cc); exists c0; auto.
split; auto.
apply CH_incremental_inv_non_p; auto.
apply succ_CH_Prop.
apply (cycle_point_extremal_t t lCC cc (succ_CH t cc)); auto.
generalize (ConvexRec_commence_par_bleu Bleu p t lCC (succ_CH t cc)); intro.
rewrite H3 in H7; simpl in H7.
elim H7; clear H7; intros.
rewrite H7; exists (succ_CH t cc); exists x.
split; auto.
rewrite H5.
apply CH_incremental_inv_non_p; auto.
apply succ_CH_Prop.
apply (cycle_point_extremal_t t lCC cc (succ_CH t cc)); auto.
auto.
auto.
 CaseEq (convexRec Rouge p t (succ_CH t cc :: lCC)).
 CaseEq p0.
cut (calculCouleur t (succ_CH t cc) p = Rouge).
intro.
rewrite H3 in H2.
 CaseEq c; CaseEq (egalBool p1 p); simpl in |- *.
 CaseEq c0; simpl in |- *.
generalize (cycleRouge_non_vide p t lCC cc (succ_CH t cc)); intro.
rewrite H2 in H8; simpl in H8.
absurd (c0 = nil :>CC); auto.
apply H8; auto.
apply sym_not_eq; elim (in_cycle_PropNeg p t (succ_CH t cc :: lCC)); auto.
elim (in_cycle_PropNeg p t (succ_CH t cc :: lCC)); auto.
generalize (convexRec_dans_CH p t lCC cc (succ_CH t cc)); intro.
rewrite H4 in H8.
rewrite H2 in H8; simpl in H8.
rewrite H7 in H8.
generalize (ConvexRec_commence_par_rouge Rouge p t lCC (succ_CH t cc)); intro.
rewrite H2 in H9; simpl in H9.
elim H9; clear H9; intros.
elim H9; clear H9; intros.
rewrite H9 in H7; injection H7; intros.
rewrite <- H11 in H8.
 CaseEq l.
generalize (convexRec_dans_CH p t lCC cc (succ_CH t cc)); intro.
cut (p1 = p); auto; intro.
rewrite H4 in H13; rewrite H2 in H13; rewrite H14 in H13; rewrite H9 in H13;
 rewrite H10 in H13; rewrite H12 in H13; simpl in H13.
absurd (p = p); auto.
apply (points_extremaux_distincts p p (p :: cc)).
cut (CH_Prop (p :: cc) (p, p)).
unfold CH_Prop in |- *; intros.
simpl in H15; elim H15; clear H15; intros.
elim H16; auto.
unfold cycle_dans_CH in H13; unfold cycle_dans_CH in H; apply H13; auto.
simpl in |- *; auto.
exists p3; exists l0; split; auto.
rewrite H12 in H8.
cut (cycle_dans_CH p1 (p :: p3 :: l0) (p :: cc)).
clear H8; intros.
unfold cycle_dans_CH in H8.
apply H8.
simpl in |- *; auto.
auto.
elim H9; intros.
rewrite H10 in H7; discriminate H7.
auto.
generalize (ConvexRec_commence_par_rouge Rouge p t lCC (succ_CH t cc)); intro.
rewrite H2 in H7; simpl in H7.
elim H7; clear H7; intros.
elim H7; clear H7; intros.
exists p; exists x; split; auto.
generalize (non_tete_egal_p p t Rouge lCC (succ_CH t cc)); intro.
rewrite H2 in H8; simpl in H8.
elim H8; clear H8.
apply CH_incremental_inv_s_egal_p.
apply (cycle_point_extremal_t t lCC cc (succ_CH t cc)); auto.
auto.
apply (non_tete_egal_p_rouge_depart p t lCC cc (succ_CH t cc)); auto.
rewrite H2; simpl in |- *.
apply sym_not_eq; auto.
apply sym_not_eq; auto.
elim H7; clear H7; intros.
absurd (p1 = p); auto.
auto.
generalize (ConvexRec_commence_par_bleu Rouge p t lCC (succ_CH t cc)); intro.
rewrite H2 in H7; simpl in H7.
elim H7; clear H7; intros; auto.
exists (succ_CH t cc); exists x; split; auto.
cut (p1 = p); auto.
intro.
rewrite H8.
apply CH_incremental_inv_t_egal_p; auto.
unfold point_extremal in |- *; left; exists t; apply succ_CH_Prop.
apply (cycle_point_extremal_t t lCC cc (succ_CH t cc)); auto.
elim (pred_de_succ t cc).
auto.
apply (cycle_point_extremal_t t lCC cc (succ_CH t cc)); auto.
generalize (convexRec_couleur_precedent p t (succ_CH t cc) Rouge lCC cc);
 intros.
rewrite H2 in H9; simpl in H9.
rewrite H5 in H9; auto.
exists p; exists c0; split; auto.
generalize (non_tete_egal_p p t Rouge lCC (succ_CH t cc)); intro.
rewrite H2 in H7; simpl in H7.
elim H7; clear H7.
apply CH_incremental_inv_s_egal_p.
apply (cycle_point_extremal_t t lCC cc (succ_CH t cc)); auto.
auto.
apply (non_tete_egal_p_rouge_depart p t lCC cc (succ_CH t cc)); auto.
rewrite H2; simpl in |- *.
apply sym_not_eq; auto.
apply sym_not_eq; auto.
auto.
Qed.