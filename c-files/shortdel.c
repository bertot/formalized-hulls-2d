/* Computing a 2-dimensional Delaunay triangulation and the corresponding
   Voronoi diagram.

   The input should be made of lines with two integers between 0 and 300
   for instance produced by the following perl command:
   perl -e 'for ($k = 0; $k < 300; $k++) {$i = int(rand(300)); $j = int(rand(300)); print "$i $j\n"}'


   The output is a postcript file where the Delaunay triangulation is displayed
   with scale 2.

   The input should not contain aligned triplets of points and the program will
   fail when handling 340 points or more.
   With option -voronoi the Voronoi diagram is also produced, there is also
   an option -only-voronoi .
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define true 1
#define false 0
#define array_size 2000
#define scale 2

struct dart {
  int index, next0,  next1, prev1, x, y, center_x, center_y, center_computed;
};

struct dart map[array_size];

int bound = 0;

int outer_edge = 0;

int buffer;

int det3(int a, int b, int c, int d, int e, int f, int g, int h, int i) {
  int value = a * e * i + d * h * c + g * b * f -
          a * f * h - b * d * i - c * e * g;
  return value;
}

int next2(int i) {
  return (map[map[i].prev1].next0);
}

int prev2(int i) {
  return (map[map[i].next0].next1);
}

int inside_triangle(int x, int y, int i) {
  int j, k;

  j = next2(i);
  k = next2(j);

  return ((det3(map[i].x, map[i].y, 1,
		   map[j].x, map[j].y, 1,
		   map[k].x, map[k].y, 1) > 0) &&
          (det3(map[i].x, map[i].y, 1,
		   map[j].x, map[j].y, 1,
		   x, y, 1) > 0) &&
          (det3(map[i].x, map[i].y, 1,
		   x, y, 1,
		   map[k].x, map[k].y, 1) > 0) &&
          (det3(x, y, 1,
		   map[j].x, map[j].y, 1,
		   map[k].x, map[k].y, 1) > 0));
           
}

void display_map_ps(int with_voronoi) {
  int i, j, k, ijx, jkx, kix, cx, ijy, jky, kiy, cy;
  int a, b, c, d, e, f, center_x, center_y, multiplier;

  printf("%%!PS\n");
  printf("/mkp { newpath 1 0 360 arc stroke} def\n");

  for(i = 0; i < bound; i++) {
    j = map[i].next0;

    if(j > i) {
      if(with_voronoi < 2) {
	printf("newpath %d %d moveto %d %d lineto stroke\n",
	       scale*map[i].x, scale*map[i].y, scale*map[j].x, scale*map[j].y);
      } else {
	printf("%d %d mkp\n", scale*map[i].x, scale*map[i].y);
      }
    }
    j = next2(i);
    k = next2(j);

    if (with_voronoi > 0) {
      if (det3(map[i].x, map[i].y, 1,
               map[j].x, map[j].y, 1,
             map[k].x, map[k].y, 1) > 0) {
	if (map[j].center_computed == true) {
	  printf("%% not computing for %d because next2 is %d\n", i, j);
	  map[i].center_computed = true;
	  map[i].center_x = map[j].center_x;
	  map[i].center_y = map[j].center_y;
	} else if (map[k].center_computed == true) {
	  printf("%% not computing for %d because next2-next is %d\n", i, k);
	  map[i].center_computed = true;
	  map[i].center_x = map[k].center_x;
	  map[i].center_y = map[k].center_y;
	} else {
	  ijx = (map[i].x + map[j].x) / 2;
	  ijy = (map[i].y + map[j].y) / 2;
	  jkx = (map[j].x + map[k].x) / 2;
	  jky = (map[j].y + map[k].y) / 2;
	  kix = (map[k].x + map[i].x) / 2;
	  kiy = (map[k].y + map[i].y) / 2;
	  a = (map[j].x - map[i].x);
	  b = (map[j].y - map[i].y);
	  c = - (ijx * (map[j].x - map[i].x) +
		 ijy * (map[j].y - map[i].y));
	  d = (map[k].x - map[j].x);
	  e = (map[k].y - map[j].y);
	  f = - (jkx * (map[k].x - map[j].x) +
		 jky * (map[k].y - map[j].y));

	  center_x = (f * b - c * e) / (a * e - b * d);
	  center_y = (c * d - f * a) / (a * e - b * d);
	  map[i].center_computed = true;
	  map[i].center_x = center_x;
	  map[i].center_y = center_y;
	  printf("%% computing for %d\n", i);
	  printf("%d %d mkp\n", scale*center_x, scale*center_y);
	}
      }
    }
  }

  if (with_voronoi) {
    printf("1 0 0 setrgbcolor\n");
    for(i = 0; i < bound; i++) {
      j = map[i].next0;
      if (map[i].center_computed && map[j].center_computed) {
	if (i < j) {
	  printf("newpath %d %d moveto %d %d lineto stroke\n",
		 scale*map[i].center_x, scale*map[i].center_y,
		 scale*map[j].center_x, scale*map[j].center_y);
	}
      } else if (map[i].center_computed) {
	if(abs(map[i].y - map[j].y) + abs(map[j].x - map[i].x) < 100) {
	  multiplier = 20;
	} else {
	  multiplier = 1;
	}
	printf("newpath %d %d moveto %d %d lineto stroke\n",
	       scale*map[i].center_x, scale*map[i].center_y,
	       scale*(multiplier * (map[i].y - map[j].y) + map[i].center_x),
	       scale*(multiplier * (map[j].x - map[i].x) + map[i].center_y));
      }
    }
    printf("showpage\n");
  }
}

int illegal (int i) {
  int x1, y1, x2, y2, x3, y3, x4, y4, det;

  x1 = map[i].x;
  y1 = map[i].y;
  x2 = map[map[i].next0].x;
  y2 = map[map[i].next0].y;
  x3 = map[next2(map[i].next0)].x;
  y3 = map[next2(map[i].next0)].y;
  x4 = map[next2(i)].x;
  y4 = map[next2(i)].y;

  det = det3(x1, y1, x1 * x1 + y1 * y1,
	     x2, y2, x2 * x2 + y2 * y2,
	     x3, y3, x3 * x3 + y3 * y3) +
    det3(x1, y1, x1 * x1 + y1 * y1,
	 x3, y3, x3 * x3 + y3 * y3,
	 x4, y4, x4 * x4 + y4 * y4) -
    det3(x1, y1, x1 * x1 + y1 * y1,
	 x2, y2, x2 * x2 + y2 * y2,
	 x4, y4, x4 * x4 + y4 * y4) -
    det3(x2, y2, x2 * x2 + y2 * y2,
	 x3, y3, x3 * x3 + y3 * y3,
	 x4, y4, x4 * x4 + y4 * y4);

  if(det > 0) {
    return true;
  } else {
    return false;
  }
}

void unhook(int i) {
  int j, k;

  j = map[i].next1;
  k = map[i].prev1;

  map[j].prev1 = k;
  map[k].next1 = j;

  map[i].next1 = i;
  map[i].prev1 = i;
}

void attach(int i, int j) {
  int k;

  k = map[i].prev1;

  map[j].next1 = i;
  map[i].prev1 = j;
  map[k].next1 = j;
  map[j].prev1 = k;

  map[j].x = map[i].x;
  map[j].y = map[i].y;
}

int find_illegal() {
  int i, j;

  for(i = 0; i < bound; i++) {
    j = map[i].next0;

/* TODO: the test here is probably meant to detect edges that are between
   two triangles, but it won't work if the convex hull is itself a triangle,
   an orientation test should also be included. */
    if((next2(next2(next2(i))) == i) &&
       (next2(next2(next2(j))) == j)){
      if(illegal(i)) {
	buffer = i;
	return true;
      }
    }
  }
  return false;
}

void flip(int i) {
  int j, k, l;

  j = map[i].next0;
  k = next2(i);
  l = next2(j);

  unhook(i);
  unhook(j);
  attach(k, i);
  attach(l, j);
}

void mkdelaunay() {
  while (find_illegal()) {
    flip(buffer);
  }
}

int add_dart(int x, int y) {
  int i;

  if(bound < array_size) {
    i = bound;
    map[i].index = i;
    map[i].next0 = i;
    map[i].next1 = i;
    map[i].prev1 = i;
    map[i].x = x;
    map[i].y = y;
    map[i].center_computed = false;
    bound++;
    return i;
  } else {
    fprintf(stderr,"array capacity exceeded\n");
    exit (-1);
  }
}

void add_point_triangle(int x, int y, int j) {
  int k, l, a, b, c, d, e, f;

  a = add_dart(x, y);
  b = add_dart(x, y);
  c = add_dart(x, y);
  k = next2(j);
  l = next2(k);
  d = add_dart(map[j].x, map[j].y);
  attach(j, d);
  e = add_dart(map[k].x, map[k].y);
  attach(k, e);
  f = add_dart(map[l].x, map[l].y);
  attach(l, f);
  map[b].prev1 = a;
  map[c].prev1 = b;
  map[a].prev1 = c;
  map[b].next1 = c;
  map[c].next1 = a;
  map[a].next1 = b;
  map[a].next0 = d;
  map[b].next0 = e;
  map[c].next0 = f;
  map[d].next0 = a;
  map[e].next0 = b;
  map[f].next0 = c;
  
}

int add_point_out (int x, int y) {

  int j, k, a0, a, b, det, found, stop;

  j = outer_edge;
  k = next2(j);
  found = false;

  while((k != outer_edge) && (found != true)) {
    k = next2(j);
    det = det3(map[j].x, map[j].y, 1, map[k].x, map[k].y, 1, x, y, 1);
    if(det > 0) {
      found = true;
    } else {
      j = k;
    }
  }
  if(true == found) {
    stop = false;
    while(stop != true) {
      k = prev2(j);
      det = det3(map[k].x, map[k].y, 1, map[j].x, map[j].y, 1, x, y, 1);
      if(det <= 0) {
	stop = true;
      } else {
	j = k;
      }
    }
    stop = false;
    a0 = add_dart(x, y);
    outer_edge = a0;
    b = add_dart(map[j].x, map[j].y);
    map[a0].next0 = b;
    map[b].next0 = a0;
    while(stop != true) {
      k = next2(j);
      attach(j, b);
      det = det3(map[j].x, map[j].y, 1, map[k].x, map[k].y, 1, x, y, 1);
      if(det > 0) {
	a = add_dart(x, y);
	attach(a0, a);
	b = add_dart(map[k].x, map[k].y);
	map[a].next0 = b;
	map[b].next0 = a;
	j = k;
      } else if(det == false) {
	fprintf(stderr,"%% new point is aligned with existing (%d, %d)\n", x, y);
	exit (-1);
      }else {
	stop = true;
      }
    }
  }
  return found;
}

void add_point(int x, int y) {
  int found, i, j, k, det;

  if(0 == bound) {
    add_dart(x, y);
  } else if(1 == bound) {
    add_dart(x, y);
    map[0].next0 = 1;
    map[1].next0 = 0;
  } else {
    found = add_point_out(x, y);
    if(true != found) {
      i = map[outer_edge].next0;

      found = false;
      while(true != found) {
	j = next2(i);
	k = next2(j);
	det = det3(map[i].x, map[i].y, 1,
		   map[j].x, map[j].y, 1,
		   x, y, 1);
	if(det > 0) {
	  det = det3(map[j].x, map[j].y, 1,
		     map[k].x, map[k].y, 1,
		     x, y, 1);
	  if(det > 0) {
	    det = det3(map[k].x, map[k].y, 1,
		       map[i].x, map[i].y, 1,
		       x, y, 1);
	    if(det > 0) {
	      found = true;
	    } else if (0 == det) { 
	      fprintf(stderr,"%% new point is aligned with existing ones (%d, %d)\n",
		 x, y);
	      exit(-1);
	    } else {
	      i = map[i].next0;
	    }
	  } else if (0 == det) {
	    fprintf(stderr,"%% new point is aligned with existing ones (%d, %d)\n",
		 x, y);
	      exit(-1);
	  } else {
	    i = map[k].next0;
	  }
	} else if (0 == det) {
	  fprintf(stderr,"%% new point is aligned with existing ones (%d, %d)\n",
		 x, y);
	  exit(-1);
	} else {
	  i = map[j].next0;
	}
      }
      add_point_triangle(x, y, i);
    }
    mkdelaunay();
  }
}

void read_points () {
  int escape = false;
  int x, y, success;

  while(true != escape) {
    success = scanf("%d %d", &x, &y);
    if(2 == success) {
      add_point(x, y);
    } else {
      escape = true;
    }
  }
}

int main(int argc, char *argv[]) {
  int with_voronoi = 0;
  if (argc == 2 && strcmp(argv[1], "-voronoi") == 0) {
    fprintf(stderr, "adding Voronoi diagram\n");
    with_voronoi = 1;
  } else if (argc == 2 && strcmp(argv[1], "-only-voronoi") == 0) {
    fprintf(stderr, "no triangles, only Voronoi diagram\n");
    with_voronoi = 2;
  }
  read_points();
  display_map_ps(with_voronoi);
  return 0;
}
