Require Export List.
Require Export Arith.
Require Export list_complements.

Section gen.
Variable plane : Type.
Variable eq_plane_dec : forall p q : plane, {p = q} + {p <> q}.
Variable oriented : plane -> plane -> plane -> Prop.
(*Knuth's axioms are given by the following variables.  But axiom 4 is not used
  In Jarvis' algorithm and axiom 3 is a property of the data, not of the
  plane. *)
Variables (Ax1 : forall p q r : plane, oriented p q r -> oriented q r p)
  (Ax2 : forall p q r : plane, oriented p q r -> ~ oriented p r q)
  (Ax5 :
     forall p q r s t : plane,
     oriented t s p ->
     oriented t s q ->
     oriented t s r -> oriented t p q -> oriented t q r -> oriented t p r).
 
Theorem oriented_distinct : forall p q r : plane, oriented p q r -> q <> r.
intros p q r H'. red. intros H'0.
rewrite H'0 in H'.
assert (H'2:=H').
apply Ax2 in H'2.
contradiction.
Qed.
 
Theorem oriented_distinct1 : forall p q r : plane, oriented q r p -> q <> r.
intros; apply oriented_distinct with p; auto.
Qed.
 
Theorem oriented_distinct2 : forall p q r : plane, oriented r p q -> q <> r.
intros; apply oriented_distinct with p; auto.
Qed.
 
Ltac neq_tac :=
  (eapply oriented_distinct; eauto 1; fail) ||
    (apply sym_not_equal; eapply oriented_distinct; eauto 1; fail) ||
      (eapply oriented_distinct1; eauto 1; fail) ||
        (apply sym_not_equal; eapply oriented_distinct1; eauto 1; fail) ||
          (eapply oriented_distinct2; eauto 1; fail) ||
            (apply sym_not_equal; eapply oriented_distinct2; eauto 1; fail).
 
Theorem implication_for_Ax5bis :
 forall l : list plane,
 (forall x y z : plane,
  In x l ->
  In y l ->
  In z l -> x <> y -> x <> z -> y <> z -> oriented x y z \/ oriented x z y) ->
 forall t s p q r : plane,
 In t l ->
 In s l ->
 In p l ->
 In q l ->
 In r l ->
 oriented s t p ->
 oriented s t q ->
 oriented s t r ->
 oriented t p q ->
 oriented t q r -> oriented t r p -> oriented s p q -> oriented s r p.
intros l Ax3 t s p q r Hin1 Hin2 Hin3 Hin4 Hin5 H' H'0 H'1 H'2 H'3 H'4 H'5.
elim (Ax3 p q r); try neq_tac; auto.
intros H'6.
apply Ax1.
apply Ax5 with (q := t) (s := q); auto.
intros H'6.
elim (Ax3 s r p); try neq_tac; auto.
intros H'7.
elim (Ax3 q s r); try neq_tac; auto.
intros H'8.
elim (Ax2 _ _ _ H'2).
apply Ax1.
apply Ax1.
apply Ax5 with (q := r) (s := s); auto.
intros H'8.
elim (Ax2 _ _ _ H'3).
apply Ax1.
apply Ax1.
apply Ax5 with (q := p) (s := s); auto.
Qed.
 
Theorem Ax5bis :
 forall l : list plane,
 (forall x y z : plane,
  In x l ->
  In y l ->
  In z l -> x <> y -> x <> z -> y <> z -> oriented x y z \/ oriented x z y) ->
 forall t s p q r : plane,
 In t l ->
 In s l ->
 In p l ->
 In q l ->
 In r l ->
 oriented s t p ->
 oriented s t q ->
 oriented s t r -> oriented t p q -> oriented t q r -> oriented t p r.
intros l Ax3 t s p q r Hin1 Hin2 Hin3 Hin4 Hin5 H' H'0 H'1 H'2 H'3.
elim (eq_plane_dec p r).
intros Heq; elim (Ax2 _ _ _ H'2).
rewrite Heq; auto.
intros Hneq.
elim (Ax3 t p r); try neq_tac; auto.
intros H'4.
cut (oriented s p q -> oriented s r p).
intros H'5.
cut (oriented s r p -> oriented s q r).
intros H'6.
cut (oriented s q r -> oriented s p q).
intros H'7.
elim (Ax3 s p q); try neq_tac; auto.
intros H'8.
elim (Ax2 _ _ _ (H'5 H'8)).
apply Ax5 with (s := t) (q := q); auto.
intros H'8.
elim (Ax2 s q r).
apply Ax5 with (s := t) (q := p); auto.
elim (Ax3 s p r); try neq_tac; auto.
intros H'9.
elim (Ax2 _ _ _ H'8); auto.
elim (Ax3 s r q); try neq_tac; auto.
intros H'9.
elim (Ax2 _ _ _ H'8); auto.
intros H'7.
apply (implication_for_Ax5bis l Ax3) with (t := t) (q := r); auto.
intros H'6.
apply (implication_for_Ax5bis l Ax3) with (t := t) (q := p); auto.
intros H'5.
apply (implication_for_Ax5bis l Ax3) with (t := t) (q := q); auto.
Qed.
 
Inductive all_left (t1 t2 : plane) : list plane -> Prop :=
  | all_left_end : all_left t1 t2 nil
  | all_left_rec :
      forall (a : plane) (l : list plane),
      t1 = a \/ t2 = a \/ oriented t1 t2 a ->
      all_left t1 t2 l -> all_left t1 t2 (a :: l).
Hint Resolve all_left_end all_left_rec.
 
Theorem oriented_all_left :
 forall (l : list plane) (t1 t2 t3 t4 : plane),
 all_left t3 t1 l ->
 oriented t3 t1 t2 ->
 all_left t1 t2 l ->
 oriented t1 t2 t4 -> oriented t1 t3 t4 -> all_left t4 t1 l.
intros l; elim l; auto.
intros a l0 H' t1 t2 t3 t4 H'0 H'1 H'2 H'3 H'4.
inversion H'2.
inversion H'0.
apply all_left_rec.
elim H1.
intros H'5; right; left; try assumption.
intros H'5; elim H'5; intros H'6.
rewrite <- H'6; auto.
elim H5;
 [ intros H'7; clear H5
 | intros H'7; elim H'7;
    [ intros H'8; auto; clear H'7 H5 | intros H'8; clear H'7 H5 ] ].
rewrite <- H'7.
right; right; auto.
right; right; try assumption.
do 2 apply Ax1.
apply Ax5 with (q := t3) (s := t2); auto.
apply H' with (t3 := t3) (t2 := t2); auto.
Qed.
 
Inductive encompass_aux (l : list plane) : list plane -> Prop :=
  | enc_aux_end : forall t1 : plane, encompass_aux l (t1 :: nil)
  | enc_aux_rec :
      forall (t1 t2 : plane) (l' : list plane),
      all_left t1 t2 l ->
      encompass_aux l (t2 :: l') -> encompass_aux l (t1 :: t2 :: l').
 
Inductive encompass : list plane -> list plane -> Prop :=
    encompass_def :
      forall (t1 : plane) (l s : list plane),
      last plane t1 l -> encompass_aux s (t1 :: l) -> encompass l s.
 
Definition convexHullSpec (l1 l2 : list plane) :=
  included plane l2 l1 /\ encompass l2 l1.
Variable
  extremal_points :
    forall (a b : plane) (l : list plane),
    (forall x y z: plane, In x l -> In y l -> In z l -> x <> y -> x <> z ->
     y <> z -> oriented x y z \/ oriented x z y) ->
    a <> b ->
    In a l ->
    In b l ->
    {p : plane * plane |
    all_left (fst p) (snd p) l /\
    fst p <> snd p /\ In (fst p) l /\ In (snd p) l}.
Variable
  oriented_dec : forall p q r : plane, {oriented p q r} + {~ oriented p q r}.
 
Fixpoint max_oriented (t v : plane) (l : list plane) {struct l} : plane :=
  match l with
  | nil => v
  | a :: l' =>
      match oriented_dec t v a with
      | left h => max_oriented t a l'
      | right h => max_oriented t v l'
      end
  end.
 
Theorem all_left_max_oriented :
 forall (l : list plane) (p q r : plane),
 all_left p q l -> oriented p q r -> oriented p q (max_oriented p r l).
intros l; elim l.
simpl in |- *; auto.
intros a l0 H' p q r H'0; inversion H'0.
simpl in |- *.
case (oriented_dec p r a).
intros H'1 H'2.
elim H1; intros H'3.
elim (oriented_distinct2 _ _ _ H'1); auto.
elim H'3; intros H'4.
rewrite H'4 in H'2; elim (Ax2 _ _ _ H'1); auto.
apply H'; auto.
intros H'1 H'2; apply H'; auto.
Qed.
 
Theorem max_oriented_left :
 forall (l : list plane) (p q a : plane),
 oriented p q a ->
 all_left p q l ->
 a = max_oriented p a l \/ oriented p a (max_oriented p a l).
intros l; elim l.
simpl in |- *.
intros p q a H' H'0; auto.
intros a l0 H' p q a0 H'0 H'1; inversion H'1.
simpl in |- *.
case (oriented_dec p a0 a).
elim H1;
 [ intros H'2; clear H1
 | intros H'2; elim H'2;
    [ intros H'3; clear H'2 H1 | intros H'3; try exact H'3; clear H'2 H1 ] ].
rewrite H'2.
intros H'3; elim (oriented_distinct2 _ _ _ H'3); auto.
rewrite <- H'3.
intros H'2; elim (Ax2 _ _ _ H'2); auto.
intros H'2; try assumption.
lapply (H' p q a);
 [ intros H'7; lapply H'7;
    [ intros H'8; try exact H'8; clear H'7 | clear H'7 ]
 | idtac ]; auto.
right; try assumption.
elim H'8; [ intros H'4; rewrite <- H'4; clear H'8 | intros H'4; clear H'8 ].
try assumption.
apply Ax5 with (q := a) (s := q); auto.
apply all_left_max_oriented; auto.
intros H'2; try assumption.
apply H' with (q := q); auto.
Qed.
 
Theorem max_oriented_correct :
 forall L : list plane,
 (forall x y z : plane,
  In x L ->
  In y L ->
  In z L -> x <> y -> x <> z -> y <> z -> oriented x y z \/ oriented x z y) ->
 forall l : list plane,
 included plane l L ->
 forall p q r : plane,
 In p L ->
 In q L ->
 In r L ->
 all_left q r l ->
 p <> q -> oriented q r p -> all_left (max_oriented q p l) q l.
intros L Ax3 l; elim l; auto.
intros a l0 H' Hincluded p q r Hinp Hinq Hinr H'0; inversion H'0.
simpl in |- *.
case (oriented_dec q p a).
intros H'1 H'2 Horient.
case (eq_plane_dec a q); intros H'3.
rewrite H'3 in H'1; elim (oriented_distinct2 _ _ _ H'1); auto.
inversion Hincluded.
generalize (H' H7 _ _ _ H5 Hinq Hinr H2 H'3); intros H'4.
elim H1;
 [ intros H'5; clear H1
 | intros H'5; elim H'5;
    [ intros H'6; clear H'5 H1 | intros H'6; try exact H'6; clear H'5 H1 ] ].
elim H'3; auto.
rewrite H'6 in Horient; elim (Ax2 _ _ _ Horient); auto.
apply all_left_rec; auto.
elim (max_oriented_left _ _ _ _ H'6 H2); auto.
intros H'1 H'2 H'3.
lapply H'.
intros H''.
lapply (H'' p q r);
 [ intros H'7; lapply H'7;
    [ intros H'8; lapply H'8;
       [ intros H'9; try exact H'9; clear H'8 H'7 | clear H'8 H'7 ]
    | clear H'7 ]
 | idtac ]; auto.
apply all_left_rec; auto.
case (eq_plane_dec q a); intros Hneq1; auto.
elim H1; [ intros H'4; auto; clear H1 | intros H'4; clear H1 ].
elim H'4; [ intros H'5; clear H'4 | intros H'5; try exact H'5; clear H'4 ].
elim (max_oriented_left l0 q r p);
 [ intros H'12 | intros H'12; try exact H'12 | idtac | idtac ]; 
 auto.
rewrite <- H'5.
rewrite <- H'12.
right; right; auto.
right; right; rewrite <- H'5.
apply Ax1.
apply Ax1.
apply all_left_max_oriented; auto.
elim (max_oriented_left l0 q r p);
 [ intros H'12 | intros H'12; try exact H'12 | idtac | idtac ]; 
 auto.
case (eq_plane_dec p a).
intros H'4; rewrite <- H'4; auto.
intros H'4; try assumption.
elim (Ax3 q p a); auto.
intros H'7; try assumption.
apply H'1 || case H'1; auto.
rewrite <- H'12.
auto.
inversion Hincluded; auto.
case (eq_plane_dec p a).
intros H'4; right; right.
rewrite <- H'4; auto.
intros H'4; right; right.
do 2 apply Ax1.
apply Ax5 with (q := p) (s := r); auto.
apply all_left_max_oriented; auto.
case (Ax3 q p a); auto.
inversion Hincluded; auto.
intros H'6; case H'1; auto.
inversion Hincluded; auto.
Qed.
 
Theorem all_left_point :
 forall (l : list plane) (p q r : plane),
 all_left p q l -> In r l -> p = r \/ q = r \/ oriented p q r.
intros l; elim l.
intros p q r H' H'0; elim H'0.
simpl in |- *; intros a l0 H' p q r H'0 H'1; elim H'1; intros H'2;
 inversion H'0; auto.
rewrite <- H'2; auto.
Qed.
 
Theorem unique_successor :
 forall (l : list plane) (p q r : plane),
 all_left p q l -> all_left p r l -> In r l -> In q l -> q = r.
intros l p q r H' H'0 H'1 H'2.
elim (all_left_point l p q r); auto.
elim (all_left_point l p r q); auto.
intros H'3; rewrite H'3; auto.
intros H'3; elim H'3; intros H'4; clear H'3; auto.
intros H'3; rewrite H'3 in H'4; elim (oriented_distinct1 _ _ _ H'4); auto.
intros H'3; elim H'3; intros H'4; clear H'3; auto.
elim (all_left_point l p r q); auto.
intros H'3; rewrite H'3 in H'4; elim (oriented_distinct1 _ _ _ H'4); auto.
intros H'3; elim H'3; intros; auto.
elim (Ax2 _ _ _ H'4); auto.
Qed.
 
Theorem unique_predecessor :
 forall (l : list plane) (p q r : plane),
 all_left p q l -> all_left r q l -> In p l -> In r l -> p = r.
intros l p q r H' H'0 H'1 H'2.
elim (all_left_point l p q r); auto; elim (all_left_point l r q p); auto.
intros H'3; elim H'3;
 [ intros H'4; rewrite H'4; clear H'3 | intros H'4; clear H'3 ].
intros H'3; elim H'3; [ intros H'5; auto; clear H'3 | intros H'5; clear H'3 ].
elim (oriented_distinct1 _ _ _ H'5); auto.
intros H'3; elim H'3;
 [ intros H'5; try exact H'5; clear H'3 | intros H'5; clear H'3 ].
rewrite H'5 in H'4; elim (oriented_distinct1 _ _ _ H'4); auto.
elim (Ax2 _ _ _ H'4); auto.
Qed.
 
Theorem all_left_id :
 forall (t : plane) (l : list plane),
 all_left t t l -> forall t' : plane, In t' l -> t' = t.
intros t l H'; elim H'.
intros t' H'0; simpl in H'0; elim H'0.
intros a l0 H'0 H'1 H'2 t' H'3; simpl in H'3.
elim H'3.
intros Heq.
elim H'0; [ intros H'4; rewrite H'4; clear H'0 | intros H'4; clear H'0 ];
 auto.
elim H'4; [ intros H'0; rewrite H'0; clear H'4 | intros H'0; clear H'4 ];
 auto.
elim (oriented_distinct1 a t t H'0); auto.
auto.
Qed.
 
Theorem encompass_aux_point :
 forall l1 l2 : list plane,
 encompass_aux l2 l1 ->
 forall t : plane, In t l2 -> encompass_aux (t :: nil) l1.
intros l1; elim l1.
intros l2 H'; inversion H'.
intros a l H' l2 H'0 t H'1; try assumption.
inversion H'0.
apply enc_aux_end.
apply enc_aux_rec.
apply all_left_rec.
elim (all_left_point l2 a t2 t); auto.
apply all_left_end.
rewrite H0.
apply H' with (l2 := l2); auto.
rewrite <- H0.
try exact H2.
Qed.
 
Theorem convexHull_to_encompass_point :
 forall l1 l2 : list plane,
 convexHullSpec l1 l2 -> forall t : plane, In t l1 -> encompass l2 (t :: nil).
intros l1 l2 H'; elim H'.
clear H'.
intros H' H'0; elim H'0; clear H' H'0 l2 l1.
intros t1 l s H' H'0 t H'1; try assumption.
apply encompass_def with (t1 := t1); auto.
apply encompass_aux_point with (l2 := s); auto.
Qed.
 
Theorem encompassing_triangle_from_Hull :
 forall L : list plane,
 (forall x y z : plane,
  In x L ->
  In y L ->
  In z L -> x <> y -> x <> z -> y <> z -> oriented x y z \/ oriented x z y) ->
 forall l : list plane,
 included plane l L ->
 forall t : plane,
 In t L ->
 encompass l (t :: nil) ->
 In t l \/
 (exists p : plane,
    (exists q : plane,
       (exists r : plane,
          oriented p q t /\
          oriented q r t /\ oriented r p t /\ In p l /\ In q l /\ In r l))).
intros L Ax3 l; elim l.
intros Hincluded t Hin H'; inversion H'.
inversion H.
intros a l0; case l0.
intros H' Hincluded t Hin H'0; inversion H'0.
generalize H0; inversion H.
intros H'1; inversion H'1.
generalize (all_left_id _ _ H6).
intros H'2; rewrite <- (H'2 t).
left; simpl in |- *.
left; auto.
simpl in |- *.
left; auto.
inversion H4.
intros b l1; case l1.
intros H' Hincluded t Hin H'0; inversion H'0.
inversion H.
inversion H4.
2: inversion H7.
inversion H0.
rewrite H7 in H9.
inversion H9.
elim H14; [ intros H'1; rewrite <- H'1; clear H14 | intros H'1; clear H14 ].
left; auto.
simpl in |- *; auto.
elim H'1; [ intros H'2; rewrite <- H'2; clear H'1 | intros H'2; clear H'1 ].
left; simpl in |- *.
left; auto.
inversion H11.
inversion H17.
elim H22; [ intros H'1; rewrite <- H'1; clear H22 | intros H'1; clear H22 ].
left; simpl in |- *.
left; auto.
elim H'1; [ intros H'3; rewrite <- H'3; clear H'1 | intros H'3; clear H'1 ].
left; simpl in |- *.
right; left; auto.
elim (Ax2 _ _ _ H'3); auto.
intros c l2.
intros H' Hincluded t Hin H'0; inversion H'0.
inversion H0.
case (eq_plane_dec t t1).
intros H'1; rewrite H'1.
left.
apply last_in; auto.
intros H'1; inversion H7.
case (eq_plane_dec t1 b).
intros H'2; rewrite H'2 in H5; inversion H5.
elim H15; [ intros H'3; rewrite <- H'3; clear H15 | intros H'3; clear H15 ].
left; simpl in |- *.
right; left; auto.
elim H'3; [ intros H'4; rewrite <- H'4; clear H'3 | intros H'4; clear H'3 ].
left; simpl in |- *.
left; auto.
inversion H10.
elim H18; [ intros H'3; rewrite <- H'3; clear H18 | intros H'3; clear H18 ].
left; simpl in |- *.
left; auto.
elim H'3; [ intros H'5; rewrite <- H'5; clear H'3 | intros H'5; clear H'3 ].
left; simpl in |- *.
right; left; auto.
elim (Ax2 _ _ _ H'5); auto.
intros H'2; try assumption.
elim (eq_plane_dec t b).
intros H'3; rewrite H'3.
left; simpl in |- *.
right; left; auto.
intros H'3; try assumption.
elim (Ax3 t1 b t); auto.
intros H'4; try assumption.
lapply H'; [ intros H'' | idtac ].
lapply (H'' t); [ intros H'6; lapply H'6; [ intros H''6 | idtac ] | idtac ].
elim H''6; [ intros H'5; try exact H'5; clear H'6 | intros H'5; clear H'6 ].
left; right; assumption.
right.
elim H'5; intros p E; elim E; intros q E0; elim E0; intros r E1; elim E1;
 intros H'6 H'7; elim H'7; intros H'8 H'9; elim H'9; 
 intros H'10 H'11; elim H'11; intros H'12 H'13; elim H'13; 
 intros H'14 H'15; try exact H'15; clear H'13 H'11 H'9 H'7 E1 E0 E H'5.
exists p; exists q; exists r; simpl in |- *; auto 10.
apply encompass_def with (t1 := t1).
inversion H; auto.
apply enc_aux_rec; auto.
auto.
inversion Hincluded; auto.
intros H'4; inversion H5.
elim H15; [ intros H'5; rewrite <- H'5; clear H15 | intros H'5; clear H15 ].
left; apply last_in; auto.
elim H'5; [ intros H'6; rewrite <- H'6; clear H'5 | intros H'6; clear H'5 ].
left; simpl in |- *; auto.
inversion H10.
elim H18; [ intros H'5; rewrite <- H'5; clear H18 | intros H'5; clear H18 ].
left; simpl in |- *; auto.
elim H'5; [ intros H'7; rewrite <- H'7; clear H'5 | intros H'7; clear H'5 ].
left; simpl in |- *; auto.
right; exists a; exists b; exists t1; split; [ try assumption | split ]; auto.
repeat split; [ auto | simpl in |- *; auto | simpl in |- *; auto | idtac ].
apply last_in; auto.
apply included_in with (l1 := a :: b :: c :: l2); auto.
apply last_in; auto.
apply included_in with (l1 := a :: b :: c :: l2); auto.
simpl in |- *; auto.
Qed.
 
Theorem left_max_oriented :
 forall L : list plane,
 (forall x y z : plane,
  In x L ->
  In y L ->
  In z L -> x <> y -> x <> z -> y <> z -> oriented x y z \/ oriented x z y) ->
 forall l : list plane,
 included plane l L ->
 forall p q r t : plane,
 In p L ->
 In q L ->
 In t L ->
 all_left p q l ->
 oriented p q t ->
 oriented p t r ->
 oriented p q r -> In r l -> max_oriented p t l = max_oriented p r l.
intros L Ax3 l; elim l.
intros Hincluded p q r t Hinp Hinq Hint H' H'0 H'1 H'2 H'3; elim H'3.
intros a l0 H' Hincluded p q r t Hinp Hinq Hint H'0 H'1 H'2 H'3 H'4.
cut (p <> q).
intros Hneq; cut (p <> r).
intros Hneq1; cut (q <> r).
intros Hneq2; cut (p <> t).
intros Hneq3; cut (q <> t).
intros Hneq4.
elim H'4.
intros H'5; rewrite H'5; simpl in |- *.
case (oriented_dec p t r).
case (oriented_dec p r r); auto.
case (oriented_dec p r r); auto.
intros H'6 H'7; apply H'7 || case H'7; auto.
intros H'6 H'7; apply H'7 || case H'7; auto.
intros H.
simpl in |- *.
case (oriented_dec p t a).
case (oriented_dec p r a).
auto.
intros H'5 H'6; try assumption.
cut (a <> p).
intros H'7; try assumption.
case (eq_plane_dec a r).
intros H'8; rewrite H'8; auto.
intros H'8; try assumption.
case (Ax3 p r a); auto.
apply included_in with (a :: l0); auto.
apply included_in with (a :: l0); simpl in |- *; auto.
intros H'10; try assumption.
apply H'5 || case H'5; auto.
clear H'5.
intros H'10; try assumption.
inversion H'0.
elim H2;
 [ intros H'5; clear H2
 | intros H'5; elim H'5;
    [ intros H'11; clear H'5 H2 | intros H'11; try exact H'11; clear H'5 H2 ] ].
apply H'7 || case H'7; auto.
rewrite <- H'11 in H'6; elim (Ax2 _ _ _ H'6); auto.
apply H' with (q := q); auto.
inversion Hincluded; auto.
apply included_in with (a :: l0); simpl in |- *; auto.
red in |- *; intros H'7; auto.
rewrite H'7 in H'6; elim (oriented_distinct2 _ _ _ H'6); auto.
intros H'5; try assumption.
case (oriented_dec p r a).
intros H'6; try assumption.
inversion H'0.
elim H2;
 [ intros H'7; clear H2
 | intros H'7; elim H'7;
    [ intros H'8; clear H'7 H2 | intros H'8; try exact H'8; clear H'7 H2 ] ].
rewrite H'7 in H'6; elim (oriented_distinct2 _ _ _ H'6); auto.
rewrite <- H'8 in H'6; elim (Ax2 _ _ _ H'6); auto.
apply H'5 || case H'5; auto.
apply Ax5 with (q := r) (s := q); auto.
intros H'6; try assumption.
apply H' with (q := q); auto.
inversion Hincluded; auto.
inversion H'0; auto.
red in |- *; intros H'5; auto.
rewrite H'5 in H'1; elim (oriented_distinct _ _ _ H'1); auto.
red in |- *; intros H'5; auto.
rewrite H'5 in H'2; elim (oriented_distinct1 _ _ _ H'2); auto.
red in |- *; intros H'5; auto.
rewrite H'5 in H'3; elim (oriented_distinct _ _ _ H'3); auto.
red in |- *; intros H'5; auto.
rewrite H'5 in H'3; elim (oriented_distinct2 _ _ _ H'3); auto.
red in |- *; intros H'5; auto.
rewrite H'5 in H'3; elim (oriented_distinct1 _ _ _ H'3); auto.
Qed.
 
Theorem max_oriented_in :
 forall (l : list plane) (p q : plane),
 q = max_oriented p q l \/ In (max_oriented p q l) l.
intros l; elim l; auto.
intros a l0 H' p q; simpl in |- *.
case (oriented_dec p q a).
intros H'0; right; try assumption.
apply H'.
elim (H' p q).
intros H'0 H'1; left; auto.
intros H'0 H'1; right; right; auto.
Qed.
 
Theorem left_max_oriented2 :
 forall L : list plane,
 (forall x y z : plane,
  In x L ->
  In y L ->
  In z L -> x <> y -> x <> z -> y <> z -> oriented x y z \/ oriented x z y) ->
 forall l : list plane,
 included plane l L ->
 forall p q r : plane,
 In p L ->
 In q L ->
 all_left p q l ->
 oriented p q r -> In r l -> max_oriented p q l = max_oriented p r l.
intros L Ax3 l; elim l.
intros Hincluded p q r Hinp Hinq Hall_left H' H'0; inversion H'0.
intros a l0 H' Hincluded p q r Hinp Hinq Hall_left H'0 H'1.
cut (p <> q).
cut (q <> r).
cut (r <> p).
intros Hneq1 Hneq2 Hneq3.
case (eq_plane_dec a r).
intros H'2; rewrite H'2.
simpl in |- *.
case (oriented_dec p q r).
case (oriented_dec p r r); auto.
intros H'3; apply H'3 || case H'3; auto.
intros H'2; try assumption.
case (eq_plane_dec a q); simpl in |- *.
intros H'3; rewrite H'3.
case (oriented_dec p q q).
intros H'5; elim (oriented_distinct _ _ _ H'5); auto.
case (oriented_dec p r q).
intros H'4; (elim (Ax2 _ _ _ H'4); auto).
intros H'4 H'5.
inversion Hall_left.
apply H'; auto.
inversion Hincluded; auto.
elim H'1; auto.
intros H'6; try assumption.
apply H'2 || case H'2; auto.
case (oriented_dec p q a).
intros H'3.
cut (p <> a).
case (oriented_dec p r a).
auto.
intros H'4 H'5 Hneq4.
inversion Hall_left.
apply (left_max_oriented L Ax3) with (q := q); auto.
inversion Hincluded; auto.
apply included_in with (1 := Hincluded); simpl in |- *; auto.
elim (Ax3 p a r); auto.
intros H'6; try assumption.
apply H'4 || case H'4; auto.
apply included_in with (1 := Hincluded); simpl in |- *; auto.
apply included_in with (1 := Hincluded); simpl in |- *; auto.
elim H'1; auto.
intros H'6; case H'2; auto.
red in |- *; intros H'4;
 (rewrite H'4 in H'3; elim (oriented_distinct2 _ _ _ H'3); auto).
case (oriented_dec p r a).
intros H'3.
cut (p <> a).
intros H'4 H'5 H'6; try assumption.
inversion Hall_left.
elim H1;
 [ intros H'7; clear H1
 | intros H'7; elim H'7;
    [ intros H'8; clear H'7 H1 | intros H'8; try exact H'8; clear H'7 H1 ] ].
apply H'4 || case H'4; auto.
apply H'6 || case H'6; auto.
apply H'5 || case H'5; auto.
red in |- *; intros H'4; auto.
rewrite H'4 in H'3; elim (oriented_distinct2 _ _ _ H'3); auto.
intros H'4 H'5 H'6; try assumption.
apply H'; auto.
inversion Hincluded; auto.
inversion Hall_left.
auto.
elim H'1; auto.
intros H'7; try assumption.
apply H'2 || case H'2; auto.
apply oriented_distinct2 with q; auto.
apply oriented_distinct with p; auto.
apply oriented_distinct1 with r; auto.
Qed.
 
Theorem max_included_is_last :
 forall L : list plane,
 (forall x y z : plane,
  In x L ->
  In y L ->
  In z L -> x <> y -> x <> z -> y <> z -> oriented x y z \/ oriented x z y) ->
 forall l1 l2 : list plane,
 included plane l2 L ->
 included plane l1 l2 ->
 forall head snd t : plane,
 In head l2 ->
 In snd l2 ->
 ~ In head l1 ->
 head <> snd ->
 head <> t ->
 snd <> t ->
 In t l2 ->
 all_left head snd l2 ->
 encompass_aux l2 l1 ->
 In (max_oriented head t l2) l1 ->
 forall t' : plane, last plane t' l1 -> max_oriented head t l2 = t'.
intros L Ax3 l1; elim l1.
intros l2 Hincluded H' head snd t H'0 H'1 H'2 H'3 H'4 Hneq' H'5 H'6 H'7 H'8;
 elim H'8.
intros a l; case l.
intros H' l2 Hincluded H'0 head snd t H'1 H'2 H'3 H'4 H'5 Hneq' H'6 H'7 H'8
 H'9 t' H'10.
elim H'9.
intros H'11; rewrite <- H'11.
inversion H'10; auto.
inversion H0.
intros H'11; elim H'11.
intros a1 l0 H' l2 Hincluded H'0 head snd t H'1 H'2 H'3 H'4 H'5 Hneq' H'6 H'7
 H'8 H'9 t' H'10; try assumption.
cut (head <> a1); [ intros Hneq1 | idtac ].
inversion H'8.
cut (a <> a1); [ intros Hneq3 | idtac ].
simpl in H'9.
cut (head <> a).
intros Hneq2.
elim H'9; intros H'11; clear H'9.
cut (oriented head a a1).
intros Horient.
cut (oriented head a1 (max_oriented head t l2)).
rewrite <- H'11; intros H'12.
elim (Ax2 _ _ _ H'12); auto.
lapply (all_left_point l2 (max_oriented head t l2) head a1);
 [ intros H'15; lapply H'15;
    [ intros H'16; elim H'16; clear H'15 | clear H'15 ]
 | idtac ].
intros H'9; try assumption.
apply Hneq3 || case Hneq3; auto.
rewrite <- H'9.
rewrite H'11.
auto.
intros H'9; elim H'9;
 [ intros H'12; clear H'9 | intros H'12; auto; clear H'9 ].
elim H'3.
rewrite H'12.
simpl in |- *; auto.
apply included_in with (a :: a1 :: l0); auto.
simpl in |- *; auto.
apply (max_oriented_correct L Ax3) with (r := snd); auto.
apply included_in with (1 := Hincluded); auto.
apply included_in with (1 := Hincluded); auto.
apply included_in with (1 := Hincluded); auto.
elim (all_left_point l2 head snd t);
 [ intros H'17
 | intros H'17; elim H'17;
    [ intros H'18; clear H'17 | intros H'18; try exact H'18; clear H'17 ]
 | idtac
 | idtac ]; auto.
apply H'5 || case H'5; auto.
apply Hneq' || case Hneq'; auto.
elim (all_left_point l2 a a1 head);
 [ intros H'17
 | intros H'17; elim H'17;
    [ intros H'18; clear H'17 | intros H'18; try exact H'18; clear H'17 ]
 | idtac
 | idtac ].
apply Hneq2 || case Hneq2; auto.
apply Hneq1 || case Hneq1; auto.
auto.
auto.
auto.
apply H' with (snd := snd); auto.
inversion H'0; auto.
red in |- *; intros H'9; try exact H'9.
apply H'3 || case H'3; try assumption.
right; auto.
inversion H'10.
auto.
red in |- *; intros H'11; try exact H'11.
apply H'3 || case H'3; try assumption.
simpl in |- *.
left; auto.
red in |- *; intros H'11; try exact H'11.
rewrite H'11 in H1.
rewrite (all_left_id _ _ H1 head) in H'3.
apply H'3 || case H'3; simpl in |- *.
right; left; auto.
auto.
red in |- *; intros H'11; try exact H'11.
rewrite H'11 in H'3.
apply H'3 || case H'3; simpl in |- *.
right; left; auto.
Qed.
 
Theorem encompass_aux_sub :
 forall (l1 : list plane) (a : plane) (l : list plane),
 encompass_aux (a :: l) l1 -> encompass_aux l l1.
intros l1 a l H'; elim H'.
intros t1; auto.
apply enc_aux_end.
intros t1 t2 l' H'0 H'1 H'2; try assumption.
apply enc_aux_rec.
inversion H'0; auto.
auto.
Qed.
 
Theorem all_left_point_rev :
 forall (l : list plane) (a b : plane),
 (forall t : plane, In t l -> a = t \/ b = t \/ oriented a b t) ->
 all_left a b l.
intros l; elim l.
intros a b H'; auto.
intros a l0 H' a0 b H'0; try assumption.
apply all_left_rec.
apply H'0.
simpl in |- *.
left; auto.
apply H'.
intros t H'1; try assumption.
apply H'0.
simpl in |- *.
right; auto.
Qed.
 
Theorem max_oriented_two :
 forall (l : list plane) (a b : plane),
 encompass_aux l (a :: b :: nil) ->
 max_oriented a b l = b -> forall t : plane, In t l -> t = a \/ t = b.
intros l; elim l.
intros a b H' H'0 t H'1; elim H'1.
intros a l0 H' a0 b H'0; try exact H'0.
inversion H'0.
simpl in |- *.
case (oriented_dec a0 b a).
intros H'1 H'2; try assumption.
elim (max_oriented_left l0 a0 b a);
 [ intros H'9; try exact H'9 | intros H'9 | idtac | idtac ]; 
 auto.
rewrite H'9.
rewrite H'2.
intros t H'3; try assumption.
elim H'3; [ intros H'4; try exact H'4; clear H'3 | intros H'4; clear H'3 ].
right; auto.
apply H'.
apply encompass_aux_sub with (a := a).
auto.
rewrite <- H'2.
rewrite <- H'9.
auto.
auto.
rewrite H'2 in H'9; elim (Ax2 _ _ _ H'9); auto.
inversion H1; auto.
intros H'1; try assumption.
inversion H1.
intros H'2 t H'3; try assumption.
elim H6;
 [ intros H'4; clear H6
 | intros H'4; elim H'4;
    [ intros H'5; clear H'4 H6 | intros H'5; try exact H'5; clear H'4 H6 ] ].
elim H'3; [ intros H'5; clear H'3 | intros H'5; try exact H'5; clear H'3 ].
rewrite H'4.
auto.
apply H'.
apply encompass_aux_sub with (a := a).
auto.
auto.
auto.
elim H'3; [ intros H'4; clear H'3 | intros H'4; try exact H'4; clear H'3 ].
rewrite <- H'4.
right; auto.
apply H'.
apply encompass_aux_sub with (a := a).
auto.
auto.
auto.
apply H'1 || case H'1; auto.
Qed.
 
Theorem max_oriented_not_head :
 forall (l : list plane) (p q : plane), p <> q -> max_oriented p q l <> p.
intros l; elim l.
intros p q H'; auto.
intros a l0 H' p q H'0; simpl in |- *.
case (oriented_dec p q a).
intros H'1; try assumption.
apply H'.
red in |- *; intros H'2; try exact H'2.
elim (oriented_distinct2 _ _ _ H'1); auto.
intros H'1; try assumption.
apply H'.
try assumption.
Qed.
 
Theorem max_oriented_left2 :
 forall (l : list plane) (p q a : plane),
 all_left p q l ->
 a = q \/ oriented p q a ->
 a = max_oriented p a l :>plane \/ oriented p a (max_oriented p a l).
intros l; elim l.
intros p q a H' H'0; left; simpl in |- *.
auto.
intros a l0 H' p q a0 H'0 H'1; elim H'1;
 [ intros H'2; clear H'1 | intros H'2; try exact H'2; clear H'1 ].
simpl in |- *.
case (oriented_dec p a0 a).
intros H'1; try assumption.
elim (max_oriented_left l0 p q a).
intros H'3; rewrite <- H'3.
right; auto.
intros H'3; try assumption.
rewrite H'2.
right; try assumption.
rewrite H'2 in H'1.
elim (all_left_point (a :: l0) p q (max_oriented p a l0));
 [ intros H'10
 | intros H'10; elim H'10;
    [ intros H'11; try exact H'11; clear H'10 | intros H'11; clear H'10 ]
 | idtac
 | idtac ].
rewrite H'10 in H'3; elim (oriented_distinct2 _ _ _ H'3); auto.
rewrite <- H'10.
auto.
rewrite <- H'11 in H'3; elim (Ax2 _ _ _ H'3); auto.
auto.
try assumption.
elim (max_oriented_in l0 p a); [ intros H'7 | intros H'7; try exact H'7 ].
rewrite <- H'7.
simpl in |- *.
left; auto.
simpl in |- *.
right; auto.
rewrite <- H'2.
auto.
inversion H'0; auto.
intros H'1; try assumption.
apply H' with (q := q).
inversion H'0; auto.
auto.
apply max_oriented_left with (q := q).
auto.
auto.
Qed.
 
Theorem max_oriented_correct2 :
 forall L : list plane,
 (forall x y z : plane,
  In x L ->
  In y L ->
  In z L -> x <> y -> x <> z -> y <> z -> oriented x y z \/ oriented x z y) ->
 forall l : list plane,
 included plane l L ->
 forall p q r : plane,
 In p L ->
 In q L ->
 In r L ->
 all_left q r l ->
 r = p \/ oriented q r p -> all_left (max_oriented q p l) q l.
intros L Ax3 l; elim l.
intros Hincluded p q r Hinp Hinq Hinr H' H'0; auto.
intros a l0 H' Hincluded p q r Hinp Hinq Hinr H'0 H'1; elim H'1;
 [ intros H'2; auto; clear H'1 | intros H'2; clear H'1 ].
simpl in |- *.
inversion H'0.
case (oriented_dec q p a).
rewrite <- H'2.
intros H'1; try assumption.
apply all_left_rec.
elim (max_oriented_left l0 q r a);
 [ intros H'9 | intros H'9; try exact H'9 | idtac | idtac ].
auto.
right; right; auto.
auto.
auto.
apply (max_oriented_correct L Ax3) with (r := r); auto.
inversion Hincluded; auto.
apply included_in with (1 := Hincluded); simpl in |- *; auto.
apply oriented_distinct2 with r.
auto.
rewrite <- H'2.
intros H'1; try assumption.
elim H1;
 [ intros H'3; clear H1
 | intros H'3; elim H'3;
    [ intros H'4; clear H'3 H1 | intros H'4; try exact H'4; clear H'3 H1 ] ].
rewrite H'3.
apply all_left_rec.
right; left; auto.
apply H' with (r := r).
inversion Hincluded; auto.
auto.
apply included_in with (1 := Hincluded); simpl in |- *; auto.
auto.
rewrite <- H'3.
auto.
left; auto.
apply all_left_rec.
rewrite H'4.
elim (max_oriented_left2 l0 q r a);
 [ intros H'10; try exact H'10 | intros H'10 | idtac | idtac ].
auto.
auto.
auto.
left; auto.
apply H' with (r := r).
inversion Hincluded; auto.
auto.
auto.
auto.
auto.
left; auto.
apply H'1 || case H'1; auto.
apply (max_oriented_correct L Ax3) with (r := r); auto.
red in |- *; intros H'1; try exact H'1.
rewrite H'1 in H'2; elim (oriented_distinct2 _ _ _ H'2); auto.
Qed.
 
Theorem max_oriented_is_last2 :
 forall (l l1 : list plane) (head snd t : plane),
 encompass_aux l (head :: snd :: l1) ->
 ~ In head (snd :: l1) ->
 In head l ->
 included plane (snd :: l1) l ->
 all_single plane (snd :: l1) ->
 max_oriented head t l = snd ->
 last plane t (snd :: l1) -> max_oriented head t l = t.
intros l l1 head snd t H'; inversion H'.
inversion H3.
intros H'0 H'1 H'2 H'3 H'4 H'5; inversion H'5.
lapply (max_oriented_two l head t);
 [ intros H'9; lapply H'9;
    [ intros H'10; elim (H'10 (max_oriented head snd l));
       [ intros H'13; try exact H'13; clear H'9
       | intros H'13; clear H'9
       | clear H'9 ]
    | clear H'9 ]
 | idtac ].
lapply (max_oriented_not_head l head snd);
 [ intros H'9; apply H'9 || case H'9; try assumption | idtac ].
red in |- *; intros H'6; try exact H'6.
apply H'0 || case H'0; simpl in |- *.
left; auto.
generalize H'13.
rewrite <- H7.
auto.
rewrite <- H7.
rewrite H'4.
inversion H'2; auto.
generalize H'4.
rewrite <- H7.
auto.
try assumption.
rewrite H7.
auto.
rewrite H6.
auto.
inversion H7.
intros H'0 H'1 H'2 H'3 H'4 H'5; try assumption.
elim (max_oriented_left2 l head snd t);
 [ intros H'6 | intros H'6; try exact H'6 | idtac | idtac ]; 
 auto.
elim (Ax2 _ _ _ H'6).
rewrite H'4.
elim (all_left_point l head snd t).
intros H'7; try assumption.
apply H'0 || case H'0; try assumption.
apply in_cons.
apply last_in.
inversion H'5.
rewrite H'7.
auto.
intros H'7; elim H'7; [ intros H'8; clear H'7 | intros H'8; auto; clear H'7 ].
inversion H'3.
apply H11 || case H11; try assumption.
apply last_in.
rewrite H'8.
inversion H'5.
auto.
auto.
apply included_in with (snd :: t3 :: l'0).
auto.
apply last_in.
auto.
elim (all_left_point l head snd t);
 [ intros H'6
 | intros H'6; elim H'6;
    [ intros H'7; auto; clear H'6 | intros H'7; clear H'6 ]
 | idtac
 | idtac ].
apply H'0 || case H'0; try assumption.
apply in_cons.
rewrite H'6.
apply last_in.
inversion H'5; auto.
right; auto.
auto.
apply included_in with (snd :: t3 :: l'0).
auto.
apply last_in; auto.
Qed.
 
Theorem Jarvis_final_spec_satisfied :
 forall L : list plane,
 (forall x y z : plane,
  In x L ->
  In y L ->
  In z L -> x <> y -> x <> z -> y <> z -> oriented x y z \/ oriented x z y) ->
 forall l l1 : list plane,
 included plane l L ->
 all_single plane l1 ->
 included plane l1 l ->
 forall head : plane,
 In head l ->
 ~ In head l1 ->
 encompass_aux l (head :: l1) ->
 forall t : plane,
 last plane t l1 ->
 t = max_oriented head t l :>plane -> convexHullSpec l (head :: l1).
intros L Ax3 l l1 Hincluded' Hsingle Hincluded head Hin Hnot_in Henc t Hlast
 H'.
unfold convexHullSpec in |- *.
split; [ try assumption | idtac ].
apply included_step; auto.
apply encompass_def with (t1 := t).
apply last_rec.
try assumption.
apply enc_aux_rec.
rewrite H'.
inversion Henc.
rewrite <- H1 in Hlast; inversion Hlast.
cut (head <> t2).
cut (head <> t).
intros Hneq1 Hneq2.
elim (all_left_point l head t2 t);
 [ intros H'6
 | intros H'6; elim H'6;
    [ intros H'7; clear H'6 | intros H'7; try exact H'7; clear H'6 ]
 | idtac
 | idtac ].
apply Hneq1 || case Hneq1; auto.
inversion H2.
rewrite <- H'.
lapply (max_oriented_two l head t2);
 [ intros H'3; lapply H'3;
    [ intros H'4; try exact H'4; clear H'3 | clear H'3 ]
 | idtac ]; auto.
apply all_left_point_rev.
intros t3 H'0; try assumption.
elim (H'4 t3); [ intros H'3; try exact H'3 | intros H'3 | idtac ]; auto.
rewrite H'3.
left; auto.
rewrite H'7.
auto.
rewrite H5.
rewrite H0.
auto.
rewrite <- H0 in Hlast; rewrite <- H4 in Hlast.
generalize H0 Hsingle; intros Dummy Dummy1; simpl in Dummy.
rewrite <- Dummy in Dummy1.
inversion Dummy1.
rewrite <- H4 in H9.
inversion H9.
inversion Hlast.
apply H10 || case H10; try assumption.
rewrite H'7.
apply last_in.
rewrite <- H4.
auto.
apply (max_oriented_correct L Ax3) with (r := t2); auto.
apply included_in with (1 := Hincluded').
apply included_in with (1 := Hincluded).
apply last_in; auto.
apply included_in with (1 := Hincluded'); auto.
apply included_in with (1 := Hincluded');
 apply included_in with (1 := Hincluded); rewrite <- H0; 
 simpl in |- *; auto.
auto.
apply included_in with l1.
apply Hincluded.
apply last_in.
auto.
red in |- *; intros H'0; try exact H'0.
apply Hnot_in || case Hnot_in; simpl in |- *.
rewrite H'0.
apply last_in.
apply Hlast.
red in |- *; intros H'0; try exact H'0.
apply Hnot_in || case Hnot_in; try assumption.
rewrite <- H0.
rewrite H'0.
simpl in |- *.
left; auto.
auto.
Qed.
 
Lemma Jarvis_not_In_invariant :
 forall L : list plane,
 (forall x y z : plane,
  In x L ->
  In y L ->
  In z L -> x <> y -> x <> z -> y <> z -> oriented x y z \/ oriented x z y) ->
 forall l l1 : list plane,
 included plane l L ->
 all_single plane l1 ->
 included plane l1 l ->
 forall t : plane,
 last plane t l1 ->
 forall head : plane,
 In head l ->
 ~ In head l1 ->
 encompass_aux l (head :: l1) ->
 t <> max_oriented head t l :>plane ->
 ~ In (max_oriented head t l) (head :: l1).
intros L Ax3 l l1 Hincluded' Hsingle Hincluded t Hlast head Hin Hnotin
 Hencompass H'.
red in |- *; intros H'0.
inversion Hencompass.
rewrite <- H1 in Hlast; inversion Hlast.
elim H'0; intros H'1.
elim (max_oriented_not_head l head t); auto.
red in |- *; intros H'2; elim H'; pattern t at 1 in |- *; rewrite <- H'2;
 auto.
(* ici, un nouveau but important *)
inversion H2.
rewrite <- H0 in H'1; rewrite <- H5 in H'1; elim H'1.
intros H'2; try assumption.
apply H'.
symmetry  in |- *.
apply max_oriented_is_last2 with (l1 := l') (snd := t2); try rewrite H0; auto.
auto.
apply H'; symmetry  in |- *.
generalize Hnotin Hincluded Hsingle Hlast;
 clear Hnotin Hincluded Hsingle Hlast.
rewrite <- H0; rewrite <- H4.
intros Hnotin H'2 H'3 H'4.
inversion H'2; inversion H'3.
inversion H'4; inversion H14; inversion H11.
apply (max_included_is_last L Ax3) with (l1 := t3 :: l'0) (snd := t2); auto.
red in |- *; intros H'5; apply Hnotin; right; auto.
red in |- *; intros H'5; apply Hnotin; left; auto.
red in |- *; intros H'5; apply Hnotin; right.
rewrite H'5; apply last_in; auto.
red in |- *; intros H'5; apply H15; apply last_in; rewrite H'5; auto.
apply included_in with (t3 :: l'0); auto.
apply last_in; auto.
elim (max_oriented_in l head t); [ intros H'7 | intros H'7; try exact H'7 ];
 auto.
case H'; auto.
generalize H'1; rewrite <- H0; rewrite <- H4.
intros H'5; elim H'5; intros H'6; auto.
elim H'; symmetry  in |- *.
apply max_oriented_is_last2 with (l1 := l') (snd := t2); auto;
 try (rewrite H0; auto; fail); try (rewrite <- H4; auto; fail).
Qed.
 
Theorem Jarvis_encompass_invariant :
 forall L : list plane,
 (forall x y z : plane,
  In x L ->
  In y L ->
  In z L -> x <> y -> x <> z -> y <> z -> oriented x y z \/ oriented x z y) ->
 forall l l1 : list plane,
 included plane l L ->
 all_single plane l1 ->
 included plane l1 l ->
 forall t : plane,
 last plane t l1 ->
 forall head : plane,
 In head l ->
 ~ In head l1 ->
 encompass_aux l (head :: l1) ->
 t <> max_oriented head t l :>plane ->
 encompass_aux l (max_oriented head t l :: head :: l1).
intros L Ax3 l l1 Hincluded' Hsingle Hincluded t Hlast head Hin Hnotin
 Hencompass H'.
apply enc_aux_rec.
generalize Hsingle Hincluded Hlast; inversion Hencompass.
intros H'0 H'1 H'2; inversion H'2.
inversion H2.
intros H'0 H'1 H'2; inversion H'2.
apply (max_oriented_correct2 L Ax3) with (r := t2); auto.
apply included_in with (1 := Hincluded');
 apply included_in with (1 := Hincluded); rewrite <- H0; 
 simpl in |- *; auto.
apply included_in with (1 := Hincluded'); auto.
apply included_in with (1 := Hincluded');
 apply included_in with (1 := Hincluded); rewrite <- H0; 
 simpl in |- *; auto.
inversion H6.
intros H'0 H'1 H'2.
apply (max_oriented_correct2 L Ax3) with (r := t2); auto.
apply included_in with (1 := Hincluded');
 apply included_in with (1 := Hincluded); apply last_in; 
 auto.
apply included_in with (1 := Hincluded'); auto.
apply included_in with (1 := Hincluded'); apply included_in with (1 := H'1);
 simpl in |- *; auto.
right; inversion H'0.
elim (all_left_point l head t2 t);
 [ intros H'9
 | intros H'9; elim H'9;
    [ intros H'10; clear H'9 | intros H'10; try exact H'10; clear H'9 ]
 | idtac
 | idtac ]; auto.
(* stupid step *)
generalize Hnotin; rewrite <- H0; rewrite <- H4.
intros H'3; case H'3.
apply last_in.
rewrite H'9; auto.
inversion H'2.
case H10.
rewrite H'10.
apply last_in; auto.
apply included_in with (t2 :: t3 :: l'0); auto.
apply last_in; auto.
auto.
Qed.
 
(* Archeological note: this is probably an attempt to use another
 technique to define the final function, which was aborted.
Definition Jarvis_F' :=
   [input : (list plane)]
   [f : (list plane) ->  (list plane)]
   [current : (list plane)]
   [x : plane]
    Cases (eq_plane_dec t (max_oriented head t input)) of a => b | c => d
    end.
*)
 
Definition Jarvis_F :
  forall l : list plane,
  (forall x y z : plane,
   In x l ->
   In y l ->
   In z l -> x <> y -> x <> z -> y <> z -> oriented x y z \/ oriented x z y) ->
  forall (l1 : single_sublist plane l)
    (jarvis : forall (l2 : single_sublist plane l)
                (h : pre_hull_order plane l l2 l1) 
                (x : plane),
              In x l ->
              ~ In x l2 ->
              encompass_aux l (x :: l2) ->
              forall t : plane,
              last plane t l2 -> {l3 : list plane | convexHullSpec l l3})
    (x : plane),
  In x l ->
  ~ In x l1 ->
  encompass_aux l (x :: l1) ->
  forall t : plane,
  last plane t l1 -> {l3 : list plane | convexHullSpec l l3}.
intros l Ax3 p_l1; case p_l1; intros l1 Hsingle Hincluded.
intros jarvis head Hin Hnot_in Henc t Hlast.
case (eq_plane_dec t (max_oriented head t l)).
intros H'; exists (head :: l1); try assumption.
exact
 (Jarvis_final_spec_satisfied l Ax3 l l1 (included_refl plane l) Hsingle
    Hincluded head Hin Hnot_in Henc t Hlast H').
intros H'; simpl in Hnot_in, Henc, Hlast.
apply
 jarvis
  with
    (l2 := pre_hull_add_element plane l
             (mk_single_sublist plane l l1 Hsingle Hincluded) head Hin
             Hnot_in)
    (x := max_oriented head t l)
    (t := t).
apply (pre_hull_add_element_decrease plane eq_plane_dec).
elim (max_oriented_in l head t);
 [ intros H'3; case H'; auto | intros H'3; exact H'3 ].
exact
 (Jarvis_not_In_invariant l Ax3 l l1 (included_refl plane l) Hsingle
    Hincluded t Hlast head Hin Hnot_in Henc H').
exact
 (Jarvis_encompass_invariant l Ax3 l l1 (included_refl plane l) Hsingle
    Hincluded t Hlast head Hin Hnot_in Henc H').
simpl in |- *.
apply last_rec.
auto.
Qed.
 
Definition jarvis_rec (l : list plane)
  (ax3 : forall x y z : plane,
         In x l ->
         In y l ->
         In z l ->
         x <> y -> x <> z -> y <> z -> oriented x y z \/ oriented x z y) :=
  Fix (pre_hull_order_wf plane l)
    (fun l1 : single_sublist plane l =>
     forall x : plane,
     In x l ->
     ~ In x l1 ->
     encompass_aux l (x :: l1) ->
     forall t : plane,
     last plane t l1 -> {l3 : list plane | convexHullSpec l l3})
    (Jarvis_F l ax3).
 
Inductive fst_different_spec (a : plane) : list plane -> Type :=
  | fds_one :
      forall l : list plane,
      (forall b : plane, In b l -> a = b) -> fst_different_spec a l
  | fds_two :
      forall (l : list plane) (b : plane),
      a <> b -> In b l -> fst_different_spec a l.
 
Definition fst_different' :
  forall (a : plane) (l : list plane), fst_different_spec a l.
intros a l; elim l.
apply fds_one.
intros b H'; elim H'.
intros a0 l0 H'; try assumption.
case (eq_plane_dec a a0).
intros H'0; try assumption.
try exact H'.
elim H'.
intros l1 H'1; try assumption.
apply fds_one.
intros b H'2; try assumption.
elim H'2.
rewrite <- H'0.
auto.
intros H'3; try assumption.
apply H'1.
auto.
intros l1 b H'1 H'2; try assumption.
apply fds_two with (b := b).
auto.
simpl in |- *.
right; try assumption.
intros H'0; try assumption.
apply fds_two with (b := a0).
auto.
simpl in |- *.
left; auto.
Qed.
 
Definition jarvis :
  forall l : list plane,
  (forall x y z : plane,
   In x l ->
   In y l ->
   In z l -> x <> y -> x <> z -> y <> z -> oriented x y z \/ oriented x z y) ->
  l <> nil -> {l2 : list plane | convexHullSpec l l2}.
intros l; case l.
intros Ax3 H'; exists (nil (A:=plane)); case H'; auto.
intros p l0; case l0.
intros Ax3 H'.
exists (p :: nil).
unfold convexHullSpec in |- *.
split; [ apply included_refl | idtac ].
apply encompass_def with (t1 := p).
apply last_found.
apply enc_aux_rec.
apply all_left_rec; auto.
apply enc_aux_end.
intros p0 l1; case (fst_different' p (p0 :: l1)).
intros l2 H'0 Ax3 H'; exists (p :: nil).
unfold convexHullSpec in |- *.
split.
apply included_step; simpl in |- *; auto.
apply encompass_def with (t1 := p).
apply last_found.
apply enc_aux_rec.
apply all_left_point_rev.
intros t H'1; elim H'1; intros H'2; left; try assumption.
apply H'0; auto.
apply enc_aux_end.
intros l2 b H'0 H'1 Ax3 H'; try assumption.
elim (extremal_points p b (p :: l2)); auto.
intros x; case x; intros head snd.
simpl in |- *; intros H'2.
cut (In snd (p :: l2)).
intros Hin.
generalize
 (jarvis_rec (p :: l2) Ax3
    (mk_single_sublist plane (p :: l2) (snd :: nil)
       (single_step plane snd nil (single_nil plane)
          (fun x : In snd nil => x))
       (included_step plane snd nil (p :: l2) Hin
          (included_empty plane (p :: l2)))) head).
intros H'3.
apply H'3 with (t := snd).
clear H'3.
elim H'2; intros H'3 H'4; elim H'4; intros H'5 H'6; elim H'6; intros H'7 H'8;
 try exact H'7; clear H'6 H'4 H'2.
red in |- *; intros H'4; elim H'4; intros H'5; try exact H'5.
elim H'2; intros H'6 H'7; elim H'7; intros H'8 H'9; apply H'8 || case H'8;
 auto; clear H'7 H'2.
simpl in |- *; apply enc_aux_rec.
elim H'2; intros H'4 H'5; auto; clear H'2.
apply enc_aux_end.
simpl in |- *; apply last_found.
simpl in |- *; elim H'2; intros H'3 H'4; elim H'4; intros H'5 H'6; elim H'6;
 intros H'7 H'8; try exact H'8.
simpl in |- *.
left; auto.
apply in_cons.
auto.
Qed.
End gen.
