Require Import Ring.

Parameter R : Set.

Parameter one : R.

Parameter zero : R.

Parameter Rplus : R -> R -> R.

Parameter Rtimes : R -> R -> R.

Parameter Roppose : R -> R.

Definition Rsub (x y : R) := Rplus x (Roppose y).

Parameter
  RT : ring_theory zero one Rplus Rtimes Rsub Roppose eq.

Add Ring R_Ring_theory : RT (abstract).

Parameter Gt : R -> R -> Prop.
Definition Ge (x y : R) : Prop :=
  reels.Gt x y /\ x <> y \/ ~ reels.Gt x y /\ x = y.
Definition Lt (x y : R) : Prop := reels.Gt y x.
Definition Le (x y : R) : Prop := Ge y x.

Parameter
  Gt_stable_plus :
    forall x y : R,
    reels.Gt x zero /\ reels.Gt y zero -> reels.Gt (Rplus x y) zero.
Parameter
  Gt_stable_mult :
    forall x y : R,
    reels.Gt x zero /\ reels.Gt y zero -> reels.Gt (Rtimes x y) zero.

Parameter
  Inverse_signe_r :
    forall x : R, reels.Gt x zero -> ~ reels.Gt (Roppose x) zero.
Parameter
  Inverse_signe_l :
    forall x : R, ~ reels.Gt (Roppose x) zero -> reels.Gt x zero \/ x = zero.
Parameter
  Roppose_inverse_plus : forall x y : R, Rplus x y = zero -> y = Roppose x.
Parameter Gt_non_nul : forall x : R, reels.Gt x zero -> x <> zero.


