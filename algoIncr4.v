(* AddPath "$HOME/ctcoq/stage". *)
Require Export algoIncr3.
Require Export Arith.
Require Export List.
Require Export Classical.
 
Inductive cycle_points_distincts : Plan -> CC -> Prop :=
  | cycle_nil : forall t : Plan, cycle_points_distincts t nil
  | cycle_rec :
      forall (t a : Plan) (lCC : CC),
      t <> a ->
      ~ in_CC a lCC ->
      cycle_points_distincts t lCC -> cycle_points_distincts t (a :: lCC).
 
Lemma non_tete_dans_cycle_distinct :
 forall (t : Plan) (lCC : CC), cycle_points_distincts t lCC -> ~ in_CC t lCC.
simple induction lCC; intros.
simpl in |- *; auto.
red in |- *; intros.
elim H1; clear H1; intros.
inversion H0; absurd (a = t :>Plan); auto.
inversion H0; absurd (in_CC t l); auto.
Qed.
Transparent convexRec.
 
Lemma cycleRouge_non_vide :
 forall (p t : Plan) (lCC cc : CC) (b : Plan),
 cycle_dans_CH t (b :: lCC) cc ->
 t <> p ->
 ~ in_CC p (b :: lCC) ->
 infPolarEq t p b ->
 @fst _ _ (convexRec Rouge p t (b :: lCC)) = Rouge ->
 @snd _ _ (@snd _ _ (convexRec Rouge p t (b :: lCC))) <> nil.
intros p t lCC cc; induction lCC as [| a lCC HreclCC]; intros.
simpl in |- *.
 CaseEq (directBool b t p); simpl in |- *.
red in |- *; intros Hfaux; discriminate Hfaux.
absurd (sensDirect b t p); auto.
apply Axiom1; apply Axiom1; apply infPolarEq_strict; auto.
red in |- *; intro.
rewrite H5 in H1; elim H1; simpl in |- *; auto.
apply sym_not_eq; apply (points_extremaux_distincts b t cc).
elim (H b t); simpl in |- *; intros.
elim H6; auto.
auto.
 CaseEq (convexRec Rouge p t (a :: lCC)).
 CaseEq p0.
rewrite H5 in H4.
generalize H4; simpl in |- *; intros Htemp; rewrite Htemp; clear Htemp.
generalize H4; intros Htemp; simpl in Htemp; simpl in H3; rewrite Htemp in H3;
 clear Htemp.
 CaseEq c; CaseEq (directBool b a p); simpl in |- *; rewrite H6 in H3;
  rewrite H7 in H3; simpl in H3.
discriminate H3.
generalize (HreclCC a); rewrite H4; simpl in |- *; intro.
apply H8; auto.
eauto.
red in |- *; intros Htemp; elim Htemp; clear Htemp; intros.
rewrite H9 in H1; elim H1; simpl in |- *; auto.
elim H1; simpl in |- *; auto.
elim (classic (a = p)); intro; auto.
elim (classic (t = b)); intro.
rewrite H10; auto.
elim (classic (sensDirect t a p)); intro; auto.
absurd (sensDirect t a b).
apply infPolarEq_non_sensDirect.
apply infPolarEq1.
apply (points_extremaux_Prop b a t cc).
apply point_extremal_dans_cc.
apply (cycle_point_extremal_t t (a :: lCC) cc b); auto.
generalize (H b a); intros.
elim H12; auto.
simpl in |- *; intros.
elim H14; auto.
simpl in |- *; auto.
apply (Axiom4 t a b p); auto.
cut (infPolarEq p a b); auto.
intros.
unfold infPolarEq in H12; elim H12; clear H12; intros.
auto.
elim H12; clear H12; intros.
absurd (b = a); auto.
apply (points_extremaux_distincts b a cc).
generalize (H b a); intros.
elim H13; auto.
simpl in |- *; intros.
elim H15; auto.
simpl in |- *; auto.
elim H12; clear H12; intros.
rewrite H12 in H1; elim H1; simpl in |- *; auto.
absurd (a = p); auto.
apply infPolarEq_strict; auto.
red in |- *; intro.
rewrite H12 in H1; elim H1; simpl in |- *; auto.
red in |- *; intros Htemp; discriminate Htemp.
red in |- *; intros Htemp; discriminate Htemp.
Qed.
 