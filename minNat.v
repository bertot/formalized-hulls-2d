Require Import Arith.
Require Import PolyList.
 
Inductive minList  : (list nat) -> nat -> Prop :=
  min_nil: (s : nat)  (minList (nil nat) s)
 | min_rec: (l : (list nat)) (p, s : nat) (minList l s) -> (le s p) -> (minList (cons p l) s) .
Variable boolLe:nat -> nat -> bool.
Parameter LeTrue:(a, b : nat) (boolLe a b) = true -> (le a b).
Parameter LeFalse:(a, b : nat) (boolLe a b) = false -> (lt b a).
 
Fixpoint calculMinRec [min : nat; l : (list nat)] : nat :=
 Cases l of
   nil => min
  | (cons a q) =>
      Cases (boolLe (calculMinRec min q) a) of   true => (calculMinRec min q)
                                                | false => a
      end
 end.
 
Lemma min_trans: (a, min : nat) (l : (list nat)) (lt a min) -> (minList l min) -> (minList l a).
Intros.
Induction l.
Apply min_nil.
Apply min_rec.
Apply Hrecl.
Inversion H0 .
Apply H3.
Inversion H0 .
Apply (le_trans a min a0); Auto.
Apply lt_le_weak; Auto.
Qed.
 
Theorem minRecOK: (l : (list nat)) (min : nat)  (minList l (calculMinRec min l)).
Intros.
Induction l.
Apply min_nil.
Simpl.
Generalize (refl_equal ? (boolLe (calculMinRec min l) a)); Pattern -1 (boolLe (calculMinRec min l) a);
 Case (boolLe (calculMinRec min l) a); Intros.
Apply min_rec.
Apply Hrecl.
Apply LeTrue.
Apply H.
Apply min_rec.
Apply (min_trans a (calculMinRec min l) l).
Apply LeFalse.
Auto.
Auto.
Auto.
Qed.
 
