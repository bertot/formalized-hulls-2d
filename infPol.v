AddPath "$HOME/ctcoq/stage".
Require Export axiomsKnuth.
 
Definition infPol :=
   [t, s, a, b : Plan]
       Cases (directBool t s a) of
         true =>
           Cases (directBool t s b) of   true => (directBool t a b)
                                        | false => true
           end
        | false =>
            Cases (directBool t s b) of   true => false
                                         | false => (directBool t a b)
            end
       end.
Parameter sensDirectTrue:
          (a, b, c : Plan) (sensDirect a b c) -> (directBool a b
                                                             c) = true.
Hints Resolve directBoolTrue directBoolFalse sensDirectTrue.
 
Lemma infPol_trans: (t, s, a, b, c : Plan) (infPol t s a b) = true -> (infPol t s
                                                                              b c) = true -> (infPol t s
                                                                                                     a c) = true.
Intros t s a b c.
Unfold infPol; Generalize (refl_equal ? (directBool t s a));
 Pattern -1 (directBool t s a); Case (directBool t s a);
 Generalize (refl_equal ? (directBool t s b)); Pattern -1 (directBool t s b);
 Case (directBool t s b); Generalize (refl_equal ? (directBool t s c));
 Pattern -1 (directBool t s c); Case (directBool t s c);
 Generalize (refl_equal ? (directBool t a b)); Pattern -1 (directBool t a b);
 Case (directBool t a b); Generalize (refl_equal ? (directBool t b a));
 Pattern -1 (directBool t b a); Case (directBool t b a); Simpl; Intros; Auto;
 Apply sensDirectTrue; Auto.
Absurd (sensDirect t a b); Auto.
Apply (Axiom5 t s a b c); Auto.
Discriminate H4.
Discriminate H4.
Discriminate H5.
Discriminate H5.
Discriminate H5.
Discriminate H5.
Discriminate H4.
Discriminate H4.
Discriminate H4.
Discriminate H4.
Absurd (sensDirect t a b); Auto.
 
