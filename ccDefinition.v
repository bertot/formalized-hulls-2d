(* AddPath "$HOME/ctcoq/stage". *)
Require Export axiomsKnuth.
Require Import Classical.
Require Import List.

Ltac CaseEq a :=
  generalize (@refl_equal _ a); pattern a at -1 in |- *; case a; intros.

Definition idb (b : bool) := b.

Lemma toto : forall b : bool, b = true \/ b = false.
intros b. CaseEq (idb b).
left; assumption.
right; assumption.
Qed.


(*---------------------------     CC     --------------------------- *)

Definition CC := list Plan.
Parameter bidon : Plan.

Fixpoint element (ch : CC) (n : nat) {struct n} : Plan :=
  match ch with
  | nil => bidon
  | a :: l => match n with
              | O => a
              | S m => element l m
              end
  end.

Definition in_CC (p : Plan) (cc : CC) : Prop := In p cc.

Lemma not_in_CC_tete :
 forall (t a : Plan) (cc : CC), ~ in_CC t (a :: cc) -> t <> a.
intros.
simpl in H.
auto.
Qed.

Lemma not_in_CC_suite :
 forall (t a : Plan) (cc : CC), ~ in_CC t (a :: cc) -> ~ in_CC t cc.
intros.
simpl in H.
auto.
Qed.

Lemma not_in_CC_saute :
 forall (t a b : Plan) (cc : CC),
 ~ in_CC t (a :: b :: cc) -> ~ in_CC t (a :: cc).
intros.
simpl in |- *.
apply and_not_or.
split.
red in |- *; intro; absurd (t = a); auto.
apply (not_in_CC_tete t a (b :: cc)); auto.
apply (not_in_CC_suite t b cc).
apply (not_in_CC_suite t a (b :: cc)); auto.
Qed.
Hint Resolve not_in_CC_tete not_in_CC_suite not_in_CC_saute.

(*--------------------------      egalBool    -------------------------------*)
Parameter egalBool : Plan -> Plan -> bool.
Parameter egalBoolTrue : forall a b : Plan, egalBool a b = true -> a = b.
Parameter egalBoolFalse : forall a b : Plan, egalBool a b = false -> a <> b.
Hint Resolve egalBoolTrue egalBoolFalse.

Lemma egalBoolTrue_inv : forall a b : Plan, a = b -> egalBool a b = true.
intros.
CaseEq (egalBool a b).
(* Generalize (refl_equal ? (egalBool a b)); Pattern -1 (egalBool a b);
Case (egalBool a b); Intros. *)
 
auto.
absurd (a = b); auto.
Qed.
Hint Resolve egalBoolTrue_inv.

(*-------------------------- infPolarEq -------------------------------*)

Definition infPolarEq (t a b : Plan) : Prop :=
  sensDirect t a b \/ a = b \/ t = b \/ t = a.

Lemma egal_infPolarEq :
 forall p q r : Plan, p = q \/ p = r \/ q = r -> infPolarEq p q r.
intros.
unfold infPolarEq in |- *.
elim H; clear H; intros; auto.
elim H; clear H; auto.
Qed.
Hint Resolve egal_infPolarEq.

Lemma infPolarEq1 : forall p q r : Plan, infPolarEq p q r -> infPolarEq r p q.
intros.
unfold infPolarEq in |- *; unfold infPolarEq in H.
repeat (elim H; clear H; intros H; auto).
Qed.
Hint Resolve infPolarEq1.

Lemma non_sensDirect_infPolarEq :
 forall p q r : Plan, ~ sensDirect p q r -> infPolarEq p r q.
intros p q r H.
unfold infPolarEq; elim (Axiom3 p q r); intro; auto.
elim H0; intro; clear H0; auto.
elim H1; intro; clear H1; auto.
elim H0; intro; clear H0; auto.
Qed.
Hint Resolve non_sensDirect_infPolarEq.

Lemma non_infPolarEq :
 forall p q r : Plan, ~ infPolarEq p q r -> sensDirect p r q.
intros.
elim (Axiom3 p q r); intros.
elim H; clear H; intros; auto.
elim H0; clear H0; intros; auto.
Qed.
Hint Resolve non_infPolarEq.

Lemma infPolarEq_strict :
 forall t a b : Plan,
 infPolarEq t a b -> a <> b -> t <> a -> t <> b -> sensDirect t a b.
intros.
simpl in H; elim H; intros; clear H.
auto.
elim H3; intros; clear H3.
absurd (a = b); auto.
elim H; intros; clear H.
absurd (t = b); auto.
absurd (t = a); auto.
Qed.

Lemma infPolarEq_egal :
 forall t a b : Plan,
 infPolarEq t a b -> infPolarEq t b a -> a = b \/ t = a \/ t = b.
intros.
unfold infPolarEq in H0.
elim H0; intros; clear H0.
unfold infPolarEq in H.
elim H; intros; clear H.
absurd (sensDirect t a b); auto.
elim H0; intros; clear H0; auto.
elim H; intros; clear H; auto.
elim H1; intros; clear H1; auto.
Qed.

Lemma infPolarStrict_xor_Eq :
 forall t a b : Plan, sensDirect t a b -> ~ infPolarEq t b a.
intros.
unfold infPolarEq in |- *.
apply and_not_or; split; auto.
apply and_not_or; split; auto.
red in |- *; intro.
absurd (a = b); auto.
eauto.
apply and_not_or; split; auto.
eauto.
red in |- *; intro.
absurd (b = t); auto.
eauto.
Qed.
Hint Resolve infPolarStrict_xor_Eq.

Lemma infPolarEq_non_sensDirect :
 forall t a b : Plan, infPolarEq t a b -> ~ sensDirect t b a.
intros.
red in |- *; intro; absurd (infPolarEq t a b); auto.
Qed.
Hint Resolve infPolarEq_non_sensDirect.

Lemma infPolarEq_trans :
 forall s t a b c : Plan,
 s <> t ->
 t <> b ->
 infPolarEq t s a ->
 infPolarEq t s b ->
 infPolarEq t s c -> infPolarEq t a b -> infPolarEq t b c -> infPolarEq t a c.
intros.
unfold infPolarEq in |- *.
unfold infPolarEq in H5.
elim H5; intro; clear H5.
elim H4; intro; clear H4.
elim H3; intro; clear H3.
elim H2; intro; clear H2.
elim H1; intro; clear H1.
left; apply (Axiom5 t s a b c); auto.
elim H2; intro; clear H2; auto.
rewrite H1 in H4; auto.
elim H1; intro; clear H1; auto.
elim H3; intro; clear H3.
rewrite H2 in H1; absurd (infPolarEq t b a); auto.
elim H2; intro; clear H2.
absurd (t = b :>Plan); auto.
absurd (s = t :>Plan); auto.
elim H4; intro; clear H4; auto.
rewrite H3 in H2; absurd (infPolarEq t c b); auto.
elim H3; intro; clear H3.
auto.
absurd (s = t :>Plan); auto.
elim H5; intro; clear H5.
rewrite H4; auto.
elim H4; intro; clear H4; auto.
elim H6; intro; clear H6.
rewrite H5 in H4; unfold infPolarEq in H4; auto.
elim H5; intro; clear H5; auto.
Qed.

Lemma infPolarEq_trans_bis :
 forall s t a b c : Plan,
 s <> t ->
 t <> b ->
 infPolarEq s t a ->
 infPolarEq s t b ->
 infPolarEq s t c -> infPolarEq t a b -> infPolarEq t b c -> infPolarEq t a c.
intros.
unfold infPolarEq in |- *.
unfold infPolarEq in H5.
elim H5; intro; clear H5.
elim H4; intro; clear H4.
elim H3; intro; clear H3.
elim H2; intro; clear H2.
elim H1; intro; clear H1.
left; apply (Axiom5bis t s a b c); auto.
elim H2; intro; clear H2; auto.
elim H1; intro; clear H1.
rewrite H2 in H3.
absurd (sensDirect a t b); auto.
absurd (s = t :>Plan); auto.
elim H3; intro; clear H3.
absurd (t = b :>Plan); auto.
elim H2; intro; clear H2.
rewrite H3 in H4.
absurd (sensDirect b t c); auto.
absurd (s = t :>Plan); auto.
elim H4; intro; clear H4; auto.
elim H3; intro; clear H3.
rewrite <- H4; unfold infPolarEq in H1.
elim H1; intro; clear H1; auto.
elim H3; intro; clear H3; auto.
elim H1; intro; clear H1; auto.
absurd (s = t :>Plan); auto.
elim H5; intro; clear H5.
rewrite H4; auto.
elim H4; intro; clear H4; auto.
elim H6; intro; clear H6.
rewrite H5 in H4; unfold infPolarEq in H4; auto.
elim H5; intro; clear H5; auto.
Qed.

(* -------------------- cas de base ------------------------ *)

Definition au_plus_deux_points (cc : CC) :=
  exists a : Plan,
     exists b : Plan, forall p : Plan, in_CC p cc -> p = a \/ p = b.

Definition au_moins_deux_points (cc : CC) :=
  exists
     a : Plan,
     exists b : Plan, in_CC a cc /\ in_CC b cc /\ a <> b.

Lemma plus_de_deux_pas_moins :
 forall (p : Plan) (cc : CC),
 ~ in_CC p cc -> au_plus_deux_points (p :: cc) -> ~ au_moins_deux_points cc.
intros.
red in |- *; intros.
elim H1; clear H1; intros a H1.
elim H1; clear H1; intros b H1.
elim H1; clear H1; intros.
elim H2; clear H2; intros.
elim H0; clear H0; intros c H0.
elim H0; clear H0; intros d H0.
elim (H0 p).
intro.
elim (H0 a).
intro.
rewrite <- H5 in H4; rewrite <- H4 in H1; absurd (in_CC p cc); auto.
intro.
elim (H0 b).
intro.
rewrite <- H6 in H4; rewrite <- H4 in H2; absurd (in_CC p cc); auto.
intro.
rewrite <- H6 in H5; absurd (a = b); auto.
simpl in |- *; auto.
simpl in |- *; auto.
intro.
elim (H0 a).
intro.
elim (H0 b).
intro.
rewrite <- H6 in H5; absurd (a = b); auto.
intro.
rewrite <- H6 in H4; rewrite <- H4 in H2; absurd (in_CC p cc); auto.
simpl in |- *; auto.
intro.
rewrite <- H5 in H4; rewrite <- H4 in H1; absurd (in_CC p cc); auto.
simpl in |- *; auto.
simpl in |- *; auto.
Qed.

(*-------------------------- points_extremaux -------------------------------*)

Inductive points_extremaux : CC -> Plan -> Plan -> Prop :=
  | points_extremaux_nil :
      forall t s : Plan, t <> s -> points_extremaux nil t s
  | points_extremaux_rec :
      forall (cc : CC) (t s p : Plan),
      points_extremaux cc t s ->
      infPolarEq t s p -> points_extremaux (p :: cc) t s.
Hint Resolve points_extremaux_nil points_extremaux_rec.

Lemma points_extremaux_distincts :
 forall (t s : Plan) (cc : CC), points_extremaux cc t s -> t <> s.
intros.
induction cc as [| a cc Hreccc].
inversion H; auto.
apply Hreccc.
inversion H; auto.
Qed.
Hint Resolve points_extremaux_distincts.

Lemma points_extremaux_Prop :
 forall (t s a : Plan) (cc : CC),
 in_CC a cc -> points_extremaux cc t s -> infPolarEq t s a.
intros.
induction cc as [| a0 cc Hreccc].
simpl in H; elim H.
simpl in H.
inversion H0.
clear H5 H4 H2 H1 p s0 t0 cc0.
elim H; intros; clear H.
rewrite <- H1; auto.
apply Hreccc; auto.
Qed.

Lemma points_extremaux_non_ind :
 forall (s t : Plan) (cc : CC),
 t <> s ->
 (forall a : Plan, in_CC a cc -> infPolarEq s t a) -> points_extremaux cc s t.
intros.
induction cc as [| a cc Hreccc].
auto.
apply points_extremaux_rec.
apply Hreccc.
intros.
apply H0.
simpl in |- *; auto.
apply H0; simpl in |- *; auto.
Qed.

Lemma Points_extremaux_trans_max :
 forall (s t a max : Plan) (cc : CC),
 ~ sensDirect t a max ->
 infPolarEq t s max ->
 a <> t ->
 points_extremaux cc max t ->
 points_extremaux (a :: cc) t s -> points_extremaux cc a t.
intros.
induction cc as [| a0 cc Hreccc].
auto.
apply points_extremaux_rec.
apply Hreccc; auto.
inversion H2; auto.
inversion H3.
inversion H6; auto.
apply infPolarEq1.
apply (infPolarEq_trans s t a0 max a); auto.
apply sym_not_eq; apply (points_extremaux_distincts t s (a :: a0 :: cc));
 auto.
apply sym_not_eq; apply (points_extremaux_distincts max t (a0 :: cc)); auto.
inversion H3.
inversion H6; auto.
inversion H3; auto.
inversion H2; auto.
Qed.

Lemma Points_extremaux_trans :
 forall (s t a min : Plan) (cc : CC),
 ~ sensDirect t min a ->
 infPolarEq s t min ->
 a <> t ->
 points_extremaux cc t min ->
 points_extremaux (a :: cc) s t -> points_extremaux cc t a.
intros.
induction cc as [| a0 cc Hreccc].
auto.
apply points_extremaux_rec.
apply Hreccc; auto.
inversion H2; auto.
inversion H3.
inversion H6; auto.
apply (infPolarEq_trans_bis s t a min a0); auto.
apply (points_extremaux_distincts s t (a :: a0 :: cc)); auto.
apply (points_extremaux_distincts t min (a0 :: cc)); auto.
inversion H3; auto.
inversion H3.
inversion H6; auto.
inversion H2; auto.
Qed.

Lemma points_extremaux_sym :
 forall (t s : Plan) (cc : CC),
 points_extremaux cc t s -> points_extremaux cc s t -> au_plus_deux_points cc.
intros.
unfold au_plus_deux_points in |- *.
exists s.
exists t.
intros.
induction cc as [| a cc Hreccc].
elim H1.
elim H1; clear H1; intro.
elim (infPolarEq_egal s t a).
rewrite H1; auto.
intro.
elim H2; clear H2; intro.
absurd (s = t); eauto.
rewrite H1 in H2; auto.
inversion H0; auto.
inversion H; auto.
inversion H; inversion H0; auto.
Qed.

(*--------------------------      CH       -------------------------------*)

Definition CH (cc : CC) : Set :=
  {ts : Plan * Plan |
  in_CC (@fst _ _ ts) cc /\
  in_CC (@snd _ _ ts) cc /\ points_extremaux cc (@fst _ _ ts) (@snd _ _ ts)}.

Definition inj (cc : CC) (e : CH cc) := match e with
                                        | exist _ x p => x
                                        end.
Parameter
  sensDirect_directBoolTrue :
    forall p q r : Plan, sensDirect p q r -> directBool p q r = true.

Definition CH_Prop (cc : CC) (ts : Plan * Plan) :=
  in_CC (@fst _ _ ts) cc /\
  in_CC (@snd _ _ ts) cc /\ points_extremaux cc (@fst _ _ ts) (@snd _ _ ts).

Definition point_extremal (t : Plan) (cc : CC) :=
  (exists s : Plan, CH_Prop cc (s, t)) \/
  (exists r : Plan, CH_Prop cc (t, r)).

Definition dansTriangle (t p q r : Plan) :=
  sensDirect p q t /\ sensDirect q r t /\ sensDirect r p t.
(*--------------------------  Couleur  -------------------------------*)

Inductive CouleurType : Set :=
  | Rouge : CouleurType
  | Bleu : CouleurType.

Definition calculCouleur (a b p : Plan) :=
  match directBool a b p with
  | true => Bleu
  | false => Rouge
  end.

Lemma calculCouleurBleu :
 forall a b p : Plan, calculCouleur a b p = Bleu -> directBool a b p = true.
intros a b p.
unfold calculCouleur in |- *.
CaseEq (directBool a b p).
auto.
discriminate H0.
Qed.

Lemma calculCouleurRouge :
 forall a b p : Plan, calculCouleur a b p = Rouge -> directBool a b p = false.
intros a b p.
unfold calculCouleur in |- *.
CaseEq (directBool a b p).
discriminate H0.
auto.
Qed.

Lemma calculCouleurBleu_inv :
 forall a b p : Plan, directBool a b p = true -> calculCouleur a b p = Bleu.
intros; unfold calculCouleur in |- *.
rewrite H; auto.
Qed.

Lemma calculCouleurRouge_inv :
 forall a b p : Plan, directBool a b p = false -> calculCouleur a b p = Rouge.
intros; unfold calculCouleur in |- *.
rewrite H; auto.
Qed.
Hint Resolve calculCouleurBleu calculCouleurRouge calculCouleurBleu_inv
  calculCouleurRouge_inv.
