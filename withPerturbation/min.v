Require Import List.
Section min_function.
Variables (A : Type) (R : A -> A -> Prop)
  (R_dec : forall x y : A, {R x y} + {~ R x y}).
Variables (P : A -> Prop)
  (trans_in_P :
     forall x y z : A, P x -> P y -> P z -> R x y -> R y z -> R x z)
  (total_in_P : forall x y : A, P x -> P y -> R x y \/ R y x \/ x = y).

Theorem all_sat_step_inv :
 forall (Q : A -> Prop) (a : A) (l : list A),
 (forall x : A, In x (a :: l) -> Q x) -> forall x : A, In x l -> Q x.
intros Q a l Hinc x Hin; apply Hinc; right; auto.
Qed.

Fixpoint min (a : A) (l : list A) {struct l} : A :=
  match l with
  | nil => a
  | b :: tl =>
      match R_dec b a with
      | left H => min b tl
      | right H' => min a tl
      end
  end.

Theorem min_in : forall (a : A) (l : list A), a = min a l \/ In (min a l) l.
intros a l; generalize a; clear a; elim l; auto.
intros b tl Hrec a; simpl in |- *.
case (R_dec b a).
elim (Hrec b); auto.
elim (Hrec a); auto.
Qed.

Theorem min_in2 : forall (a : A) (l : list A), In a l -> In (min a l) l.
intros a l H; elim (min_in a l); auto.
intros H1; rewrite <- H1; auto.
Qed.

Theorem min_R_current :
 forall (a : A) (l : list A),
 (forall x y : A, In x l -> R x y -> P x) ->
 P a -> R (min a l) a \/ min a l = a.
intros a l; generalize a; clear a; elim l; auto.
intros b tl Hrec a Hincl Hin.
simpl in |- *.
case (R_dec b a).
intros Hr.
cut (P b).
intros Hinb.
elim (Hrec b); auto.
intros Hrec'; left; apply trans_in_P with b; auto.
apply Hincl with (y := b); auto.
simpl in |- *; apply min_in; auto.
intros H'; rewrite H'; auto.
intros x y H' H'0; apply Hincl with (y := y); auto.
right; auto.
apply Hincl with (y := a); simpl in |- *; auto.
intros; apply Hrec; auto.
intros x y; intros; apply Hincl with (y := y); simpl in |- *; auto.
Qed.

Theorem min_R_all :
 forall (a : A) (l : list A),
 (forall x y : A, In x l -> R x y -> P x) ->
 P a -> forall x : A, In x l -> P x -> R (min a l) x \/ min a l = x.
intros a l; generalize a; clear a; elim l.
intros a H' H'0 x H'1; elim H'1.
intros b tl Hrec a Hincl HPa x Hinx Hpx.
elim Hinx.
intros H'; generalize Hpx; rewrite <- H'; intros Hpb.
simpl in |- *.
case (R_dec b a).
intros H'0; apply min_R_current; auto.
intros; eapply Hincl; simpl in |- *; eauto.
elim (min_R_current a tl); eauto.
intros Hrmin Hnr.
elim (total_in_P a b); auto.
intros Hr; left; apply trans_in_P with a; auto.
elim (min_in a tl).
intros H'1; rewrite <- H'1; auto.
intros; eapply Hincl; simpl in |- *; eauto.
intros Hdisj; elim Hdisj; [ intros Hr | intros Heq ].
elim Hnr; assumption.
rewrite <- Heq; auto.
elim (total_in_P a b); auto.
intros H'0 H'1; rewrite H'1; auto.
intros Hdisj; elim Hdisj; [ intros Hr | intros Heq ].
intros H'0 H'1; apply H'1 || case H'1; auto.
intros H'0; rewrite H'0; auto.
intros; eapply Hincl; simpl in |- *; eauto.
intros H'0; simpl in |- *; case (R_dec b a).
intros H'1; apply Hrec; auto.
intros; eapply Hincl; simpl in |- *; eauto.
eapply Hincl; simpl in |- *; eauto.
intros H'1; apply Hrec; auto.
intros; eapply Hincl; simpl in |- *; eauto.
Qed.
End min_function.