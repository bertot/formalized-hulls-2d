Require Export reels lemmas.

Module pol_sign(A:OField).
Module D := det_lemmas(A).
Import D KA P C A.

Section polynomial.
Variable a b c : K.
Hint Resolve Gt_stable_plus Gt_stable_mult inv_sign Gt_one_zero.
 
Theorem Gt_mult_inv :
 forall x y z,
 Gt x zero ->
 Gt z zero ->
 Gt (abs z) (Ktimes (abs y) x) -> Gt (Kplus (Ktimes y x) z) zero.
intros x y z Gtx H' H'1.
case (ordre_decidable zero y).
intros H'0.
replace (Kplus (Ktimes y x) z) with
 (Kplus z (Koppose (Ktimes (Koppose y) x))); try ring.
apply Gt_zero_GT.
rewrite <- abs_pos_eq in H'1; auto.
rewrite abs_neg_eq in H'1; auto.
intros H'2; generalize (non_Gt_Ge _ _ H'2).
intros; rewrite <- abs_pos_eq in H'1; auto.
Qed.
 
Theorem Gt_2_zero : Gt (Kplus one one) zero.
auto.
Qed.
Hint Resolve Gt_2_zero Gt_mult_inv.
 
Theorem div2_pos :
 forall x,
 Gt x zero -> Gt (Ktimes x (Kinv (Kplus one one))) zero.
auto.
Qed.
Hint Resolve div2_pos.
 
Theorem abs_zero_eq : forall x, abs x = zero -> x = zero.
intros x; unfold abs in |- *; case (ordre_decidable zero x); intros H' H'0.
rewrite <- (Kopp_opp x); rewrite H'0; ring.
auto.
Qed.
Hint Resolve inv_non_nul abs_zero_eq abs_gt.
 
Theorem Gt_bound_zero :
 forall x y,
 x <> zero ->
 y <> zero ->
 Gt (abs (Ktimes (Ktimes x (Kinv (Kplus one one))) (Kinv y))) zero.
intros x y H' H'y.
elim (abs_ge (Ktimes (Ktimes x (Kinv (Kplus one one))) (Kinv y))); auto.
repeat rewrite abs_mult.
intros H'0; elim K_integre with (1 := H'0); auto.
clear H'0; intros H'0.
elim K_integre with (1 := H'0).
intros H'1; case H'; auto.
intros; elim (Gt_not_eq (Kinv (Kplus one one)) zero); auto.
intros; elim inv_non_nul with (1 := H'y); auto.
Qed.
 
Theorem Gt_mv_inv :
 forall x y z,
 Gt y zero -> Gt (Ktimes x (Kinv y)) z -> Gt x (Ktimes y z).
intros x y z H' H'0.
replace x with (Ktimes y (Ktimes x (Kinv y))).
apply Gt_Gt_zero.
replace (Kplus (Ktimes y (Ktimes x (Kinv y))) (Koppose (Ktimes y z))) with
 (Ktimes (Kplus (Ktimes x (Kinv y)) (Koppose z)) y); 
 try ring.
apply Gt_stable_mult; auto.
apply Gt_zero_GT; auto.
replace (Ktimes y (Ktimes x (Kinv y))) with (Ktimes x (Ktimes y (Kinv y)));
 try (ring; fail).
rewrite (Kinv_def y).
ring.
auto.
Qed.
 
Theorem smaller_square :
 forall x, Gt one x -> Gt x zero -> Gt one (Ktimes x x).
intros x H' H'0.
apply Gt_trans with (Ktimes one x).
pattern one at 1 in |- *; replace one with (Ktimes one one).
apply Gt_Gt_zero.
replace (Kplus (Ktimes one one) (Koppose (Ktimes one x))) with
 (Ktimes (Kplus one (Koppose x)) one); try ring.
apply Gt_stable_mult; auto.
apply Gt_zero_GT; auto.
ring.
apply Gt_Gt_zero.
replace (Kplus (Ktimes one x) (Koppose (Ktimes x x))) with
 (Ktimes (Kplus one (Koppose x)) x); try ring.
apply Gt_stable_mult; auto.
apply Gt_zero_GT; auto.
Qed.
 
Theorem decompose_Gt_sum :
 forall x y z,
 Gt (Kplus (Ktimes x (Kinv (Kplus one one))) y) zero ->
 Gt (Kplus (Ktimes x (Kinv (Kplus one one))) z) zero ->
 Gt (Kplus x (Kplus y z)) zero.
intros x y z H' H'0.
replace x with
 (Kplus (Ktimes x (Kinv (Kplus one one))) (Ktimes x (Kinv (Kplus one one)))).
replace
 (Kplus
    (Kplus (Ktimes x (Kinv (Kplus one one)))
       (Ktimes x (Kinv (Kplus one one)))) (Kplus y z)) with
 (Kplus (Kplus (Ktimes x (Kinv (Kplus one one))) y)
    (Kplus (Ktimes x (Kinv (Kplus one one))) z)); try ring.
apply Gt_stable_plus; auto.
replace
 (Kplus (Ktimes x (Kinv (Kplus one one))) (Ktimes x (Kinv (Kplus one one))))
 with (Ktimes (Ktimes (Kplus one one) (Kinv (Kplus one one))) x);
 try (ring; fail).
rewrite Kinv_def.
ring.
auto.
Qed.
 
Theorem smaller_one_smaller_square :
 forall x, Gt one x -> Gt x zero -> Gt x (Ktimes x x).
intros x H' H'0.
pattern x at 1 in |- *; replace x with (Ktimes one x).
apply Gt_Gt_zero.
replace (Kplus (Ktimes one x) (Koppose (Ktimes x x))) with
 (Ktimes (Kplus one (Koppose x)) x); try ring.
apply Gt_stable_mult; auto.
apply Gt_zero_GT; auto.
ring.
Qed.
 
Theorem not_two_zero : Kplus one one <> zero.
apply Gt_not_eq; auto.
Qed.
Hint Resolve not_two_zero.
 
Theorem th1 :
 Gt a zero ->
 b <> zero ->
 exists e,
   Gt e zero /\
   (forall x,
    Gt x zero ->
    Gt e x ->
    Gt (Kplus a (Kplus (Ktimes b x) (Ktimes c (Ktimes x x)))) zero).
intros H' H'0.
cut (b <> zero); auto.
cut (a <> zero); auto.
intros H'1 H'2.
case (egal_decidable c zero).
intros H'3; exists (abs (Ktimes a (Kinv b))); split;
 [ idtac | intros x H'4 H'5 ].
rewrite abs_mult.
rewrite <- (abs_pos_eq a); auto.
rewrite H'3.
apply Gt_Gt_zero.
replace
 (Kplus (Kplus a (Kplus (Ktimes b x) (Ktimes zero (Ktimes x x))))
    (Koppose zero)) with
 (Kplus (Kplus a (Ktimes b x))
    (Koppose (Kplus a (Ktimes (abs b) (Koppose (abs (Ktimes a (Kinv b))))))));
 try (ring; fail).
apply Gt_zero_GT; apply Gt_plus.
case (ordre_decidable b zero).
intros H'6; rewrite <- (abs_pos_eq b); auto.
rewrite abs_mult.
rewrite <- (abs_pos_eq a); auto.
rewrite abs_inv_eq; auto.
rewrite <- (abs_pos_eq b); auto.
apply Gt_Gt_zero.
replace (Koppose (Ktimes b (Koppose (Ktimes a (Kinv b))))) with a.
auto.
replace (Koppose (Ktimes b (Koppose (Ktimes a (Kinv b))))) with
 (Koppose (Koppose (Ktimes a (Ktimes b (Kinv b))))); 
 try (ring; fail).
rewrite (Kinv_def b).
ring.
auto.
intros H'6.
elim (non_Gt_Ge b zero).
intros H'7.
rewrite abs_mult.
rewrite <- (abs_pos_eq a); auto.
rewrite abs_inv_eq; auto.
rewrite abs_neg_eq; auto.
apply Gt_Gt_zero.
replace
 (Koppose (Ktimes (Koppose b) (Koppose (Ktimes a (Kinv (Koppose b)))))) with
 a.
apply Gt_mult_inv; auto.
apply Gt_mv_inv.
apply abs_gt.
apply sym_not_equal; apply Gt_not_eq.
auto.
rewrite <- abs_inv_eq; auto.
rewrite <- abs_mult; auto.
match goal with |- _ = ?c =>
  replace c with (Ktimes a (Ktimes (Koppose b) (Kinv (Koppose b)))) by ring
end.
rewrite Kinv_def; auto;  ring. 
intros H'7; case H'2; auto.
auto.
rewrite abs_mult.
rewrite abs_inv_eq; auto.
rewrite Kmult_Koppose_permute.
rewrite (Rmul_assoc RT).
rewrite (Rmul_comm RT (abs b)).
rewrite <- (Rmul_assoc RT).
generalize (not_abs_zero b).
intros H'6; try assumption.
rewrite (Kinv_def (abs b)).
rewrite <- abs_pos_eq; auto.
ring.
auto.
intros H'3; try assumption.
case
 (ordre_decidable (abs (Ktimes (Ktimes a (Kinv (Kplus one one))) (Kinv b)))
    one).
case
 (ordre_decidable (abs (Ktimes (Ktimes a (Kinv (Kplus one one))) (Kinv c)))
    one).
intros H'4 H'5; exists one; split;
 [ idtac | intros x H'6 H'7; try assumption ].
apply Gt_one_zero.
replace a with
 (Kplus (Ktimes a (Kinv (Kplus one one))) (Ktimes a (Kinv (Kplus one one)))).
rewrite
 (Radd_assoc RT
    (Kplus (Ktimes a (Kinv (Kplus one one)))
       (Ktimes a (Kinv (Kplus one one)))) (Ktimes b x)
    (Ktimes c (Ktimes x x))).
rewrite <-
 (Radd_assoc RT (Ktimes a (Kinv (Kplus one one)))
    (Ktimes a (Kinv (Kplus one one))) (Ktimes b x))
 .
rewrite (Radd_comm RT (Ktimes a (Kinv (Kplus one one))) (Ktimes b x)).
rewrite <-
 (Radd_assoc RT (Ktimes a (Kinv (Kplus one one)))
    (Kplus (Ktimes b x) (Ktimes a (Kinv (Kplus one one))))
    (Ktimes c (Ktimes x x))).
rewrite <-
 (Radd_assoc RT (Ktimes b x) (Ktimes a (Kinv (Kplus one one)))
    (Ktimes c (Ktimes x x))).
rewrite
 (Radd_assoc RT (Ktimes a (Kinv (Kplus one one))) 
    (Ktimes b x)
    (Kplus (Ktimes a (Kinv (Kplus one one))) (Ktimes c (Ktimes x x))))
 .
apply Gt_stable_plus.
rewrite (Radd_comm RT (Ktimes a (Kinv (Kplus one one))) (Ktimes b x)).
apply Gt_mult_inv; auto.
apply Gt_mv_inv; auto.
rewrite <- abs_inv_eq; auto.
rewrite <- abs_mult.
apply Gt_trans with one; auto.
rewrite
 (Radd_comm RT (Ktimes a (Kinv (Kplus one one))) (Ktimes c (Ktimes x x)))
 .
apply Gt_mult_inv; auto.
apply Gt_mv_inv; auto.
rewrite <- abs_inv_eq; auto.
rewrite <- abs_mult.
apply Gt_trans with one; auto.
apply smaller_square; auto.
replace
 (Kplus (Ktimes a (Kinv (Kplus one one))) (Ktimes a (Kinv (Kplus one one))))
 with (Ktimes a (Ktimes (Kplus one one) (Kinv (Kplus one one))));
 try (ring; fail).
rewrite (Kinv_def (Kplus one one)).
ring.
auto.
intros H'4 H'5;
 exists (abs (Ktimes (Ktimes a (Kinv (Kplus one one))) (Kinv c))); 
 split; [ idtac | intros x H'6 H'7; try assumption ].
apply Gt_bound_zero; auto.
apply decompose_Gt_sum.
rewrite (Radd_comm RT (Ktimes a (Kinv (Kplus one one))) (Ktimes b x)).
apply Gt_mult_inv; auto.
apply Gt_mv_inv; auto.
rewrite <- abs_inv_eq; auto.
rewrite <- abs_mult.
apply Gt_trans with one; auto.
apply
 Ge_Gt_trans with (abs (Ktimes (Ktimes a (Kinv (Kplus one one))) (Kinv c)));
 auto.
rewrite
 (Radd_comm RT (Ktimes a (Kinv (Kplus one one))) (Ktimes c (Ktimes x x)))
 .
apply Gt_mult_inv; auto.
apply Gt_mv_inv; auto.
rewrite <- abs_inv_eq; auto.
rewrite <- abs_mult.
apply Gt_trans with x; auto.
apply smaller_one_smaller_square; auto.
apply
 Ge_Gt_trans with (abs (Ktimes (Ktimes a (Kinv (Kplus one one))) (Kinv c)));
 auto.
intros H'4.
case
 (ordre_decidable (abs (Ktimes (Ktimes a (Kinv (Kplus one one))) (Kinv c)))
    (abs (Ktimes (Ktimes a (Kinv (Kplus one one))) (Kinv b)))).
intros H'5; exists (abs (Ktimes (Ktimes a (Kinv (Kplus one one))) (Kinv b)));
 split; [ idtac | intros x H'6 H'7 ].
apply Gt_bound_zero; auto.
apply decompose_Gt_sum.
rewrite (Radd_comm RT (Ktimes a (Kinv (Kplus one one))) (Ktimes b x)).
apply Gt_mult_inv; auto.
apply Gt_mv_inv; auto.
rewrite <- abs_inv_eq; auto.
rewrite <- abs_mult; auto.
rewrite
 (Radd_comm RT (Ktimes a (Kinv (Kplus one one))) (Ktimes c (Ktimes x x)))
 .
apply Gt_mult_inv; auto.
apply Gt_mv_inv; auto.
rewrite <- abs_inv_eq; auto.
rewrite <- abs_mult.
apply Gt_trans with x.
apply Gt_trans with (1 := H'5); auto.
apply smaller_one_smaller_square; auto.
apply
 Ge_Gt_trans with (abs (Ktimes (Ktimes a (Kinv (Kplus one one))) (Kinv b)));
 auto.
intros H'5; exists (abs (Ktimes (Ktimes a (Kinv (Kplus one one))) (Kinv c)));
 split; [ idtac | intros x H'6 H'7 ].
apply Gt_bound_zero; auto.
apply decompose_Gt_sum.
rewrite (Radd_comm RT (Ktimes a (Kinv (Kplus one one))) (Ktimes b x)).
apply Gt_mult_inv; auto.
apply Gt_mv_inv; auto.
rewrite <- abs_inv_eq; auto.
rewrite <- abs_mult.
apply Ge_Gt_trans with (2 := H'7); auto.
rewrite
 (Radd_comm RT (Ktimes a (Kinv (Kplus one one))) (Ktimes c (Ktimes x x)))
 .
apply Gt_mult_inv; auto.
apply Gt_mv_inv; auto.
rewrite <- abs_inv_eq; auto.
rewrite <- abs_mult.
apply Gt_trans with x; auto.
apply smaller_one_smaller_square; auto.
apply
 Ge_Gt_trans with (abs (Ktimes (Ktimes a (Kinv (Kplus one one))) (Kinv c)));
 auto.
apply Ge_trans with (abs (Ktimes (Ktimes a (Kinv (Kplus one one))) (Kinv b)));
 auto.
Qed.
 
Theorem th2 :
 Gt a zero ->
 b = zero ->
 ex
   (fun e =>
    Gt e zero /\
    (forall x,
     Gt x zero ->
     Gt e x ->
     Gt (Kplus a (Kplus (Ktimes b x) (Ktimes c (Ktimes x x)))) zero)).
intros H' H'0; try assumption.
elim (egal_decidable c zero).
intros H'1; exists one; split; [ apply Gt_one_zero | intros x H'2 H'3 ].
rewrite H'1.
rewrite H'0.
replace (Kplus a (Kplus (Ktimes zero x) (Ktimes zero (Ktimes x x)))) with a;
 auto.
ring.
elim (ordre_decidable (abs (Ktimes a (Kinv c))) one).
intros H'1 Hneq; exists one; split;
 [ idtac | intros x H'2 H'3; try assumption ].
apply Gt_one_zero.
rewrite H'0.
rewrite (mult_zero x).
rewrite (Radd_0_l RT (Ktimes c (Ktimes x x))).
rewrite (Radd_comm RT a (Ktimes c (Ktimes x x))).
apply Gt_mult_inv; auto.
apply Gt_mv_inv.
apply abs_gt.
auto.
apply Gt_trans with one.
rewrite <- abs_inv_eq.
rewrite <- abs_mult; auto.
auto.
apply smaller_square.
auto.
auto.
intros H'1 H'2; exists (Ktimes a (Kinv (abs c))); split;
 [ idtac | intros x H'3 H'4; try assumption ].
apply Gt_stable_mult.
auto.
apply inv_sign.
apply abs_gt.
auto.
rewrite H'0.
rewrite (mult_zero x).
rewrite (Radd_0_l RT (Ktimes c (Ktimes x x))).
rewrite (Radd_comm RT a (Ktimes c (Ktimes x x))).
apply Gt_mult_inv; auto.
apply Gt_mv_inv.
apply abs_gt.
auto.
apply Gt_trans with x.
rewrite <- (abs_pos_eq a).
auto.
auto.
apply smaller_one_smaller_square.
apply Ge_Gt_trans with (abs (Ktimes a (Kinv c))).
auto.
rewrite abs_mult.
rewrite <- (abs_pos_eq a).
rewrite abs_inv_eq; auto.
auto.
auto.
Qed.
 
Theorem th3 :
 a = zero ->
 Gt b zero ->
 ex
   (fun e =>
    Gt e zero /\
    (forall x,
     Gt x zero ->
     Gt e x ->
     Gt (Kplus a (Kplus (Ktimes b x) (Ktimes c (Ktimes x x)))) zero)).
intros H' H'0; try assumption.
elim (egal_decidable c zero).
intros H'1; exists one.
split.
apply Gt_one_zero.
rewrite H'; rewrite H'1.
intros x H'2 H'3; try assumption.
rewrite (Radd_0_l RT).
rewrite mult_zero.
rewrite (Radd_comm RT).
rewrite (Radd_0_l RT).
apply Gt_stable_mult; auto.
elim (ordre_decidable (abs (Ktimes b (Kinv c))) one).
intros H'1 H'2; exists one; split;
 [ idtac | intros x H'3 H'4; try assumption ].
apply Gt_one_zero.
rewrite H'.
rewrite (Radd_0_l RT).
rewrite (Rmul_assoc RT c x x).
rewrite <- (Rdistr_l RT b (Ktimes c x) x).
apply Gt_stable_mult; auto.
rewrite (Radd_comm RT b (Ktimes c x)).
apply Gt_mult_inv; auto.
apply Gt_mv_inv; auto.
apply Gt_trans with one; auto.
rewrite <- abs_inv_eq; auto.
rewrite <- abs_mult; auto.
intros H'1 H'2; exists (Ktimes b (Kinv (abs c))); split;
 [ idtac | intros x H'3 H'4 ].
apply Gt_stable_mult; auto.
rewrite H'.
rewrite (Radd_0_l RT).
rewrite (Rmul_assoc RT c x x).
rewrite <- (Rdistr_l RT b (Ktimes c x) x).
apply Gt_stable_mult; auto.
rewrite (Radd_comm RT b (Ktimes c x)).
apply Gt_mult_inv; auto.
apply Gt_mv_inv; auto.
rewrite <- (abs_pos_eq b); auto.
Qed.
End polynomial.
Section polynomial2.
Variable a b c: K.
Hypothesis not_all_null : ~ (a = zero /\ b = zero /\ c = zero).
 
Theorem not_oppose_zero_not_zero :
 forall x, x <> zero -> Koppose x <> zero.
intros x H; red in |- *; intros H1; elim H;
 replace zero with (Kplus x (Koppose x)); try (ring; fail).
rewrite H1; ring.
Qed.
 
Theorem polynomial_sign :
 {(exists e,
     Gt e zero /\
     (forall x,
      Gt x zero ->
      Gt e x ->
      Gt (Kplus a (Kplus (Ktimes b x) (Ktimes c (Ktimes x x)))) zero))} +
 {(exists e,
     Gt e zero /\
     (forall x,
      Gt x zero ->
      Gt e x ->
      Gt zero (Kplus a (Kplus (Ktimes b x) (Ktimes c (Ktimes x x))))))}.
case (egal_decidable a zero).
case (egal_decidable b zero).
case (ordre_decidable c zero).
intros H'1 H' H'0; left; exists one.
split; [ apply Gt_one_zero | intros x H'2 H'3; rewrite H'; rewrite H'0 ].
replace (Kplus zero (Kplus (Ktimes zero x) (Ktimes c (Ktimes x x)))) with
 (Ktimes c (Ktimes x x)); try ring.
auto.
intros H'1 H' H'0; right; exists one; split;
 [ apply Gt_one_zero | intros x H'2 H'3; rewrite H'; rewrite H'0 ].
replace (Kplus zero (Kplus (Ktimes zero x) (Ktimes c (Ktimes x x)))) with
 (Ktimes c (Ktimes x x)); try ring.
apply Gt_Koppose_Koppose2.
rewrite oppose_zero.
replace (Koppose (Ktimes c (Ktimes x x))) with
 (Ktimes (Koppose c) (Ktimes x x)); try ring.
apply Gt_stable_mult; auto.
elim (Ordre_total c zero).
intros; case H'1; auto.
intros H'4; elim H'4; intros H'5; auto.
elim not_all_null; auto.
intros H' H'0.
elim (ordre_decidable b zero).
intros H'1; left.
elim (th3 a b c); auto.
intros e H'2; elim H'2; exists e; auto.
intros H''1; right; elim (ordre_decidable zero b); intros H'2.
elim (th3 (Koppose a) (Koppose b) (Koppose c)).
intros e H'1; elim H'1; intros H'3 H'4; clear H'1.
exists e; split; auto.
intros x H'1 H'5; try assumption.
apply Gt_Koppose_Koppose2; rewrite oppose_zero.
replace (Koppose (Kplus a (Kplus (Ktimes b x) (Ktimes c (Ktimes x x))))) with
 (Kplus (Koppose a)
    (Kplus (Ktimes (Koppose b) x) (Ktimes (Koppose c) (Ktimes x x))));
 try ring.
auto.
rewrite H'0; apply oppose_zero.
auto.
elim H'; elim (Ordre_total b zero).
intros H'1; case H''1; auto.
intros H'1; elim H'1; intros H'3; auto.
case H'2; auto.
intros H'.
case (ordre_decidable a zero).
intros H'0; left.
elim (egal_decidable b zero).
intros Heq; elim (th2 a b c); auto.
intros e H'1; elim H'1; intros H'2 H'3; exists e; auto.
intros Hneq; elim (th1 a b c); auto.
intros e H'1; elim H'1; exists e; auto.
intros H''0; right.
case (ordre_decidable zero a); intros H'1.
elim (egal_decidable b zero).
intros H'0; elim (th2 (Koppose a) (Koppose b) (Koppose c)); auto.
intros e H'2; elim H'2; exists e; split; auto.
intros x H'3 H'4.
apply Gt_Koppose_Koppose2; rewrite oppose_zero.
replace (Koppose (Kplus a (Kplus (Ktimes b x) (Ktimes c (Ktimes x x))))) with
 (Kplus (Koppose a)
    (Kplus (Ktimes (Koppose b) x) (Ktimes (Koppose c) (Ktimes x x))));
 try ring.
auto.
rewrite H'0; apply oppose_zero.
intros H'0; elim (th1 (Koppose a) (Koppose b) (Koppose c)).
intros e H'2; elim H'2; intros H'3 H'4; exists e; split; auto.
intros x H'5 H'6; try assumption.
apply Gt_Koppose_Koppose2; rewrite oppose_zero.
replace (Koppose (Kplus a (Kplus (Ktimes b x) (Ktimes c (Ktimes x x))))) with
 (Kplus (Koppose a)
    (Kplus (Ktimes (Koppose b) x) (Ktimes (Koppose c) (Ktimes x x))));
 try ring.
auto.
auto.
apply not_oppose_zero_not_zero; auto.
elim H'; elim (Ordre_total a zero).
intros H'2; elim H''0; auto.
intros H'2; elim H'2; auto.
intros H'3; elim H'1; auto.
Defined.
 
Definition polynomial_sign_bool :=
  match polynomial_sign with
  | left h => true
  | right h => false
  end.
End polynomial2.

End pol_sign.