(* AddPath "$HOME/ctcoq/stage". *)
Require Export minPolar.
Require Import Classical.
Require Import List.
 
 
Fixpoint maxPolarRec (t maxDep : Plan) (cc : CC) {struct cc} : Plan :=
  match cc with
  | nil => maxDep
  | a :: l =>
      (fun max : Plan =>
       match directBool t max a with
       | true => a
       | false => max
       end) (maxPolarRec t maxDep l)
  end.
 
Lemma maxPolarRec_dans_cc :
 forall (t max : Plan) (cc : CC), in_CC (maxPolarRec t max cc) (max :: cc).
intros.
induction cc as [| a cc Hreccc].
simpl in |- *; auto.
simpl in |- *.
 CaseEq (directBool t (maxPolarRec t max cc) a).
auto.
elim Hreccc; auto.
Qed.
 
Theorem maxPolarRecOK :
 forall (cc : CC) (s t max : Plan),
 t <> max ->
 points_extremaux (max :: cc) t s ->
 points_extremaux cc (maxPolarRec t max cc) t.
intros.
induction cc as [| a cc Hreccc].
simpl in |- *; auto.
cut (points_extremaux cc (maxPolarRec t max cc) t).
intros HypRec; clear Hreccc.
simpl in |- *.
 CaseEq (directBool t (maxPolarRec t max cc) a).
apply points_extremaux_rec; auto.
apply (Points_extremaux_trans_max s t a (maxPolarRec t max cc) cc); auto.
cut (in_CC (maxPolarRec t max cc) (max :: cc)).
intros.
simpl in H2; elim H2; intros; clear H2.
rewrite <- H3; inversion H0; auto.
apply (points_extremaux_Prop t s (maxPolarRec t max cc) cc); auto.
inversion H0.
inversion H5; auto.
apply maxPolarRec_dans_cc.
apply (direct_distincts a t (maxPolarRec t max cc)); auto.
inversion H0; auto.
apply points_extremaux_rec; auto.
apply Hreccc.
inversion H0.
inversion H3; auto.
Qed.
 
Lemma maxPolarRec_non_t :
 forall (t max : Plan) (cc : CC), t <> max -> t <> maxPolarRec t max cc.
intros.
induction cc as [| a cc Hreccc].
simpl in |- *; auto.
simpl in |- *.
 CaseEq (directBool t (maxPolarRec t max cc) a).
apply sym_not_eq; apply (direct_distincts a t (maxPolarRec t max cc)); auto.
auto.
Qed.
 
Theorem maxPolarRecOK_max :
 forall (cc : CC) (s t max : Plan),
 t <> max ->
 points_extremaux (max :: cc) t s -> infPolarEq t max (maxPolarRec t max cc).
intros.
induction cc as [| a cc Hreccc].
simpl in |- *; auto.
simpl in |- *.
cut (infPolarEq t max (maxPolarRec t max cc)).
intros HypRec; clear Hreccc.
 CaseEq (directBool t (maxPolarRec t max cc) a).
apply (infPolarEq_trans s t max (maxPolarRec t max cc) a); auto.
apply sym_not_eq; apply (points_extremaux_distincts t s (max :: a :: cc));
 auto.
apply maxPolarRec_non_t; auto.
inversion H0; auto.
cut (in_CC (maxPolarRec t max cc) (max :: cc)).
intros.
simpl in H2; elim H2; intros; clear H2.
rewrite <- H3; inversion H0; auto.
apply (points_extremaux_Prop t s (maxPolarRec t max cc) cc); auto.
inversion H0.
inversion H5; auto.
apply maxPolarRec_dans_cc; auto.
inversion H0.
inversion H4; auto.
auto.
inversion H0.
inversion H3.
auto.
Qed.
 
Fixpoint maxPolar (t : Plan) (cc : CC) {struct cc} : Plan :=
  match cc with
  | nil => t
  | a :: l =>
      match egalBool a t with
      | true => maxPolar t l
      | false => maxPolarRec t a l
      end
  end.
 
Theorem maxPolarOK :
 forall (t s : Plan) (cc : CC),
 (exists a : Plan, a <> t /\ in_CC a cc) ->
 points_extremaux cc t s -> points_extremaux cc (maxPolar t cc) t.
intros.
elim H; intros.
elim H1; (intros; clear H H1).
induction cc as [| a cc Hreccc].
auto.
simpl in |- *.
 CaseEq (egalBool a t).
elim H3; clear H3; intros.
rewrite <- H1 in H2; absurd (a = t); auto.
apply points_extremaux_rec.
inversion H0; auto.
cut (a = t); auto.
apply points_extremaux_rec.
apply (maxPolarRecOK cc s t a); auto.
apply sym_not_eq; auto.
apply infPolarEq1; apply (maxPolarRecOK_max cc s t a); auto.
apply sym_not_eq; auto.
Qed.
 
Lemma maxPolar_dans_cc :
 forall (cc : CC) (t : Plan),
 (exists a : Plan, a <> t /\ in_CC a cc) -> in_CC (maxPolar t cc) cc.
intros.
elim H; intros.
elim H0; intros; clear H H0.
induction cc as [| a cc Hreccc].
simpl in H2; elim H2; auto.
simpl in |- *.
 CaseEq (egalBool a t).
simpl in H2; elim H2; intros; clear H2; auto.
rewrite <- H0 in H1; absurd (a = t); auto.
cut (in_CC (maxPolarRec t a cc) (a :: cc)).
intros H3; simpl in H3; auto.
apply maxPolarRec_dans_cc; auto.
Qed.
 
Theorem points_extremal_precedent :
 forall (cc : CC) (s t : Plan),
 CH_Prop cc (t, s) -> exists r : Plan, CH_Prop cc (r, t).
intros.
unfold CH_Prop in H; simpl in H; elim H; intros; clear H.
elim H1; intros; clear H1.
exists (maxPolar t cc).
cut (s <> t).
intro.
unfold CH_Prop in |- *; split; simpl in |- *.
apply maxPolar_dans_cc.
exists s; auto.
split; auto.
apply (maxPolarOK t s cc).
exists s; auto.
auto.
apply sym_not_eq; apply (points_extremaux_distincts t s cc); auto.
Qed.
 