(* In this file we describe the function first_two_points that is to be used
  by the wrapping algorithm defined in hulls. *)
Require Export hulls.
Require Export axiomsKnuth.
Require Export lemmas.
Require Export min.
Require Export perturbations.
 
Module extr(A:OField).
Module PE := perturb(A).
Import PE DA PS D KA P C A.

(* One of the two points will be the minimal point in the lexicographical ordering.*)
Definition infLex (a b : Plane) :=
  Gt (abscisse b) (abscisse a) \/
  abscisse a = abscisse b /\ Gt (ordonnee b) (ordonnee a).
 
Definition infLex_dec :
  forall t1 t2 : Plane, {infLex t1 t2} + {~ infLex t1 t2}.
intros t1 t2; case (ordre_decidable (abscisse t2) (abscisse t1)); intros H.
left; left; auto.
case (egal_decidable (abscisse t1) (abscisse t2)); intros H0.
case (ordre_decidable (ordonnee t2) (ordonnee t1)); intros H1.
left; right; auto.
right; red in |- *; intros H2; elim H2; clear H2; intros H2.
elim Gt_antisym with (1 := H2); generalize H2; rewrite H0; auto.
elim H2; intros H' H'0; apply H1; auto.
right; red in |- *; intros H'; elim H'; intros H'0.
apply H; auto.
elim H'0; intros H'1 H'2; apply H0; auto.
Qed.
 
Definition plan_eq_dec : forall p1 p2 : Plane, {p1 = p2} + {p1 <> p2}.
intros p1; case p1; intros x1 y1 p2; case p2; intros x2 y2.
case (egal_decidable x1 x2).
intros H'; rewrite H'.
case (egal_decidable y1 y2).
intros H'0; rewrite H'0; auto.
intros H'0; right; red in |- *; intros H'1; injection H'1.
intros; apply H'0; auto.
intros H'; right; red in |- *; intros H'0; injection H'0.
intros; apply H'; auto.
Qed.
 
Lemma infLex_trans :
 forall a b c : Plane, infLex a b -> infLex b c -> infLex a c.
intros.
elim H0; clear H0; intros.
elim H; clear H; intros.
left; apply Gt_trans with (abscisse b); auto.
elim H; clear H; intros.
left; rewrite H; auto.
elim H0; clear H0; intros.
elim H; clear H; intros.
left; rewrite <- H0; auto.
elim H; clear H; intros.
right; split.
rewrite H; auto.
apply Gt_trans with (ordonnee b); auto.
Qed.
 
Lemma non_infLex : forall a b : Plane, ~ infLex a b -> infLex b a \/ b = a.
intros.
elim (Ordre_total (abscisse a) (abscisse b)); intro.
left; left; auto.
elim H0; clear H0; intros.
elim (Ordre_total (ordonnee a) (ordonnee b)); intro.
left; right; auto.
elim H1; clear H1; intros.
right; CaseEq a; intros r r0 H2; CaseEq b; intros r1 t2 H3.
rewrite H3 in H1; rewrite H2 in H1; rewrite H3 in H0; rewrite H2 in H0;
 simpl in H1; simpl in H0.
rewrite H0; rewrite H1; auto.
elim H; right; auto.
elim H; left; auto.
Qed.
 
Theorem infLex_total : forall a b : Plane, infLex a b \/ infLex b a \/ a = b.
intros a b; elim (infLex_dec a b); auto.
intros H; right; elim (non_infLex _ _ H); auto.
Qed.
(* The point that is minimal for lexicographical ordering is computed using the generic min function. *)
 
Definition minLexico :
  forall (t : Plane) (l : list Plane),
  {v : Plane |
  In v (t :: l) /\ (forall x : Plane, In x (t :: l) -> x = v \/ infLex v x)}.
intros t l; exists (min Plane infLex infLex_dec t l).
split.
simpl in |- *; apply min_in.
intros x Hinx; elim Hinx.
intros Heq; rewrite <- Heq; generalize (min_R_current Plane infLex infLex_dec).
intros H'; elim H' with (P := fun x : Plane => True) (a := t) (l := l); auto.
intros; eapply infLex_trans; eauto.
intros Hinx'; generalize (min_R_all Plane infLex infLex_dec).
intros H';
 elim H' with (P := fun x : Plane => True) (a := t) (l := l) (x := x); 
 auto.
intros; eapply infLex_trans; eauto.
intros; apply infLex_total; auto.
Qed.
(* The second point will be computed using the same orientation predicate as in the main algorithm.*)
 
Definition OT_dec :
  forall p q r : Plane, {OT p q r} + {~ OT p q r}.
intros p q r; case (ordre_decidable (det p q r) zero).
intros H; left; exact H.
intros H; right; exact H.
Qed.
 
Theorem infLex_antisym : forall t1 t2 : Plane, infLex t1 t2 -> ~ infLex t2 t1.
intros t1 t2 H; elim H.
intros H'; red in |- *; intros H'0; elim H'0.
intros H'1; elim Gt_antisym with (1 := H'1); auto.
intros H'1; elim H'1; intros H'2 H'3; elim Gt_not_eq with (1 := H');
 clear H'1; auto.
intros H'; elim H'; intros H'0 H'1; try exact H'0; clear H'.
red in |- *; intros H'; elim H'.
intros H'2.
elim Gt_not_eq with (1 := H'2); auto.
intros H'2; elim H'2; intros H'3 H'4; elim Gt_antisym with (1 := H'4);
 clear H'2; auto.
Qed.
(* Here again, we use the same generic minimal function to compute the second point, which
  will be the witness for the right most point, as seen from the lexicographic minimum. *)
 
Definition min_slope (t : Plane) := min Plane (OT t) (OT_dec t).
 
Definition min_slope' (t: Plane) := min Plane (oriented'_prop t) (oriented'' t).

Theorem min_slope_in :
 forall (l : list Plane) (pivot current : Plane),
 min_slope pivot current l = current \/ In (min_slope pivot current l) l.
intros l pivot current;
 elim (min_in Plane (OT pivot) (OT_dec pivot) current l); 
 auto.
Qed.
 
Theorem min_slope_in2 :
 forall (pivot current : Plane) (l : list Plane),
 In current l -> In (min_slope pivot current l) l.
intros pivot; exact (min_in2 Plane (OT pivot) (OT_dec pivot)).
Qed.
Hint Resolve min_slope_in2.
 
Theorem min_slope_not_pivot :
 forall (l : list Plane) (pivot current : Plane),
 current <> pivot -> min_slope pivot current l <> pivot.
intros l; elim l; simpl in |- *; auto.
intros a l0 H' pivot current H'0; case (OT_dec pivot a current); auto.
intros H'1; apply H'; apply sym_not_equal; eapply direct_distincts; eauto.
Qed.
Hint Resolve min_slope_not_pivot.
 
Theorem min_slope_not_pivot2 :
 forall (l : list Plane) (pivot current : Plane),
 current <> pivot -> pivot <> min_slope pivot current l.
intros; apply sym_not_equal; auto.
Qed.
Hint Resolve min_slope_not_pivot2.
 
Theorem tech_lemma_gt :
 forall a : K, Gt a zero -> forall b : K, a = b -> Gt b zero.
intros a b H1 H2; rewrite <- H2; auto.
Qed.
(* To show that the second point is well-chosen, we will need to show that the relation
  (OT t) is a transitive relation, when t is the lexicographic minimum. For this we
  show that the determinant used in OT is equivalent to a cross product of
  vector coordinates, and that OT express that two cross-products are ordered.
  The transitivity of (OT t) then relies on the fact the cross-products order is also transitive.
  This is intuitively true, since the order on cross-products has the same form as the order on slopes.*)
 
Theorem crossed_product_Gt_trans :
 forall a1 b1 c1 a2 b2 c2 : K,
 Gt a1 zero \/ a1 = zero /\ Gt a2 zero ->
 Gt b1 zero \/ b1 = zero /\ Gt b2 zero ->
 Gt c1 zero \/ c1 = zero /\ Gt c2 zero ->
 Gt (Ktimes a2 b1) (Ktimes a1 b2) ->
 Gt (Ktimes b2 c1) (Ktimes b1 c2) ->
 Gt (Ktimes a2 c1) (Ktimes c2 a1).
intros a1 b1 c1 a2 b2 c2 H' H'0 H'1 H'2 H'3.
elim H'0; [ intros H'4 | intros (H''4, H'''4) ].
apply Gt_Gt_zero.
apply Ktimes_Gt with b1; auto.
apply
 (tech_lemma_gt
    (Kplus (Ktimes (Ktimes a2 b1) c1) (Koppose (Ktimes (Ktimes b1 c2) a1))));
 try ring.
apply Gt_zero_GT.
elim H'1; intros H'5.
elim H'; intros H'6.
apply Gt_trans with (Ktimes (Ktimes a1 b2) c1).
repeat rewrite <- (Rmul_comm RT c1).
apply Gt_mult; auto.
rewrite <- (Rmul_assoc RT); rewrite <- (Rmul_comm RT a1).
apply Gt_mult; auto.
elim H'6; intros H'7 H'8; rewrite H'7; clear H'6.
rewrite zero_mult; apply Gt_stable_mult; auto.
elim (Gt_antisym _ _ H'3); elim H'5; intros H'7 H'8; rewrite H'7.
rewrite zero_mult; apply Gt_stable_mult; auto.
elim H'.
elim H'1.
intros H'7 H'8.
elim (Gt_antisym _ _ H'2); rewrite H''4; rewrite zero_mult; auto.
intros (H'6, H'7); elim (Gt_not_eq _ _ H'3); rewrite H'6; rewrite H''4; ring.
intros (H'6, H'7); elim (Gt_not_eq _ _ H'2); rewrite H'6; rewrite H''4; ring.
Qed.

(*
Lemma infLex_oriented'_trans :
 forall (l : list Plane) (pivot current : Plane),
 infLex pivot current ->
 (forall x : Plane, In x l -> x <> pivot -> infLex pivot x) ->
 forall x y z : Plane,
 In x (current :: l) -> In y (current :: l) -> In z (current :: l) ->
 oriented'_prop pivot x y -> oriented'_prop pivot y z -> oriented'_prop pivot x z.
intros l pivot current Hinflex1 Hinflex2.
intros x y z H' H'0 H'1 H'2 H'3; red in |- *; unfold det in |- *;
 unfold determinant3x3 in |- *.
*)

Theorem infLex_OT_trans :
 forall (l : list Plane) (pivot current : Plane),
 infLex pivot current ->
 (forall x : Plane, In x l -> x <> pivot :>Plane -> infLex pivot x) ->
 forall x y z : Plane,
 In x (current :: l) ->
 In y (current :: l) ->
 In z (current :: l) ->
 OT pivot x y -> OT pivot y z -> OT pivot x z.
intros l pivot current Hinflex1 Hinflex2.
intros x y z H' H'0 H'1 H'2 H'3; red in |- *; unfold det in |- *;
 unfold determinant3x3 in |- *.
cut
 (forall t : Plane,
  In t (current :: l) ->
  t <> pivot ->
  Gt (Kplus (abscisse t) (Koppose (abscisse pivot))) zero \/
  Kplus (abscisse t) (Koppose (abscisse pivot)) = zero :>K /\
  Gt (Kplus (ordonnee t) (Koppose (ordonnee pivot))) zero).
intros Hyp.
unfold sensDirect.
apply
 (tech_lemma_gt
    ((fun x1 x2 y1 y2 z1 z2 : K =>
      Kplus (Ktimes (Kplus z2 (Koppose x2)) (Kplus y1 (Koppose x1)))
        (Koppose (Ktimes (Kplus y2 (Koppose x2)) (Kplus z1 (Koppose x1)))))
       (abscisse pivot) (ordonnee pivot) (abscisse x) 
       (ordonnee x) (abscisse z) (ordonnee z))); try ring.
apply Gt_zero_GT.
apply
 crossed_product_Gt_trans
  with
    (b1 := Kplus (abscisse y) (Koppose (abscisse pivot)))
    (b2 := Kplus (ordonnee y) (Koppose (ordonnee pivot))).
apply Hyp; auto.
eapply OT_distincts; eauto; fail.
apply Hyp; auto.
apply sym_not_equal; eapply direct_distincts; eauto.
apply Hyp; auto.
apply sym_not_equal; eapply direct_distincts; eauto.
apply Gt_Gt_zero.
generalize H'3; unfold OT, det, determinant3x3 in |- *; intros H''3.
apply (tech_lemma_gt _ H''3); unfold det, determinant3x3; ring.
apply Gt_Gt_zero.
generalize H'2; unfold OT, det, determinant3x3 in |- *; intros H''2.
apply (tech_lemma_gt _ H''2); unfold OT, det, determinant3x3; ring.
unfold det, determinant3x3; ring.
intros t Hint Hnoteq.
cut (infLex pivot t).
unfold infLex in |- *; intros H; elim H.
intros Hgt; left; apply Gt_zero_GT; auto.
intros H'4; elim H'4; intros H'5 H'6; clear H'4.
right; split.
rewrite H'5; ring.
apply Gt_zero_GT; auto.
elim Hint; auto.
intros H'4; rewrite <- H'4; auto.
Qed.
 
Theorem tech_lemma_ge :
 forall a : K, Ge a zero -> forall b : K, a = b -> Ge b zero.
intros a H b H1; rewrite <- H1; auto.
Qed.
 
Theorem infLex_OT_total :
 forall l : list Plane,
 (forall x y z : Plane,
  In x l ->
  In y l ->
  In z l ->
  x <> y -> x <> z -> y <> z -> OT x y z \/ OT x z y) ->
 forall pivot x y : Plane,
 In pivot l ->
 In x l ->
 x <> pivot ->
 In y l ->
 y <> pivot -> OT pivot x y \/ OT pivot y x \/ x = y :>Plane.
intros l H' pivot x y H'0 H'1 H'2 H'3 H'4.
elim (plan_eq_dec x y); auto.
intros; elim (H' pivot x y); auto.
Qed.
 
Theorem tech_lemma_and : forall a b : Prop, a /\ b -> a.
intuition.
Qed.
 
Definition first_two_points :
  forall (a b : Plane) (l : list Plane),
  (forall x y z : Plane,
   In x l ->
   In y l ->
   In z l ->
   x <> y -> x <> z -> y <> z -> OT x y z \/ OT x z y) ->
  a <> b ->
  In a l ->
  In b l ->
  {p : Plane * Plane |
  all_left Plane OT (fst p) (snd p) l /\
  fst p <> snd p /\ In (fst p) l /\ In (snd p) l}.
intros a b l; case l.
intros Hax3 H' H'0; elim H'0.
intros c tl.
intros Hax3 H' H'0 H'1.
elim (minLexico c tl).
intros t (Htin, HminLex).
cut {d : Plane | In d (c :: tl) /\ t <> d}.
intros Hsig; elim Hsig; intros d (Hind, Hntqed); clear Hsig.
exists (t, min_slope t d (c :: tl)); unfold fst, snd in |- *.
split; auto.
apply all_left_point_rev.
intros t0 H'3.
case (plan_eq_dec t t0); auto.
case (plan_eq_dec (min_slope t d (c :: tl)) t0); auto.
intros H'4 H'5; right; right.
elim (Hax3 t (min_slope t d (c :: tl)) t0); auto.
elim
 (min_R_all Plane (OT t) (OT_dec t)
    (fun x : Plane => In x (c :: tl) /\ x <> t))
  with (a := d) (l := c :: tl) (x := t0); auto.
intros H'6; elim H'4; exact H'6.
(* Intros H'6; Elim H'4; Exact H'6. *)
intros x y z H'2 H'6 H'7 H'8 H'9.
apply infLex_OT_trans with (l := c :: tl) (current := d) (y := y);
 auto; try (right; eapply tech_lemma_and; eauto; fail).
elim (HminLex d); auto.
intros; elim Hntqed; auto.
intros x0 H'10 H'11.
elim (HminLex x0); try intros H'14; auto.
case H'11; auto.
intros x y H'2 H'6.
elim H'2; elim H'6; intros.
apply infLex_OT_total with (l := c :: tl); auto.
intros x y H'2 H'6; split; [ assumption | idtac ].
apply sym_not_equal.
apply oriented_distinct1 with (oriented := OT) (p := y); auto.
case (plan_eq_dec t a).
intros H'2; exists b; rewrite H'2; auto.
intros H'2; exists a; auto.
Qed.

(*
Definition first_two_points' :
  forall (a b : Plane) (l : list Plane),
  (forall x y z : Plane,
   In x l ->
   In y l ->
   In z l ->
   x <> y -> x <> z -> y <> z -> OT x y z \/ OT x z y) ->
  a <> b ->
  In a l ->
  In b l ->
  {p : Plane * Plane |
  all_left Plane oriented'_prop (fst p) (snd p) l /\
  fst p <> snd p /\ In (fst p) l /\ In (snd p) l}.
intros a b l; case l.
intros Hax3 H' H'0; elim H'0.
intros c tl.
intros Hax3 H' H'0 H'1.
elim (minLexico c tl).
intros t (Htin, HminLex).
cut {d : Plane | In d (c :: tl) /\ t <> d}.
intros Hsig; elim Hsig; intros d (Hind, Hntqed); clear Hsig.
exists (t, min_slope' t d (c :: tl)); unfold fst, snd in |- *.
split; auto.
apply all_left_point_rev.
intros t0 H'3.
case (plan_eq_dec t t0); auto.
case (plan_eq_dec (min_slope' t d (c :: tl)) t0); auto.
intros H'4 H'5; right; right.
elim (Hax3 t (min_slope' t d (c :: tl)) t0); auto.
elim
 (min_R_all Plane (oriented'_prop t) (oriented'' t)
    (fun x : Plane => In x (c :: tl) /\ x <> t))
  with (a := d) (l := c :: tl) (x := t0); auto.
intros H'6; elim H'4; exact H'6.
intros x y z H'2 H'6 H'7 H'8 H'9.
apply infLex_oriented'_trans with (l := c :: tl) (current := d) (y := y);
 auto; try (right; eapply tech_lemma_and; eauto; fail).
elim (HminLex d); auto.
intros; elim Hntqed; auto.
intros x0 H'10 H'11.
elim (HminLex x0); try intros H'14; auto.
case H'11; auto.
intros x y H'2 H'6.
elim H'2; elim H'6; intros.
apply infLex_OT_total with (l := c :: tl); auto.
intros x y H'2 H'6; split; [ assumption | idtac ].
apply sym_not_equal.
apply oriented_distinct1 with (oriented := OT) (p := y); auto.
case (plan_eq_dec t a).
intros H'2; exists b; rewrite H'2; auto.
intros H'2; exists a; auto.
*)

End extr.