(* AddPath "$HOME/ctcoq/stage". *)
Require Export sensDirect.
Require Import Classical.
Require Import List.
Require Import Arith.
 
Theorem Axiom1 : forall p q r : Plan, sensDirect p q r -> sensDirect q r p.
unfold sensDirect, det in |- *.
intros.
rewrite <- det3x3_cyclique_ligne.
apply H.
Qed.
 
Theorem Axiom2 : forall p q r : Plan, sensDirect p q r -> ~ sensDirect p r q.
unfold sensDirect, det in |- *.
intros.
rewrite det3x3_inverse_permutation.
apply Inverse_signe_r.
apply H.
Qed.
 
Theorem Axiom3 :
 forall p q r : Plan,
 sensDirect p q r \/ sensDirect p r q \/ p = q \/ p = r \/ q = r.
intros.
elim (classic (sensDirect p q r)).
auto.
unfold sensDirect in |- *.
intros.
right.
elim (Inverse_signe_l (det p r q)); intros; auto.
cut (det p r q <> zero :>R \/ p = q :>Plan \/ p = r :>Plan \/ q = r :>Plan).
intros.
elim H1; intros; clear H1.
absurd (det p r q = zero :>R); auto.
elim H2; intros; clear H2; auto.
apply (aucun_triplet_aligne p q r).
unfold det in |- *.
rewrite <- det3x3_inverse_permutation.
unfold det in H.
trivial.
Qed.
 
Theorem Axiom4 :
 forall p q r t : Plan,
 sensDirect t q r -> sensDirect p t r -> sensDirect p q t -> sensDirect p q r.
unfold sensDirect in |- *.
intros.
rewrite (decompose_det p q r t).
apply Gt_stable_plus; split; auto.
apply Gt_stable_plus; split; auto.
Qed.
Hint Resolve Axiom1 Axiom2 Axiom3 Axiom4.
 
Theorem Axiom5 :
 forall t s p q r : Plan,
 sensDirect t s p ->
 sensDirect t s q ->
 sensDirect t s r -> sensDirect t p q -> sensDirect t q r -> sensDirect t p r.
intros t s p q r tsp tsq tsr tpq tqr.
elim (classic (sensDirect t p r)).
trivial.
intros.
cut (sensDirect t r p).
intros trp.
cut
 (Rplus (Rtimes (det t q r) (det t s p))
    (Rplus (Rtimes (det t r p) (det t s q)) (Rtimes (det t p q) (det t s r))) <>
  zero).
intro.
elim H0.
apply (convex_combinaison p q r s t).
apply Gt_non_nul.
apply Gt_stable_plus.
split.
apply Gt_stable_mult.
split.
trivial.
trivial.
apply Gt_stable_plus.
split.
apply Gt_stable_mult.
tauto.
apply Gt_stable_mult.
tauto.
elim (Axiom3 t r p).
trivial.
intro.
elim H0; intros; clear H0.
absurd (sensDirect t p r); auto.
elim H1; intros; clear H1.
absurd (r = t); auto.
eauto.
elim H0; intros; clear H0.
absurd (t = p :>Plan); auto.
eauto.
rewrite H1 in tqr.
absurd (sensDirect t q p); auto.
Qed.
 
Lemma Axiom5bis :
 forall t s p q r : Plan,
 sensDirect s t p ->
 sensDirect s t q ->
 sensDirect s t r -> sensDirect t p q -> sensDirect t q r -> sensDirect t p r.
intros t s p q r stp stq str tpq tqr.
cut (q <> r); intro H.
cut (p <> r); intro H0.
cut (p <> t); intro H1.
cut (r <> t); intro H2.
cut (s <> r); intro H3.
cut (s <> p); intro H4.
cut (s <> q); intro H5.
cut (p <> q); intro H6.
elim (Axiom3 t p r); intros HH1.
trivial; fail.
elim HH1; intros HH2; clear HH1.
cut (sensDirect s p q -> sensDirect s r p); intro.
cut (sensDirect s r p -> sensDirect s q r); intro.
cut (sensDirect s q r -> sensDirect s p q); intro.
elim (Axiom3 s p q); intros HH1.
absurd (sensDirect s p r); auto.
apply (Axiom5 s t p q r); auto.
elim HH1; intros HH3; clear HH1.
absurd (sensDirect s q r); auto.
cut (~ sensDirect s p q -> ~ sensDirect s q r); intro.
  auto; fail. 
    elim (classic (sensDirect s q r)); auto; fail.
apply (Axiom5 s t q p r); auto.
elim (classic (sensDirect s r p)); auto; intro.
absurd (sensDirect s q p); auto; fail.
elim (Axiom3 s p r); intros HH1; auto.
elim HH1; intros HH4; clear HH1; auto.
elim HH4; intros HH1; clear HH4.
absurd (s = p :>Plan); auto.
elim HH1; intros HH0; clear HH1.
absurd (s = r :>Plan); auto.
absurd (p = r :>Plan); auto.
elim HH3; intros HH0; clear HH3.
absurd (s = p :>Plan); auto.
elim HH0; intros HH3; clear HH0.
absurd (s = q :>Plan); auto.
absurd (p = q :>Plan); auto.
elim (Axiom3 q r p); intros HH1; auto.
apply Axiom1; apply (Axiom5 q r s t p); auto.
elim HH1; intros HH0; clear HH1.
elim (Axiom3 p r s); intros HH3; auto.
absurd (sensDirect q p r); auto.
apply Axiom2.
apply Axiom1; apply Axiom1; apply (Axiom5 r s p t q); auto.
elim HH3; intros HH1; clear HH3.
elim (Axiom3 s p q); intros HH3; auto.
elim HH3; intros HH4; clear HH3.
absurd (sensDirect p r t); auto.
apply (Axiom5 p s r q t); auto.
elim HH4; intros HH3; clear HH4.
absurd (s = p :>Plan); auto.
elim HH3; intros HH4; clear HH3.
absurd (s = q :>Plan); auto.
absurd (p = q :>Plan); auto.
elim HH1; intros HH4; clear HH1.
absurd (p = r :>Plan); auto.
elim HH4; intros HH3; clear HH4.
absurd (p = s :>Plan); auto.
absurd (r = s :>Plan); auto.
elim HH0; intros HH1; clear HH0.
absurd (q = r :>Plan); auto.
elim HH1; intros HH0; clear HH1.
absurd (q = p :>Plan); auto.
absurd (r = p :>Plan); auto.
elim (Axiom3 r p q); intros HH1; auto.
apply Axiom1; apply (Axiom5 r p s t q); auto.
elim HH1; intros HH0; clear HH1.
elim (Axiom3 q p s); intros HH3; auto.
absurd (sensDirect r q p); auto.
apply Axiom2.
apply Axiom1; apply Axiom1; apply (Axiom5 p s q t r); auto.
elim HH3; intros HH1; clear HH3.
elim (Axiom3 s q r); intros HH3; auto.
elim HH3; intros HH4; clear HH3.
absurd (sensDirect q p t); auto.
apply (Axiom5 q s p r t); auto.
elim HH4; intros HH3; clear HH4.
absurd (s = q :>Plan); auto.
elim HH3; intros HH4; clear HH3.
absurd (s = r :>Plan); auto.
absurd (q = r :>Plan); auto.
elim HH1; intros HH4; clear HH1.
absurd (q = p :>Plan); auto.
elim HH4; intros HH3; clear HH4.
absurd (q = s :>Plan); auto.
absurd (p = s :>Plan); auto.
elim HH0; intros HH1; clear HH0.
absurd (r = p :>Plan); auto.
elim HH1; intros HH0; clear HH1.
absurd (r = q :>Plan); auto.
absurd (p = q :>Plan); auto.
elim (Axiom3 p q r); intros HH1; auto.
apply Axiom1; apply (Axiom5 p q s t r); auto.
elim HH1; intros HH0; clear HH1.
elim (Axiom3 r q s); intros HH3; auto.
absurd (sensDirect p r q); auto.
apply Axiom2.
apply Axiom1; apply Axiom1; apply (Axiom5 q s r t p); auto.
elim HH3; intros HH1; clear HH3.
elim (Axiom3 s r p); intros HH3; auto.
elim HH3; intros HH4; clear HH3.
absurd (sensDirect r q t); auto.
apply (Axiom5 r s q p t); auto.
elim HH4; intros HH3; clear HH4.
absurd (s = r :>Plan); auto.
elim HH3; intros HH4; clear HH3.
absurd (s = p :>Plan); auto.
absurd (r = p :>Plan); auto.
elim HH1; intros HH4; clear HH1.
absurd (r = q :>Plan); auto.
elim HH4; intros HH3; clear HH4.
absurd (r = s :>Plan); auto.
absurd (q = s :>Plan); auto.
elim HH0; intros HH1; clear HH0.
absurd (p = q :>Plan); auto.
elim HH1; intros HH0; clear HH1.
absurd (p = r :>Plan); auto.
absurd (q = r :>Plan); auto.
elim HH2; intros HH3; clear HH2.
absurd (p = t :>Plan); auto.
elim HH3; intros HH2; clear HH3.
absurd (t = r :>Plan); auto.
absurd (p = r :>Plan); auto.
absurd (p = q); eauto.
absurd (q = s); eauto.
absurd (p = s); eauto.
absurd (r = s); eauto.
absurd (t = r); eauto.
absurd (t = p); eauto.
rewrite H0 in tpq; absurd (sensDirect t q r); auto.
absurd (q = r); eauto.
Qed.

Hint Resolve Axiom5 Axiom5bis.
 