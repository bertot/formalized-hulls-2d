(* AddPath "$HOME/ctcoq/stage". *)
Require Export minPolar.
Require Export List.
Require Export Classical.

Fixpoint maxPolaireHaut (t s : Plan) (cc : CC) {struct cc} : Plan :=
  match cc with
  | nil => s
  | a :: l =>
      match directBool t s a with
      | true =>
          (fun max : Plan =>
           match directBool t a max with
           | true => max
           | false => a
           end) (maxPolaireHaut t s l)
      | false => maxPolaireHaut t s l
      end
  end.

Lemma maxPolaireHaut_en_haut :
 forall (t s : Plan) (cc : CC),
 s = maxPolaireHaut t s cc \/ sensDirect t s (maxPolaireHaut t s cc).
intros.
induction cc as [| a cc Hreccc].
left; simpl in |- *; auto.
simpl in |- *.
CaseEq (directBool t s a).
CaseEq (directBool t a (maxPolaireHaut t s cc)).
apply Hreccc.
right; apply directBoolTrue; auto.
auto.
Qed.

Lemma maxPolaireHaut_init :
 forall (t s a : Plan) (cc : CC),
 s = maxPolaireHaut t s cc -> sensDirect t s a -> in_CC a cc -> a = s.
intros.
induction cc as [| a0 cc Hreccc].
simpl in H1; elim H1.
simpl in H1; elim H1; intro; clear H1.
simpl in H.
CaseEq (directBool t s a0).
rewrite H1 in H.
CaseEq (directBool t a0 (maxPolaireHaut t s cc)).
rewrite H3 in H.
rewrite <- H in H3.
absurd (sensDirect t a0 s).
apply Axiom2.
apply directBoolTrue; auto.
apply directBoolTrue; auto.
rewrite H3 in H.
rewrite H2 in H; auto.
rewrite H2 in H1.
absurd (sensDirect t s a); auto.
apply Hreccc; auto.
simpl in H.
CaseEq (directBool t s a0).
rewrite H1 in H.
CaseEq (directBool t a0 (maxPolaireHaut t s cc)).
rewrite H3 in H.
auto.
rewrite H3 in H.
absurd (s = a0 :>Plan); auto.
apply (direct_distincts s a0 t); auto.
rewrite H1 in H.
auto.
Qed.

Theorem maxPolaireHautOK :
 forall (t s a : Plan) (cc : CC),
 directBool t s a = true ->
 in_CC a cc ->
 sensDirect t a (maxPolaireHaut t s cc) \/ a = maxPolaireHaut t s cc.
intros.
induction cc as [| a0 cc Hreccc].
simpl in H0; elim H0.
simpl in H0; elim H0; intros; clear H0.
rewrite H1.
simpl in |- *.
rewrite H.
CaseEq (directBool t a (maxPolaireHaut t s cc)).
left; apply directBoolTrue; auto.
auto.
simpl in |- *.
CaseEq (directBool t s a0).
CaseEq (directBool t a0 (maxPolaireHaut t s cc)).
auto.
cut
 (sensDirect t a (maxPolaireHaut t s cc) \/ a = maxPolaireHaut t s cc :>Plan).
intros.
elim H3; intros HypRec; clear H3 Hreccc.
cut (infPolarEq t (maxPolaireHaut t s cc) a0).
intros.
unfold infPolarEq in H3; elim H3; intro; clear H3.
left.
apply (Axiom5 t s a (maxPolaireHaut t s cc) a0); auto.
cut (s = maxPolaireHaut t s cc \/ sensDirect t s (maxPolaireHaut t s cc)).
intro.
elim H3; intro; clear H3.
absurd (s = a).
apply (direct_distincts s a t); auto.
apply sym_eq; apply (maxPolaireHaut_init t s a cc); auto.
auto.
apply maxPolaireHaut_en_haut.
auto.
elim H4; intro; clear H4.
left; rewrite <- H3; auto.
elim H3; intro; clear H3.
absurd (a0 = t :>Plan); auto.
apply (direct_distincts a0 t s); auto.
absurd (maxPolaireHaut t s cc = t :>Plan); auto.
apply (direct_distincts (maxPolaireHaut t s cc) t a); auto.
apply non_sensDirect_infPolarEq; auto.
rewrite <- HypRec in H2.
cut (infPolarEq t a a0).
intros.
unfold infPolarEq in H3; elim H3; intro; clear H3; auto.
elim H4; intro; clear H4; auto.
elim H3; intro; clear H3.
absurd (a0 = t); auto.
apply (direct_distincts a0 t s); auto.
absurd (a = t); auto.
apply (direct_distincts a t s); auto.
apply non_sensDirect_infPolarEq; auto.
auto.
auto.
Qed.

Fixpoint minPolaireBas (t s : Plan) (cc : CC) {struct cc} : Plan :=
  match cc with
  | nil => s
  | a :: l =>
      match directBool t a s with
      | true =>
          (fun min : Plan =>
           match directBool t min a with
           | true => min
           | false => a
           end) (minPolaireBas t s l)
      | false => minPolaireBas t s l
      end
  end.

Lemma minPolaireBas_en_bas :
 forall (t s : Plan) (cc : CC),
 s = minPolaireBas t s cc \/ sensDirect t (minPolaireBas t s cc) s.
intros.
induction cc as [| a cc Hreccc].
left; simpl in |- *; auto.
simpl in |- *.
CaseEq (directBool t a s).
CaseEq (directBool t (minPolaireBas t s cc) a).
apply Hreccc.
right; auto.
auto.
Qed.

Lemma minPolaireBas_init :
 forall (t s a : Plan) (cc : CC),
 s = minPolaireBas t s cc -> sensDirect t a s -> in_CC a cc -> a = s.
intros.
induction cc as [| a0 cc Hreccc].
simpl in H1; elim H1.
simpl in H1; elim H1; intro; clear H1.
simpl in H.
CaseEq (directBool t a0 s).
rewrite H1 in H.
CaseEq (directBool t (minPolaireBas t s cc) a0).
rewrite H3 in H.
rewrite <- H in H3.
absurd (sensDirect t s a0).
apply Axiom2.
auto.
auto.
rewrite H3 in H.
rewrite H2 in H; auto.
rewrite H2 in H1.
absurd (sensDirect t a s); auto.
auto.
apply Hreccc; auto.
simpl in H.
CaseEq (directBool t a0 s).
rewrite H1 in H.
CaseEq (directBool t (minPolaireBas t s cc) a0).
rewrite H3 in H.
auto.
rewrite H3 in H.
absurd (a0 = s :>Plan); auto.
apply (direct_distincts a0 s t); auto.
rewrite H1 in H.
auto.
Qed.

Theorem minPolaireBasOK :
 forall (t s a : Plan) (cc : CC),
 directBool t a s = true ->
 in_CC a cc ->
 sensDirect t (minPolaireBas t s cc) a \/ a = minPolaireBas t s cc.
intros.
induction cc as [| a0 cc Hreccc].
simpl in H0; elim H0.
simpl in H0; elim H0; intros; clear H0.
rewrite H1.
simpl in |- *.
rewrite H.
CaseEq (directBool t (minPolaireBas t s cc) a).
left; auto.
auto.
simpl in |- *.
CaseEq (directBool t a0 s).
CaseEq (directBool t (minPolaireBas t s cc) a0).
auto.
cut
 (sensDirect t (minPolaireBas t s cc) a \/ a = minPolaireBas t s cc :>Plan).
intros.
elim H3; intros HypRec; clear H3 Hreccc.
cut (infPolarEq t a0 (minPolaireBas t s cc)).
intros.
unfold infPolarEq in H3; elim H3; intro; clear H3.
left.
apply (Axiom5bis t s a0 (minPolaireBas t s cc) a); auto.
cut (s = minPolaireBas t s cc \/ sensDirect t (minPolaireBas t s cc) s).
intro.
elim H3; intro; clear H3.
absurd (a = s).
apply (direct_distincts a s t); auto.
apply (minPolaireBas_init t s a cc); auto.
auto.
apply minPolaireBas_en_bas.
elim H4; intro; clear H4.
left; rewrite H3; auto.
elim H3; intro; clear H3.
absurd (t = minPolaireBas t s cc :>Plan); auto.
apply (direct_distincts t (minPolaireBas t s cc) a); auto.
absurd (t = a0 :>Plan); auto.
apply (direct_distincts t a0 s); auto.
apply non_sensDirect_infPolarEq; auto.
rewrite <- HypRec in H2.
cut (infPolarEq t a0 a).
intros.
unfold infPolarEq in H3; elim H3; intro; clear H3; auto.
elim H4; intro; clear H4; auto.
elim H3; intro; clear H3.
absurd (t = a); auto.
apply (direct_distincts t a s); auto.
absurd (t = a0); auto.
apply (direct_distincts t a0 s); auto.
apply non_sensDirect_infPolarEq; auto.
auto.
auto.
Qed.

Theorem minPolaireBasOK_pour_s :
 forall (t s : Plan) (cc : CC),
 sensDirect t (minPolaireBas t s cc) s \/ s = minPolaireBas t s cc.
intros.
induction cc as [| a cc Hreccc].
right; simpl in |- *; auto.
simpl in |- *.
CaseEq (directBool t a s).
CaseEq (directBool t (minPolaireBas t s cc) a).
auto.
left; auto.
auto.
Qed.

Theorem maxPolaireHautOK_pour_s :
 forall (t s : Plan) (cc : CC),
 sensDirect t s (maxPolaireHaut t s cc) \/ s = maxPolaireHaut t s cc.
intros.
induction cc as [| a cc Hreccc].
right; simpl in |- *; auto.
simpl in |- *.
CaseEq (directBool t s a).
CaseEq (directBool t a (maxPolaireHaut t s cc)).
auto.
left; auto.
auto.
Qed.

Lemma maxPolaireHaut_dans_cc :
 forall (t s : Plan) (cc : CC),
 in_CC (maxPolaireHaut t s cc) cc \/ s = maxPolaireHaut t s cc.
intros.
induction cc as [| a cc Hreccc].
simpl in |- *; auto.
simpl in |- *.
CaseEq (directBool t s a).
CaseEq (directBool t a (maxPolaireHaut t s cc)).
elim Hreccc; intros HypRec; clear Hreccc; auto.
auto.
elim Hreccc; intros HypRec; clear Hreccc; auto.
Qed.

Lemma minPolaireBas_dans_cc :
 forall (t s : Plan) (cc : CC),
 in_CC (minPolaireBas t s cc) cc \/ s = minPolaireBas t s cc.
intros.
induction cc as [| a cc Hreccc].
simpl in |- *; auto.
simpl in |- *.
CaseEq (directBool t a s).
CaseEq (directBool t (minPolaireBas t s cc) a).
elim Hreccc; intros HypRec; clear Hreccc; auto.
auto.
elim Hreccc; intros HypRec; clear Hreccc; auto.
Qed.

Lemma maxPolaireHaut_non_t :
 forall (t s : Plan) (cc : CC), t <> s -> t <> maxPolaireHaut t s cc.
intros.
induction cc as [| a cc Hreccc].
simpl in |- *; auto.
simpl in |- *.
CaseEq (directBool t s a).
CaseEq (directBool t a (maxPolaireHaut t s cc)).
auto.
apply sym_not_eq; apply (direct_distincts a t s); auto.
auto.
Qed.

Lemma minPolaireBas_non_t :
 forall (t s : Plan) (cc : CC), t <> s -> minPolaireBas t s cc <> t.
intros.
induction cc as [| a cc Hreccc].
simpl in |- *; auto.
simpl in |- *.
CaseEq (directBool t a s).
CaseEq (directBool t (minPolaireBas t s cc) a).
auto.
apply sym_not_eq; apply (direct_distincts t a s); auto.
auto.
Qed.

Theorem point_dans_triangle :
 forall (t s : Plan) (cc : CC),
 t <> s ->
 in_CC t cc ->
 in_CC s cc ->
 IF sensDirect t (maxPolaireHaut t s cc) (minPolaireBas t s cc)
 then dansTriangle t s (maxPolaireHaut t s cc) (minPolaireBas t s cc)
 else CH_Prop cc (maxPolaireHaut t s cc, t).
intros.
elim (classic (sensDirect t (maxPolaireHaut t s cc) (minPolaireBas t s cc)));
 intro.
unfold IF_then_else in |- *.
left.
split.
auto.
cut (sensDirect t (minPolaireBas t s cc) s \/ s = minPolaireBas t s cc).
cut (sensDirect t s (maxPolaireHaut t s cc) \/ s = maxPolaireHaut t s cc).
intros.
elim H3; elim H4; intros; clear H4 H3; auto.
unfold dansTriangle in |- *; split.
auto.
split; auto.
rewrite <- H5 in H2.
absurd (sensDirect t (maxPolaireHaut t s cc) s); auto.
rewrite <- H6 in H2.
absurd (sensDirect t s (minPolaireBas t s cc)); auto.
rewrite <- H6 in H2; rewrite <- H5 in H2.
absurd (s = s); auto.
eauto.
apply maxPolaireHautOK_pour_s.
apply minPolaireBasOK_pour_s.
unfold IF_then_else in |- *.
right; split; auto.
unfold CH_Prop in |- *; simpl in |- *.
split.
cut (in_CC (maxPolaireHaut t s cc) cc \/ s = maxPolaireHaut t s cc).
intro.
elim H3; intro; clear H3.
auto.
rewrite <- H4; auto.
apply maxPolaireHaut_dans_cc.
split; auto.
cut (infPolarEq t (minPolaireBas t s cc) (maxPolaireHaut t s cc)).
intro.
cut (forall a : Plan, in_CC a cc -> infPolarEq (maxPolaireHaut t s cc) t a).
intro; apply points_extremaux_non_ind; auto.
apply maxPolaireHaut_non_t; auto.
intros.
elim (classic (sensDirect t s a)).
intro.
cut (sensDirect t a (maxPolaireHaut t s cc) \/ a = maxPolaireHaut t s cc).
intro.
unfold infPolarEq in |- *.
elim H6; intro; auto.
apply maxPolaireHautOK; auto.
apply sensDirect_directBoolTrue; auto.
intro.
cut (infPolarEq t a s).
intro; clear H5.
unfold infPolarEq in H6; elim H6; intro; clear H6.
cut (infPolarEq t a (maxPolaireHaut t s cc)).
intro.
unfold infPolarEq in H6; unfold infPolarEq in |- *; elim H6; intro; clear H6;
 auto.
elim H7; intro; auto.
elim H6; intro; auto.
apply (infPolarEq_trans (minPolaireBas t s cc) t a s (maxPolaireHaut t s cc));
 auto.
apply minPolaireBas_non_t; auto.
cut (sensDirect t (minPolaireBas t s cc) a \/ a = minPolaireBas t s cc).
intro; unfold infPolarEq in |- *.
elim H6; intro; auto.
apply minPolaireBasOK; auto.
apply sensDirect_directBoolTrue; auto.
cut (sensDirect t (minPolaireBas t s cc) s \/ s = minPolaireBas t s cc).
intro; unfold infPolarEq in |- *.
elim H6; intro; auto.
apply minPolaireBasOK_pour_s; auto.
unfold infPolarEq in |- *; auto.
cut (sensDirect t s (maxPolaireHaut t s cc) \/ s = maxPolaireHaut t s cc).
intro; unfold infPolarEq in |- *.
elim H6; intro; auto.
apply maxPolaireHautOK_pour_s; auto.
elim H5; intro; clear H5.
rewrite H6;
 cut (sensDirect t s (maxPolaireHaut t s cc) \/ s = maxPolaireHaut t s cc).
intro; unfold infPolarEq in |- *.
elim H5; intro; auto.
apply maxPolaireHautOK_pour_s; auto.
elim H6; intro; clear H6.
absurd (t = s); auto.
unfold infPolarEq in |- *; auto.
apply non_sensDirect_infPolarEq; auto.
apply non_sensDirect_infPolarEq; auto.
Qed.

Lemma maxPolaireHaut_egal_s :
 forall (t s : Plan) (cc : CC),
 t <> s ->
 in_CC t cc ->
 in_CC s cc -> s = maxPolaireHaut t s cc -> points_extremaux cc s t.
intros.
cut (forall a : Plan, in_CC a cc -> infPolarEq s t a).
intro.
apply points_extremaux_non_ind; auto.
intros.
elim (classic (sensDirect s a t)); intro.
cut (sensDirect t a (maxPolaireHaut t s cc) \/ a = maxPolaireHaut t s cc).
intro.
elim H5; intro; clear H5.
rewrite <- H2 in H6; unfold infPolarEq in |- *; auto.
rewrite <- H2 in H6; unfold infPolarEq in |- *; auto.
apply maxPolaireHautOK; auto.
apply sensDirect_directBoolTrue; auto.
apply non_sensDirect_infPolarEq; auto.
Qed.

Lemma minPolaireBas_egal_s :
 forall (t s : Plan) (cc : CC),
 t <> s ->
 in_CC t cc ->
 in_CC s cc -> s = minPolaireBas t s cc -> points_extremaux cc t s.
intros.
cut (forall a : Plan, in_CC a cc -> infPolarEq t s a).
intro.
apply points_extremaux_non_ind; auto.
intros.
elim (classic (sensDirect t a s)); intro.
cut (sensDirect t (minPolaireBas t s cc) a \/ a = minPolaireBas t s cc).
intro.
elim H5; intro; clear H5.
rewrite <- H2 in H6; unfold infPolarEq in |- *; auto.
rewrite <- H2 in H6; unfold infPolarEq in |- *; auto.
apply minPolaireBasOK; auto.
apply sensDirect_directBoolTrue; auto.
apply non_sensDirect_infPolarEq; auto.
Qed.

Theorem pas_point_extremal_dans_triangle :
 forall (t : Plan) (cc : CC),
 in_CC t cc ->
 (exists s : Plan, in_CC s cc /\ s <> t) ->
 ~ point_extremal t cc ->
   (exists p : Plan,
      (exists q : Plan,
         (exists r : Plan,
          in_CC p cc /\ in_CC q cc /\ in_CC r cc /\ dansTriangle t p q r))).
intros.
elim H0.
clear H0; intros s H0.
elim H0; intros; clear H0.
exists s.
elim (classic (sensDirect t (maxPolaireHaut t s cc) (minPolaireBas t s cc)));
 intro.
exists (maxPolaireHaut t s cc); exists (minPolaireBas t s cc).
split; auto.
split.
cut (in_CC (maxPolaireHaut t s cc) cc \/ s = maxPolaireHaut t s cc).
intro.
elim H4; intro; clear H4; auto.
rewrite <- H5; auto.
apply maxPolaireHaut_dans_cc.
split.
cut (in_CC (minPolaireBas t s cc) cc \/ s = minPolaireBas t s cc).
intro.
elim H4; intro; clear H4; auto.
rewrite <- H5; auto.
apply minPolaireBas_dans_cc.
elim (point_dans_triangle t s cc); auto.
intro.
elim H4; intros; auto.
intro.
elim H4; intros;
 absurd (sensDirect t (maxPolaireHaut t s cc) (minPolaireBas t s cc)); 
 auto.
elim (point_dans_triangle t s cc); auto.
intro.
elim H4; intros;
 absurd (sensDirect t (maxPolaireHaut t s cc) (minPolaireBas t s cc)); 
 auto.
intro.
elim H4; intros.
clear H5 H4 H0.
unfold CH_Prop in H6; simpl in H6.
elim H6; intros; clear H6.
elim H4; intros; clear H4.
absurd (point_extremal t cc); auto.
unfold point_extremal in |- *.
left; exists (maxPolaireHaut t s cc).
unfold CH_Prop in |- *; simpl in |- *.
split.
cut (in_CC (maxPolaireHaut t s cc) cc \/ s = maxPolaireHaut t s cc).
intro.
elim H4; intro; clear H4; auto.
apply maxPolaireHaut_dans_cc.
split; auto.
Qed.

Theorem dans_triangle_pas_point_extremal :
 forall (t : Plan) (cc : CC),
   (exists p : Plan,
      (exists q : Plan,
         (exists r : Plan,
          in_CC p cc /\ in_CC q cc /\ in_CC r cc /\ dansTriangle t p q r))) ->
 ~ point_extremal t cc.
intros.
elim H; clear H; intros p H.
elim H; clear H; intros q H.
elim H; clear H; intros r H.
elim H; clear H; intros.
elim H0; clear H0; intros.
elim H1; clear H1; intros.
red in |- *; intro.
unfold point_extremal in H3.
unfold dansTriangle in H2.
elim H2; clear H2; intros pqt H2.
elim H2; clear H2; intros qrt rpt.
cut (exists r0 : Plan, CH_Prop cc (pair t r0)).
clear H3; intro.
elim H2; clear H2; intros s H2.
unfold CH_Prop in H2; simpl in H2.
elim H2; clear H2; intros.
elim H3; clear H3; intros.
absurd (sensDirect t r q); auto.
cut (t <> s).
intro.
cut (infPolarEq t s r).
cut (infPolarEq t s q).
cut (infPolarEq t s p).
intros.
apply (Axiom5 t s r p q); auto.
unfold infPolarEq in H8.
elim H8; clear H8; intro; auto.
elim H8; clear H8; intro.
rewrite H8 in H7.
absurd (infPolarEq t r q); auto.
elim H8; clear H8; intro.
absurd (r = t); eauto.
absurd (s = t :>Plan); auto.
unfold infPolarEq in H6.
elim H6; clear H6; intro; auto.
elim H6; clear H6; intro.
rewrite H6 in H8.
absurd (infPolarEq t p r); auto.
elim H6; clear H6; intro.
absurd (p = t); eauto.
absurd (s = t :>Plan); auto.
unfold infPolarEq in H7.
elim H7; clear H7; intro; auto.
elim H7; clear H7; intro.
rewrite H7 in H6.
absurd (infPolarEq t q p); auto.
elim H7; clear H7; intro.
absurd (q = t); eauto.
absurd (s = t :>Plan); auto.
apply (points_extremaux_Prop t s p cc); auto.
apply (points_extremaux_Prop t s q cc); auto.
apply (points_extremaux_Prop t s r cc); auto.
apply (points_extremaux_distincts t s cc); auto.
elim H3; clear H3; intro; auto.
elim H2; clear H2; intros s H2.
apply (points_extremal_suivant cc s t); auto.
Qed.
