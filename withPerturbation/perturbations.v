Require Export reels.
Require Export sensDirect.
Require Export lemmas.
Require Export polynomial_sign.
Require Export determinant_algebra.

Module perturb(A:OField).
Module DA := det_alg(A).
Import DA PS D KA P C A.

Section epsilon.
Variable p q r : Plane.
Section epsilon2.
Variable xp yp xq yq xr yr : K.
Hypotheses (def_p : p = (xp, yp)) (def_q : q = (xq, yq))
  (def_r : r = (xr, yr)).
 
Definition D_2 :=
  determinant3x3 one xp (Kplus (Ktimes xp xp) (Ktimes yp yp)) one xq
    (Kplus (Ktimes xq xq) (Ktimes yq yq)) one xr
    (Kplus (Ktimes xr xr) (Ktimes yr yr)).
 
Definition D_3 :=
  determinant3x3 one yp (Kplus (Ktimes xp xp) (Ktimes yp yp)) one yq
    (Kplus (Ktimes xq xq) (Ktimes yq yq)) one yr
    (Kplus (Ktimes xr xr) (Ktimes yr yr)).
 
Theorem move_oppose_zero :
 forall x y : K, zero = Kplus (Koppose x) y -> x = y.
intros; replace x with (Kplus x zero).
rewrite H; ring.
ring.
Qed.
 
Theorem switch_sum_determinant :
 forall a b c d e f g h i j k l : K,
 determinant3x3 a b (Kplus c d) e f (Kplus g h) i j (Kplus k l) =
 determinant3x3 a b (Kplus d c) e f (Kplus h g) i j (Kplus l k).
intros; unfold determinant3x3 in |- *; ring.
Qed.
 
Infix "*" := Ktimes.
Infix "+" := Kplus.

Lemma not_all_determinants_zero :
 p<>q->q<>r->r<>p -> det p q r = zero -> D_3 = zero -> D_2 <> zero.
intros H' H'0 H'1 H'2; case (colinear_decompose p q r); auto.
intros mn [md [b [H'4 dummy]]]; revert dummy.
rewrite def_p; rewrite def_q; rewrite def_r.
unfold line_equation, abscisse, ordonnee; intros [H'6 [H'7 H'8]].
do 3 match goal with H : _ |- _ => apply move_oppose_zero in H end.
case (egal_decidable mn zero).
intros Heq.
assert (H'9: yr=yq).
apply move_oppose_zero;
 assert (H'':md*(Koppose yr + yq)=zero) by ring [H'7 H'8 Heq];
  case (K_integre _ _ H''); intuition.
assert (H'10: yq=yp).
apply move_oppose_zero;
 assert (H'':md*(Koppose yq + yp)=zero) by ring [H'6 H'7 Heq];
  case (K_integre _ _ H''); intuition.
intros _; unfold D_2.
rewrite determinant_column_sum3.
subst yr yq; replace (yp*yp) with ((yp*yp)*one) by ring; rewrite
factorize_van_der_monde ; rewrite determinant_rotate_column.
rewrite determinant_column_mult2.
replace (yp*yp*determinant3x3 xp one one xq one one xr one one) with zero by
  (unfold determinant3x3; ring).
assert (L:forall a, a + zero = a) by (intros; ring); rewrite L.
intros H'12; case K_integre with (1:= H'12).
rewrite (Radd_comm RT); intros H'13; apply sym_equal in H'13; 
  rewrite (move_oppose_zero _ _  H'13) in def_q; subst p q; intuition.
clear H'12; intros H'12; case K_integre with (1:= H'12).
rewrite (Radd_comm RT); intros H'13; apply sym_equal in H'13; 
  rewrite (move_oppose_zero _ _  H'13) in def_r; subst q r; intuition.
rewrite (Radd_comm RT); intros H'13; apply sym_equal in H'13; 
  rewrite (move_oppose_zero _ _  H'13) in def_p; subst p r; intuition.
intros Hneqn.
intros Hd3; assert (Hneqd : md<> zero).
intros Hmd; subst md; repeat rewrite mult_zero in *.
do 3match goal with H : zero = mn * ?x + b |- _ => assert (Hdummy : mn * x = Koppose b) by
  (replace (mn*x) with ((mn*x+b) +Koppose b);[rewrite <- H; try ring| try ring]);
  assert (Hdummy1 :x=Koppose b *Kinv mn)by
  (replace x with ((mn * x) *Kinv mn);[rewrite Hdummy; auto |
  rewrite (Rmul_comm RT mn); rewrite <- (Rmul_assoc RT); rewrite Kinv_def; auto; ring]);
  clear H Hdummy; rename Hdummy1 into H
end.
assert (xr=xp) by (subst; auto); assert (xq=xp) by (subst;auto);clear H'6 H'7 H'8; subst.
unfold D_3 in Hd3; rewrite determinant_column_sum3 in Hd3.
match goal with H: ?a + ?b = zero |- _ => replace a with zero in H end;
try rewrite (Radd_0_l RT) in Hd3; rewrite factorize_van_der_monde in Hd3.
case K_integre with (1:=Hd3).
intros H'5; apply sym_equal in H'5; rewrite (Radd_comm RT) in H'5;
  apply move_oppose_zero in H'5; subst yq; intuition.
intros H''; case K_integre with (1:=H'');
intros H'5; apply sym_equal in H'5; rewrite (Radd_comm RT) in H'5;
  apply move_oppose_zero in H'5; subst yq || subst yr; intuition.
subst; unfold determinant3x3; ring.
intros Hd2; assert (Hd2':md*md*D_2 =zero) by (rewrite Hd2; ring); clear Hd2.
generalize Hd2'; clear Hd2'; unfold D_2; rewrite determinant_rotate_column;
 rewrite <- determinant_column_mult2; repeat rewrite (Rmul_comm RT (md*md));
 repeat rewrite (Rdistr_l RT).
replace (yp*yp*(md*md)) with (md*yp * (md*yp)) by ring; rewrite H'6.
replace (yq*yq*(md*md)) with (md*yq * (md*yq)) by ring; rewrite H'7.
replace (yr*yr*(md*md)) with (md*yr * (md*yr)) by ring; rewrite H'8.
repeat match goal with |- context[?x*?x*(md*md)+(mn*?x+b)*(mn*?x+b)] =>
  replace (x*x*(md*md)+(mn*x+b)*(mn*x+b)) with ((md*md+mn*mn)*(x*x)+((mn+mn)*b)*x+b*b) by ring
end.
repeat rewrite determinant_column_sum2.
do 2 match goal with |- ?a + ?b = zero -> _ => 
  replace b with zero by (unfold determinant3x3; try ring);
  replace (a+zero) with a by ring end.
rewrite determinant_column_mult2; rewrite <- determinant_rotate_column;
 rewrite factorize_van_der_monde.
intros Hd2; case K_integre with (1:= Hd2); clear Hd2.
assert (Hmd:=carre_positif md); apply (Gt_plus_GeGt (md*md) (mn*mn) zero zero) in Hmd.
rewrite (Radd_0_l RT) in Hmd; intros Heq; rewrite <- Heq in Hmd.
case Gt_antisym with (1:=Hmd); auto.
case (carre_positif mn); auto.
intros Habs; case K_integre with (1:=Habs); intuition.
do 3 match goal with H : md*?y = mn * ?x + b |- _ =>
  assert (Hdummy : y = (mn*x+ b)*Kinv md) by
  (replace y with ((md * y) *Kinv md);[rewrite H; auto |
  rewrite (Rmul_comm RT md); rewrite <- (Rmul_assoc RT); rewrite Kinv_def; auto; ring]);
  clear H; rename Hdummy into H
end.
intros Hd2; case K_integre with (1:=Hd2); clear Hd2.
rewrite (Radd_comm RT); intros Hpq; apply sym_equal in Hpq; apply move_oppose_zero in Hpq.
subst xq; assert (yq = yp) by (subst yq;auto); subst yq yp p q; intuition.
intros Hd2; case K_integre with (1:=Hd2); clear Hd2;
rewrite (Radd_comm RT); intros Hpq; apply sym_equal in Hpq; apply move_oppose_zero in Hpq.
subst xr; assert (yr = yq) by (subst yr;auto); subst yq yr q r; intuition.
subst xr; assert (yp = yr) by (subst yr;auto); subst yr yp p r; intuition.
Qed.

Definition perturbate_det (t : K) :=
  det (perturbate_point t p) (perturbate_point t q) (perturbate_point t r).
 
Theorem perturbate_det_polynomial :
 forall t : K,
 perturbate_det t =
 Kplus (det p q r) (Kplus (Ktimes t D_2) (Ktimes (Ktimes t t) D_3)).
intros t; try assumption.
unfold perturbate_det in |- *.
unfold perturbate_point in |- *.
rewrite def_p; rewrite def_q; rewrite def_r.
unfold det in |- *.
simpl in |- *.
rewrite determinant_column_sum1.
rewrite determinant_column_sum2.
rewrite determinant_column_sum2.
rewrite determinant_column_mult2.
rewrite determinant_column_mult2.
replace
 (determinant3x3 (Ktimes t yp) yp one (Ktimes t yq) yq one 
    (Ktimes t yr) yr one) with zero.
2: unfold determinant3x3 in |- *; ring.
rewrite determinant_column_mult1.
unfold D_3 in |- *.
unfold D_2 in |- *.
repeat rewrite <- (Radd_assoc RT).
apply
 f_equal
  with (f := fun x => Kplus (determinant3x3 xp yp one xq yq one xr yr one) x).
rewrite determinant_rotate_column.
rewrite determinant_rotate_column.
apply
 f_equal
  with
    (f := fun x =>
          Kplus
            (Ktimes t
               (determinant3x3 one xp (Kplus (Ktimes xp xp) (Ktimes yp yp))
                  one xq (Kplus (Ktimes xq xq) (Ktimes yq yq)) one xr
                  (Kplus (Ktimes xr xr) (Ktimes yr yr)))) x).
replace
 (determinant3x3 one xp (Kplus (Ktimes xp xp) (Ktimes yp yp)) one xq
    (Kplus (Ktimes xq xq) (Ktimes yq yq)) one xr
    (Kplus (Ktimes xr xr) (Ktimes yr yr))) with
 (determinant3x3 one xp (Kplus (Ktimes xp xp) (Ktimes yp yp)) one xq
    (Kplus (Ktimes xq xq) (Ktimes yq yq)) one xr
    (Kplus (Ktimes xr xr) (Ktimes yr yr))).
2: ring.
rewrite determinant_rotate_column.
rewrite determinant_rotate_column.
ring.
Qed.
End epsilon2.
 
Infix "+" := Kplus.
Infix "*" := Ktimes.
Infix "-" := Ksub.



Theorem point_decompose : forall p : Plane, p = (abscisse p, ordonnee p).
intros x; case x; auto.
Qed.
Hint Resolve point_decompose.
 
Theorem perturbate_det_polynomial' :
 forall t : K,
 perturbate_det t =
 Kplus (det p q r)
   (Kplus
      (Ktimes
         (D_2 (abscisse p) (ordonnee p) (abscisse q) 
            (ordonnee q) (abscisse r) (ordonnee r)) t)
      (Ktimes
         (D_3 (abscisse p) (ordonnee p) (abscisse q) 
            (ordonnee q) (abscisse r) (ordonnee r)) 
         (Ktimes t t))) :>K.
intros t; rewrite <- (Rmul_comm RT t);
 rewrite <- (Rmul_comm RT (Ktimes t t)); auto.
rewrite <- perturbate_det_polynomial; auto.
Qed.
 
Theorem pre_ax3 :
 p <> q ->
 q <> r ->
 r <> p ->
 exists eps : K,
   Gt eps zero /\
   ((forall t : K,
     Gt t zero -> Gt eps t -> Gt (perturbate_det t) zero) \/
    (forall t : K,
     Gt t zero -> Gt eps t -> Gt zero (perturbate_det t))).
intros H' H'0 H'1; try assumption.
elim
 (polynomial_sign (det p q r)
    (D_2 (abscisse p) (ordonnee p) (abscisse q) (ordonnee q) 
       (abscisse r) (ordonnee r))
    (D_3 (abscisse p) (ordonnee p) (abscisse q) (ordonnee q) 
       (abscisse r) (ordonnee r))).
intros H'2; elim H'2.
intros e H'3; elim H'3.
intros H'4 H'5.
exists e; split; [ auto | left; clear H'4; intros t H'4 H'6; try assumption ];
 auto.
rewrite perturbate_det_polynomial'; auto.
intros H'2; elim H'2.
intros e H'3; elim H'3; intros H'4 H'5.
exists e; split; [ auto | right; clear H'4; intros t H'4 H'6 ].
rewrite perturbate_det_polynomial'; auto.
red in |- *; intros H'2; elim H'2; intros H'3 H'4; elim H'4; intros H'5 H'6;
 try exact H'6; clear H'4 H'2.
elim
 (not_all_determinants_zero (abscisse p) (ordonnee p) 
    (abscisse q) (ordonnee q) (abscisse r) (ordonnee r)); 
 auto.
Qed.
(* well_aligned is supposed to be used on the first coordinate
   (respectively the second coordinate) of three points, an it returns
   a boolean value indicating whether these three points (or a 3-cycle
   of these points) have their first coordinate (respectively second 
   coordinate) in increasing order.
   This computation could be done by the sign of a single polynomial,
   but it is interesting to avoid increasing the degree of computations,
   To ensure the algorithm is more robust.*)
 
Definition well_aligned (x1 x2 x3 : K) :=
  match ordre_decidable x2 x1 with
  | left h =>
      match ordre_decidable x3 x2 with
      | left h' => true
      | right h' =>
          match ordre_decidable x1 x3 with
          | left h'' => true
          | right h'' => false
          end
      end
  | right h =>
      match ordre_decidable x3 x1 with
      | left h' => false
      | right h' =>
          match ordre_decidable x3 x2 with
          | left h'' =>
              match egal_decidable x1 x2 with
              | left h''' => false
              | right h''' =>
                  match egal_decidable x3 x1 with
                  | left h''' => false
                  | right h''' => true
                  end
              end
          | right h'' => false
          end
      end
  end.
(* The function oriented' is supposed to be used for the parameter
   oriented_dec of the jarvis function. *)
 
Definition oriented' (p q r : Plane) :=
  match egal_decidable (det p q r) zero with
  | left h =>
      match p, q, r with
      | (xp, yp), (xq, yq), (xr, yr) =>
          match egal_decidable xp xq with
          | left h => well_aligned yp yq yr
          | right h => well_aligned xp xq xr
          end
      end
  | right h =>
      match ordre_decidable (det p q r) zero with
      | left h' => true
      | right h' => false
      end
  end.

Definition oriented'_prop := oriented' p q r = true.

Definition oriented'': {oriented'_prop}+{~oriented'_prop}.
unfold oriented'_prop; case (oriented' p q r);
  (left; reflexivity) || (right; discriminate).
Defined.
 
Theorem well_aligned_distinct :
 forall x y z : K, well_aligned x y z = true -> x <> y /\ y <> z /\ z <> x.
intros x y z.
unfold well_aligned in |- *.
case (ordre_decidable y x).
case (ordre_decidable z y).
intros H' H'0 H'1; split; [ idtac | split ]; (red in |- *; intros H'4).
intros; elim (Gt_not_eq _ _ H'0); auto.
intros; elim (Gt_not_eq _ _ H'); auto.
intros; elim (Gt_not_eq _ _ (Gt_trans _ _ _ H' H'0)); auto.
case (ordre_decidable x z).
intros H' H'0 H'1 H'2; split; [ idtac | split ]; (red in |- *; intros H'4).
intros; elim (Gt_not_eq _ _ H'1); auto.
intros; elim (Gt_not_eq _ _ (Gt_trans _ _ _ H'1 H')); auto.
intros; elim (Gt_not_eq _ _ H'); auto.
intros H' H'0 H'1 H'2; discriminate H'2.
case (ordre_decidable z x).
intros H' H'0 H'1; discriminate H'1.
case (ordre_decidable z y).
case (egal_decidable x y).
intros H' H'0 H'1 H'2 H'3; discriminate H'3.
case (egal_decidable z x).
intros H' H'0 H'1 H'2 H'3 H'4; discriminate H'4.
intros H' H'0 H'1 H'2 H'3 H'4; split; [ idtac | split ];
 (red in |- *; intros H'5).
intros; elim H'0; auto.
intros; elim (Gt_not_eq _ _ H'1); auto.
intros; elim H'; auto.
intros H' H'0 H'1 H'2; discriminate H'2.
Qed.
 
Theorem well_aligned_distinct' :
 forall f : Plane -> K,
 well_aligned (f p) (f q) (f r) = true -> p <> q /\ q <> r /\ r <> p.
intros f H'; elim well_aligned_distinct with (1 := H'); intros H'1 H'2;
 elim H'2; clear H'2; intros H'2 H'3.
repeat split; (red in |- *; intros H'4).
elim H'1; rewrite H'4; auto.
elim H'2; rewrite H'4; auto.
elim H'3; rewrite H'4; auto.
Qed.
 
Theorem sum_square_gt_zero :
 forall a b : K,
 ~ (a = zero /\ b = zero) -> Gt (Kplus (Ktimes a a) (Ktimes b b)) zero.
intros a b H'; elim (carre_positif a).
intros H'0; try assumption.
replace zero with (Kplus zero zero).
rewrite (Radd_comm RT (Ktimes a a)).
apply Gt_plus_GeGt.
apply carre_positif.
auto.
ring.
intros H'0; try assumption.
rewrite H'0.
elim (carre_positif b).
rewrite (Radd_0_l RT (Ktimes b b)).
auto.
intros H'1; try assumption.
apply H' || case H'; split.
elim K_integre with (1 := H'0); auto.
elim K_integre with (1 := H'1); auto.
Qed.
 
Theorem mult_opp_opp :
 forall x y : K, Ktimes x y = Ktimes (Koppose x) (Koppose y).
intros; ring.
Qed.
 
Theorem well_aligned_det2 :
 forall xp yp xq yq xr yr : K,
 Gt
   (Ktimes (Kplus xq (Koppose xp))
      (Ktimes (Kplus xr (Koppose xq)) (Kplus xr (Koppose xp)))) zero ->
 (exists mn : K,
    (exists md : K,
       (exists b : K,
          ~ (mn = zero :>K /\ md = zero :>K) /\
          zero = Kplus (Koppose (Ktimes md yp)) (Kplus (Ktimes mn xp) b) :>K /\
          zero = Kplus (Koppose (Ktimes md yq)) (Kplus (Ktimes mn xq) b) :>K /\
          zero = Kplus (Koppose (Ktimes md yr)) (Kplus (Ktimes mn xr) b) :>K))) ->
 well_aligned xp xq xr = true -> Gt (D_2 xp yp xq yq xr yr) zero.
intros xp yp xq yq xr yr.
intros Dummy.
unfold D_2 in |- *.
intros H'1 H'2; try assumption.
elim H'1; auto.
intros mn.
intros H'3; elim H'3; intros md E; elim E; intros b E0; elim E0; clear E H'3.
case (egal_decidable mn zero).
intros H'3; rewrite H'3.
unfold line_equation in |- *.
repeat rewrite mult_zero.
repeat rewrite (Radd_0_l RT).
intros H'4 H'5; elim H'5; intros H'6 H'7; elim H'7; intros H'8 H'9;
 try exact H'8; clear H'7 H'5.
generalize (move_oppose_zero _ _ H'6); clear H'6; intros H'6.
generalize (move_oppose_zero _ _ H'9); clear H'9; intros H'9.
generalize (move_oppose_zero _ _ H'8); clear H'8; intros H'8.
rewrite determinant_column_sum3.
replace
 (determinant3x3 one xp (Ktimes yp yp) one xq (Ktimes yq yq) one xr
    (Ktimes yr yr)) with zero.
rewrite
 (Radd_comm RT
    (determinant3x3 one xp (Ktimes xp xp) one xq (Ktimes xq xq) one xr
       (Ktimes xr xr)) zero).
rewrite
 (Radd_0_l RT
    (determinant3x3 one xp (Ktimes xp xp) one xq (Ktimes xq xq) one xr
       (Ktimes xr xr))).
rewrite factorize_van_der_monde.
replace
 (Ktimes (Kplus xp (Koppose xq))
    (Ktimes (Kplus xq (Koppose xr)) (Kplus xr (Koppose xp)))) with
 (Ktimes (Kplus xq (Koppose xp))
    (Ktimes (Kplus xr (Koppose xq)) (Kplus xr (Koppose xp)))).
2: ring.
exact Dummy.
elim
 K_integre
  with
    (a := Ktimes md md)
    (b := determinant3x3 one xp (Ktimes yp yp) one xq 
            (Ktimes yq yq) one xr (Ktimes yr yr)); 
 auto.
intros H'5; try assumption.
apply H'4 || case H'4; split; [ idtac | auto ].
auto.
elim K_integre with (1 := H'5); auto.
rewrite determinant_rotate_column.
rewrite <- determinant_column_mult2.
repeat rewrite (Rmul_assoc RT).
repeat rewrite (Rmul_comm RT (Ktimes md md)).
repeat rewrite <- (Rmul_assoc RT).
rewrite H'8; rewrite H'9; rewrite H'6.
repeat rewrite (Rmul_assoc RT).
repeat rewrite <- (Rmul_comm RT md).
rewrite H'8; rewrite H'9; rewrite H'6.
unfold determinant3x3 in |- *; ring.
unfold line_equation in |- *.
case (egal_decidable md zero).
intros H'3; rewrite H'3.
elim well_aligned_distinct with (1 := H'2).
repeat rewrite mult_zero.
rewrite oppose_zero.
repeat rewrite (Radd_0_l RT).
intros H' H'0 H'4 H'5 H'6; elim H'6; intros H'7 H'8; elim H'8;
 intros H'9 H'10; try exact H'9; clear H'8 H'6.
apply H' || case H'; auto.
apply times_simplify with (a := mn); auto.
apply plus_simplify with (a := b).
repeat rewrite (Radd_comm RT b); rewrite <- H'7; auto.
intros H'3 H'4 H'5 H'6; try assumption.
apply Ktimes_Gt with (a := Ktimes md md).
apply carre_positif.
rewrite determinant_rotate_column.
rewrite <- determinant_column_mult2.
repeat rewrite (Rmul_comm RT (Ktimes md md)).
repeat rewrite (Rdistr_l RT).
cut
 (forall y x : K,
  Ktimes (Ktimes x x) (Ktimes y y) = Ktimes (Ktimes y x) (Ktimes y x)).
intros H''7; try assumption.
repeat rewrite (H''7 md).
cut (Ktimes md yp = Kplus (Ktimes mn xp) b).
cut (Ktimes md yq = Kplus (Ktimes mn xq) b).
cut (Ktimes md yr = Kplus (Ktimes mn xr) b).
clear H'6.
intros H'6 H'7 H'8; try assumption.
rewrite H'8.
rewrite H'7.
rewrite H'6.
repeat rewrite (Rdistr_l RT).
cut
 (forall x y z : K, Ktimes x (Kplus y z) = Kplus (Ktimes x y) (Ktimes x z)).
intros H'9; try assumption.
repeat rewrite H'9.
cut (forall x y : K, Ktimes (Ktimes x y) b = Ktimes b (Ktimes x y)).
intros H'10; try assumption.
repeat rewrite H'10.
repeat rewrite determinant_column_sum2.
replace
 (determinant3x3 xp (Ktimes b (Ktimes mn xp)) one xq
    (Ktimes b (Ktimes mn xq)) one xr (Ktimes b (Ktimes mn xr)) one) with zero.
replace
 (determinant3x3 xp (Ktimes b b) one xq (Ktimes b b) one xr (Ktimes b b) one)
 with zero.
repeat rewrite <- H''7.
repeat rewrite <- (Rmul_comm RT (Ktimes md md)).
repeat rewrite <- (Rmul_comm RT (Ktimes mn mn)).
repeat rewrite determinant_column_mult2.
rewrite (Radd_0_l RT).
rewrite <- (Radd_comm RT zero).
rewrite (Radd_0_l RT).
rewrite <- (Radd_comm RT zero).
rewrite (Radd_0_l RT).
rewrite <-
 (Rdistr_l RT (Ktimes md md) (Ktimes mn mn)
    (determinant3x3 xp (Ktimes xp xp) one xq (Ktimes xq xq) one xr
       (Ktimes xr xr) one)).
rewrite <- determinant_rotate_column.
rewrite factorize_van_der_monde.
apply Gt_stable_mult.
apply sum_square_gt_zero; auto.
red in |- *; intros H'11; elim H'11; intros H'12 H'13; try exact H'12;
 clear H'11.
apply H'5 || case H'5; auto.
rewrite mult_opp_opp.
rewrite (Koppose_Kplus xp (Koppose xq)).
rewrite (Kopp_opp xq).
rewrite (Radd_comm RT (Koppose xp) xq).
rewrite (Rmul_comm RT (Kplus xq (Koppose xr)) (Kplus xr (Koppose xp))).
rewrite <-
 (Kmult_Koppose_permute (Kplus xr (Koppose xp)) (Kplus xq (Koppose xr)))
 .
rewrite (Koppose_Kplus xq (Koppose xr)).
rewrite (Kopp_opp xr).
rewrite (Radd_comm RT (Koppose xq) xr).
rewrite (Rmul_comm RT (Kplus xr (Koppose xp)) (Kplus xr (Koppose xq))).
apply Dummy.
unfold determinant3x3 in |- *; ring.
unfold determinant3x3 in |- *; ring.
intros; ring.
intros; ring.
elim H'6; intros H'7 H'8; elim H'8; intros H'9 H'10; try exact H'9;
 clear H'8 H'6; cut (forall x : K, Ktimes md x = Kplus (Ktimes md x) zero);
 [ intros H'6; rewrite H'6 | intros; ring ].
rewrite H'10; ring.
elim H'6; intros H'7 H'8; elim H'8; intros H'9 H'10; try exact H'9;
 clear H'8 H'6; cut (forall x : K, Ktimes md x = Kplus (Ktimes md x) zero);
 [ intros H'6; rewrite H'6 | intros; ring ].
rewrite H'9; ring.
elim H'6; intros H'7 H'8; elim H'8; intros H'9 H'10; try exact H'9;
 clear H'8 H'6; cut (forall x : K, Ktimes md x = Kplus (Ktimes md x) zero);
 [ intros H'6; rewrite H'6 | intros; ring ].
rewrite H'7; ring.
intros; ring.
Qed.
 
Theorem well_aligned_sign :
 forall x y z : K,
 well_aligned x y z = true :>bool ->
 Gt
   (Ktimes (Kplus y (Koppose x))
      (Ktimes (Kplus z (Koppose y)) (Kplus z (Koppose x)))) zero.
intros x y z.
unfold well_aligned in |- *.
case (ordre_decidable y x).
case (ordre_decidable z y).
intros H' H'0 H'1; apply Gt_stable_mult.
apply Gt_zero_GT; auto.
apply Gt_stable_mult.
apply Gt_zero_GT; auto.
apply Gt_zero_GT.
apply Gt_trans with y; auto.
case (ordre_decidable x z).
intros H' H'0 H'1 H'2; try assumption.
apply Gt_stable_mult.
apply Gt_zero_GT; auto.
rewrite mult_opp_opp.
repeat rewrite Koppose_Kplus.
repeat rewrite Kopp_opp.
repeat rewrite (Radd_comm RT (Koppose z)).
apply Gt_stable_mult.
apply Gt_zero_GT.
apply Gt_trans with x; auto.
apply Gt_zero_GT; auto.
intros H' H'0 H'1 H'2; discriminate H'2.
case (ordre_decidable z x).
intros H' H'0 H'1; discriminate H'1.
case (ordre_decidable z y).
case (egal_decidable x y).
intros H' H'0 H'1 H'2 H'3; discriminate H'3.
case (egal_decidable z x).
intros H' H'0 H'1 H'2 H'3 H'4; discriminate H'4.
intros H' H'0 H'1 H'2 H'3 H'4; try assumption.
rewrite mult_opp_opp.
rewrite (Koppose_Kplus y (Koppose x)).
rewrite (Kopp_opp x).
rewrite (Radd_comm RT (Koppose y) x).
apply Gt_stable_mult.
apply Gt_zero_GT; auto.
elim non_Gt_Ge with (1 := H'3); auto.
intros H'5; try assumption.
apply H'0 || case H'0; auto.
rewrite <-
 (Kmult_Koppose_permute (Kplus z (Koppose y)) (Kplus z (Koppose x)))
 .
rewrite (Koppose_Kplus z (Koppose x)).
rewrite (Kopp_opp x).
rewrite (Radd_comm RT (Koppose z) x).
apply Gt_stable_mult.
apply Gt_zero_GT; auto.
apply Gt_zero_GT.
elim non_Gt_Ge with (1 := H'2); auto.
intros H'5; try assumption.
apply H' || case H'; auto.
intros H' H'0 H'1 H'2; discriminate H'2.
Qed.
 
Theorem well_aligned_sign_2 :
 forall x y z : K,
 well_aligned x y z = false :>bool ->
 ~
 Gt
   (Ktimes (Kplus y (Koppose x))
      (Ktimes (Kplus z (Koppose y)) (Kplus z (Koppose x)))) zero.
intros x y z.
unfold well_aligned in |- *.
case (ordre_decidable y x).
case (ordre_decidable z y).
intros H' H'0 H'1; discriminate H'1.
case (ordre_decidable x z).
intros H' H'0 H'1 H'2; discriminate H'2.
intros H' H'0 H'1 H'2; red in |- *; intros H'3; try exact H'3.
cut (Ge (Kplus y (Koppose x)) zero).
intros H'4; try assumption.
generalize (Ktimes_Gt _ _ H'4 H'3).
2: apply Ge_move_right_2.
2: rewrite (Kopp_opp x).
2: apply Gt_Ge.
2: auto.
cut (Ge (Kplus y (Koppose z)) zero).
intros H'5; try assumption.
rewrite mult_opp_opp.
rewrite (Radd_comm RT z (Koppose y)).
rewrite (Koppose_Kplus (Koppose y) z).
rewrite (Kopp_opp y).
intros H'6; try assumption.
generalize (Ktimes_Gt _ _ H'5 H'6).
rewrite (Koppose_Kplus z (Koppose x)).
rewrite (Kopp_opp x).
rewrite (Radd_comm RT (Koppose z) x).
intros H'7; try assumption.
apply H' || case H'; try assumption.
apply Gt_Gt_zero.
auto.
apply Ge_move_right_2.
rewrite (Kopp_opp z).
apply non_Gt_Ge.
auto.
case (ordre_decidable z x).
intros H' H'0 H'1; red in |- *.
cut (Ge (Kplus x (Koppose y)) zero).
intros H'2; try assumption.
rewrite mult_opp_opp.
rewrite (Koppose_Kplus y (Koppose x)).
rewrite (Kopp_opp x).
rewrite (Radd_comm RT (Koppose y) x).
intros H'3; try assumption.
generalize (Ktimes_Gt _ _ H'2 H'3).
rewrite <-
 (Kmult_Koppose_permute (Kplus z (Koppose y)) (Kplus z (Koppose x)))
 .
cut (Ge (Kplus z (Koppose y)) zero).
intros H'4 H'5; try assumption.
generalize (Ktimes_Gt _ _ H'4 H'5).
rewrite (Koppose_Kplus z (Koppose x)).
rewrite (Kopp_opp x).
rewrite (Radd_comm RT (Koppose z) x).
intros H'6; try assumption.
elim (Gt_antisym _ _ H').
apply Gt_Gt_zero.
auto.
apply Gt_Ge.
apply Gt_zero_GT.
apply Gt_Ge_trans with x.
auto.
apply non_Gt_Ge; auto.
apply Ge_move_right_2.
rewrite (Kopp_opp y).
apply non_Gt_Ge; auto.
case (ordre_decidable z y).
case (egal_decidable x y).
intros H'; rewrite H'.
rewrite (Ropp_def RT y).
rewrite (mult_zero (Ktimes (Kplus z (Koppose y)) (Kplus z (Koppose y)))).
intros H'0 H'1 H'2 H'3; try assumption.
red in |- *; intros H'4; try exact H'4.
elim (Gt_not_eq _ _ H'4); auto.
case (egal_decidable z x).
intros H'; rewrite H'.
rewrite (Ropp_def RT x).
rewrite (Rmul_comm RT (Kplus x (Koppose y)) zero).
rewrite (mult_zero (Kplus x (Koppose y))).
rewrite (Rmul_comm RT (Kplus y (Koppose x)) zero).
rewrite (mult_zero (Kplus y (Koppose x))).
intros H'0 H'1 H'2 H'3 H'4; red in |- *; intros H'5; try exact H'5.
elim (Gt_not_eq _ _ H'5); auto.
intros H' H'0 H'1 H'2 H'3 H'4; try exact H'4.
discriminate H'4.
intros H' H'0 H'1 H'2; try assumption.
rewrite mult_opp_opp.
rewrite (Koppose_Kplus y (Koppose x)).
rewrite (Kopp_opp x).
rewrite (Radd_comm RT (Koppose y) x).
red in |- *; intros H'3; try exact H'3.
cut (Ge (Kplus x (Koppose y)) zero).
intros H'4; try assumption.
generalize (Ktimes_Gt _ _ H'4 H'3).
clear H'4 H'3.
rewrite <-
 (Kmult_Koppose_permute (Kplus z (Koppose y)) (Kplus z (Koppose x)))
 .
rewrite (Koppose_Kplus z (Koppose x)).
rewrite (Kopp_opp x).
rewrite (Rmul_comm RT (Kplus z (Koppose y)) (Kplus (Koppose z) x)).
rewrite (Radd_comm RT (Koppose z) x).
cut (Ge (Kplus x (Koppose z)) zero).
intros H'3 H'4; try assumption.
generalize (Ktimes_Gt _ _ H'3 H'4).
intros H'5; try assumption.
apply H' || case H'; auto.
apply Gt_Gt_zero.
auto.
apply Ge_move_right_2.
rewrite (Kopp_opp z).
apply non_Gt_Ge; auto.
apply Ge_move_right_2.
rewrite (Kopp_opp y).
apply non_Gt_Ge; auto.
Qed.
 
Theorem well_aligned_abscisse_Gt_D_3 :
 det p q r = zero ->
 well_aligned (abscisse p) (abscisse q) (abscisse r) = true ->
 Gt
   (D_2 (abscisse p) (ordonnee p) (abscisse q) (ordonnee q) 
      (abscisse r) (ordonnee r)) zero.
intros H' H'0; apply well_aligned_det2; auto.
generalize H'0; clear H'0.
exact (well_aligned_sign (abscisse p) (abscisse q) (abscisse r)).
exact (colinear_decompose p q r H').
Qed.
 
Theorem well_aligned_det3 :
 Gt
   (Ktimes (Kplus (ordonnee q) (Koppose (ordonnee p)))
      (Ktimes (Kplus (ordonnee r) (Koppose (ordonnee q)))
         (Kplus (ordonnee r) (Koppose (ordonnee p))))) zero ->
 det p q r = zero ->
 well_aligned (ordonnee p) (ordonnee q) (ordonnee r) = true ->
 Gt
   (D_3 (abscisse p) (ordonnee p) (abscisse q) (ordonnee q) 
      (abscisse r) (ordonnee r)) zero.
replace
 (D_3 (abscisse p) (ordonnee p) (abscisse q) (ordonnee q) 
    (abscisse r) (ordonnee r)) with
 (D_2 (ordonnee p) (abscisse p) (ordonnee q) (abscisse q) 
    (ordonnee r) (abscisse r)).
intros H' H'0 H'1; try assumption.
apply well_aligned_det2; auto.
elim (colinear_decompose p q r H'0).
intros mn.
intros H'2; elim H'2; intros md E; elim E; intros b E0; elim E0;
 intros H'3 H'4; elim H'4; intros H'5 H'6; elim H'6; 
 intros H'7 H'8; try exact H'7; clear H'6 H'4 E0 E H'2.
exists (Koppose md).
exists (Koppose mn).
exists b.
repeat rewrite (Rmul_comm RT (Koppose mn)).
repeat rewrite <- Kmult_Koppose_permute.
repeat rewrite Kopp_opp.
split;
 [ red in |- *; intros H'2; elim H'2; intros H'4 H'6; try exact H'6;
    clear H'2
 | idtac ].
apply H'3 || case H'3; split; [ try assumption | idtac ].
rewrite <- (Kopp_opp mn).
rewrite H'6.
apply oppose_zero.
rewrite <- (Kopp_opp md).
rewrite H'4.
apply oppose_zero.
split; [ idtac | split; [ try assumption | idtac ] ].
rewrite H'5.
unfold line_equation in |- *; ring.
rewrite H'7.
unfold line_equation in |- *; ring.
rewrite H'8.
unfold line_equation in |- *; ring.
unfold D_3 in |- *; unfold D_2 in |- *.
rewrite (Radd_comm RT (Ktimes (ordonnee p) (ordonnee p))).
rewrite (Radd_comm RT (Ktimes (ordonnee q) (ordonnee q))).
rewrite (Radd_comm RT (Ktimes (ordonnee r) (ordonnee r))).
auto.
Qed.
 
Theorem well_aligned_ordonnee_Gt_D_2 :
 det p q r = zero :>K ->
 well_aligned (ordonnee p) (ordonnee q) (ordonnee r) = true :>bool ->
 Gt (D_3 (abscisse p) (ordonnee p) (abscisse q) (ordonnee q) 
      (abscisse r) (ordonnee r)) zero.
intros H' H'0; try assumption.
apply well_aligned_det3; auto.
apply well_aligned_sign; auto.
Qed.
 
Theorem oriented'_distinct :
 oriented' p q r = true -> p <> q /\ q <> r /\ r <> p.
unfold oriented' in |- *.
case (egal_decidable (det p q r) zero).
case p; case q; case r.
intros xr yr xq yq xp yp.
case (egal_decidable xp xq); unfold well_aligned in |- *.
case (ordre_decidable yr yq).
case (ordre_decidable yq yp).
intros H' H'0 H'1; rewrite H'1.
intros H'2 H'3; split; [ red in |- *; intros H'4; try exact H'4 | idtac ].
injection H'4.
intros H'5; try assumption.
elim (Gt_not_eq _ _ H'); auto.
split; [ red in |- *; intros H'4; try exact H'4 | idtac ].
injection H'4.
intros H'5 H'6; try assumption.
elim (Gt_not_eq _ _ H'0); auto.
red in |- *; intros H'4; injection H'4.
intros H'5 H'6; try assumption.
generalize (Gt_trans _ _ _ H'0 H'); intros H'7; elim (Gt_not_eq _ _ H'7);
 auto.
case (ordre_decidable yr yp).
intros H' H'0 H'1 H'2 H'3 H'4; discriminate H'4.
case (egal_decidable yp yq).
intros H' H'0 H'1 H'2 H'3 H'4 H'5; discriminate H'5.
case (egal_decidable yr yp).
intros H' H'0 H'1 H'2 H'3 H'4 H'5 H'6; discriminate H'6.
intros H' H'0 H'1 H'2 H'3 H'4 H'5 H'6; split;
 [ red in |- *; intros H'7; injection H'7 | idtac ].
intros; elim H'0; auto.
split; (red in |- *; intros H'7; injection H'7).
intros; elim (Gt_not_eq _ _ H'3); auto.
intros; elim H'; auto.
case (ordre_decidable yq yp).
case (ordre_decidable yp yr).
intros H' H'0 H'1 H'2 H'3 H'4; split;
 [ red in |- *; intros H'5; try exact H'5 | idtac ].
injection H'5.
intros; elim (Gt_not_eq _ _ H'0); auto.
split; (red in |- *; intros H'5; injection H'5).
intros; elim (Gt_not_eq _ _ (Gt_trans _ _ _ H'0 H')); auto.
intros; elim (Gt_not_eq _ _ H'); auto.
intros H' H'0 H'1 H'2 H'3 H'4; discriminate H'4.
case (ordre_decidable yr yp).
intros H' H'0 H'1 H'2 H'3 H'4; discriminate H'4.
intros H' H'0 H'1 H'2 H'3 H'4; discriminate H'4.
case (ordre_decidable xq xp).
case (ordre_decidable xr xq).
intros H' H'0 H'1 H'2 H'3; split; [ idtac | split ];
 (red in |- *; intros H'4; injection H'4).
intros; elim H'1; auto.
intros; elim (Gt_not_eq _ _ H'); auto.
intros; elim (Gt_not_eq _ _ (Gt_trans _ _ _ H' H'0)); auto.
case (ordre_decidable xp xr).
intros H' H'0 H'1 H'2 H'3 H'4; split; [ idtac | split ];
 (red in |- *; intros H'5; injection H'5).
intros; elim H'2; auto.
intros; elim (Gt_not_eq _ _ (Gt_trans _ _ _ H'1 H')); auto.
intros; elim (Gt_not_eq _ _ H'); auto.
intros H' H'0 H'1 H'2 H'3 H'4; discriminate H'4.
case (ordre_decidable xr xp).
intros H' H'0 H'1 H'2 H'3; discriminate H'3.
case (ordre_decidable xr xq).
case (egal_decidable xp xq).
intros H' H'0 H'1 H'2 H'3; apply H'3 || case H'3; auto.
case (egal_decidable xr xp).
intros H' H'0 H'1 H'2 H'3 H'4 H'5 H'6; discriminate H'6.
intros H' H'0 H'1 H'2 H'3 H'4 H'5 H'6; split; [ idtac | split ];
 (red in |- *; intros H'7; injection H'7).
intros H'8 H'9; try assumption.
apply H'0 || case H'0; auto.
intros H'8 H'9; try assumption.
elim (Gt_not_eq _ _ H'1); auto.
intros H'8 H'9; try assumption.
apply H' || case H'; auto.
intros H' H'0 H'1 H'2 H'3 H'4; discriminate H'4.
intros H' H'0; try assumption.
split; [ idtac | split ]; (red in |- *; intros H'1; elim H'; rewrite H'1).
case q; case r; intros; unfold det, determinant3x3 in |- *; ring.
case p; case r; intros; unfold det, determinant3x3 in |- *; ring.
case p; case q; intros; unfold det, determinant3x3 in |- *; ring.
Qed.
End epsilon.

Theorem vertical_line_D_2_zero :
 forall xp yp xq yq xr yr,
 det (xp, yp) (xq, yq) (xr, yr) = zero ->
 xp = xq ->
 D_2 (abscisse (pair xp yp)) (ordonnee (pair xp yp)) 
   (abscisse (pair xq yq)) (ordonnee (pair xq yq)) 
   (abscisse (pair xr yr)) (ordonnee (pair xr yr)) = zero.
intros xp yp xq yq xr yr H' H'0; try assumption.
case (egal_decidable yp yq).
intros H'1; rewrite H'1.
rewrite H'0.
unfold D_2, determinant3x3 in |- *; ring.
generalize (colinear_decompose (xp, yp) (xq, yq) (xr, yr) H').
intros H'1; elim H'1; intros mn E; elim E; intros md E0; elim E0; intros b E1;
 elim E1; intros H'2 H'3; elim H'3; intros H'4 H'5; 
 elim H'5; intros H'6 H'7; try exact H'7; clear H'5 H'3 E1 E0 E H'1.
intros H'1; try assumption.
unfold line_equation in H'4, H'6, H'7.
generalize H'6; pattern zero at 1 in |- *; rewrite H'4.
rewrite H'0.
simpl in |- *.
repeat rewrite <- (Radd_comm RT (Kplus (Ktimes mn xq) b)).
intros H'3; generalize (plus_simplify _ _ _ H'3); clear H'3; intros H'3.
cut (md = zero).
intros H'5; try assumption.
rewrite H'5 in H'4.
rewrite H'5 in H'6.
rewrite H'5 in H'7.
unfold D_2 in |- *.
apply times_simplify with mn.
auto.
rewrite <- determinant_column_mult2.
simpl in H'4, H'6, H'7.
cut (Ktimes mn xr = Koppose b).
intros H'8; rewrite H'8; clear H'8.
cut (Ktimes mn xq = Koppose b).
intros H'8; rewrite H'8; clear H'8.
intros; unfold determinant3x3 in |- *; ring.
replace (Koppose b) with (Kplus (Koppose b) zero).
rewrite H'6.
ring.
ring.
replace (Koppose b) with (Kplus (Koppose b) zero).
rewrite H'7.
ring.
ring.
apply times_simplify with (Kplus yp (Koppose yq)).
red in |- *; intros H'5; try exact H'5.
apply H'1 || case H'1; auto.
symmetry  in |- *.
apply move_oppose_zero.
rewrite (Radd_comm RT (Koppose yq) yp).
auto.
rewrite (Rdistr_l RT yp (Koppose yq) md).
rewrite (Rmul_comm RT (Kplus yp (Koppose yq)) zero).
rewrite (mult_zero (Kplus yp (Koppose yq))).
rewrite (Kmult_Koppose_permute1 yq md).
rewrite (Rmul_comm RT yq md).
rewrite <- H'3.
ring.
Qed.

Section oriented_to_sensDirect.
Variable p q r : Plane.
 
Theorem oriented'_is_sensDirect_perturbate :
 oriented' p q r = true ->
 exists eps : K,
   Gt eps zero /\
   (forall t : K,
    Gt t zero ->
    Gt eps t ->
    sensDirect (perturbate_point t p) (perturbate_point t q)
      (perturbate_point t r)).
unfold sensDirect in |- *.
unfold oriented' in |- *.
case (egal_decidable (det p q r) zero).
case p; intros xp yp.
case q; intros xq yq.
case r; intros xr yr.
case (egal_decidable xp xq).
intros H' H'0 H'1; try assumption.
exists one; split.
apply Gt_one_zero.
intros t H'2 H'3; try assumption.
generalize (perturbate_det_polynomial'(xp, yp) (xq, yq) (xr, yr)); 
 intros H'5.
unfold perturbate_det in H'5.
rewrite H'5.
rewrite H'0.
replace
 (D_2 (abscisse (pair xp yp)) (ordonnee (pair xp yp)) 
    (abscisse (pair xq yq)) (ordonnee (pair xq yq)) 
    (abscisse (pair xr yr)) (ordonnee (pair xr yr))) with zero.
rewrite (mult_zero t).
rewrite
 (Radd_0_l RT
    (Ktimes
       (D_3 (abscisse (pair xp yp)) (ordonnee (pair xp yp))
          (abscisse (pair xq yq)) (ordonnee (pair xq yq))
          (abscisse (pair xr yr)) (ordonnee (pair xr yr))) 
       (Ktimes t t))).
rewrite
 (Radd_0_l RT
    (Ktimes
       (D_3 (abscisse (pair xp yp)) (ordonnee (pair xp yp))
          (abscisse (pair xq yq)) (ordonnee (pair xq yq))
          (abscisse (pair xr yr)) (ordonnee (pair xr yr))) 
       (Ktimes t t))).
apply Gt_stable_mult.
apply well_aligned_ordonnee_Gt_D_2; auto.
auto.
rewrite vertical_line_D_2_zero; auto.
intros H' H'0 H'1; try assumption.
generalize
 (th3 (det (pair xp yp) (pair xq yq) (pair xr yr)) 
    (D_2 xp yp xq yq xr yr) (D_3 xp yp xq yq xr yr)).
intros H'2; elim H'2;
 [ intros e E; elim E; intros H'5 H'6; try exact H'6; clear E H'2
 | clear H'2
 | clear H'2 ].
exists e; split; auto.
intros t H'2 H'3; try assumption.
generalize (perturbate_det_polynomial' (xp, yp) (xq, yq) (xr, yr)).
unfold perturbate_det in |- *.
intros H'4; rewrite H'4.
apply H'6; auto.
auto.
replace xp with (abscisse (xp, yp)); auto.
replace xq with (abscisse (xq, yq)); auto.
replace xr with (abscisse (xr, yr)); auto.
pattern yp at 2 in |- *; replace yp with (ordonnee (xp, yp)); auto.
pattern yq at 2 in |- *; replace yq with (ordonnee (xq, yq)); auto.
pattern yr at 2 in |- *; replace yr with (ordonnee (xr, yr)); auto.
apply well_aligned_abscisse_Gt_D_3; auto.
case (ordre_decidable (det p q r) zero).
case
 (egal_decidable
    (D_2 (abscisse p) (ordonnee p) (abscisse q) (ordonnee q) 
       (abscisse r) (ordonnee r)) zero).
intros H' H'0 H'1 H'2; try assumption.
lapply
 (th2 (det p q r)
    (D_2 (abscisse p) (ordonnee p) (abscisse q) (ordonnee q) 
       (abscisse r) (ordonnee r))
    (D_3 (abscisse p) (ordonnee p) (abscisse q) (ordonnee q) 
       (abscisse r) (ordonnee r)));
 [ intros H'6; lapply H'6;
    [ intros H'7; try exact H'7; clear H'6 | clear H'6 ]
 | idtac ].
elim H'7; intros e E; elim E; intros H'3 H'4; try exact H'3; clear E H'7.
exists e; split; auto.
intros t H'5 H'6; try assumption.
specialize (perturbate_det_polynomial' p q r t); intros H'10.
unfold perturbate_det in H'10.
rewrite H'10.
apply H'4; auto.
auto.
auto.
intros H' H'0 H'1 H'2; try assumption.
elim
 (th1 (det p q r)
    (D_2 (abscisse p) (ordonnee p) (abscisse q) (ordonnee q) 
       (abscisse r) (ordonnee r))
    (D_3 (abscisse p) (ordonnee p) (abscisse q) (ordonnee q) 
       (abscisse r) (ordonnee r)));
 [ intros e E; elim E; intros H'8 H'9; try exact H'9; clear E
 | idtac
 | idtac ].
exists e; split; auto.
intros t H'3 H'4; try assumption.
specialize 
 (perturbate_det_polynomial' p q r t);
 intros H'10; unfold perturbate_det in H'10.
rewrite H'10.
apply H'9; auto.
auto.
auto.
intros H' H'0 H'1; discriminate H'1.
Qed.
 
Theorem div2_decrease :
 forall x : K,
 Gt x zero -> Gt x (Ktimes x (Kinv (Kplus one one))).
intros x H'; try assumption.
apply Gt_Gt_zero.
pattern x at 1 in |- *; replace x with (Ktimes x one).
rewrite <- (Kmult_Koppose_permute x (Kinv (Kplus one one))).
rewrite (Rmul_comm RT x one).
rewrite (Rmul_comm RT x (Koppose (Kinv (Kplus one one)))).
rewrite <- (Rdistr_l RT one (Koppose (Kinv (Kplus one one))) x).
apply Gt_stable_mult.
apply Ktimes_Gt with (a := Kplus one one).
apply Gt_Ge.
apply Gt_2_zero.
rewrite
 (Rmul_comm RT (Kplus one one) (Kplus one (Koppose (Kinv (Kplus one one)))))
 .
rewrite
 (Rdistr_l RT one (Koppose (Kinv (Kplus one one))) (Kplus one one))
 .
rewrite (Kmult_Koppose_permute1 (Kinv (Kplus one one)) (Kplus one one)).
rewrite (Rmul_comm RT (Kinv (Kplus one one)) (Kplus one one)).
rewrite (Kinv_def (Kplus one one)).
rewrite (Rmul_1_l RT (Kplus one one)).
apply Gt_zero_GT.
pattern one at 3 in |- *; replace one with (Kplus one zero).
apply Gt_plus.
apply Gt_one_zero.
ring.
apply Gt_not_eq.
apply Gt_2_zero.
auto.
ring.
Qed.
 
Theorem eq_to_plus_oppose_zero :
 forall x y : K, x = y -> Kplus x (Koppose y) = zero.
intros x y H'; rewrite H'.
ring.
Qed.
 
Theorem det_same_point_zero : forall a b : Plane, det a a b = zero.
intros a b; case a; intros xa ya; case b; intros xb yb;
 unfold det, determinant3x3 in |- *; ring.
Qed.
 
Theorem min_exists :
 forall e1 e2 : K,
 Gt e1 zero ->
 Gt e2 zero ->
 exists e : K, Gt e zero /\ Gt e1 e /\ Gt e2 e.
intros e1 e2 H' H'0; case (ordre_decidable e1 e2).
intros H'1; exists (Ktimes e2 (Kinv (Kplus one one))).
split; [ try assumption | idtac ].
apply div2_pos.
auto.
split; [ try assumption | idtac ].
apply Gt_trans with e2; auto.
apply div2_decrease; auto.
apply div2_decrease; auto.
intros H'1; exists (Ktimes e1 (Kinv (Kplus one one))); split;
 [ idtac | split; [ try assumption | idtac ] ].
apply div2_pos.
auto.
apply div2_decrease; auto.
apply Ge_Gt_trans with e1; auto.
apply div2_decrease; auto.
Qed.
 
Theorem Koppose_zero_is_zero : forall a : K, Koppose a = zero -> a = zero.
intros a H'; try assumption.
replace zero with (Kplus a (Koppose a)).
rewrite H'.
ring.
ring.
Qed.
 
Theorem sensDirect_perturbate_is_oriented'' :
 forall eps : K,
 Gt eps zero ->
 (forall t : K,
  Gt t zero ->
  Gt eps t ->
  sensDirect (perturbate_point t p) (perturbate_point t q)
    (perturbate_point t r)) -> oriented' p q r = true.
unfold sensDirect in |- *.
intros eps H'; try assumption.
generalize perturbate_det_polynomial'.
unfold perturbate_det in |- *.
intros H'0 H''1; lapply (H''1 (Ktimes eps (Kinv (Kplus one one))));
 [ intros H'3; lapply H'3; [ clear H'3 | clear H'3 ] | idtac ].
intros H''; generalize H''.
rewrite H'0.
clear H'0.
2: apply div2_decrease; auto.
2: apply div2_pos; auto.
generalize H'' H''1; clear H'' H''1.
case p; intros xp yp; case q; intros xq yq; case r; intros xr yr.
intros H'' H''1.
intros H'0.
unfold oriented' in |- *.
case (egal_decidable (det (pair xp yp) (pair xq yq) (pair xr yr)) zero).
intros H'2; try assumption.
elim (colinear_decompose _ _ _ H'2).
intros mn H'3; elim H'3; intros md E; elim E; intros b E0; elim E0;
 intros H'4 H'5; elim H'5; intros H'6 H'7; elim H'7; 
 intros H'8 H'9; try exact H'9; clear H'7 H'5 E0 E H'3.
cut (Ktimes mn xp = Kplus (Ktimes md yp) (Koppose b) :>K).
cut (Ktimes mn xq = Kplus (Ktimes md yq) (Koppose b) :>K).
cut (Ktimes mn xr = Kplus (Ktimes md yr) (Koppose b) :>K).
intros Hline1 Hline2 Hline3.
case (egal_decidable xp xq).
intros H'1.
CaseEq (well_aligned yp yq yr); intros H; auto.
generalize H'0.
rewrite H'2.
replace
 (D_2 (abscisse (pair xp yp)) (ordonnee (pair xp yp)) 
    (abscisse (pair xq yq)) (ordonnee (pair xq yq)) 
    (abscisse (pair xr yr)) (ordonnee (pair xr yr))) with zero.
2: rewrite vertical_line_D_2_zero; auto.
repeat rewrite mult_zero.
repeat rewrite (Radd_0_l RT).
unfold D_3 in |- *.
rewrite
 (Radd_comm RT (Ktimes (abscisse (pair xp yp)) (abscisse (pair xp yp))))
 .
rewrite
 (Radd_comm RT (Ktimes (abscisse (pair xq yq)) (abscisse (pair xq yq))))
 .
rewrite
 (Radd_comm RT (Ktimes (abscisse (pair xr yr)) (abscisse (pair xr yr))))
 .
cut (Gt (Ktimes mn mn) zero).
2: elim (carre_positif mn).
2: auto.
2: intros H'3; try assumption.
2: cut (mn = zero).
2: intros H'5; try assumption.
2: generalize H'8.
2: rewrite H'6.
2: unfold line_equation in |- *.
2: simpl in |- *.
2: rewrite H'5.
2: repeat rewrite mult_zero.
2: repeat rewrite (Radd_0_l RT).
2: repeat rewrite <- (Radd_comm RT b).
2: intros H'7; try assumption.
2: generalize (plus_simplify _ _ _ H'7).
2: intros H'10; try assumption.
2: generalize (eq_to_plus_oppose_zero _ _ H'10).
2: rewrite (Kopp_opp (Ktimes md yq)).
2: rewrite (Rmul_comm RT md yq).
2: pattern (Koppose (Ktimes md yp)) at 1 in |- *;
    rewrite <- (Kmult_Koppose_permute md yp).
2: rewrite (Rmul_comm RT md (Koppose yp)).
2: rewrite <- (Rdistr_l RT (Koppose yp) yq md).
2: intros H'12; try assumption.
2: elim (K_integre _ _ H'12).
2: intros H'13; try assumption.
2: generalize (Koppose_inverse_plus _ _ H'13).
2: rewrite (Kopp_opp yp).
2: intros H'14; try assumption.
2: generalize H''.
2: rewrite H'1.
2: rewrite H'14.
2: rewrite det_same_point_zero.
2: intros H'15; try assumption.
2: elim (Gt_not_eq _ _ H'15); auto.
2: intros H'13; try assumption.
2: apply H'4 || case H'4; auto.
2: elim (K_integre _ _ H'3); auto.
intros H'3.
rewrite <-
 (Rmul_comm RT
    (Ktimes (Ktimes eps (Kinv (Kplus one one)))
       (Ktimes eps (Kinv (Kplus one one))))).
intros H'5; try assumption.
generalize (carre_positif (Ktimes eps (Kinv (Kplus one one)))).
intros H'7.
generalize (Ktimes_Gt _ _ H'7 H'5); clear H'5 H'7; intros H'5.
generalize (Gt_stable_mult _ _ H'3 H'5).
simpl in |- *.
rewrite factorize_second_determinant with (t := md) (b := Koppose b); auto.
clear H'3 H'5.
intros H'3; try assumption.
generalize (Ktimes_Gt _ _ (Gt_Ge _ _ (sum_square_gt_zero _ _ H'4)) H'3).
intros H'5.
clear H'3.
elim (well_aligned_sign_2 _ _ _ H).
rewrite mult_opp_opp.
rewrite (Koppose_Kplus yq (Koppose yp)).
rewrite (Kopp_opp yp).
rewrite (Radd_comm RT (Koppose yq) yp).
rewrite (Rmul_comm RT (Kplus yr (Koppose yq)) (Kplus yr (Koppose yp))).
rewrite <-
 (Kmult_Koppose_permute (Kplus yr (Koppose yp)) (Kplus yr (Koppose yq)))
 .
rewrite (Koppose_Kplus yr (Koppose yq)).
rewrite (Kopp_opp yq).
rewrite (Rmul_comm RT (Kplus yr (Koppose yp)) (Kplus (Koppose yr) yq)).
rewrite (Radd_comm RT (Koppose yr) yq).
auto.
intros H'1; try assumption.
cut (Gt (Ktimes md md) zero).
intros H'3.
case (ordre_decidable (D_2 xp yp xq yq xr yr) zero).
CaseEq (well_aligned xp xq xr).
auto.
intros H g.
elim (well_aligned_sign_2 _ _ _ H).
unfold D_2 in g.
generalize (Gt_stable_mult _ _ H'3 g); clear g H'3.
rewrite factorize_second_determinant with (t := mn) (b := b); auto.
intros H'3; try assumption.
rewrite mult_opp_opp.
rewrite (Koppose_Kplus xq (Koppose xp)).
rewrite (Kopp_opp xp).
rewrite (Radd_comm RT (Koppose xq) xp).
rewrite (Rmul_comm RT (Kplus xr (Koppose xq))).
rewrite <- Kmult_Koppose_permute.
rewrite (Koppose_Kplus xr (Koppose xq)).
rewrite (Kopp_opp xq).
rewrite (Radd_comm RT (Koppose xr) xq).
rewrite (Rmul_comm RT (Kplus xr (Koppose xp))).
apply Ktimes_Gt with (a := Kplus (Ktimes md md) (Ktimes mn mn)).
apply Ge_stable_plus.
apply carre_positif.
apply carre_positif.
auto.
rewrite Hline3; ring.
rewrite Hline2; ring.
rewrite Hline1; ring.
intros H'5; try assumption.
cut (D_2 xp yp xq yq xr yr <> zero).
intros H'7; try assumption.
elim
 (th3 (Koppose (det (pair xp yp) (pair xq yq) (pair xr yr)))
    (Koppose (D_2 xp yp xq yq xr yr)) (Koppose (D_3 xp yp xq yq xr yr)));
 [ intros e E; elim E; intros H'15 H'16; try exact H'16; clear E
 | idtac
 | idtac ].
elim (min_exists eps e); auto.
intros e' H'10; elim H'10; intros H'11 H'12; elim H'12; intros H'13 H'14;
 try exact H'13; clear H'12 H'10.
generalize (H''1 e' H'11 H'13).
generalize perturbate_det_polynomial'.
unfold perturbate_det in |- *; intros H'10; rewrite H'10.
clear H'10.
intros H'10; elim (Gt_antisym _ _ H'10).
apply Gt_Koppose_Koppose2.
repeat rewrite Koppose_Kplus.
rewrite oppose_zero.
rewrite
 (Rmul_comm RT
    (D_2 (abscisse (pair xp yp)) (ordonnee (pair xp yp))
       (abscisse (pair xq yq)) (ordonnee (pair xq yq))
       (abscisse (pair xr yr)) (ordonnee (pair xr yr))))
 .
rewrite <- (Kmult_Koppose_permute e').
rewrite
 (Rmul_comm RT e'
    (Koppose
       (D_2 (abscisse (pair xp yp)) (ordonnee (pair xp yp))
          (abscisse (pair xq yq)) (ordonnee (pair xq yq))
          (abscisse (pair xr yr)) (ordonnee (pair xr yr)))))
 .
rewrite
 (Rmul_comm RT
    (D_3 (abscisse (pair xp yp)) (ordonnee (pair xp yp))
       (abscisse (pair xq yq)) (ordonnee (pair xq yq))
       (abscisse (pair xr yr)) (ordonnee (pair xr yr))))
 .
rewrite <- Kmult_Koppose_permute.
rewrite (Rmul_comm RT (Ktimes e' e')).
apply H'16; auto.
rewrite H'2.
apply oppose_zero.
apply Gt_Koppose_Koppose2.
rewrite oppose_zero.
rewrite (Kopp_opp (D_2 xp yp xq yq xr yr)).
elim (non_Gt_Ge _ _ H'5); auto.
intros H'10; elim H'7; auto.
red in |- *; intros H'7; try exact H'7.
cut (Ktimes (Ktimes md md) (D_2 xp yp xq yq xr yr) = zero).
unfold D_2 in |- *.
rewrite factorize_second_determinant with (t := mn) (b := b); auto.
intros H'10; try assumption.
elim K_integre with (1 := H'10); clear H'10.
intros H'10; try assumption.
elim (Gt_not_eq (Kplus (Ktimes md md) (Ktimes mn mn)) zero).
apply sum_square_gt_zero; auto.
red in |- *; intros H'11; elim H'11; intros H'12 H'13; try exact H'13;
 clear H'11.
apply H'4 || case H'4; auto.
auto.
intros H'10; try assumption.
elim K_integre with (1 := H'10); clear H'10.
intros H'10; try assumption.
apply H'1 || case H'1; auto.
apply diff_nul.
auto.
intros H'10; try assumption.
elim K_integre with (1 := H'10); clear H'10.
intros H'10; try assumption.
generalize (diff_nul _ _ H'10).
intros H'11; try assumption.
generalize Hline1.
rewrite <- H'11.
rewrite Hline2.
rewrite (Radd_comm RT (Ktimes md yq) (Koppose b)).
rewrite (Radd_comm RT (Ktimes md yr) (Koppose b)).
intros H'12; try assumption.
generalize (plus_simplify _ _ _ H'12).
intros H'13; try assumption.
lapply (times_simplify md yq yr);
 [ intros H'17; lapply H'17;
    [ intros H'18; try exact H'18; clear H'17 | clear H'17 ]
 | idtac ].
generalize H''.
rewrite H'18.
rewrite H'11.
rewrite det_cyclique.
rewrite det_same_point_zero.
intros H'14; try assumption.
elim (Gt_not_eq _ _ H'14); auto.
auto.
red in |- *; intros H'14; try exact H'14.
generalize H'3.
rewrite H'14.
rewrite (mult_zero zero).
intros H'15; try assumption.
elim (Gt_not_eq _ _ H'15); auto.
intros H'10; try assumption.
generalize (diff_nul _ _ H'10).
intros H'11; try assumption.
generalize Hline1.
rewrite H'11.
rewrite Hline3.
repeat rewrite <- (Radd_comm RT (Koppose b)).
intros H'12; try assumption.
generalize (plus_simplify _ _ _ H'12).
intros H'13; try assumption.
lapply (times_simplify md yp yr);
 [ intros H'17; lapply H'17; [ intros H'18 | try assumption ] | idtac ].
generalize H''.
rewrite H'18.
rewrite H'11.
rewrite <- det_cyclique.
rewrite det_same_point_zero.
intros H'14; try assumption.
elim (Gt_not_eq _ _ H'14); auto.
red in |- *; intros H'14; try exact H'14.
generalize H'3.
rewrite H'14.
rewrite (mult_zero zero).
intros H'15; try assumption.
elim (Gt_not_eq _ _ H'15); auto.
rewrite Hline3; ring.
rewrite Hline2; ring.
rewrite Hline1; ring.
rewrite H'7.
ring.
elim (carre_positif md); auto.
intros H'3; try assumption.
cut (md = zero).
intros H'5; try assumption.
generalize Hline3.
generalize Hline2.
rewrite H'5.
repeat rewrite mult_zero.
intros H'7; rewrite <- H'7.
intros H'10; try assumption.
generalize (times_simplify mn xp xq).
intros H'11; lapply H'11;
 [ intros H'12; lapply H'12;
    [ intros H'13; clear H'11 | try assumption; clear H'11 ]
 | clear H'11 ].
apply H'1 || case H'1; auto.
red in |- *; intros H'11; try exact H'11.
apply H'4 || case H'4; auto.
elim (K_integre _ _ H'3); auto.
replace (Ktimes md yr) with (Kplus (Ktimes md yr) zero).
rewrite H'9.
unfold line_equation in |- *.
simpl in |- *.
ring.
ring.
replace (Ktimes md yq) with (Kplus (Ktimes md yq) zero).
rewrite H'8.
unfold line_equation in |- *.
simpl in |- *.
ring.
ring.
replace (Ktimes md yp) with (Kplus (Ktimes md yp) zero).
rewrite H'6.
unfold line_equation in |- *.
simpl in |- *.
ring.
ring.
intros H'1; try assumption.
case (ordre_decidable (det (pair xp yp) (pair xq yq) (pair xr yr)) zero).
intros H'2; auto.
intros H'2; try exact H'2.
case
 (egal_decidable
    (D_2 (abscisse (pair xp yp)) (ordonnee (pair xp yp))
       (abscisse (pair xq yq)) (ordonnee (pair xq yq))
       (abscisse (pair xr yr)) (ordonnee (pair xr yr))) zero).
intros H'3; try assumption.
clear H'0.
clear H''.
elim
 (th2 (Koppose (det (pair xp yp) (pair xq yq) (pair xr yr)))
    (Koppose
       (D_2 (abscisse (pair xp yp)) (ordonnee (pair xp yp))
          (abscisse (pair xq yq)) (ordonnee (pair xq yq))
          (abscisse (pair xr yr)) (ordonnee (pair xr yr))))
    (Koppose
       (D_3 (abscisse (pair xp yp)) (ordonnee (pair xp yp))
          (abscisse (pair xq yq)) (ordonnee (pair xq yq))
          (abscisse (pair xr yr)) (ordonnee (pair xr yr)))));
 [ intros e E; elim E; intros H'8 H'9; clear E | idtac | idtac ].
elim (min_exists e eps);
 [ intros e0 E; elim E; intros H'7 H'10; elim H'10; intros H'11 H'12;
    try exact H'11; clear H'10 E
 | idtac
 | idtac ].
lapply (H''1 e0);
 [ intros H'4; lapply H'4;
    [ intros H'5; try exact H'5; clear H'4 | clear H'4 ]
 | idtac ].
generalize perturbate_det_polynomial'; unfold perturbate_det in |- *.
intros H'0; try exact H'0.
generalize H'5.
rewrite H'0.
intros H'4; try exact H'4.
elim (Gt_antisym _ _ H'4).
apply Gt_Koppose_Koppose2.
repeat rewrite Koppose_Kplus.
rewrite oppose_zero.
rewrite
 (Rmul_comm RT
    (D_2 (abscisse (pair xp yp)) (ordonnee (pair xp yp))
       (abscisse (pair xq yq)) (ordonnee (pair xq yq))
       (abscisse (pair xr yr)) (ordonnee (pair xr yr))) e0)
 .
rewrite <-
 (Kmult_Koppose_permute e0
    (D_2 (abscisse (pair xp yp)) (ordonnee (pair xp yp))
       (abscisse (pair xq yq)) (ordonnee (pair xq yq))
       (abscisse (pair xr yr)) (ordonnee (pair xr yr))))
 .
rewrite
 (Rmul_comm RT e0
    (Koppose
       (D_2 (abscisse (pair xp yp)) (ordonnee (pair xp yp))
          (abscisse (pair xq yq)) (ordonnee (pair xq yq))
          (abscisse (pair xr yr)) (ordonnee (pair xr yr)))))
 .
rewrite
 (Rmul_comm RT
    (D_3 (abscisse (pair xp yp)) (ordonnee (pair xp yp))
       (abscisse (pair xq yq)) (ordonnee (pair xq yq))
       (abscisse (pair xr yr)) (ordonnee (pair xr yr))) 
    (Ktimes e0 e0)).
rewrite <-
 (Kmult_Koppose_permute (Ktimes e0 e0)
    (D_3 (abscisse (pair xp yp)) (ordonnee (pair xp yp))
       (abscisse (pair xq yq)) (ordonnee (pair xq yq))
       (abscisse (pair xr yr)) (ordonnee (pair xr yr))))
 .
rewrite
 (Rmul_comm RT (Ktimes e0 e0)
    (Koppose
       (D_3 (abscisse (pair xp yp)) (ordonnee (pair xp yp))
          (abscisse (pair xq yq)) (ordonnee (pair xq yq))
          (abscisse (pair xr yr)) (ordonnee (pair xr yr)))))
 .
auto.
auto.
auto.
auto.
auto.
apply Gt_Koppose.
elim (non_Gt_Ge _ _ H'2).
auto.
intros H'0; try assumption.
apply H'1 || case H'1; auto.
rewrite H'3.
apply oppose_zero.
intros H'3; try assumption.
clear H'0.
clear H''.
elim
 (th1 (Koppose (det (pair xp yp) (pair xq yq) (pair xr yr)))
    (Koppose
       (D_2 (abscisse (pair xp yp)) (ordonnee (pair xp yp))
          (abscisse (pair xq yq)) (ordonnee (pair xq yq))
          (abscisse (pair xr yr)) (ordonnee (pair xr yr))))
    (Koppose
       (D_3 (abscisse (pair xp yp)) (ordonnee (pair xp yp))
          (abscisse (pair xq yq)) (ordonnee (pair xq yq))
          (abscisse (pair xr yr)) (ordonnee (pair xr yr)))));
 [ intros e E; elim E; intros H'8 H'9; try exact H'9; clear E
 | idtac
 | idtac ].
elim (min_exists e eps);
 [ intros e0 E; elim E; intros H'7 H'10; elim H'10; intros H'11 H'12;
    try exact H'11; clear H'10 E
 | idtac
 | idtac ].
lapply (H''1 e0);
 [ intros H'4; lapply H'4; [ clear H'4 | clear H'4 ] | idtac ].
generalize perturbate_det_polynomial'; unfold perturbate_det in |- *.
intros H'0; rewrite H'0.
intros H'4; try assumption.
elim (Gt_antisym _ _ H'4).
apply Gt_Koppose_Koppose2.
repeat rewrite Koppose_Kplus.
rewrite oppose_zero.
rewrite
 (Rmul_comm RT
    (D_2 (abscisse (pair xp yp)) (ordonnee (pair xp yp))
       (abscisse (pair xq yq)) (ordonnee (pair xq yq))
       (abscisse (pair xr yr)) (ordonnee (pair xr yr))) e0)
 .
rewrite <-
 (Kmult_Koppose_permute e0
    (D_2 (abscisse (pair xp yp)) (ordonnee (pair xp yp))
       (abscisse (pair xq yq)) (ordonnee (pair xq yq))
       (abscisse (pair xr yr)) (ordonnee (pair xr yr))))
 .
rewrite
 (Rmul_comm RT e0
    (Koppose
       (D_2 (abscisse (pair xp yp)) (ordonnee (pair xp yp))
          (abscisse (pair xq yq)) (ordonnee (pair xq yq))
          (abscisse (pair xr yr)) (ordonnee (pair xr yr)))))
 .
rewrite
 (Rmul_comm RT
    (D_3 (abscisse (pair xp yp)) (ordonnee (pair xp yp))
       (abscisse (pair xq yq)) (ordonnee (pair xq yq))
       (abscisse (pair xr yr)) (ordonnee (pair xr yr))) 
    (Ktimes e0 e0)).
rewrite <-
 (Kmult_Koppose_permute (Ktimes e0 e0)
    (D_3 (abscisse (pair xp yp)) (ordonnee (pair xp yp))
       (abscisse (pair xq yq)) (ordonnee (pair xq yq))
       (abscisse (pair xr yr)) (ordonnee (pair xr yr))))
 .
rewrite
 (Rmul_comm RT (Ktimes e0 e0)
    (Koppose
       (D_3 (abscisse (pair xp yp)) (ordonnee (pair xp yp))
          (abscisse (pair xq yq)) (ordonnee (pair xq yq))
          (abscisse (pair xr yr)) (ordonnee (pair xr yr)))))
 .
auto.
auto.
auto.
auto.
auto.
apply Gt_Koppose.
elim (non_Gt_Ge _ _ H'2); auto.
intros H'0; try assumption.
apply H'1 || case H'1; auto.
red in |- *; intros H'0; try exact H'0.
apply H'3 || case H'3; try assumption.
auto.
apply Koppose_zero_is_zero.
auto.
Qed.
End oriented_to_sensDirect.

Theorem oriented'_ax1 :
 forall p q r : Plane, oriented' p q r = true -> oriented' q r p = true.
intros p q r H'.
elim (oriented'_is_sensDirect_perturbate p q r);
 [ intros eps E; elim E; intros H'3 H'4; try exact H'3; clear E | idtac ];
 auto.
apply sensDirect_perturbate_is_oriented'' with (eps := eps); auto.
unfold sensDirect in *; intros; rewrite <- det_cyclique; auto.
Qed.

Theorem oriented'_ax2 :
 forall p q r : Plane, oriented' p q r = true -> oriented' p r q <> true.
intros p q r H'; red in |- *; intros H'0; try exact H'0.
elim (oriented'_is_sensDirect_perturbate p q r);
 [ intros eps E; elim E; intros H'3 H'4; try exact H'3; clear E | idtac ];
 auto.
elim (oriented'_is_sensDirect_perturbate p r q);
 [ intros eps0 E; elim E; intros H'7 H'8; try exact H'7; clear E | idtac ];
 auto.
elim (min_exists eps eps0);
 [ intros e E; elim E; intros H'9 H'10; elim H'10; intros H'11 H'12;
    try exact H'11; clear H'10 E
 | idtac
 | idtac ]; auto.
lapply (H'4 e);
 [ intro H'2; lapply H'2; [ intro H'5; try exact H'5; clear H'2 | clear H'2 ]
 | idtac ]; auto.
elim (Axiom2 _ _ _ H'5).
apply H'8; auto.
Qed.

Theorem oriented'_ax3 :
 forall p q r : Plane,
 p <> q ->
 q <> r -> r <> p -> oriented' p q r = true \/ oriented' p r q = true.
intros p q r H' H'0 H'1; try assumption.
elim
 (polynomial_sign (det p q r)
    (D_2 (abscisse p) (ordonnee p) (abscisse q) (ordonnee q) 
       (abscisse r) (ordonnee r))
    (D_3 (abscisse p) (ordonnee p) (abscisse q) (ordonnee q) 
       (abscisse r) (ordonnee r)));
 [ intros H'6; elim H'6; intros e E; elim E; intros H'7 H'8; try exact H'7;
    clear E H'6
 | intros H'6
 | idtac ].
left; try assumption.
apply sensDirect_perturbate_is_oriented'' with (eps := e); auto.
intros t H'2 H'3; unfold sensDirect in |- *.
generalize perturbate_det_polynomial'.
unfold perturbate_det in |- *.
intros H'4; rewrite H'4.
auto.
right; try assumption.
elim H'6; intros e E; elim E; intros H'2 H'3; try exact H'3; clear E H'6.
apply sensDirect_perturbate_is_oriented'' with (eps := e); auto.
intros t H'4 H'5; try assumption.
unfold sensDirect in |- *.
rewrite det_inverse.
apply Gt_Koppose.
generalize perturbate_det_polynomial'.
unfold perturbate_det in |- *.
intros H'6; rewrite H'6.
apply H'3; auto.
red in |- *; intros H'2; elim H'2; intros H'3 H'4; elim H'4; intros H'5 H'6;
 try exact H'5; clear H'4 H'2.
lapply
 (not_all_determinants_zero p q r (abscisse p) (ordonnee p) 
    (abscisse q) (ordonnee q) (abscisse r) (ordonnee r));
 [ intro H'14; lapply H'14;
    [ intro H'15; lapply H'15;
       [ intro H'16; lapply H'16;
          [ intro H'17; lapply H'17;
             [ intro H'18; lapply H'18;
                [ intro H'19; lapply H'19;
                   [ intro H'20; lapply H'20;
                      [ intro H'21; apply H'21 || case H'21; try assumption;
                         clear H'20 H'19 H'18 H'17 H'16 H'15 H'14
                      | clear H'20 H'19 H'18 H'17 H'16 H'15 H'14 ]
                   | clear H'19 H'18 H'17 H'16 H'15 H'14 ]
                | clear H'18 H'17 H'16 H'15 H'14 ]
             | clear H'17 H'16 H'15 H'14 ]
          | clear H'16 H'15 H'14 ]
       | clear H'15 H'14 ]
    | clear H'14 ]
 | idtac ]; auto.
case r; auto.
case q; auto.
case p; auto.
Qed.

Theorem oriented'_ax4 :
 forall p q r s : Plane,
 oriented' s q r = true ->
 oriented' p s r = true -> oriented' p q s = true -> oriented' p q r = true.
intros p q r s H' H'0 H'1; try assumption.
elim (oriented'_is_sensDirect_perturbate s q r);
 [ intros eps E; elim E; intros H'6 H'7; try exact H'7; clear E | idtac ];
 auto.
elim (oriented'_is_sensDirect_perturbate p s r);
 [ intros eps0 E; elim E; intros H'8 H'9; try exact H'9; clear E | idtac ];
 auto.
elim (oriented'_is_sensDirect_perturbate p q s);
 [ intros eps1 E; elim E; intros H'10 H'11; try exact H'11; clear E | idtac ];
 auto.
elim (min_exists eps eps0);
 [ intros e E; elim E; intros H'12 H'13; elim H'13; intros H'14 H'15;
    try exact H'14; clear H'13 E
 | idtac
 | idtac ].
elim (min_exists e eps1);
 [ intros e0 E; elim E; intros H'13 H'16; elim H'16; intros H'17 H'18;
    try exact H'17; clear H'16 E
 | idtac
 | idtac ].
apply sensDirect_perturbate_is_oriented'' with (eps := e0); auto.
intros t H'2 H'3; try assumption.
apply Axiom4 with (t := perturbate_point t s); auto.
apply H'7.
auto.
apply Gt_trans with e; auto.
apply Gt_trans with e0; auto.
apply H'9.
auto.
apply Gt_trans with e; auto.
apply Gt_trans with e0; auto.
apply H'11.
auto.
apply Gt_trans with e0; auto.
auto.
auto.
auto.
auto.
Qed.

Theorem oriented'_ax5 :
 forall p q r s t : Plane,
 oriented' t s p = true ->
 oriented' t s q = true ->
 oriented' t s r = true ->
 oriented' t p q = true -> oriented' t q r = true -> oriented' t p r = true.
intros p q r s t H' H'0 H'1 H'2 H'3; try assumption.
elim oriented'_is_sensDirect_perturbate with (1 := H').
intros x H'4; elim H'4; intros H'5 H'6; try exact H'6; clear H'4.
elim oriented'_is_sensDirect_perturbate with (1 := H'0).
intros x0 H'4; elim H'4; intros H'7 H'8; try exact H'8; clear H'4.
elim oriented'_is_sensDirect_perturbate with (1 := H'1).
intros x1 H'4; elim H'4; intros H'9 H'10; try exact H'10; clear H'4.
elim oriented'_is_sensDirect_perturbate with (1 := H'2).
intros x2 H'4; elim H'4; intros H'11 H'12; try exact H'12; clear H'4.
elim oriented'_is_sensDirect_perturbate with (1 := H'3).
intros x3 H'4; elim H'4; intros H'13 H'14; try exact H'14; clear H'4.
elim (min_exists x x0);
 [ intros e E; elim E; intros H'18 H'19; elim H'19; intros H'20 H'21;
    try exact H'20; clear H'19 E
 | idtac
 | idtac ]; auto.
elim (min_exists e x1);
 [ intros e0 E; elim E; intros H'19 H'22; elim H'22; intros H'23 H'24;
    try exact H'23; clear H'22 E
 | idtac
 | idtac ]; auto.
elim (min_exists e0 x2);
 [ intros e1 E; elim E; intros H'22 H'25; elim H'25; intros H'26 H'27;
    try exact H'26; clear H'25 E
 | idtac
 | idtac ]; auto.
elim (min_exists e1 x3);
 [ intros e2 E; elim E; intros H'25 H'28; elim H'28; intros H'29 H'30;
    try exact H'29; clear H'28 E
 | idtac
 | idtac ]; auto.
apply sensDirect_perturbate_is_oriented'' with (eps := e2); auto.
intros t0 H'4 H'15; try assumption.
apply Axiom5 with (s := perturbate_point t0 s) (q := perturbate_point t0 q);
 auto.
apply H'6; auto.
apply Gt_trans with e; auto.
apply Gt_trans with e0; auto.
apply Gt_trans with e1; auto.
apply Gt_trans with e2; auto.
apply H'8; auto.
apply Gt_trans with e; auto.
apply Gt_trans with e0; auto.
apply Gt_trans with e1; auto.
apply Gt_trans with e2; auto.
apply H'10; auto.
apply Gt_trans with e0; auto.
apply Gt_trans with e1; auto.
apply Gt_trans with e2; auto.
apply H'12; auto.
apply Gt_trans with e1; auto.
apply Gt_trans with e2; auto.
apply H'14; auto.
apply Gt_trans with e2; auto.
Qed.

Require Import List.

(*
Lemma exists_perturbation :
  forall l, exists e, forall t, Gt t zero -> Gt e t ->
  (forall p q r, In p l -> In q l -> In r l -> 
    (oriented'_prop p q r <->
       OT (perturbate_point t p) (perturbate_point t q) (perturbate_point t r)))/\
  (forall p q r, In p l -> In q l -> In r l -> p <> q -> q <> r -> p <> r ->
    OT (perturbate_point t p) (perturbate_point t q) (perturbate_point t r)).
intros l; induction l.
exists one.
intros t tpos tlit; split.
simpl; intuition.
*)
End perturb.
