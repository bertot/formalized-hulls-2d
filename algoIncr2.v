(* AddPath "$HOME/ctcoq/stage". *)
Require Export algoIncr.
Require Export Arith.
Require Export List.
Require Export Classical.
 
Theorem convexRec_dans_CH :
 forall (p t : Plan) (lCC cc : CC) (u : Plan),
 cycle_dans_CH t (u :: lCC) cc ->
 cycle_dans_CH
   (@fst _ _
      (@snd _ _ (convexRec (calculCouleur t (succ_CH t cc) p) p t (u :: lCC))))
   (@snd _ _
      (@snd _ _ (convexRec (calculCouleur t (succ_CH t cc) p) p t (u :: lCC))))
   (p :: cc).
intros p t lCC cc.
induction lCC as [| a lCC HreclCC]; intros u.
intros.
 CaseEq (calculCouleur t (succ_CH t cc) p); simpl in |- *;
  CaseEq (directBool u t p); unfold cycle_dans_CH in |- *; 
  simpl in |- *; intros.
elim H2; clear H2; intros.
rewrite <- H3; rewrite <- H2; clear H2 H3.
unfold cycle_dans_CH in H; simpl in H.
apply CH_incremental_inv_non_p; auto.
elim H2.
elim H2; clear H2; intros.
rewrite <- H3; rewrite <- H2; clear H2 H3.
unfold cycle_dans_CH in H; simpl in H.
apply CH_incremental_inv_non_p; auto.
elim H2; clear H2; intros.
rewrite <- H3; rewrite <- H2; clear H2 H3.
apply CH_incremental_inv_t_egal_p; auto.
apply (cycle_point_extremal_t t nil cc u); auto.
cut (u = pred_CH t cc).
intro.
rewrite <- H2; auto.
apply (unicite_CH_Prop_gauche u (pred_CH t cc) t cc).
unfold cycle_dans_CH in H; simpl in H; auto.
apply pred_CH_Prop.
apply (cycle_point_extremal_t t nil cc u); auto.
generalize
 (@refl_equal _ (convexRec (calculCouleur t (succ_CH t cc) p) p t (a :: lCC))).
pattern (convexRec (calculCouleur t (succ_CH t cc) p) p t (a :: lCC)) at -1
 in |- *.
case (convexRec (calculCouleur t (succ_CH t cc) p) p t (a :: lCC)).
intros coulRec pRec HconvexRec.
generalize (@refl_equal _ pRec); pattern pRec at -1 in |- *; case pRec.
intros tRec lRec HpRec.
rewrite HpRec in HconvexRec.
intro.
generalize HconvexRec; intro.
simpl in |- *; simpl in HconvexRec0; rewrite HconvexRec0; clear HconvexRec0.
 CaseEq coulRec; CaseEq (directBool u a p); simpl in |- *;
  unfold cycle_dans_CH in |- *; intros.
simpl in H2.
elim H2; clear H2; intros.
elim H2; clear H2; intros.
rewrite <- H3; rewrite <- H2; clear H2 H3.
unfold cycle_dans_CH in H; simpl in H.
apply CH_incremental_inv_non_p; auto.
cut (CH_Prop (p :: cc) (pair a p)).
intro.
 CaseEq lRec.
rewrite H4 in H2; elim H2; clear H2; intros.
rewrite <- H5; rewrite <- H2; clear H2 H5.
elim
 (ConvexRec_commence_par_rouge (calculCouleur t (succ_CH t cc) p) p t lCC a);
 intros.
generalize HconvexRec; intro.
simpl in H2; simpl in HconvexRec0; rewrite HconvexRec0 in H2; simpl in H2;
 clear HconvexRec0.
rewrite H4 in H2; elim H2; intros.
discriminate H5.
generalize HconvexRec; intro.
simpl in H2; simpl in HconvexRec0; rewrite HconvexRec0 in H2; simpl in H2;
 clear HconvexRec0.
elim H2; clear H2 H4; intros.
rewrite H4; auto.
generalize HconvexRec; intro.
simpl in |- *; simpl in HconvexRec0; rewrite HconvexRec0; clear HconvexRec0;
 simpl in |- *.
auto.
rewrite H4 in H2; elim H2; clear H2; intros.
elim H2; clear H2; intros.
rewrite <- H5; rewrite <- H2; clear H2 H5.
elim
 (ConvexRec_commence_par_rouge (calculCouleur t (succ_CH t cc) p) p t lCC a);
 intros.
generalize HconvexRec; intro.
simpl in H2; simpl in HconvexRec0; rewrite HconvexRec0 in H2; simpl in H2;
 clear HconvexRec0.
elim H2; intros.
rewrite H4 in H5; injection H5; intros.
rewrite H7; auto.
generalize HconvexRec; intro.
simpl in H2; simpl in HconvexRec0; rewrite HconvexRec0 in H2; simpl in H2;
 clear HconvexRec0.
elim H2; clear H2; intros.
rewrite H2 in H4; discriminate H4.
generalize HconvexRec; intro.
simpl in |- *; simpl in HconvexRec0; rewrite HconvexRec0; clear HconvexRec0;
 simpl in |- *.
auto.
unfold cycle_dans_CH in HreclCC; apply (HreclCC a).
intros.
unfold cycle_dans_CH in H; apply H.
simpl in |- *; auto.
generalize HconvexRec; intro.
simpl in |- *; simpl in HconvexRec0; rewrite HconvexRec0; clear HconvexRec0;
 simpl in |- *.
rewrite H4; auto.
apply CH_incremental_inv_s_egal_p; auto.
apply (cycle_point_extremal t (a :: lCC) cc u); auto.
simpl in |- *; auto.
cut
 (calculCouleur a (succ_CH a cc) p =
  @fst _ _ (convexRec (calculCouleur t (succ_CH t cc) p) p t (a :: lCC))).
intro.
generalize HconvexRec; intro.
simpl in H3; simpl in HconvexRec0; rewrite HconvexRec0 in H3; simpl in H3;
 clear HconvexRec0.
rewrite H0 in H3; auto.
apply
 (convexRec_couleur_precedent p t a (calculCouleur t (succ_CH t cc) p) lCC cc).
eauto.
cut (u = pred_CH a cc).
intro.
rewrite <- H3; auto.
apply (pred_CH_dans_cycle t (a :: lCC) cc u u a); auto.
simpl in |- *; auto.
unfold cycle_dans_CH in HreclCC; apply (HreclCC a).
intros.
 apply H; auto.
simpl in |- *; auto.
generalize HconvexRec; intro.
simpl in |- *; simpl in HconvexRec0; rewrite HconvexRec0; clear HconvexRec0;
 simpl in |- *.
auto.
elim
 (ConvexRec_commence_par_bleu (calculCouleur t (succ_CH t cc) p) p t lCC a).
generalize HconvexRec; intro.
simpl in |- *; simpl in HconvexRec0; rewrite HconvexRec0; clear HconvexRec0;
 simpl in |- *.
intros.
rewrite H3 in H2; elim H2; clear H2; intros.
elim H2; clear H2; intros.
rewrite <- H4; rewrite <- H2; clear H4 H2.
unfold cycle_dans_CH in H; simpl in H.
apply CH_incremental_inv_non_p; auto.
rewrite <- H3 in H2.
unfold cycle_dans_CH in HreclCC; apply (HreclCC a).
intros.
 apply H; auto.
simpl in |- *; auto.
generalize HconvexRec; intro.
simpl in |- *; simpl in HconvexRec0; rewrite HconvexRec0; clear HconvexRec0;
 simpl in |- *.
auto.
generalize HconvexRec; intro.
simpl in |- *; simpl in HconvexRec0; rewrite HconvexRec0; clear HconvexRec0;
 simpl in |- *.
auto.
elim
 (ConvexRec_commence_par_bleu (calculCouleur t (succ_CH t cc) p) p t lCC a).
generalize HconvexRec; intro.
simpl in |- *; simpl in HconvexRec0; rewrite HconvexRec0; clear HconvexRec0;
 simpl in |- *.
intros.
rewrite H3 in H2; elim H2; clear H2; intros.
elim H2; clear H2; intros.
rewrite <- H4; rewrite <- H2; clear H4 H2.
apply CH_incremental_inv_t_egal_p; auto.
apply (cycle_point_extremal t lCC cc a); auto.
eauto.
simpl in |- *; auto.
cut (u = pred_CH a cc).
intro.
rewrite <- H2; auto.
apply (pred_CH_dans_cycle t (a :: lCC) cc u u a); auto.
simpl in |- *; auto.
cut
 (calculCouleur a (succ_CH a cc) p =
  @fst _ _ (convexRec (calculCouleur t (succ_CH t cc) p) p t (a :: lCC))).
intro.
generalize HconvexRec; intro.
simpl in H2; simpl in HconvexRec0; rewrite HconvexRec0 in H2; simpl in H2;
 clear HconvexRec0.
rewrite H0 in H2; auto.
apply
 (convexRec_couleur_precedent p t a (calculCouleur t (succ_CH t cc) p) lCC cc).
eauto.
unfold cycle_dans_CH in HreclCC; apply (HreclCC a).
intros.
 apply H; auto.
simpl in |- *; auto.
generalize HconvexRec; intro.
simpl in |- *; simpl in HconvexRec0; rewrite HconvexRec0; clear HconvexRec0;
 simpl in |- *.
rewrite H3; auto.
generalize HconvexRec; intro.
simpl in |- *; simpl in HconvexRec0; rewrite HconvexRec0; clear HconvexRec0;
 simpl in |- *.
auto.
Qed.
 
Definition CH_dans_cycle (t : Plan) (lCC cc : CC) :=
  forall a b : Plan, CH_Prop cc (a, b) -> in_couple a b t lCC.
 
Lemma point_extremal_dans_cc :
 forall (t : Plan) (cc : CC), point_extremal t cc -> in_CC t cc.
intros.
elim H; clear H; intros; elim H; clear H; intros; unfold CH_Prop in H;
 simpl in H; repeat (elim H; intro; clear H; intros H; auto).
Qed.
 
 
Lemma convexRecBleu_garde_tete :
 forall (t p : Plan) (lCC : CC) (a : Plan),
 @fst _ _ (@snd _ _ (convexRec Bleu p t (a :: lCC))) = t.
intros t p lCC.
induction lCC as [| a lCC HreclCC]; intros.
simpl in |- *.
 CaseEq (directBool a t p); simpl in |- *; auto.
 CaseEq (convexRec Bleu p t (a :: lCC)).
 CaseEq p0.
rewrite H0 in H.
generalize H; simpl in |- *; intros Htemp; rewrite Htemp; clear Htemp;
 simpl in |- *.
 CaseEq c; CaseEq (directBool a0 a p); simpl in |- *; elim (HreclCC a);
  generalize H; simpl in |- *; intros Htemp; rewrite Htemp; 
  clear Htemp; simpl in |- *; auto.
Qed.
