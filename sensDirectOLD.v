AddPath "$HOME/ctcoq/stage".
Require Export determinant.
 
Definition Plan := R * R.
 
Definition abscisse : Plan -> R :=
   [p : Plan]
       Cases p of   (pair x y) => x
       end.
 
Definition ordonnee : Plan -> R :=
   [p : Plan]
       Cases p of   (pair x y) => y
       end.
 
Definition det : Plan -> Plan -> Plan -> R :=
   [p, q, r : Plan]  (determinant3x3 (abscisse p) (ordonnee p) one (abscisse q)
                                     (ordonnee q) one (abscisse r) (ordonnee r)
                                     one).
 
Definition sensDirect : Plan -> Plan -> Plan -> Prop := [p, q, r : Plan]  (Gt (det p q
                                                                                   r) zero).
Parameter aucun_triplet_aligne:(p, q, r : Plan)  ~ (det p q r) = zero.
 
Lemma elimine_tete_plus: (a, b, c, d, e : R)  (Rminus (Rplus a (Rplus b c)) (Rplus a (Rplus d e))) = (Rminus (Rplus b c) (Rplus d e)).
Intros.
Rewrite (Rminus_plus_assoc_r (Rplus a (Rplus b c)) a (Rplus d e)).
Rewrite (Rplus_commutative a (Rplus b c)).
Rewrite (Rminus_plus_assoc_l (Rplus b c) a a).
Rewrite Rminus_self; Rewrite zero_neutral_r.
Trivial.
Qed.
 
Lemma remonte_element_plus: (a, b, c : R)  (Rplus b (Rplus a c)) = (Rplus a (Rplus b c)).
Intros.
Rewrite (Rplus_associative b a c).
Rewrite (Rplus_commutative b a).
Rewrite (Rplus_associative a b c).
Trivial.
Qed.
 
Lemma remonte_moins: (a, b, c, d : R)  (Rplus (Rminus a b) (Rminus c d)) = (Rminus (Rplus a c) (Rplus b d)).
Intros.
Rewrite <- (Rminus_plus_assoc_l (Rminus a b) c d).
Rewrite (Rplus_commutative (Rminus a b) c).
Rewrite <- (Rminus_plus_assoc_l c a b).
Rewrite <- (Rminus_plus_assoc_r (Rplus c a) b d).
Rewrite (Rplus_commutative c a).
Trivial.
Qed.
 
Lemma decompose_det: (p, q, r, t : Plan)  (det p q r) = (Rplus (det t q r) (Rplus (det p t
                                                                                       r) (det p q
                                                                                               t))).
Intros.
Unfold det determinant3x3.
Generalize (abscisse p) (ordonnee p) (abscisse q) (ordonnee q) (abscisse r)
           (ordonnee r) (abscisse t) (ordonnee t).
Intros.
Repeat Rewrite one_neutral_r.
Generalize (Rtimes r6 r3) (Rtimes r2 r5) (Rtimes r4 r7) (Rtimes r4 r3)
           (Rtimes r2 r7) (Rtimes r6 r5) (Rtimes r0 r7) (Rtimes r6 r1)
           (Rtimes r4 r1) (Rtimes r0 r5) (Rtimes r0 r3) (Rtimes r2 r1).
Intros.
Repeat Rewrite remonte_moins.
Repeat Rewrite <- Rplus_associative.
Repeat Rewrite (remonte_element_plus r8).
Rewrite elimine_tete_plus.
Repeat Rewrite (remonte_element_plus r10); Rewrite elimine_tete_plus.
Rewrite (Rplus_commutative r19 r14).
Repeat Rewrite (remonte_element_plus r14); Rewrite elimine_tete_plus.
Repeat Rewrite (remonte_element_plus r13); Rewrite elimine_tete_plus.
Repeat Rewrite (remonte_element_plus r12); Rewrite elimine_tete_plus.
Rewrite (Rplus_commutative r18 r15).
Repeat Rewrite (remonte_element_plus r15); Rewrite elimine_tete_plus.
Rewrite (remonte_element_plus r9); Rewrite (Rplus_commutative r19 r17).
Rewrite (Rplus_commutative r18 r16).
Trivial.
Qed.
 
Lemma barycentre_abscisse: (p, q, r, t : Plan)  (Rtimes (det p q r) (abscisse t)) =
                                                (Rplus (Rtimes (det t q r) (abscisse p)) (Rplus (Rtimes (det p t
                                                                                                             r) (abscisse q)) (Rtimes (det p q
                                                                                                                                           t) (abscisse r)))).
Intros.
Unfold det determinant3x3.
Generalize (abscisse p) (ordonnee p) (abscisse q) (ordonnee q) (abscisse r)
           (ordonnee r) (abscisse t) (ordonnee t).
Intros xP yP xQ yQ xR yR xT yT.
Repeat Rewrite one_neutral_r.
Repeat Rewrite Rtimes_distribute_minus_r.
Repeat Rewrite Rtimes_distribute_plus_r.
Repeat Rewrite remonte_moins.
Repeat Rewrite <- Rplus_associative.
Cut (a, b, c : R)  (Rtimes (Rtimes a b) c) = (Rtimes (Rtimes c b) a); Intro.
Rewrite (H xP yR xQ).
Repeat Rewrite (remonte_element_plus (Rtimes (Rtimes xQ yR) xP)).
Rewrite (elimine_tete_plus (Rtimes (Rtimes xQ yR) xP)).
Rewrite (H xQ yT xP).
Repeat Rewrite (remonte_element_plus (Rtimes (Rtimes xP yT) xQ)).
Rewrite (elimine_tete_plus (Rtimes (Rtimes xP yT) xQ)).
Rewrite (H xP yT xR).
Rewrite (Rplus_commutative (Rtimes (Rtimes xQ yP) xR) (Rtimes (Rtimes xR yT) xP)).
Repeat Rewrite (remonte_element_plus (Rtimes (Rtimes xR yT) xP)).
Rewrite (elimine_tete_plus (Rtimes (Rtimes xR yT) xP)).
Rewrite (H xR yT xQ).
Repeat Rewrite (remonte_element_plus (Rtimes (Rtimes xQ yT) xR)).
Rewrite (elimine_tete_plus (Rtimes (Rtimes xQ yT) xR)).
Rewrite (Rplus_commutative (Rtimes (Rtimes xT yQ) xR) (Rtimes (Rtimes xQ yP) xR)).
Rewrite (H xQ yP xR).
Repeat Rewrite (remonte_element_plus (Rtimes (Rtimes xR yP) xQ)).
Rewrite (elimine_tete_plus (Rtimes (Rtimes xR yP) xQ)).
Rewrite (H xR yQ xP).
Repeat Rewrite (remonte_element_plus (Rtimes (Rtimes xP yQ) xR)).
Rewrite (elimine_tete_plus (Rtimes (Rtimes xP yQ) xR)).
Rewrite (H xT yQ xP).
Rewrite (H xT yR xQ).
Rewrite (H xT yP xR).
Rewrite (H xT yR xP).
Rewrite (H xT yP xQ).
Rewrite (H xT yQ xR).
Repeat Rewrite (remonte_element_plus (Rtimes (Rtimes xQ yP) xT)).
Rewrite (Rplus_commutative (Rtimes (Rtimes xP yR) xT) (Rtimes (Rtimes xR yQ) xT)).
Trivial.
Intros.
Rewrite (Rtimes_commutative (Rtimes a b) c).
Rewrite (Rtimes_commutative a b).
Rewrite (Rtimes_associative c b a); Trivial.
Qed.
 
Lemma barycentre_ordonnee: (p, q, r, t : Plan)  (Rtimes (det p q r) (ordonnee t)) =
                                                (Rplus (Rtimes (det t q r) (ordonnee p)) (Rplus (Rtimes (det p t
                                                                                                             r) (ordonnee q)) (Rtimes (det p q
                                                                                                                                           t) (ordonnee r)))).
Intros.
Unfold det determinant3x3.
Generalize (abscisse p) (ordonnee p) (abscisse q) (ordonnee q) (abscisse r)
           (ordonnee r) (abscisse t) (ordonnee t).
Intros xP yP xQ yQ xR yR xT yT.
Repeat Rewrite one_neutral_r.
Repeat Rewrite Rtimes_distribute_minus_r.
Repeat Rewrite Rtimes_distribute_plus_r.
Repeat Rewrite remonte_moins.
Repeat Rewrite <- Rplus_associative.
Cut (a, b, c : R)  (Rtimes (Rtimes a b) c) = (Rtimes (Rtimes a c) b); Intro.
Rewrite (H xT yP yQ).
Repeat Rewrite (remonte_element_plus (Rtimes (Rtimes xT yQ) yP)).
Rewrite (elimine_tete_plus (Rtimes (Rtimes xT yQ) yP)).
Rewrite (H xQ yP yR).
Repeat Rewrite (remonte_element_plus (Rtimes (Rtimes xQ yR) yP)).
Rewrite (elimine_tete_plus (Rtimes (Rtimes xQ yR) yP)).
Rewrite (H xT yQ yR).
Repeat Rewrite (remonte_element_plus (Rtimes (Rtimes xT yR) yQ)).
Rewrite (elimine_tete_plus (Rtimes (Rtimes xT yR) yQ)).
Rewrite (H xR yQ yP).
Repeat Rewrite (remonte_element_plus (Rtimes (Rtimes xR yP) yQ)).
Rewrite (elimine_tete_plus (Rtimes (Rtimes xR yP) yQ)).
Rewrite (H xP yR yQ).
Repeat Rewrite (remonte_element_plus (Rtimes (Rtimes xP yQ) yR)).
Rewrite (elimine_tete_plus (Rtimes (Rtimes xP yQ) yR)).
Rewrite (H xT yR yP).
Rewrite (Rplus_commutative (Rtimes (Rtimes xQ yT) yR) (Rtimes (Rtimes xT yP) yR)).
Repeat Rewrite (remonte_element_plus (Rtimes (Rtimes xT yP) yR)).
Rewrite (elimine_tete_plus (Rtimes (Rtimes xT yP) yR)).
Rewrite (H xP yT yQ).
Rewrite (H xQ yT yR).
Rewrite (H xQ yT yP).
Rewrite (H xP yT yR).
Rewrite (H xR yT yQ).
Repeat Rewrite (remonte_element_plus (Rtimes (Rtimes xQ yP) yT)).
Rewrite (Rplus_commutative (Rtimes (Rtimes xQ yR) yT) (Rtimes (Rtimes xR yP) yT)).
Rewrite (H xR yT yP).
Repeat Rewrite (remonte_element_plus (Rplus (Rtimes (Rtimes xP yQ) yT) (Rtimes (Rtimes xQ yR) yT))).
Repeat Rewrite (remonte_element_plus (Rtimes (Rtimes xR yP) yT)).
Trivial.
Intros.
Rewrite <- (Rtimes_associative a b c).
Rewrite (Rtimes_commutative b c).
Rewrite (Rtimes_associative a c b); Trivial.
Qed.
 
Lemma convex_combinaison: (p, q, r, t : Plan) (a, b, c, d : R) (Rtimes d (abscisse t)) = (Rplus (Rtimes a (abscisse p)) (Rplus (Rtimes b (abscisse q)) (Rtimes c (abscisse r)))) -> (Rtimes d (ordonnee t)) = (Rplus (Rtimes a (ordonnee p)) (Rplus (Rtimes b (ordonnee q)) (Rtimes c (ordonnee r)))) -> (Rtimes d (det t s
                                                                                                                                                                                                                                                                                                                        t)) =
                                                                                                                                                                                                                                                                                                         (Rplus (Rtimes a (det t s
                                                                                                                                                                                                                                                                                                                               p)) (Rplus (Rtimes b (det t s
                                                                                                                                                                                                                                                                                                                                                         q)) (Rtimes c (det t s
                                                                                                                                                                                                                                                                                                                                                                            r)))).
