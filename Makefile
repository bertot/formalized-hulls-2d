algoIncr6.vo : algoIncr6.v algoIncr5.vo triangle.vo
	coqc algoIncr6.v
triangle.vo : triangle.v minPolar.vo 
	coqc triangle.v
algoIncr5.vo : algoIncr5.v algoIncr4.vo 
	coqc algoIncr5.v
algoIncr4.vo : algoIncr4.v algoIncr3.vo 
	coqc algoIncr4.v
algoIncr3.vo : algoIncr3.v algoIncr2.vo 
	coqc algoIncr3.v
algoIncr2.vo : algoIncr2.v algoIncr.vo 
	coqc algoIncr2.v
algoIncr.vo : algoIncr.v convexIncr.vo 
	coqc algoIncr.v
convexIncr.vo : convexIncr.v maxPolar.vo 
	coqc convexIncr.v
maxPolar.vo : maxPolar.v minPolar.vo
	coqc maxPolar.v
minPolar.vo : minPolar.v ccDefinition.vo
	coqc minPolar.v
ccDefinition.vo : ccDefinition.v axiomsKnuth.vo
	coqc ccDefinition.v
axiomsKnuth.vo : axiomsKnuth.v sensDirect.vo
	coqc axiomsKnuth.v
sensDirect.vo : sensDirect.v determinant.vo
	coqc sensDirect.v
determinant.vo : determinant.v reels.vo
	coqc determinant.v
reels.vo : reels.v
	coqc reels.v
