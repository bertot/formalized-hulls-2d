Require Export List.
Require Export Arith.
 
Theorem In_decide :
 forall A : Type,
 (forall x y : A, {x = y} + {x <> y}) ->
 forall (l : list A) (a : A), In a l \/ ~ In a l.
intros A eq_A_dec l; elim l.
intros a; right; red in |- *; intros H'; elim H'.
intros a l0 H' a0; try assumption.
case (eq_A_dec a a0).
intros H'0; left; simpl in |- *.
left; auto.
intros H'0; try assumption.
elim (H' a0); [ intros H'2; try exact H'2 | intros H'2 ].
left; simpl in |- *.
right; auto.
right; red in |- *; intros H'1; try exact H'1.
simpl in H'1.
elim H'1; [ intros H'3; try exact H'3; clear H'1 | intros H'3; clear H'1 ].
apply H'0 || case H'0; auto.
apply H'2 || case H'2; auto.
Qed.
 
Inductive included (A : Type) : list A -> list A -> Prop :=
  | included_empty : forall l : list A, included A nil l
  | included_step :
      forall (a : A) (l1 l2 : list A),
      In a l2 -> included A l1 l2 -> included A (a :: l1) l2.
 
Theorem included_in :
 forall (A : Type) (l1 l2 : list A),
 included A l1 l2 -> forall t : A, In t l1 -> In t l2.
intros A l1 l2 H'; elim H'; auto.
intros l t H'0; elim H'0.
intros a l3 l4 H'0 H'1 H'2 t H'3; simpl in H'3.
elim H'3; [ intros H'4; rewrite <- H'4; clear H'3 | intros H'4; clear H'3 ];
 auto.
Qed.

Theorem included_step_inv :
 forall (A : Type) (a : A) (l1 l2 : list A),
 included A (a :: l1) l2 -> included A l1 l2.
intros A a l1 l2 H; inversion H; auto.
Qed.

Inductive last (A : Type) (t : A) : list A -> Prop :=
  | last_found : last A t (t :: nil)
  | last_rec : forall (t' : A) (l : list A), last A t l -> last A t (t' :: l).
 
Theorem last_in : forall (A : Type) (l : list A) (t : A), last A t l -> In t l.

intros A l t H'; elim H'; simpl in |- *; auto.
Qed.
 
Inductive remove (A : Type) (a : A) : list A -> list A -> Prop :=
  | remove_end : remove A a nil nil
  | remove_found :
      forall l1 l2 : list A, remove A a l1 l2 -> remove A a (a :: l1) l2
  | remove_not_found :
      forall (l1 l2 : list A) (b : A),
      a <> b -> remove A a l1 l2 -> remove A a (b :: l1) (b :: l2).
Hint Resolve included_empty included_step.
 
Theorem included_step' :
 forall (A : Type) (l1 l2 : list A) (a : A),
 included A l1 l2 -> included A l1 (a :: l2).
intros A l1; elim l1; auto.
intros a l H' l2 a0 H'0.
inversion H'0.
apply included_step; simpl in |- *; auto.
Qed.
Hint Resolve included_step'.
 
Theorem included_refl : forall (A : Type) (l : list A), included A l l.
intros A l; elim l; simpl in |- *; auto.
intros a l0 H'; apply included_step; simpl in |- *; auto.
Qed.
 
Theorem remove_le :
 forall (A : Type) (l l1 : list A) (a : A),
 remove A a l l1 -> length l1 <= length l.
intros A l l1 a H'; elim H'; auto.
intros l2 l3 H'0 H'1; simpl in |- *.
auto.
intros l2 l3 b H'0 H'1 H'2; simpl in |- *.
auto with arith.
Qed.
 
Theorem remove_lt :
 forall (A : Type) (l l1 : list A) (a : A),
 remove A a l l1 -> In a l -> length l1 < length l.
intros A l l1 a H'; elim H'.
intros H'0; elim H'0.
intros l2 l3 H'0 H'1 H'2; simpl in |- *.
generalize (remove_le _ _ _ _ H'0).
auto with arith.
intros l2 l3 b H'0 H'1 H'2 H'3; simpl in H'3.
elim H'3; [ intros H'4; try exact H'4; clear H'3 | intros H'4; clear H'3 ].
apply H'0 || case H'0; auto.
simpl in |- *.
auto with arith.
Qed.
 
Theorem included_not_first :
 forall (A : Type) (l l1 : list A) (a : A),
 included A l (a :: l1) -> ~ In a l -> included A l l1.
intros A l; elim l.
intros l1 a H' H'0; auto.
intros a l0 H' l1 a0 H'0 H'1; try assumption.
apply included_step.
inversion H'0.
elim H1.
intros H'2; try assumption.
apply H'1 || case H'1; try assumption.
simpl in |- *.
left; auto.
intros H'2; auto.
apply H' with (a := a0).
inversion H'0; auto.
red in |- *; intros H'2; try exact H'2.
apply H'1 || case H'1; simpl in |- *.
right; auto.
Qed.
 
Theorem remove_keep_in :
 forall (A : Type) (l l1 : list A) (a : A),
 remove A a l l1 -> forall a1 : A, a <> a1 -> In a1 l -> In a1 l1.
intros A l l1 a H'; elim H'.
intros a1 H'0 H'1; elim H'1.
intros l2 l3 H'0 H'1 a1 H'2 H'3; elim H'3.
intros H'4; try assumption.
apply H'2 || case H'2; try assumption.
intros H'4; try assumption.
apply H'1.
auto.
auto.
intros l2 l3 b H'0 H'1 H'2 a1 H'3 H'4; elim H'4.
intros H'5; try assumption.
simpl in |- *.
left; auto.
intros H'5; try assumption.
simpl in |- *.
right; auto.
Qed.
 
Theorem included_remove :
 forall (A : Type) (l l1 : list A) (a : A),
 remove A a l l1 ->
 forall l2 : list A, included A l1 l2 -> included A l (a :: l2).
intros A l l1 a H'; elim H'.
auto.
intros l2 l3 H'0 H'1 l4 H'2; try assumption.
apply included_step.
simpl in |- *.
left; auto.
apply H'1.
auto.
intros l2 l3 b H'0 H'1 H'2 l4 H'3; try assumption.
inversion H'3.
apply included_step.
simpl in |- *.
auto.
apply H'2.
try exact H3.
Qed.
 
Definition remove_fun :
  forall A : Type,
  (forall x y : A, {x = y} + {x <> y}) ->
  forall (l : list A) (a : A), {l' : list A | remove A a l l'}.
intros A eq_A_dec l; elim l.
intros a; exists (nil (A:=A)); auto.
apply remove_end.
intros a l0 H' a0; try assumption.
case (eq_A_dec a a0).
elim (H' a0); intros l' E; try exact E.
intros H'0; exists l'; try assumption.
rewrite H'0.
apply remove_found.
try assumption.
intros H'0; try assumption.
elim (H' a0); intros l' E; try exact E.
exists (a :: l'); try assumption.
apply remove_not_found.
auto.
auto.
Qed.
 
Theorem In_remove_from :
 forall (A : Type) (l l1 : list A) (a : A),
 remove A a l l1 -> forall b : A, In b l1 -> In b l.
intros A l l1 a H'; elim H'.
intros b H'0; elim H'0.
intros l2 l3 H'0 H'1 b H'2; simpl in |- *.
right; auto.
intros l2 l3 b H'0 H'1 H'2 b0 H'3; try assumption.
simpl in H'3.
elim H'3; [ intros H'4; clear H'3 | intros H'4; try exact H'4; clear H'3 ].
rewrite <- H'4.
simpl in |- *.
left; auto.
simpl in |- *.
auto.
Qed.
 
Theorem included_removed :
 forall (A : Type) (l l1 : list A) (a : A),
 remove A a l l1 ->
 forall l2 : list A, included A l (a :: l2) -> included A l1 l2.
intros A l l1 a H'; elim H'.
intros l2 H'0; auto.
intros l2 l3 H'0 H'1 l4 H'2; try assumption.
inversion H'2; auto.
intros l2 l3 b H'0 H'1 H'2 l4 H'3; try exact H'3.
inversion H'3.
apply included_step.
simpl in H1.
elim H1; [ intros H'4; clear H1 | intros H'4; auto; clear H1 ].
apply H'0 || case H'0; auto.
apply H'2.
try exact H3.
Qed.
 
Theorem included_not_in_remove :
 forall A : Type,
 (forall x y : A, {x = y} + {x <> y}) ->
 forall (l1 l2 : list A) (a : A),
 remove A a l1 l2 ->
 forall l : list A, ~ In a l -> included A l l1 -> included A l l2.
intros A eq_A_dec l1 l2 a H'; elim H'.
auto.
intros l3 l4 H'0 H'1 l H'2 H'3; inversion H'3.
auto.
cut (a <> a0).
intros Hneq.
cut (~ In a l0).
intros Hnin.
apply included_step.
apply remove_keep_in with (l := l3) (a := a).
auto.
auto.
simpl in H.
elim H; [ intros H'4; clear H | intros H'4; auto; clear H ].
apply Hneq || case Hneq; auto.
apply H'1.
try assumption.
apply included_not_first with (a := a).
try exact H0.
try assumption.
red in |- *; intros H'4; try exact H'4.
apply H'2 || case H'2; try assumption.
rewrite <- H1.
simpl in |- *.
right; auto.
red in |- *; intros H'4; try exact H'4.
apply H'2 || case H'2; try assumption.
rewrite <- H1.
simpl in |- *.
left; auto.
intros l3 l4 b H'0 H'1 H'2 l H'3 H'4; try assumption.
elim (remove_fun A eq_A_dec l b).
intros l5 H'5.
apply included_remove with (l1 := l5).
try exact H'5.
apply H'2.
red in |- *; intros H'6; try exact H'6.
apply H'3 || case H'3; try assumption.
apply In_remove_from with (l1 := l5) (a := b).
try exact H'5.
try exact H'6.
apply included_removed with (l := l) (a := b).
try assumption.
try assumption.
Qed.
 
Theorem remove_not_in :
 forall (A : Type) (l l1 : list A) (a : A),
 remove A a l l1 -> ~ In a l -> l = l1.
intros A l l1 a H'; elim H'.
auto.
intros l2 l3 H'0 H'1 H'2; apply H'2 || case H'2; try assumption.
simpl in |- *.
left; auto.
intros l2 l3 b H'0 H'1 H'2 H'3; try assumption.
apply f_equal with (f := fun x : list A => b :: x).
apply H'2.
red in |- *; intros H'4; try exact H'4.
apply H'3 || case H'3; simpl in |- *.
right; try assumption.
Qed.
 
Inductive all_single (A : Type) : list A -> Prop :=
  | single_nil : all_single A nil
  | single_step :
      forall (a : A) (l : list A),
      all_single A l -> ~ In a l -> all_single A (a :: l).
 
Theorem all_single_remove_length :
 forall (A : Type) (l : list A),
 all_single A l ->
 forall (a : A) (l1 : list A), remove A a l l1 -> length l <= S (length l1).
intros A l H'; elim H'.
intros a l1 H'0; try exact H'0.
inversion H'0.
auto.
intros a l0 H'0 H'1 H'2 a0 l1 H'3; try assumption.
inversion H'3.
rewrite <- (remove_not_in A l0 l1 a).
auto.
auto.
try assumption.
simpl in |- *.
apply le_n_S.
apply H'1 with (a := a0).
auto.
Qed.
 
Theorem all_single_remove :
 forall (A : Type) (l l1 : list A) (a : A),
 remove A a l l1 -> all_single A l -> all_single A l1.
intros A l l1 a H'; elim H'.
auto.
intros l2 l3 H'0 H'1 H'2; try exact H'2.
inversion H'2; auto.
intros l2 l3 b H'0 H'1 H'2 H'3; try assumption.
inversion H'3.
apply single_step.
apply H'2.
auto.
red in |- *; intros H'4; try exact H'4.
apply H2 || case H2; try assumption.
apply In_remove_from with (l1 := l3) (a := a).
auto.
auto.
Qed.
 
Theorem single_included_shorter :
 forall A : Type,
 (forall x y : A, {x = y} + {x <> y}) ->
 forall l : list A,
 all_single A l ->
 forall l1 : list A, included A l l1 -> length l <= length l1.
intros A eq_A_dec l H'; elim H'.
intros l1 H'0; simpl in |- *.
auto with arith.
intros a l0 H'0 H'1 H'2 l1 H'3; try assumption.
inversion H'3.
elim (remove_fun A eq_A_dec l1 a).
intros l4 Hrem.
lapply (included_not_in_remove A eq_A_dec l1 l4 a);
 [ intros H'7; lapply (H'7 l0);
    [ intros H'9; lapply H'9;
       [ intros H'10; try exact H'10; clear H'9 | clear H'9 ]
    | idtac ]
 | idtac ]; auto.
lapply (remove_lt A l1 l4 a);
 [ intros H'8; lapply H'8;
    [ intros H'9; try exact H'9; clear H'8 | clear H'8 ]
 | idtac ]; auto.
unfold lt in H'9.
apply le_trans with (S (length l4)); auto.
simpl in |- *.
apply le_n_S.
apply H'1.
auto.
Qed.
 
Record single_sublist (A : Type) (l : list A) : Type := mk_single_sublist
  {h_list :> list A;
   single : all_single A h_list;
   incl : included A h_list l}.
Section order.
Variable A : Type.
Variable eq_A_dec : forall x y : A, {x = y} + {x <> y}.
Variable l : list A.
 
Definition p_h_measure : single_sublist A l -> nat.
intros l1; case l1; intros; apply minus; apply (length (A:=A)).
exact l.
assumption.
Defined.
Require Export Wf_nat.
 
Definition pre_hull_order := ltof (single_sublist A l) p_h_measure.
 
Definition pre_hull_order_wf : well_founded pre_hull_order.
unfold pre_hull_order in |- *.
apply well_founded_ltof.
Qed.
 
Definition pre_hull_add_element :
  forall (l1 : single_sublist A l) (a : A) (h : In a l) (h2 : ~ In a l1),
  single_sublist A l.
intros l1 a H' H'0.
apply mk_single_sublist with (h_list := a :: l1).
apply single_step; auto.
case l1; auto.
apply included_step; auto.
case l1; auto.
Defined.
 
Theorem pre_hull_add_element_decrease :
 forall (l1 : single_sublist A l) (a : A) (h : In a l) (h2 : ~ In a l1),
 pre_hull_order (pre_hull_add_element l1 a h h2) l1.
unfold pre_hull_add_element in |- *.
unfold pre_hull_order in |- *.
intros l1; elim l1.
simpl in |- *.
unfold ltof in |- *.
unfold p_h_measure in |- *.
intros h_list0 H' H'0 a H'1 H'2; try assumption.
apply plus_lt_reg_l with (p := length h_list0).
rewrite <- le_plus_minus.
apply plus_lt_reg_l with (p := length (a :: h_list0)).
rewrite
 (plus_permute (length (a :: h_list0)) (length h_list0)
    (length l - length (a :: h_list0))).
rewrite <- le_plus_minus.
simpl in |- *.
auto.
apply (single_included_shorter A eq_A_dec).
apply single_step; auto.
apply included_step; auto.
apply single_included_shorter; auto.
Qed.
End order.