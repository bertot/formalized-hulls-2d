AddPath "$HOME/ctcoq/stage".
Require Export minPolar.
Require Export maxPolar.
Require Export Arith.
Require Export PolyList.
Require Export Classical.

(* ----------- INUTILE ------------ *) 

Fixpoint minViolet [p : Plan; ch : CC] : nat :=
 Cases ch of
   nil => O
  | (cons a l) =>
      Cases l of
        nil => (S O)
       | (cons b q) =>
           Cases (directBool a p b) of   false => (S (minViolet p l))
                                        | true => O
           end
      end
 end.
 
Theorem minVioletOK_egal_p: (p : Plan) (ch : CC) ((k : nat) (lt k (minus (length ch) (1))) -> ~ (sensDirect (element ch k) p
                                                                                                            (element ch (S k)))) -> (minViolet p ch) = (length ch).
Intros.
Induction ch.
Simpl; Auto.
Generalize (refl_equal ? ch); Pattern -1 ch; Case ch; Intros.
Simpl; Auto.
Simpl.
Generalize (refl_equal ? (directBool a p p0)); Pattern -1 (directBool a p p0);
 Case (directBool a p p0); Intros.
Absurd (sensDirect a p p0).
Rewrite H0 in H.
Apply (H O).
Simpl.
Auto  with *.
Apply directBoolTrue; Auto.
Rewrite H0 in Hrecch; Apply eq_S; Apply Hrecch.
Intros.
Rewrite H0 in H.
Apply (H (S k)).
Simpl.
Simpl in H2.
Rewrite <- minus_n_O in H2; Auto  with *.
Qed.
 
Theorem egal_p_minVioletOK: (p : Plan) (ch : CC) (minViolet p ch) = (length ch) -> (k : nat) (lt k (minus (length ch) (1))) -> ~ (sensDirect (element ch k) p
                                                                                                                                             (element ch (S k))).
Intros p ch H.
Induction ch.
Intros k H0.
Simpl in H0.
Absurd (lt k O); Auto  with *.
Generalize (refl_equal ? ch); Pattern -1 ch; Case ch; Intros.
Simpl in H1.
Absurd (lt k O); Auto  with *.
Generalize (refl_equal ? k); Pattern -1 k; Case k; Intros.
Simpl.
Rewrite H0 in H; Simpl in H.
Generalize (refl_equal ? (directBool a p p0)); Pattern -1 (directBool a p p0);
 Case (directBool a p p0); Intros.
Rewrite H3 in H.
Absurd <nat> O = (S (S (length l))); Auto  with *.
Apply directBoolFalse; Auto.
Rewrite <- H0.
Simpl.
Simpl in Hrecch.
Apply Hrecch.
Rewrite H0 in H; Rewrite H0; Simpl in H; Simpl.
Generalize (refl_equal ? (directBool a p p0)); Pattern -1 (directBool a p p0);
 Case (directBool a p p0); Intros.
Rewrite H3 in H.
Absurd <nat> O = (S (S (length l))); Auto  with *.
Rewrite H3 in H.
Auto  with *.
Rewrite H0; Rewrite H2 in H1; Simpl; Simpl in H1.
Rewrite <- minus_n_O; Auto  with *.
Qed.
 
Theorem minVioletOK_non_p: (p : Plan) (ch : CC) ((ex nat [k : nat]  (lt k (minus (length ch) (1))) /\
                                                                    (sensDirect (element ch k) p
                                                                                (element ch (S k))))) -> (sensDirect (element ch (minViolet p ch)) p
                                                                                                                     (element ch (S (minViolet p ch)))).
Intros.
Induction ch.
Elim H; Clear H; Intros k H.
Elim H; Clear H; Intros.
Simpl in H.
Absurd (lt k O); Auto  with *.
Elim H; Clear H; Intros k H.
Elim H; Clear H; Intros.
Generalize (refl_equal ? ch); Pattern -1 ch; Case ch; Intros.
Rewrite H1 in H; Simpl in H.
Absurd (lt k O); Auto  with *.
Generalize (refl_equal ? (directBool a p p0)); Pattern -1 (directBool a p p0);
 Case (directBool a p p0); Intros.
Simpl.
Rewrite H2.
Apply directBoolTrue; Auto.
Generalize (refl_equal ? k); Pattern -1 k; Case k; Intros.
Rewrite H3 in H0; Rewrite H1 in H0; Simpl in H0.
Absurd (sensDirect a p p0); Auto.
Apply directBoolFalse; Auto.
Simpl.
Rewrite H2; Rewrite H1 in Hrecch.
Simpl; Simpl in Hrecch.
Apply Hrecch.
Clear Hrecch.
Exists n.
Split.
Rewrite H3 in H; Rewrite H1 in H; Simpl in H.
Rewrite <- minus_n_O; Auto  with *.
Rewrite H3 in H0; Rewrite H1 in H0; Auto.
Qed.
 
Theorem minVioletOK_inf_n: (p : Plan) (ch : CC) ((ex nat [k : nat]  (lt k (minus (length ch) (S O))) /\
                                                                    (sensDirect (element ch k) p
                                                                                (element ch (S k))))) -> (lt (minViolet p ch) (minus (length ch) (S O))).
Intros.
Induction ch.
Elim H; Clear H; Intros k H; Elim H; Clear H; Intros.
Simpl in H.
Absurd (lt k O); Auto  with *.
Elim H; Clear H; Intros k H; Elim H; Clear H; Intros.
Generalize (refl_equal ? ch); Pattern -1 ch; Case ch; Intros.
Rewrite H1 in H; Simpl in H.
Absurd (lt k O); Auto  with *.
Simpl.
Generalize (refl_equal ? (directBool a p p0)); Pattern -1 (directBool a p p0);
 Case (directBool a p p0); Intros.
Auto  with *.
Rewrite H1 in Hrecch; Simpl in Hrecch.
Rewrite <- minus_n_O in Hrecch.
Apply lt_n_S; Apply Hrecch.
Generalize (refl_equal ? k); Pattern -1 k; Case k; Intros.
Rewrite H3 in H0; Rewrite H1 in H0; Simpl in H0.
Absurd (sensDirect a p p0); Auto.
Apply directBoolFalse; Auto.
Exists n.
Split.
Rewrite H3 in H; Rewrite H1 in H; Simpl in H.
Auto  with *.
Rewrite H3 in H0; Rewrite H1 in H0; Auto.
Qed.
 
Theorem inf_n_minVioletOK: (p : Plan) (ch : CC) (lt (minViolet p ch) (minus (length ch) (S O))) -> (ex nat [k : nat]  (lt k (minus (length ch) (S O))) /\
                                                                                                                      (sensDirect (element ch k) p
                                                                                                                                  (element ch (S k)))).
Intros.
Induction ch.
Simpl in H.
Absurd (lt O O); Auto  with *.
Generalize (refl_equal ? ch); Pattern -1 ch; Case ch; Intros.
Rewrite H0 in H; Simpl in H.
Absurd (lt (1) O); Auto  with *.
Generalize (refl_equal ? (directBool a p p0)); Pattern -1 (directBool a p p0);
 Case (directBool a p p0); Intros.
Exists O.
Split.
Simpl; Auto  with *.
Simpl; Apply directBoolTrue; Auto.
Elim Hrecch; Clear Hrecch.
Intros k HypRec.
Elim HypRec; Clear HypRec; Intros HypRec1 HypRec2.
Exists (S k).
Split.
Rewrite H0 in HypRec1.
Simpl.
Simpl in HypRec1.
Rewrite <- minus_n_O in HypRec1.
Auto  with *.
Rewrite <- H0; Auto.
Rewrite H0 in H; Simpl in H; Rewrite H1 in H; Rewrite H0; Simpl.
Rewrite <- minus_n_O; Apply lt_S_n; Auto.
Qed.
 
